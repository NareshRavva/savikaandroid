package com.savika.authentication;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.savika.R;
import com.savika.constants.AppConstants;
import com.savika.amAMother.ChildModel;

import java.util.ArrayList;

/*
 * Created by Naresh Ravva on 20/09/17.
 */

public class SignUpActivity extends AppCompatActivity {

    public static String due_date;
    public static String date_of_last_period;

    private String TAG = this.getClass().getSimpleName();

    public static ArrayList<ChildModel> arrChildData = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        if (getIntent().getSerializableExtra(AppConstants.EXTRA_CHILD_DATA) != null)
            arrChildData = (ArrayList<ChildModel>) getIntent().getSerializableExtra(AppConstants.EXTRA_CHILD_DATA);

        due_date = getIntent().getStringExtra(AppConstants.EXTRA_DUE_DATE);
        date_of_last_period = getIntent().getStringExtra(AppConstants.EXTRA_DATE_OF_LAST_PERIOD);

    }

}
