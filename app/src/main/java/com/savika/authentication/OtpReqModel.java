package com.savika.authentication;

/*
 * Created by Naresh Ravva on 23/09/17.
 */

public class OtpReqModel {

    String mobilenumber,username;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getMobilenumber() {
        return mobilenumber;
    }

    public void setMobilenumber(String mobilenumber) {
        this.mobilenumber = mobilenumber;
    }
}
