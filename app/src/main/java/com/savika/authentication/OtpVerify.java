package com.savika.authentication;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.savika.Api.ApiClient;
import com.savika.Api.ApiInterface;
import com.savika.Api.ResponseModel;
import com.savika.R;
import com.savika.Splash;
import com.savika.constants.GAConstants;
import com.savika.dialogUtils.InfoDialogUtility;
import com.savika.firebase.MyFirebaseMessagingService;
import com.savika.googleAnalytics.GoogleAnalyticsHelper;
import com.savika.homeScreen.HomeActivity;
import com.savika.logger.Logger;
import com.savika.onBoardScreens.OnBoardActivity2;
import com.savika.onBoardScreens.WelcomeDetailsScreen;
import com.savika.constants.AppConstants;
import com.savika.utils.AppUtils;
import com.savika.utils.SharedPrefsUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;

/*
 * Created by Naresh Ravva on 22/09/17.
 */

public class OtpVerify extends AppCompatActivity implements View.OnClickListener, OTPListener {

    @BindView(R.id.llVerify)
    LinearLayout llVerify;

    @BindView(R.id.etvOTP)
    EditText etvOTP;

    @BindView(R.id.tvResendOTP)
    TextView tvResendOTP;

    @BindView(R.id.tvUpdateInfo)
    TextView tvUpdateInfo;

    @BindView(R.id.tvAlreadyExists)
    TextView tvAlreadyExists;

    String MOBILE_NUMBER;

    GoogleAnalyticsHelper mGoogleHelper;

    private String TAG = this.getClass().getSimpleName();

    private boolean isFromSignIN = false;
    private boolean isFromSignUP = false;

    private SignUpRequestModel signup_model;

    int REQUEST_CODE = 123;

    ResponseModel otp_res_model = new ResponseModel();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);

        //checking wether the permission is already granted
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_SMS) == PackageManager.PERMISSION_GRANTED) {
            OtpReader.bind(OtpVerify.this, "sendernumber");
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_SMS}, REQUEST_CODE);
        }

        OtpReader.bind(OtpVerify.this, "sendernumber");

        ButterKnife.bind(this);

        llVerify.setOnClickListener(this);
        tvResendOTP.setOnClickListener(this);

        MOBILE_NUMBER = getIntent().getStringExtra(AppConstants.EXTRA_MOBILE_NUMBER);
        isFromSignIN = getIntent().getBooleanExtra(AppConstants.EXTRA_IS_FROM_SIGNIN, false);
        isFromSignUP = getIntent().getBooleanExtra(AppConstants.EXTRA_SIGNUP_REQUIRED, false);

        if (getIntent().getStringExtra(AppConstants.EXTRA_SIGNUP_DATA) != null) {
            Gson gson = new Gson();
            signup_model = gson.fromJson(getIntent().getStringExtra(AppConstants.EXTRA_SIGNUP_DATA), SignUpRequestModel.class);
        }

        tvUpdateInfo.setText("Please type the verification code sent to (+91) " + MOBILE_NUMBER);

        if (isFromSignIN) {
            signIN();
        } else {
            validateOTP();
        }

        InitGoogleAnalytics();

        mGoogleHelper.SendScreenNameGA(this, GAConstants.OTP_SCREEN);
    }

    // Google Analytics
    private void InitGoogleAnalytics() {
        mGoogleHelper = new GoogleAnalyticsHelper();
        mGoogleHelper.init(this);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        if (requestCode == REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            } else {
                AppUtils.showToast(OtpVerify.this, "You denied the permission");
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        OtpReader.bind(null, "sendernumber");
    }

    /**
     * @param view
     */
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.llVerify:

                if (etvOTP.getText().toString().length() < 1) {
                    AppUtils.showToast(OtpVerify.this, "Enter OTP");
                    return;
                }

                mGoogleHelper.SendEventGA(OtpVerify.this, GAConstants.OTP_SCREEN, GAConstants.OTP_VERIFIED, "");

                Logger.info(TAG, "value1-->" + etvOTP.getText().toString().trim() + "----value2-->" + SharedPrefsUtils.getOTP(this));

                if (etvOTP.getText().toString().trim().equalsIgnoreCase(SharedPrefsUtils.getOTP(this))) {

                    if (isFromSignUP) {
                        callSignUPAPI();
                    } else {
                        saveResponseData(otp_res_model);
                        moveToRecommendArticles();
                    }
                    AppUtils.showToast(OtpVerify.this, "OTP verified succesfully.");
                } else {
                    AppUtils.showToast(OtpVerify.this, "You enter the wrong OTP.");
                }


                break;

            case R.id.tvResendOTP:

                if (isFromSignIN)
                    signIN();
                else
                    validateResendOTP();

                break;
        }
    }

    /**
     *
     */
    private void moveToRecommendArticles() {
        Intent articals = new Intent(OtpVerify.this, HomeActivity.class);
        articals.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        articals.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        articals.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(articals);
    }

    /**
     *
     */
    private void moveToWelcomeScreen() {
        Intent articals = new Intent(OtpVerify.this, WelcomeDetailsScreen.class);
        articals.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        articals.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        articals.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(articals);
    }


    /**
     *
     */
    private void signIN() {


        if (AppUtils.isNetworkConnected(OtpVerify.this)) {
            AppUtils.showDialog(this);
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            OtpReqModel model = new OtpReqModel();
            model.setMobilenumber(MOBILE_NUMBER);

            Call<ResponseModel> call = apiService.signin(model);

            call.enqueue(new retrofit2.Callback<ResponseModel>() {
                @Override
                public void onResponse(Call<ResponseModel> call, retrofit2.Response<ResponseModel> response) {
                    Logger.info(TAG, "status code-->" + response.code());

                    AppUtils.dismissDialog();

                    try {
                        if (response.code() == 200 && response.body().isSuccess() == true) {
                            //saveResponseData(response.body());
                            otp_res_model = response.body();
                            SharedPrefsUtils.setOTP(OtpVerify.this, otp_res_model.getOtpvalue());
                            SharedPrefsUtils.setToken(OtpVerify.this,otp_res_model.getToken());
                        } else {
                            AppUtils.showToast(OtpVerify.this, response.body().getMessage());

                            showAlert();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<ResponseModel> call, Throwable t) {
                    AppUtils.showToast(getApplicationContext(), getString(R.string.server_down));
                    AppUtils.dismissDialog();
                }
            });
        }
         else {
            AppUtils.showToast(OtpVerify.this,"It looks like your internet connectioin off. Please turn it on and try again.");
        }
    }

    private void showAlert() {
        runOnUiThread(new InfoDialogUtility(OtpVerify.this, this.getLayoutInflater(), getResources().getString(R.string.app_name), getResources().getString(R.string.are_you_sure_you_want_to_logout_), "OK", "Cancel", new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), Splash.class);
                startActivity(intent);
                finish();
            }
        }, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        }, true));

    }

    /**
     * @param model
     */
    private void saveResponseData(ResponseModel model) {
        SharedPrefsUtils.setMobileNum(this, model.getMobilenumber());
        SharedPrefsUtils.setOTP(this, model.getOtpvalue());
        SharedPrefsUtils.setProfilePic(this, model.getProfileimg());
        SharedPrefsUtils.setEmail(this, model.getEmailid());
        SharedPrefsUtils.setUserID(this, model.getUserid());
        SharedPrefsUtils.setMotherORPrgntType(this, model.getTypeid());
        SharedPrefsUtils.setUsername(this, model.getUsername());
        SharedPrefsUtils.setToken(this,model.getToken());
    }

    /**
     *
     */
    private void callSignUPAPI() {
        AppUtils.showDialog(this);

        //add OTP
        signup_model.setOtp(etvOTP.getText().toString());

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseModel> call = apiService.signUp(signup_model);

        call.enqueue(new retrofit2.Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, retrofit2.Response<ResponseModel> response) {
                AppUtils.dismissDialog();

                saveResponseData(response.body());
                if (response.code() == 200 && response.body().isSuccess() == true) {
                    String notify_display_title = response.body().getNotification_title();
                    String notify_display_msg = response.body().getNotification_msg();

                    MyFirebaseMessagingService.sendNotification(OtpVerify.this, notify_display_title, notify_display_msg,"", "",null);
                    moveToWelcomeScreen();
                } else {
                    //AppUtils.showToast(OtpVerify.this, response.body().getMessage());

                    if (response.body().getMessage().equalsIgnoreCase("User Already Exists....!")) {
                        moveToRecommendArticles();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                AppUtils.dismissDialog();
                AppUtils.showToast(OtpVerify.this, getString(R.string.server_down));
            }
        });
    }


    /**
     *
     */
    private void validateOTP() {

        AppUtils.showDialog(this);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        OtpReqModel model = new OtpReqModel();
        model.setMobilenumber(MOBILE_NUMBER);
        if (signup_model.getUsername() != null && signup_model.getUsername().length() > 0) {
            model.setUsername(signup_model.getUsername());
        }

        Call<ResponseModel> call = apiService.validateOTP(model);

        call.enqueue(new retrofit2.Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, retrofit2.Response<ResponseModel> response) {
                Logger.info(TAG, "status code-->" + response.code());

                AppUtils.dismissDialog();

                try {
                    if (response.code() == 200 && response.body().isSuccess() == true) {
                        String al = response.body().getAlready_exists();
                        if (al.equalsIgnoreCase("1")){
                            tvAlreadyExists.setVisibility(View.VISIBLE);
                        }
                        saveResponseData(response.body());

                    } else {
                        //JSONObject jObjError = new JSONObject(response.errorBody().string());
                        AppUtils.showToast(OtpVerify.this, response.body().getMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                AppUtils.showToast(getApplicationContext(), getString(R.string.server_down));
                AppUtils.dismissDialog();
            }
        });
    }


    /**
     *
     */
    private void validateResendOTP() {

        AppUtils.showDialog(this);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        OtpReqModel model = new OtpReqModel();
        model.setMobilenumber(MOBILE_NUMBER);

        Call<ResponseModel> call = apiService.resendOTP(model);

        call.enqueue(new retrofit2.Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, retrofit2.Response<ResponseModel> response) {
                Logger.info(TAG, "status code-->" + response.code());

                AppUtils.dismissDialog();

                try {
                    if (response.code() == 200 && response.body().isSuccess() == true) {
                        saveResponseData(response.body());

                    } else {
                        //JSONObject jObjError = new JSONObject(response.errorBody().string());
                        AppUtils.showToast(OtpVerify.this, response.body().getMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                AppUtils.showToast(getApplicationContext(), getString(R.string.server_down));
                AppUtils.dismissDialog();
            }
        });

    }

    @Override
    public void otpReceived(String messageText) {
        String otp_number = readOTP(messageText);

        Logger.info(TAG, "--otp_number-->" + otp_number);

        etvOTP.setText(otp_number);

        if (etvOTP.getText().toString().trim().equalsIgnoreCase(SharedPrefsUtils.getOTP(this))) {

            if (isFromSignUP) {
                callSignUPAPI();
            } else {
                saveResponseData(otp_res_model);
                moveToRecommendArticles();
            }
            AppUtils.showToast(OtpVerify.this, "OTP verified succesfully.");
        } else {
            AppUtils.showToast(OtpVerify.this, "You enter the wrong OTP.");
        }

    }

    private String readOTP(String str) {

        String otp = "";

        //String str="Dear Naresh,Welcome to mobizz. Your OTP is 443433 jdlajlf 2911";

        Pattern p = Pattern.compile("(\\d+)");
        Matcher m = p.matcher(str);
        while (m.find()) {
            return otp = m.group(0);

        }
        return otp;
    }
}
