package com.savika.authentication;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.savika.Api.ApiClient;
import com.savika.Api.ApiInterface;
import com.savika.Api.ResponseModel;
import com.savika.R;
import com.savika.Splash;
import com.savika.constants.GAConstants;
import com.savika.dialogUtils.InfoDialogUtility;
import com.savika.googleAnalytics.GoogleAnalyticsHelper;
import com.savika.homeScreen.HomeActivity;
import com.savika.logger.Logger;
import com.savika.onBoardScreens.OnBoardActivity2;
import com.savika.constants.AppConstants;
import com.savika.utils.AppUtils;
import com.savika.utils.SharedPrefsUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;


/*
 * A placeholder fragment containing a simple view.
 */
public class SignInFragment extends Fragment implements View.OnClickListener {

    private CallbackManager callbackManager;
    private TextView textView;

    private AccessTokenTracker accessTokenTracker;
    private ProfileTracker profileTracker;

    @BindView(R.id.etvMobileNumber)
    EditText etvMobileNumber;

    @BindView(R.id.llSignIN)
    LinearLayout llSignIN;

    LoginButton loginButton;

    GoogleAnalyticsHelper mGoogleHelper;

    private String TAG = "SignInFragment";

    private FacebookCallback<LoginResult> callback = new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(LoginResult loginResult) {
            AccessToken accessToken = loginResult.getAccessToken();
//            Profile profile = Profile.getCurrentProfile();
//            displayMessage(profile);

            if (accessToken != null && accessToken.getUserId() != null)
                signINWithFB(accessToken.getUserId());
        }

        @Override
        public void onCancel() {

        }

        @Override
        public void onError(FacebookException e) {

        }
    };

    public SignInFragment() {

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getActivity().getApplicationContext());

        callbackManager = CallbackManager.Factory.create();

        accessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldToken, AccessToken newToken) {

            }
        };

        profileTracker = new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(Profile oldProfile, Profile newProfile) {
                displayMessage(newProfile);
            }
        };

        accessTokenTracker.startTracking();
        profileTracker.startTracking();

        InitGoogleAnalytics();
    }

    // Google Analytics
    private void InitGoogleAnalytics() {
        mGoogleHelper = new GoogleAnalyticsHelper();
        mGoogleHelper.init(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_signin, container, false);

        ButterKnife.bind(SignInFragment.this, view);

        llSignIN.setOnClickListener(this);

        return view;
    }


    /*
     *
     */
    private void moveToOTP() {
        Intent otp_screen = new Intent(getActivity(), OtpVerify.class);
        otp_screen.putExtra(AppConstants.EXTRA_MOBILE_NUMBER, etvMobileNumber.getText().toString());
        otp_screen.putExtra(AppConstants.EXTRA_IS_FROM_SIGNIN, true);
        startActivity(otp_screen);
    }

    private void moveToRecommendArticles() {
        Intent articals = new Intent(getActivity(), HomeActivity.class);
        articals.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        articals.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        articals.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(articals);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loginButton = view.findViewById(R.id.login_button);
        textView = view.findViewById(R.id.txtInfo);

        //loginButton.setReadPermissions("user_friends");
        loginButton.setFragment(this);
        loginButton.registerCallback(callbackManager, callback);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);

    }

    private void displayMessage(Profile profile) {
        if (profile != null) {
            Log.e("details-->", "name-->" + profile.getName());
            Log.e("details-->", "ID-->" + profile.getId());
            Log.e("details-->", "profile-->" + profile.getProfilePictureUri(100, 100));

            LoginManager.getInstance().logOut();
        }
    }


    /*
     *
     */
    private void signINWithFB(String fb) {

        if (AppUtils.isNetworkConnected(getActivity())) {
            AppUtils.showDialog(getActivity());
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            FBReqModel model = new FBReqModel();
            model.setOauthuid(fb);

            Call<ResponseModel> call = apiService.signinWithFb(model);

            call.enqueue(new retrofit2.Callback<ResponseModel>() {
                @Override
                public void onResponse(Call<ResponseModel> call, retrofit2.Response<ResponseModel> response) {
                    Logger.info(TAG, "status code-->" + response.code());

                    AppUtils.dismissDialog();

                    try {
                        if (response.code() == 200 && response.body().isSuccess() == true) {
                            saveResponseData(response.body());

                            AppUtils.showToast(getActivity(), "Login Success");

                            mGoogleHelper.SendEventGA(getActivity(), GAConstants.SIGN_IN_SCREEN, GAConstants.FACEBOOK_SIGNIN, "");

                            moveToRecommendArticles();

                        } else {
                            //AppUtils.showToast(getActivity(), response.body().getMessage());
                            showAlert();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<ResponseModel> call, Throwable t) {
                    AppUtils.showToast(getActivity(), getString(R.string.server_down));
                    AppUtils.dismissDialog();
                }
            });
        }
        else {
            AppUtils.showToast(getActivity(),"It looks like your internet connectioin off. Please turn it on and try again.");
        }
    }

    private void showAlert() {
        getActivity().runOnUiThread(new InfoDialogUtility(getActivity(), getActivity().getLayoutInflater(), getResources().getString(R.string.app_name), getResources().getString(R.string.are_you_sure_you_want_to_logout_), "OK", "Cancel", new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), Splash.class);
                startActivity(intent);
            }
        }, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //finish();
            }
        }, true));

    }

    /*
     * @param model
     */
    private void saveResponseData(ResponseModel model) {
        SharedPrefsUtils.setUsername(getActivity(), model.getUsername());
        SharedPrefsUtils.setMobileNum(getActivity(), model.getMobilenumber());
        SharedPrefsUtils.setOTP(getActivity(), model.getOtpvalue());
        SharedPrefsUtils.setProfilePic(getActivity(), model.getProfileimg());
        SharedPrefsUtils.setEmail(getActivity(), model.getEmailid());
        SharedPrefsUtils.setMotherORPrgntType(getActivity(), model.getTypeid());
        SharedPrefsUtils.setUserID(getActivity(), model.getUserid());
        SharedPrefsUtils.setToken(getActivity(),model.getToken());
    }

    @Override
    public void onStop() {
        super.onStop();
        accessTokenTracker.stopTracking();
        profileTracker.stopTracking();
    }

    @Override
    public void onResume() {
        super.onResume();
        Profile profile = Profile.getCurrentProfile();
        displayMessage(profile);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.llSignIN:

                if (etvMobileNumber.getText().toString().length() >= 10) {
                    mGoogleHelper.SendEventGA(getActivity(), GAConstants.SIGN_IN_SCREEN, GAConstants.NORMAL_SIGNIN, "");
                    moveToOTP();
                } else {
                    AppUtils.showToast(getActivity(), "Enter MobileNumber");
                }
                break;
        }
    }
}
