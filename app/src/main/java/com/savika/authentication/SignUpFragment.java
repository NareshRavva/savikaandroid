package com.savika.authentication;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.savika.R;
import com.savika.constants.AppConstants;
import com.savika.constants.GAConstants;
import com.savika.googleAnalytics.GoogleAnalyticsHelper;
import com.savika.logger.Logger;
import com.savika.utils.AppUtils;
import com.savika.utils.SharedPrefsUtils;

import org.json.JSONObject;

import java.util.Arrays;


/**
 * A placeholder fragment containing a simple view.
 */
public class SignUpFragment extends Fragment implements View.OnClickListener {

    EditText etvMobileNumber;
    EditText etvEmail;
    EditText etvName;
    LinearLayout facebookll;
//    EditText etvReferalNumber;

    CheckBox chkAccept;
    LinearLayout llCreateAccount;

    private String TAG = this.getClass().getSimpleName();

    private String FIREBASE_TOKEN, DEVICE_ID, OS_VERSION, DEVICE_MODEL;

    private String FB_ACCESS_TOKEN, FB_ID;

    private CallbackManager callbackManager;

    private AccessTokenTracker accessTokenTracker;
    private ProfileTracker profileTracker;
    private LoginButton loginButton;

    GoogleAnalyticsHelper mGoogleHelper;

    private FacebookCallback<LoginResult> callback = new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(LoginResult loginResult) {
            AccessToken accessToken = loginResult.getAccessToken();
            FB_ACCESS_TOKEN = accessToken.getToken();
            Profile profile = Profile.getCurrentProfile();
            displayMessage(profile);

            GraphRequest graphRequest = GraphRequest.newMeRequest(accessToken, new GraphRequest.GraphJSONObjectCallback() {
                @Override
                public void onCompleted(JSONObject object, GraphResponse response) {

                    if (response.getError() != null) {
                        Log.e(TAG, "Error in Response " + response);
                    } else {
                        String email = object.optString("email");
                        FB_ID = object.optString("id");
                        Log.e(TAG, "Json Object Data " + object + " Email id " + email);
                        if (email != null)
                            etvEmail.setText(email);
                    }


                }
            });

            Bundle bundle = new Bundle();
            bundle.putString("fields", "id,email,name");
            graphRequest.setParameters(bundle);
            graphRequest.executeAsync();

        }

        @Override
        public void onCancel() {

        }

        @Override
        public void onError(FacebookException e) {

        }
    };

    public SignUpFragment() {

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getActivity().getApplicationContext());

        callbackManager = CallbackManager.Factory.create();

        accessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldToken, AccessToken newToken) {

            }
        };

        profileTracker = new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(Profile oldProfile, Profile newProfile) {
                displayMessage(newProfile);
            }
        };

        accessTokenTracker.startTracking();
        profileTracker.startTracking();

        InitGoogleAnalytics();

        mGoogleHelper.SendScreenNameGA(getActivity(), GAConstants.SIGN_UP_SCREEN);
    }

    // Google Analytics
    private void InitGoogleAnalytics() {
        mGoogleHelper = new GoogleAnalyticsHelper();
        mGoogleHelper.init(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_signup, container, false);

        etvMobileNumber = view.findViewById(R.id.etvMobileNumber);
        etvEmail = view.findViewById(R.id.etvEmail);
        etvName = view.findViewById(R.id.etvName);
        facebookll = view.findViewById(R.id.facebookll);
//        etvReferalNumber = view.findViewById(R.id.etvReferalNumber);

        chkAccept = view.findViewById(R.id.chkAccept);
        llCreateAccount = view.findViewById(R.id.llCreateAccount);

        getRequiredDeviceDetails();

        llCreateAccount.setOnClickListener(this);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loginButton = view.findViewById(R.id.login_button);

        loginButton.setReadPermissions(Arrays.asList("public_profile", "email"));
        loginButton.setFragment(this);
        loginButton.registerCallback(callbackManager, callback);
        loginButton.setReadPermissions(Arrays.asList("public_profile", "email"));


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);

    }

    private void displayMessage(Profile profile) {
        if (profile != null) {
            Log.e("details-->", "name-->" + profile.getName());
            Log.e("details-->", "ID-->" + profile.getId());
            Log.e("details-->", "profile-->" + profile.getProfilePictureUri(100, 100));

            etvName.setText(profile.getName());

            FB_ID = profile.getId();

            facebookll.setVisibility(View.INVISIBLE);

            LoginManager.getInstance().logOut();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        accessTokenTracker.stopTracking();
        profileTracker.stopTracking();
    }

    @Override
    public void onResume() {
        super.onResume();
        Profile profile = Profile.getCurrentProfile();
        displayMessage(profile);
    }

    /*
     * get device details
     */
    private void getRequiredDeviceDetails() {
        FIREBASE_TOKEN = FirebaseInstanceId.getInstance().getToken();
        Log.w(TAG, "Refreshed token: " + FIREBASE_TOKEN);

        DEVICE_ID = Settings.Secure.getString(getActivity().getContentResolver(), Settings.Secure.ANDROID_ID);
        Log.w(TAG, "Device ID : " + DEVICE_ID);

        DEVICE_MODEL = getDeviceName();
        Log.w(TAG, "Device Model : " + DEVICE_MODEL);

        OS_VERSION = android.os.Build.VERSION.RELEASE;
        Log.w(TAG, "OS Version : " + OS_VERSION);
    }

    /*
     * @return
     */
    public String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        } else {
            return capitalize(manufacturer) + " " + model;
        }
    }

    /*
     * @param s
     * @return
     */
    private String capitalize(String s) {
        if (s == null || s.length() == 0) {
            return "";
        }
        char first = s.charAt(0);
        if (Character.isUpperCase(first)) {
            return s;
        } else {
            return Character.toUpperCase(first) + s.substring(1);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.llCreateAccount:

                String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
                String namePattern = "^[a-zA-Z\\s]*$";

                if (etvMobileNumber.getText().toString().length() < 1) {
                    AppUtils.showToast(getActivity(), "Enter MobileNumber");
                    return;
                }

                if (etvMobileNumber.getText().toString().length() < 10) {
                    AppUtils.showToast(getActivity(), "Enter Valid MobileNumber");
                    return;
                }
                if (etvEmail.getText().toString().length() < 1) {
                    AppUtils.showToast(getActivity(), "Enter Email");
                    return;
                }
                if (!etvEmail.getText().toString().matches(emailPattern)) {
                    AppUtils.showToast(getActivity(), "Enter Valid Email");
                    return;
                }
                if (etvName.getText().toString().length() < 1) {
                    AppUtils.showToast(getActivity(), "Enter Name");
                    return;
                }
                if (etvName.getText().toString().length() < 3) {
                    AppUtils.showToast(getActivity(), "Minimum 3 Characters Required");
                    return;
                }
                if (!etvName.getText().toString().matches(namePattern)){
                    AppUtils.showToast(getActivity(),"Enter Valid Name");
                    return;
                }
//                if (etvReferalNumber.getText().toString().length() > 1 && etvReferalNumber.getText().toString().length() < 10) {
//                    AppUtils.showToast(getActivity(), "Enter Valid Referal Number");
//                    return;
//                }
//                if (!etvMobileNumber.getText().toString().equalsIgnoreCase("") && etvReferalNumber.getText().toString().equalsIgnoreCase(etvMobileNumber.getText().toString())){
//                    AppUtils.showToast(getActivity(),"Referal Number should not be equal");
//                    return;
//                }

                if (!chkAccept.isChecked()) {
                    AppUtils.showToast(getActivity(), "Accept Terms & Conditions");
                    return;
                }

                signUpApiCall();
                break;
        }
    }

    private void moveToOTP(SignUpRequestModel model) {
        Intent otp_screen = new Intent(getActivity(), OtpVerify.class);
        otp_screen.putExtra(AppConstants.EXTRA_MOBILE_NUMBER, etvMobileNumber.getText().toString());
        otp_screen.putExtra(AppConstants.EXTRA_SIGNUP_REQUIRED, true);

        Gson gson = new Gson();
        String myJson = gson.toJson(model);

        otp_screen.putExtra(AppConstants.EXTRA_SIGNUP_DATA, myJson);
        startActivity(otp_screen);
    }

    /*
     *
     */
    private void signUpApiCall() {
        //Formate Request Object
        SignUpRequestModel model = new SignUpRequestModel();
        model.setTypeid(SharedPrefsUtils.getMotherORPrgntType(getActivity()));
        model.setMobilenumber(etvMobileNumber.getText().toString().trim());
        model.setEmailid(etvEmail.getText().toString().trim());
        model.setUsername(etvName.getText().toString().trim());
//        model.setRefnumber(etvReferalNumber.getText().toString());
        model.setAcceptterms("Y");

        model.setPerioddate(SignUpActivity.date_of_last_period);
        model.setDuedate(SignUpActivity.due_date);

        //Add Device Details
        DeviceDetailsModel device_model = new DeviceDetailsModel();
        device_model.setFirebaseid(FIREBASE_TOKEN);
        device_model.setDeviceid(DEVICE_ID);
        device_model.setDevicemodel(DEVICE_MODEL);
        device_model.setOsversion(OS_VERSION);
        model.setDeviceDetailsModel(device_model);

        if (FB_ID != null) {
            //ADD OAUTH details
            OAuthDetailsModel oAuthModel = new OAuthDetailsModel();
            oAuthModel.setOauthuid(FB_ID);
            oAuthModel.setOauthtoken(FB_ACCESS_TOKEN);
            oAuthModel.setOauthtype("fb");
            model.setoAuthModel(oAuthModel);
            mGoogleHelper.SendEventGA(getActivity(), GAConstants.SIGN_UP_SCREEN, GAConstants.NORMAL_SIGNUP, "");
        } else {
            mGoogleHelper.SendEventGA(getActivity(), GAConstants.SIGN_UP_SCREEN, GAConstants.FACEBOOK_SIGNUP, "");
        }

        //Add Child details
        model.setArrChildData(SignUpActivity.arrChildData);

        Gson gson = new Gson();
        String json = gson.toJson(model);
        Logger.info(TAG, "SignUP REquest Objt--->" + json.toString());

        moveToOTP(model);

    }


}
