package com.savika.authentication;

/*
 * Created by Naresh Ravva on 23/09/17.
 */

public class FBReqModel {

    String oauthuid;

    public String getOauthuid() {
        return oauthuid;
    }

    public void setOauthuid(String oauthuid) {
        this.oauthuid = oauthuid;
    }
}
