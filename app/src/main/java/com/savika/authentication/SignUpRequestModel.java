package com.savika.authentication;

import com.google.gson.annotations.SerializedName;
import com.savika.amAMother.ChildModel;

import java.util.ArrayList;

/**
 * Created by Naresh Ravva on 20/09/17.
 */

public class SignUpRequestModel {

    private String typeid, mobilenumber, emailid, username, refnumber, acceptterms, duedate, perioddate, otp;

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    @SerializedName("device_details")
    DeviceDetailsModel deviceDetailsModel;

    @SerializedName("child_data")
    ArrayList<ChildModel> arrChildData = new ArrayList<>();

    @SerializedName("oauth_details")
    OAuthDetailsModel oAuthModel;

    public OAuthDetailsModel getoAuthModel() {
        return oAuthModel;
    }

    public void setoAuthModel(OAuthDetailsModel oAuthModel) {
        this.oAuthModel = oAuthModel;
    }

    public ArrayList<ChildModel> getArrChildData() {
        return arrChildData;
    }

    public void setArrChildData(ArrayList<ChildModel> arrChildData) {
        this.arrChildData = arrChildData;
    }

    public DeviceDetailsModel getDeviceDetailsModel() {
        return deviceDetailsModel;
    }

    public void setDeviceDetailsModel(DeviceDetailsModel deviceDetailsModel) {
        this.deviceDetailsModel = deviceDetailsModel;
    }

    public String getTypeid() {
        return typeid;
    }

    public void setTypeid(String typeid) {
        this.typeid = typeid;
    }

    public String getMobilenumber() {
        return mobilenumber;
    }

    public void setMobilenumber(String mobilenumber) {
        this.mobilenumber = mobilenumber;
    }

    public String getEmailid() {
        return emailid;
    }

    public void setEmailid(String emailid) {
        this.emailid = emailid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getRefnumber() {
        return refnumber;
    }

    public void setRefnumber(String refnumber) {
        this.refnumber = refnumber;
    }

    public String getAcceptterms() {
        return acceptterms;
    }

    public void setAcceptterms(String acceptterms) {
        this.acceptterms = acceptterms;
    }

    public String getDuedate() {
        return duedate;
    }

    public void setDuedate(String duedate) {
        this.duedate = duedate;
    }

    public String getPerioddate() {
        return perioddate;
    }

    public void setPerioddate(String perioddate) {
        this.perioddate = perioddate;
    }
}

class DeviceDetailsModel {
    private String firebaseid, deviceid, devicemodel, osversion;

    public String getFirebaseid() {
        return firebaseid;
    }

    public void setFirebaseid(String firebaseid) {
        this.firebaseid = firebaseid;
    }

    public String getDeviceid() {
        return deviceid;
    }

    public void setDeviceid(String deviceid) {
        this.deviceid = deviceid;
    }

    public String getDevicemodel() {
        return devicemodel;
    }

    public void setDevicemodel(String devicemodel) {
        this.devicemodel = devicemodel;
    }

    public String getOsversion() {
        return osversion;
    }

    public void setOsversion(String osversion) {
        this.osversion = osversion;
    }
}


class OAuthDetailsModel {

    private String oauthtype, oauthuid, oauthtoken;

    public String getOauthtype() {
        return oauthtype;
    }

    public void setOauthtype(String oauthtype) {
        this.oauthtype = oauthtype;
    }

    public String getOauthuid() {
        return oauthuid;
    }

    public void setOauthuid(String oauthuid) {
        this.oauthuid = oauthuid;
    }

    public String getOauthtoken() {
        return oauthtoken;
    }

    public void setOauthtoken(String oauthtoken) {
        this.oauthtoken = oauthtoken;
    }
}
