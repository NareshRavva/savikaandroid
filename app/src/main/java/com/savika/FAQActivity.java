package com.savika;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.webkit.WebView;
import com.savika.components.textview.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FAQActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.tvHeaderTitle)
    TextView tvHeaderTitle;

    @BindView(R.id.llBack)
    LinearLayout llBack;

    @BindView(R.id.faq)
    WebView faq;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faq);

        ButterKnife.bind(this);
        tvHeaderTitle.setText("FAQ's");
        llBack.setOnClickListener(this);
        faq.setWebViewClient(new WebViewClient());
        faq.getSettings().setJavaScriptEnabled(true);
        faq.loadUrl("file:///android_asset/faq.html");
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id){
            case R.id.llBack:
                finish();
                break;
        }
    }
}
