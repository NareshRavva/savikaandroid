package com.savika.tracking.lastRecords;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.savika.GlobalConfigResObjModel;
import com.savika.R;
import com.savika.logger.Logger;
import com.savika.tracking.Track;
import com.savika.utils.AppUtils;
import com.savika.utils.SharedPrefsUtils;

import java.lang.reflect.Type;
import java.util.ArrayList;

/*
 * Created by Naresh Ravva on 30/06/17.
 */

public class LastRecordsAdapter extends RecyclerView.Adapter<LastRecordsAdapter.ViewHolder> {

    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    private Track context;
    private GlobalConfigResObjModel healthrecord, healthrecord2;
    private String defaultValue;

    private ArrayList<LastRecordModel> arrData = new ArrayList<>();

    public LastRecordsAdapter(Track context, ArrayList<LastRecordModel> arrData) {
        this.mInflater = LayoutInflater.from(context);
        this.arrData = arrData;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.list_item_tracking_health, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final LastRecordModel model = arrData.get(position);
        if (SharedPrefsUtils.getGlobalConfig(context) != null) {

            Gson gson = new Gson();
            String json = SharedPrefsUtils.getGlobalConfig(context);
            Type type = new TypeToken<ArrayList<GlobalConfigResObjModel>>() {
            }.getType();
            ArrayList<GlobalConfigResObjModel> arrayList = gson.fromJson(json, type);
            saveGlobalRecords(model.getHealthdesc(),arrayList);
        }

        holder.tvDesc.setText(model.getHealthdesc());

        Logger.info("TAG","Reading-->"+model.getHealthdesc()+"====="+model.getHealthreading());

        if (model.getHealthreading() != null && model.getHealthreading().length() > 0) {
//            try {
//                if (model.getHealthdesc().equalsIgnoreCase("temperature") || model.getHealthdesc().equalsIgnoreCase("weight") || model.getHealthdesc().equalsIgnoreCase("hemoglobin"))
//                    defaultValue = model.getHealthreading() + ".00" + "";
//                else {
//                    defaultValue = model.getHealthreading();
//                }
//            } catch (Exception e) {
//                defaultValue = model.getHealthreading();
//            }
            defaultValue = model.getHealthreading();
            boolean isAbnormal = checkViewColor(model.getHealthdesc(),model.getHealthreading());
            if (isAbnormal){
                holder.tvRecord.setTextColor(Color.BLACK);
                holder.tvUnits.setTextColor(Color.BLACK);
            }
            holder.tvRecord.setText(model.getHealthreading());
            holder.tvUnits.setText(model.getUnit());
            holder.tvDate.setText(AppUtils.getDisplayDate(model.getCreatedat()));

            holder.tvRecord.setVisibility(View.VISIBLE);
            holder.tvUnits.setVisibility(View.VISIBLE);
            holder.tvDate.setVisibility(View.VISIBLE);
            holder.tvNoRecord.setVisibility(View.GONE);
        } else {
            holder.tvRecord.setVisibility(View.GONE);
            holder.tvUnits.setVisibility(View.GONE);
            holder.tvDate.setVisibility(View.GONE);
            holder.tvNoRecord.setVisibility(View.VISIBLE);
            holder.tvNoRecord.setText("Please record your "+model.getHealthdesc());
        }
        holder.card_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.moveToTrackingGraphScreen(model.getHealthdesc());
            }
        });
    }

    private boolean checkViewColor(String health_title, String last_record) {
        if (health_title.equalsIgnoreCase("Blood Pressure") || health_title.equalsIgnoreCase("Sugar")){
            String[] ss = defaultValue.split("/");
            if (health_title.equalsIgnoreCase("Blood Pressure")){
                if (Integer.parseInt(ss[0])<Integer.parseInt(healthrecord.getAbnormal_min()) || Integer.parseInt(ss[0])>Integer.parseInt(healthrecord.getAbnormal_max())){
                    return true;
                }
                else  if (Integer.parseInt(ss[1])<Integer.parseInt(healthrecord2.getAbnormal_min()) || Integer.parseInt(ss[1])>Integer.parseInt(healthrecord2.getAbnormal_max())){
                    return true;
                }
            }else {
                if (Integer.parseInt(ss[0])<Integer.parseInt(healthrecord.getAbnormal_min()) || Integer.parseInt(ss[0])>Integer.parseInt(healthrecord.getAbnormal_max())){
                    return true;
                }
                else  if (Integer.parseInt(ss[1])<Integer.parseInt(healthrecord2.getAbnormal_min()) || Integer.parseInt(ss[1])>Integer.parseInt(healthrecord2.getAbnormal_max())){
                    return true;
                }
            }
        }
        else if (health_title.equalsIgnoreCase("heart rate")){
            if (Integer.parseInt(defaultValue)<Integer.parseInt(healthrecord.getAbnormal_min()) || Integer.parseInt(defaultValue)>Integer.parseInt(healthrecord.getAbnormal_max())){
                return true;
            }
        }
        else {
//            String[] ss = defaultValue.split("\\.");
            if (Float.parseFloat(defaultValue)<Float.parseFloat(healthrecord.getAbnormal_min()) || Float.parseFloat(defaultValue)>Float.parseFloat(healthrecord.getAbnormal_max())){
                return true;
            }
        }
        return false;
    }


    @Override
    public int getItemCount() {
        return arrData.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvRecord, tvDesc, tvUnits, tvNoRecord,tvDate;
        public CardView card_view;
        RelativeLayout ll_lastrecord;

        public ViewHolder(View itemView) {
            super(itemView);
            tvRecord = itemView.findViewById(R.id.tvRecord);
            tvDesc = itemView.findViewById(R.id.tvDesc);
            tvUnits = itemView.findViewById(R.id.tvUnits);
            tvNoRecord = itemView.findViewById(R.id.tvNoRecord);
            tvDate = itemView.findViewById(R.id.tvDate);
            card_view = itemView.findViewById(R.id.card_view);
            card_view.setOnClickListener(this);
            ll_lastrecord = itemView.findViewById(R.id.ll_lastrecord);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) {
                mClickListener.onItemClick(view, arrData.get(getAdapterPosition()).getHealthdesc());
                notifyDataSetChanged();
            }
        }
    }

    public LastRecordModel getItem(int id) {
        return arrData.get(id);
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    public interface ItemClickListener {
        void onItemClick(View view, String catName);
    }

    private void saveGlobalRecords(String health_title, ArrayList<GlobalConfigResObjModel> globalRecords) {
        for (int i = 0; i < globalRecords.size(); i++){
            if (health_title.equalsIgnoreCase("weight") && globalRecords.get(i).getType().equalsIgnoreCase("weight")) {
                healthrecord = globalRecords.get(i);
                defaultValue = globalRecords.get(i).getMin()+".00";
            } else if (health_title.equalsIgnoreCase("temperature") && globalRecords.get(i).getType().equalsIgnoreCase("temperature")) {
                healthrecord = globalRecords.get(i);
                defaultValue = globalRecords.get(i).getMin()+".00";
            } else if (health_title.equalsIgnoreCase("Blood pressure") && globalRecords.get(i).getType().equalsIgnoreCase("bp_diastolic")) {
                healthrecord2 = globalRecords.get(i);
                defaultValue = defaultValue+"/"+globalRecords.get(i).getMin();
            } else if (health_title.equalsIgnoreCase("Blood pressure") && globalRecords.get(i).getType().equalsIgnoreCase("bp_systolic")) {
                healthrecord = globalRecords.get(i);
                defaultValue = globalRecords.get(i).getMin();
            } else if (health_title.equalsIgnoreCase("sugar") && globalRecords.get(i).getType().equalsIgnoreCase("sugar_fasting")) {
                healthrecord = globalRecords.get(i);
                defaultValue = globalRecords.get(i).getMin();
            } else if (health_title.equalsIgnoreCase("sugar") && globalRecords.get(i).getType().equalsIgnoreCase("sugar_post_prandial")) {
                healthrecord2 = globalRecords.get(i);
                defaultValue = defaultValue+"/"+globalRecords.get(i).getMin();
            } else if (health_title.equalsIgnoreCase("hemoglobin") && globalRecords.get(i).getType().equalsIgnoreCase("hemoglobin")) {
                healthrecord = globalRecords.get(i);
                defaultValue = globalRecords.get(i).getMin()+".00";
            } else if (health_title.equalsIgnoreCase("heart rate") && globalRecords.get(i).getType().equalsIgnoreCase("heart_rate")) {
                healthrecord = globalRecords.get(i);
                defaultValue = globalRecords.get(i).getMin();
            }
        }
    }
}