package com.savika.tracking.lastRecords;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/*
 * Created by Naresh Ravva on 08/10/17.
 */

public class LastRecordResponse {

    private String date, type;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    private boolean success;
    private String message;

    @SerializedName("your_last_records")
    private ArrayList<LastRecordModel> arrLastRecords = new ArrayList<>();

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public ArrayList<LastRecordModel> getArrLastRecords() {
        return arrLastRecords;
    }

    public void setArrLastRecords(ArrayList<LastRecordModel> arrLastRecords) {
        this.arrLastRecords = arrLastRecords;
    }
}
