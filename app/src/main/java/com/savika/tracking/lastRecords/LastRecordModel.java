package com.savika.tracking.lastRecords;

/*
 * Created by Naresh Ravva on 06/10/17.
 */

public class LastRecordModel {
    String healthreading, healthid, healthdesc, unit, createdat;

    public String getCreatedat() {
        return createdat;
    }

    public void setCreatedat(String createdat) {
        this.createdat = createdat;
    }

    public String getHealthreading() {
        return healthreading;
    }

    public void setHealthreading(String healthreading) {
        this.healthreading = healthreading;
    }

    public String getHealthid() {
        return healthid;
    }

    public void setHealthid(String healthid) {
        this.healthid = healthid;
    }

    public String getHealthdesc() {
        return healthdesc;
    }

    public void setHealthdesc(String healthdesc) {
        this.healthdesc = healthdesc;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }
}
