package com.savika.tracking.graph;

import com.google.gson.annotations.SerializedName;
import com.savika.tracking.lastRecords.LastRecordModel;

import java.util.ArrayList;

/*
 * Created by Naresh Ravva on 13/10/17.
 */

public class GraphResponseModel {

    private boolean success;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    private String message;

    @SerializedName("graph_data")
    private ArrayList<GraphObjtModel> arrGraphRecords = new ArrayList<>();

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public ArrayList<GraphObjtModel> getArrGraphRecords() {
        return arrGraphRecords;
    }

    public void setArrGraphRecords(ArrayList<GraphObjtModel> arrGraphRecords) {
        this.arrGraphRecords = arrGraphRecords;
    }
}
