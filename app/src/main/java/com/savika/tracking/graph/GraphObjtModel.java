package com.savika.tracking.graph;

/*
 * Created by Naresh Ravva on 13/10/17.
 */

public class GraphObjtModel {

    private String healthreading, createdat, unit;

    public String getHealthreading() {
        return healthreading;
    }

    public void setHealthreading(String healthreading) {
        this.healthreading = healthreading;
    }

    public String getUpdatedat() {
        return createdat;
    }

    public void setUpdatedat(String updatedat) {
        this.createdat = updatedat;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }
}
