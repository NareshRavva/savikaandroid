package com.savika.tracking;

import java.io.Serializable;

/*
 * Created by Naresh Ravva on 07/10/17.
 */

public class HealthObjtModel implements Serializable {

    private String healthid, healthdesc, unit;

    public String getHealthid() {
        return healthid;
    }

    public void setHealthid(String healthid) {
        this.healthid = healthid;
    }

    public String getHealthdesc() {
        return healthdesc;
    }

    public void setHealthdesc(String healthdesc) {
        this.healthdesc = healthdesc;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }
}
