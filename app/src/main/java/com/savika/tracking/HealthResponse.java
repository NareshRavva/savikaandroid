package com.savika.tracking;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/*
 * Created by Naresh Ravva on 07/10/17.
 */

public class HealthResponse implements Serializable {

    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    private boolean success;

    @SerializedName("health")
    private ArrayList<HealthObjtModel> arrayHealthData = new ArrayList<>();


    public ArrayList<HealthObjtModel> getArrayHealthData() {
        return arrayHealthData;
    }

    public void setArrayHealthData(ArrayList<HealthObjtModel> arrayHealthData) {
        this.arrayHealthData = arrayHealthData;
    }
}
