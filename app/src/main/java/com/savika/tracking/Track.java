package com.savika.tracking;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.savika.Api.ApiClient;
import com.savika.Api.ApiInterface;
import com.savika.R;
import com.savika.components.Button;
import com.savika.constants.AppConstants;
import com.savika.dialogUtils.DatePicketDialog;
import com.savika.amAMother.ChildModel;
import com.savika.logger.Logger;
import com.savika.profile.ProfileEditActivity;
import com.savika.trackHealthRecords.HealthRecordsActivity;
import com.savika.tracking.lastRecords.LastRecordModel;
import com.savika.tracking.lastRecords.LastRecordResponse;
import com.savika.tracking.lastRecords.LastRecordsAdapter;
import com.savika.tracking.lastRecords.LastRecordsRequest;
import com.savika.tracking.tips.TipsCardAdapter;
import com.savika.tracking.tips.TipsCardModel;
import com.savika.tracking.tips.TipsResponse;
import com.savika.utils.AppUtils;
import com.savika.utils.SharedPrefsUtils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;

/*
 * Created by Naresh Ravva on 06/10/17.
 */

public class Track extends AppCompatActivity implements TipsCardAdapter.ItemClickListener, View.OnClickListener {

    @BindView(R.id.rvHealthTips)
    RecyclerView rvHealthTips;

    @BindView(R.id.rvHealthCards)
    RecyclerView rvHealthCards;

    @BindView(R.id.tvMother)
    TextView tvMother;

    @BindView(R.id.tvChild1)
    TextView tvChild1;

    @BindView(R.id.tvChild2)
    TextView tvChild2;

    @BindView(R.id.tvWeigth)
    TextView tvWeigth;

    @BindView(R.id.tvTemp)
    TextView tvTemp;

    @BindView(R.id.tvHeartRate)
    TextView tvHeartRate;

    @BindView(R.id.tvHealthRecrds)
    TextView tvHealthRecrds;

    @BindView(R.id.tvBloodPresure)
    TextView tvBloodPresure;

    @BindView(R.id.tvSugar)
    TextView tvSugar;

    @BindView(R.id.tvHemoglobin)
    TextView tvHemoglobin;

    @BindView(R.id.cvLables)
    CardView cvMotherChildLables;

    @BindView(R.id.tvDate)
    TextView tvDate;

    @BindView(R.id.llBack)
    LinearLayout llBack;

    private String TYPE;

    private String TAG = this.getClass().getSimpleName();

    private LinearLayoutManager layoutManager;

    ArrayList<ChildModel> ARR_CHILD_DATA = new ArrayList<>();

    private ArrayList<HealthObjtModel> ARR_HEALTH_DATA = new ArrayList<>();
    private ArrayList<TipsCardModel> ARR_TIPS = new ArrayList<>();
    private ArrayList<LastRecordModel> ARR_LAST_RECORDS = new ArrayList<>();

    private String CHILD_ID = null;
    private String DATE = null;
    private String USER_ID = null;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_track);

        ButterKnife.bind(this);

        USER_ID = SharedPrefsUtils.getUserID(Track.this);
        TYPE = AppUtils.getMotherOrPregnantType(this);
        DATE = AppUtils.getTodatDate();

        //get tips
        getTips();

        tvMother.setOnClickListener(this);
        tvWeigth.setOnClickListener(this);
        tvTemp.setOnClickListener(this);
        tvHeartRate.setOnClickListener(this);
        tvHealthRecrds.setOnClickListener(this);
        tvBloodPresure.setOnClickListener(this);
        tvSugar.setOnClickListener(this);
        tvHemoglobin.setOnClickListener(this);
        tvDate.setOnClickListener(this);
        llBack.setOnClickListener(this);

        /*
         * get health traking types data
         */
        getHealthIdsAndUnits();

        if (SharedPrefsUtils.getMotherORPrgntType(this).equalsIgnoreCase("1")) {
            cvMotherChildLables.setVisibility(View.GONE);
        } else {
            if (getIntent().getSerializableExtra(AppConstants.EXTRA_CHILD_DATA) != null)
                ARR_CHILD_DATA = (ArrayList<ChildModel>) getIntent().getSerializableExtra(AppConstants.EXTRA_CHILD_DATA);
        }

        updateCardsActiveInActive();
    }


    /**
     *
     */
    private void updateCardsActiveInActive() {

        int childCount = SharedPrefsUtils.getChildCount(Track.this);

        Logger.info(TAG, "childCount-->" + childCount);

        if (childCount == 0) {
            tvChild1.setAlpha(0.3f);
            tvChild2.setAlpha(0.3f);
            tvChild1.setOnClickListener(null);
            tvChild2.setOnClickListener(null);

        } else if (childCount == 1) {
            tvChild2.setAlpha(0.3f);
            tvChild1.setOnClickListener(this);
            tvChild2.setOnClickListener(null);

        } else if (childCount == 2) {
            tvChild1.setOnClickListener(this);
            tvChild2.setOnClickListener(this);
        }
    }

    @Override
    protected void onResume() {
        //get last records by date
        getLastRecords(TYPE);

        super.onResume();
    }

    /*
     * @param ARR_TIPS
     */
    private void setDataToAdapter(ArrayList<TipsCardModel> ARR_TIPS) {
        layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, true);
        layoutManager.setReverseLayout(true);
        layoutManager.setStackFromEnd(true);
        rvHealthTips.setLayoutManager(layoutManager);


        TipsCardAdapter adapter = new TipsCardAdapter(Track.this, this.ARR_TIPS);
        adapter.setClickListener(this);
        rvHealthTips.setAdapter(adapter);
    }

    /*
     * @param arrLastRecords
     */
    private void setHealthCards(ArrayList<LastRecordModel> arrLastRecords) {

        ARR_LAST_RECORDS = arrLastRecords;

        if (arrLastRecords != null) {
            Collections.reverse(arrLastRecords);
            layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, true);
            layoutManager.setReverseLayout(true);
            layoutManager.setStackFromEnd(true);
            rvHealthCards.setLayoutManager(layoutManager);

            LastRecordsAdapter last_record_adapter = new LastRecordsAdapter(Track.this, arrLastRecords);
            //last_record_adapter.setClickListener(this);
            rvHealthCards.setAdapter(last_record_adapter);
        }
    }


    /*
     *
     */
    private void getHealthIdsAndUnits() {

        if (AppUtils.isNetworkConnected(this)) {
            AppUtils.showDialog(this);

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            Call<HealthResponse> call = apiService.getHealthData(SharedPrefsUtils.getToken(getApplicationContext()));

            call.enqueue(new retrofit2.Callback<HealthResponse>() {
                @Override
                public void onResponse(Call<HealthResponse> call, retrofit2.Response<HealthResponse> response) {

                    AppUtils.dismissDialog();

                    try {
                        if (response.code() == 200 && response.body().isSuccess()) {
                            ARR_HEALTH_DATA = response.body().getArrayHealthData();
                        }
                        else {
                            if (response.body().getMessage()!=null) {
                                if (response.body().getMessage().equalsIgnoreCase("Unauthorized")) {
                                    AppUtils.sessionExpired(Track.this);
                                } else
                                    AppUtils.showToast(getApplicationContext(), response.body().getMessage());
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<HealthResponse> call, Throwable t) {
                    AppUtils.dismissDialog();
                }
            });
        }
        else {
            AppUtils.showToast(this,"It looks like your internet connectioin off. Please turn it on and try again.");
        }
    }

    /**
     *
     */
    private void getTips() {

        if (AppUtils.isNetworkConnected(this)) {
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            Call<TipsResponse> call = apiService.getHealthTips(SharedPrefsUtils.getToken(getApplicationContext()),USER_ID, SharedPrefsUtils.getMotherORPrgntType(this));

            call.enqueue(new retrofit2.Callback<TipsResponse>() {
                @Override
                public void onResponse(Call<TipsResponse> call, retrofit2.Response<TipsResponse> response) {

                    AppUtils.dismissDialog();

                    if (response.code() == 200 && response.body().isSuccess()) {
                        ARR_TIPS = response.body().getArrTips();
                        Logger.info(TAG, "--ARR_TIPS-->" + ARR_TIPS.size());
                        Collections.reverse(ARR_TIPS);
                        setDataToAdapter(ARR_TIPS);
                    }
                    else {
                        if (response.body().getMessage()!=null) {
                            if (response.body().getMessage().equalsIgnoreCase("Unauthorized")) {
                                AppUtils.sessionExpired(Track.this);
                            } else
                                AppUtils.showToast(getApplicationContext(), response.body().getMessage());
                        }
                    }

                }

                @Override
                public void onFailure(Call<TipsResponse> call, Throwable t) {
                    AppUtils.dismissDialog();
                }
            });
        }
        else {
            AppUtils.showToast(this,"It looks like your internet connectioin off. Please turn it on and try again.");
        }
    }


    /*
     * @param TYPE
     */
    private void getLastRecords(String TYPE) {

        if (AppUtils.isNetworkConnected(this)) {
            //AppUtils.showDialog(this);
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            LastRecordsRequest model = new LastRecordsRequest();
            model.setType(TYPE);

            if (TYPE.equalsIgnoreCase(AppConstants.STR_CHILD)) {
                model.setChildid(CHILD_ID);
            }
            model.setUserid(SharedPrefsUtils.getUserID(this));
//        model.setDate(DATE);

            Call<LastRecordResponse> call = apiService.lastRecords(SharedPrefsUtils.getToken(getApplicationContext()),model);

            call.enqueue(new retrofit2.Callback<LastRecordResponse>() {
                @Override
                public void onResponse(Call<LastRecordResponse> call, retrofit2.Response<LastRecordResponse> response) {
                    Logger.info(TAG, "status code-->" + response.code());

                    AppUtils.dismissDialog();

                    if (response.code() == 200 && response.body().isSuccess()) {
                        setHealthCards(response.body().getArrLastRecords());
                    }
                    else {
                        if (response.body().getMessage()!=null) {
                            if (response.body().getMessage().equalsIgnoreCase("Unauthorized")) {
                                AppUtils.sessionExpired(Track.this);
                            } else
                                AppUtils.showToast(getApplicationContext(), response.body().getMessage());
                        }
                    }
                }

                @Override
                public void onFailure(Call<LastRecordResponse> call, Throwable t) {
                    //AppUtils.showToast(getActivity(), getString(R.string.server_down));
                    AppUtils.dismissDialog();
                }
            });
        }
        else {
            AppUtils.showToast(this,"It looks like your internet connectioin off. Please turn it on and try again.");
        }
    }

    @Override
    public void onItemClick(View view, String position, String catName) {

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.tvMother:
                updateMotherCards(tvMother);
                TYPE = AppConstants.STR_MOTHER;
                updateLabelsDependsUponType(TYPE);

                getLastRecords(TYPE);

                break;

            case R.id.tvChild1:
                if (ARR_CHILD_DATA.size() > 0 && ARR_CHILD_DATA.get(0) != null) {
                    CHILD_ID = ARR_CHILD_DATA.get(0).getChildid();

                    updateMotherCards(tvChild1);
                    TYPE = AppConstants.STR_CHILD;

                    updateLabelsDependsUponType(TYPE);

                    getLastRecords(TYPE);
                }

                break;

            case R.id.tvChild2:
                if (ARR_CHILD_DATA.size() > 0 && ARR_CHILD_DATA.get(1) != null) {
                    CHILD_ID = ARR_CHILD_DATA.get(1).getChildid();

                    updateMotherCards(tvChild2);
                    TYPE = AppConstants.STR_CHILD;

                    updateLabelsDependsUponType(TYPE);

                    getLastRecords(TYPE);
                }


                break;

            case R.id.tvWeigth:
                moveToTrackingGraphScreen(getString(R.string.weight));
                break;

            case R.id.tvTemp:
                moveToTrackingGraphScreen(getString(R.string.temperature));
                break;

            case R.id.tvHeartRate:
                moveToTrackingGraphScreen(getString(R.string.heart_rate));
                break;

            case R.id.tvHealthRecrds:
                Intent healthRecrdsScrn = new Intent(Track.this, HealthRecordsActivity.class);
                healthRecrdsScrn.putExtra(AppConstants.EXTRA_CHILD_DATA, ARR_CHILD_DATA);
                startActivity(healthRecrdsScrn);
                break;

            case R.id.tvBloodPresure:
                moveToTrackingGraphScreen(tvBloodPresure.getText().toString());
                break;

            case R.id.tvSugar:
                moveToTrackingGraphScreen(tvSugar.getText().toString());
                break;

            case R.id.tvHemoglobin:
                moveToTrackingGraphScreen(getString(R.string.hemoglobin));
                break;

            case R.id.llBack:
                finish();
                break;

            case R.id.tvDate:
                datedialog();
                break;
        }
    }

    public void datedialog()
    {
        String defaultdate = "";
        runOnUiThread(new DatePicketDialog(Track.this, getString(R.string.select_date), getString(R.string.please_pic_the_date), false, 35, 0,defaultdate, Track.this.getLayoutInflater(), new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Logger.info("TAG", "Selected prblm Date-->" + view.getTag());
                DateFormat df = new SimpleDateFormat("dd MMM yyyy", Locale.getDefault());
                try {
                    Date selecteddate = df.parse((String) view.getTag());
                    Date date = new Date();
                    Date today = df.parse(df.format(date));
                    if (selecteddate.after(today) && !selecteddate.equals(date) ){
                        showAlert(Track.this,"Please pic date before today");
                    }
                    else {
                        Logger.info("TAG", "Selected prblm Date-->" + view.getTag());

                        DATE = AppUtils.convertDateFormat("" + view.getTag());

                        getLastRecords(TYPE);
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }

            }
        }));
    }
    private void showAlert(Track ctx, String message) {
        if (ctx != null) {

            try {
                final AlertDialog.Builder builder = AppUtils.getAlertDialog(ctx);
                View view  = getLayoutInflater().inflate(R.layout.pop_alert_date,null);
                builder.setView(view);
                final AlertDialog alert = builder.create();
                alert.show();
                TextView messagetxt = view.findViewById(R.id.tvdateerrorText);
                messagetxt.setText(message);
                Button ok = view.findViewById(R.id.datebtnPositive);
                ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alert.cancel();
                        datedialog();
                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }



    /*
     * @param tvView
     */
    private void updateMotherCards(TextView tvView) {
        tvMother.setBackgroundColor(getResources().getColor(R.color.white));
        tvChild1.setBackgroundColor(getResources().getColor(R.color.white));
        tvChild2.setBackgroundColor(getResources().getColor(R.color.white));

        tvMother.setTextColor(Color.BLACK);
        tvChild1.setTextColor(Color.BLACK);
        tvChild2.setTextColor(Color.BLACK);

        tvView.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        tvView.setTextColor(Color.WHITE);
    }

    /*
     * @param TYPE
     */
    private void updateLabelsDependsUponType(String TYPE) {

        if (TYPE.equalsIgnoreCase(AppConstants.STR_MOTHER)) {
            tvBloodPresure.setText(getString(R.string.blood_pressure));
            tvSugar.setText(getString(R.string.sugar));
        } else {
            tvBloodPresure.setText(getString(R.string.head_circumference));
            tvSugar.setText(getString(R.string.height));
        }

    }

    /*
     * @param param
     */
    public void moveToTrackingGraphScreen(String param) {

        if (ARR_HEALTH_DATA.size() < 1) {
            AppUtils.showToast(Track.this, "Sorry unable to get healthID's");
            return;
        }

        String[] last_record = getLastHealthReading(param);

        for (HealthObjtModel model : ARR_HEALTH_DATA) {
            if (model.getHealthdesc().equalsIgnoreCase(param)) {
                Logger.info(TAG, "--data-->" + model.getHealthid());
                Logger.info(TAG, "-- last record-->" + last_record);

                Intent track = new Intent(Track.this, TrackGraphActivity.class);
                track.putExtra(AppConstants.EXTRA_HEALTH_ID, model.getHealthid());
                track.putExtra(AppConstants.EXTRA_HEALTH_UNIT, model.getUnit());
                track.putExtra(AppConstants.EXTRA_HEALTH_DESC, model.getHealthdesc());
                track.putExtra(AppConstants.EXTRA_HEALTH_RECORD, last_record[0]);
                track.putExtra(AppConstants.EXTRA_HEALTH_DATE,last_record[1]);
                track.putExtra(AppConstants.EXTRA_CHILD_DATA, ARR_CHILD_DATA);
                startActivity(track);
            }
        }

    }

    /*
     * @param param
     * @return
     */
    private String[] getLastHealthReading(String param) {
        String[] last_record = new String[2];

        for (LastRecordModel model : ARR_LAST_RECORDS) {
            if (model.getHealthdesc().equalsIgnoreCase(param)) {
                last_record[0] = model.getHealthreading();
                last_record[1] = model.getCreatedat();
            }
        }
        return last_record;
    }
}
