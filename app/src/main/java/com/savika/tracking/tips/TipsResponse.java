package com.savika.tracking.tips;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/*
 * Created by Naresh Ravva on 08/10/17.
 */

public class TipsResponse implements Serializable {

    private boolean success;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    private String message;

    @SerializedName("tips")
    private ArrayList<TipsCardModel> arrTips = new ArrayList<>();

    public ArrayList<TipsCardModel> getArrTips() {
        return arrTips;
    }

    public void setArrTips(ArrayList<TipsCardModel> arrTips) {
        this.arrTips = arrTips;
    }
}
