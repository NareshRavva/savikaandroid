package com.savika.tracking.tips;

/*
 * Created by Naresh Ravva on 06/10/17.
 */

public class TipsCardModel {

    private String idtips, tipstitle, tipsdescription;

    public String getIdtips() {
        return idtips;
    }

    public void setIdtips(String idtips) {
        this.idtips = idtips;
    }

    public String getTipstitle() {
        return tipstitle;
    }

    public void setTipstitle(String tipstitle) {
        this.tipstitle = tipstitle;
    }

    public String getTipsdescription() {
        return tipsdescription;
    }

    public void setTipsdescription(String tipsdescription) {
        this.tipsdescription = tipsdescription;
    }
}
