package com.savika.tracking;

import com.savika.calendar.CalendarActivity;
import com.savika.tracking.graph.GraphObjtModel;
import com.savika.utils.AppUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Locale;

/**
 * Created by nexivo on 18/11/17.
 */

public class CalculateDates {

    static String[] week_dates ;
    static String[] month_dates;
    static int diffInDays;
    ArrayList<GraphObjtModel> graphdata;
    static ArrayList<String> diffdates;

    public CalculateDates(ArrayList<GraphObjtModel> arr_graph_data) {
        graphdata = arr_graph_data;
    }


    public static int[] getXValues(){

        return null;
    }

    public static int[] createMissingMonth(String start_date){
        SimpleDateFormat s1 = new SimpleDateFormat("dd-MM-yyyy");
        Date newerDate = null;
        Date olderDate = null;
        int[] days = new int[0];
        try {
            newerDate = s1.parse(start_date);
            Calendar c = Calendar.getInstance();
            c.setTime(newerDate);
            int monthMaxDays = c.getActualMaximum(Calendar.DAY_OF_MONTH);
            int month = c.get(Calendar.MONTH)+1;
            int year = c.get(Calendar.YEAR);
            days = new int [monthMaxDays];
            week_dates = new String[monthMaxDays];
            for (int i = 1; i <= monthMaxDays; i++){
                String date ;
                if (i<10){
                    date = "0"+i;
                }
                else date = String.valueOf(i);
                week_dates[i-1] = year+"-"+month+"-"+date;
                days[i-1] = Integer.parseInt(date);
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return days;
    }

    public static ArrayList<int[]> createMissingMonthValues(String start_date, ArrayList<GraphObjtModel> graphdata){
        ArrayList<int[]> val = new ArrayList<>();
        SimpleDateFormat s1 = new SimpleDateFormat("dd-MM-yyyy");
        Date newerDate = null;
        try {
            newerDate = s1.parse(start_date);
            Calendar c = Calendar.getInstance();
            c.setTime(newerDate);
            int monthMaxDays = c.getActualMaximum(Calendar.DAY_OF_MONTH);
            int[] val1 = new int[monthMaxDays];
            int[] val2 = new int[monthMaxDays];

            String[] graphd = new String[graphdata.size()];
            for (int i = 0; i<graphdata.size();++i){
                graphd[i] = graphdata.get(i).getUpdatedat();
                int k = Arrays.asList(week_dates).indexOf(graphdata.get(i).getUpdatedat());
                if (k>=0){
                    if (graphdata.get(i).getUnit().equalsIgnoreCase("mg/dl") || graphdata.get(i).getUnit().equalsIgnoreCase("mmhg")){
                        String[] bp = graphdata.get(i).getHealthreading().split("/");
                        val1[k] = (int)Float.parseFloat(bp[0]);
                        val2[k] = (int)Float.parseFloat(bp[1]);
                    }
                    else {
                        val1[k] = (int)Float.parseFloat(graphdata.get(i).getHealthreading());
                    }
                }
            }
            val.add(val1);
            if (graphdata.get(0).getUnit().equalsIgnoreCase("mg/dl") || graphdata.get(0).getUnit().equalsIgnoreCase("mmhg")){
                val.add(val2);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return val;
    }

    public static int[] createMissingDates(String start_date, String end_date) {
        SimpleDateFormat s1 = new SimpleDateFormat("dd-MM-yyyy");
        Date newerDate = null;
        Date olderDate = null;
        int[] days = new int[0];
        try {
            newerDate = s1.parse(start_date);
            olderDate = s1.parse(end_date);
            SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");
            diffInDays = (int)( (olderDate.getTime() - newerDate.getTime())
                    / (1000 * 60 * 60 * 24) );
            days = new int [diffInDays+1];
            week_dates = new String[diffInDays+1];
            // I just want date before 90 days. you can give that you want.
            // you can specify your format here...
            //Log.d("DATE", "Date before 7 Days: " + s.format(new Date(cal.getTimeInMillis())));
            for (int i = diffInDays ; i >= 0; i--){
                Calendar cal = Calendar.getInstance();
                cal.add(Calendar.DATE, -i);
                week_dates[diffInDays-i] = s.format(new Date(cal.getTimeInMillis()));
                days[diffInDays-i] = cal.get(Calendar.DATE);
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(newerDate);
        int noofDaysInMonth = cal.getActualMaximum(Calendar.DAY_OF_MONTH);

        return checkEndofMonth(days,noofDaysInMonth);
    }

    private static int[] checkEndofMonth(int[] days, int noofDaysInMonth) {
        int count = 0;
        boolean isIncrement = false;
        if (days[0]>22) {
            int[] dd = new int[days.length];
            if (noofDaysInMonth == 28) {
                for (int i = 0;i<days.length;i++){
                    dd[i] = days[i];
                    if (days[i]==28){
                        isIncrement = true;
                    }
                    if (isIncrement){
                        dd[i] = 28+count;
                        count = count+1;
                    }


                }
            } else if (noofDaysInMonth == 29) {
                for (int i = 0;i<days.length;i++){
                    dd[i] = days[i];
                    if (days[i]==29)
                        isIncrement = true;
                    if (isIncrement){
                        dd[i] = 29+count;
                        count = count+1;
                    }

                }
            } else if (noofDaysInMonth == 30) {
                for (int i = 0;i<days.length;i++){
                    dd[i] = days[i];
                    if (days[i]==30)
                        isIncrement = true;
                    if (isIncrement){
                        dd[i] = 30+count;
                        count = count+1;
                    }

                }
            } else if (noofDaysInMonth == 31) {
                for (int i = 0;i<days.length;i++){
                    dd[i] = days[i];
                    if (days[i]==31)
                        isIncrement = true;
                    if (isIncrement){
                        dd[i] = 31+count;
                        count = count+1;
                    }

                }
            }
            return dd;
        }
        else return days;
    }


    public static ArrayList<int[]> createMissingValues(ArrayList<GraphObjtModel> graphdata) {
        ArrayList<int[]> val = new ArrayList<>();
        try{
            int[] val1 = new int[diffInDays+1];
            int[] val2 = new int[diffInDays+1];
            String[] graphd = new String[graphdata.size()];
            for (int i = 0; i<graphdata.size();++i){
                graphd[i] = graphdata.get(i).getUpdatedat();
                int k = Arrays.asList(week_dates).indexOf(graphdata.get(i).getUpdatedat());
                if (graphdata.get(i).getUnit().equalsIgnoreCase("mg/dl") || graphdata.get(i).getUnit().equalsIgnoreCase("mmhg")){
                    String[] bp = graphdata.get(i).getHealthreading().split("/");
                    val1[k] = (int)Float.parseFloat(bp[0]);
                    val2[k] = (int)Float.parseFloat(bp[1]);
                }
                else {
                    val1[k] = (int)Float.parseFloat(graphdata.get(i).getHealthreading());
                }
            }
            val.add(val1);
            if (graphdata.get(0).getUnit().equalsIgnoreCase("mg/dl") || graphdata.get(0).getUnit().equalsIgnoreCase("mmhg")){
                val.add(val2);
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return val;
    }

    static int[] createMissingCustomDates(String start_date, String end_date) {
        SimpleDateFormat s1 = new SimpleDateFormat("dd-MM-yyyy",Locale.getDefault());
        Date newerDate = null;
        Date olderDate = null;
        int[] days = new int[0];
        try {
            newerDate = s1.parse(start_date);
            olderDate = s1.parse(end_date);
            SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd",Locale.getDefault());
            SimpleDateFormat mon = new SimpleDateFormat("dd MMM", Locale.getDefault());
            diffInDays = (int) AppUtils.calculateDiffInDates(newerDate,olderDate);
            days = new int [diffInDays];
            week_dates = new String[diffInDays];
            month_dates = new String[diffInDays];

            // I just want date before 90 days. you can give that you want.
            // you can specify your format here...
            //Log.d("DATE", "Date before 7 Days: " + s.format(new Date(cal.getTimeInMillis())));
            for (int i = 0 ; i < week_dates.length; i++){
                Calendar cal = Calendar.getInstance();
                cal.setTime(newerDate);
                cal.add(Calendar.DATE, +i);
                week_dates[i] = s.format(new Date(cal.getTimeInMillis()));
                month_dates[i] = mon.format(new Date(cal.getTimeInMillis()));
                days[i] = i;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return days;
    }
}
