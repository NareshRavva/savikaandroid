package com.savika.tracking;

import android.app.Activity;
import android.app.AlertDialog;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.savika.Api.ApiClient;
import com.savika.Api.ApiInterface;
import com.savika.Api.ResponseModel;
import com.savika.GlobalConfigResObjModel;
import com.savika.R;
import com.savika.components.Button;
import com.savika.constants.AppConstants;
import com.savika.constants.GAConstants;
import com.savika.dialogUtils.CustomDatePickerDialog;
import com.savika.dialogUtils.DatePicketDialog;
import com.savika.dialogUtils.HealthTrackPicketDialog;
import com.savika.amAMother.ChildModel;
import com.savika.googleAnalytics.GoogleAnalyticsHelper;
import com.savika.logger.Logger;
import com.savika.tracking.graph.GraphObjtModel;
import com.savika.tracking.graph.GraphReqModel;
import com.savika.tracking.graph.GraphResponseModel;
import com.savika.utils.AppUtils;
import com.savika.utils.SharedPrefsUtils;

import org.achartengine.ChartFactory;
import org.achartengine.chart.PointStyle;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;


public class TrackGraphActivity extends AppCompatActivity implements View.OnClickListener {
    private static final double MAX_Y = 120;

    @BindView(R.id.chart_container)
    LinearLayout chart_container;

    @BindView(R.id.tvHeaderTitle)
    TextView tvHeaderTitle;

    @BindView(R.id.tvBottomTitle)
    TextView tvBottomTitle;

    @BindView(R.id.ivAddTrackData)
    ImageView ivAddTrackData;

    @BindView(R.id.tvLatestRecord)
    TextView tvLatestRecord;

    @BindView(R.id.tvMother)
    TextView tvMother;

    @BindView(R.id.tvChild1)
    TextView tvChild1;

    @BindView(R.id.tvChild2)
    TextView tvChild2;

    @BindView(R.id.cvLables)
    CardView cvMotherChildLables;

    @BindView(R.id.tvWeekly)
    TextView tvWeekly;

    @BindView(R.id.tvMonthly)
    TextView tvMonthly;

    @BindView(R.id.tvCustom)
    TextView tvCustom;

    @BindView(R.id.llBack)
    LinearLayout llBack;

    @BindView(R.id.tvNoData)
    TextView tvNoData;

    @BindView(R.id.bottom_graph)
    RelativeLayout bottom_graph;

    @BindView(R.id.diastolicdot)
    TextView diastolicdot;

    @BindView(R.id.sysdotdot)
    TextView sysdotdot;

    private String TYPE;
    String START_DATE, END_DATE,fromdate1="",todate1 ="";

    String defaultValue;
    private String TAG = this.getClass().getSimpleName();

    private String health_title, health_id, health_unit, last_record,last_date;
    private String CHILD_ID = null;

    ArrayList<ChildModel> ARR_CHILD_DATA = new ArrayList<>();
    int typedt = 0;

    private ArrayList<GraphObjtModel> ARR_GRAPH_DATA = new ArrayList<>();
    private ArrayList<GlobalConfigResObjModel> GLOBAL_ARR = new ArrayList<>();
    GlobalConfigResObjModel healthrecord, healthrecord2;

    GoogleAnalyticsHelper mGoogleHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_track_graph);

        ButterKnife.bind(this);

        TYPE = AppUtils.getMotherOrPregnantType(this);
        AppUtils.getDeviceWidthAndHeigth(this);
        setPreviousIntentData();
//        datedialog();
        healthrecord = new GlobalConfigResObjModel();
        weeklyGraphData();


        ivAddTrackData.setOnClickListener(this);
        tvMother.setOnClickListener(this);

        tvWeekly.setOnClickListener(this);
        tvMonthly.setOnClickListener(this);
        tvCustom.setOnClickListener(this);
        llBack.setOnClickListener(this);

        if (SharedPrefsUtils.getGlobalConfig(this) != null) {

            Gson gson = new Gson();
            String json = SharedPrefsUtils.getGlobalConfig(this);
            Type type = new TypeToken<ArrayList<GlobalConfigResObjModel>>() {
            }.getType();
            ArrayList<GlobalConfigResObjModel> arrayList = gson.fromJson(json, type);
            saveGlobalRecords(arrayList);

        }

        if (SharedPrefsUtils.getMotherORPrgntType(this).equalsIgnoreCase("1")) {
            cvMotherChildLables.setVisibility(View.GONE);
        } else {
            if (getIntent().getSerializableExtra(AppConstants.EXTRA_CHILD_DATA) != null)
                ARR_CHILD_DATA = (ArrayList<ChildModel>) getIntent().getSerializableExtra(AppConstants.EXTRA_CHILD_DATA);
        }

        last_record = getIntent().getStringExtra(AppConstants.EXTRA_HEALTH_RECORD);
        last_date = getIntent().getStringExtra(AppConstants.EXTRA_HEALTH_DATE);
        if (last_record != null) {
                try {
                    int ff = Integer.parseInt(last_record);
                    if (health_title.equalsIgnoreCase("temperature") || health_title.equalsIgnoreCase("weight")|| health_title.equalsIgnoreCase("hemoglobin"))
                        defaultValue = last_record + ".00" + "";
                    else {
                        defaultValue = last_record;
                    }
                } catch (Exception e) {
                    defaultValue = last_record;
                }
            boolean isAbnormal = checkViewColor(last_record);
//            if (isAbnormal){
//                tvLatestRecord.setBackgroundColor(Color.GREEN);
//            }
            tvLatestRecord.setText(String.format("%s %s", last_record, health_unit));
            if (health_title.equalsIgnoreCase("Temperature"))
                tvBottomTitle.setText("Your Temp as on "+AppUtils.getDisplayDate(last_date));
            else if (health_title.equalsIgnoreCase("Blood Pressure"))
                tvBottomTitle.setText("Your BP as on "+AppUtils.getDisplayDate(last_date));
            else tvBottomTitle.setText("Your " + health_title + " as on "+AppUtils.getDisplayDate(last_date));
        }
        else {
            if (health_title.equalsIgnoreCase("Temperature"))
                tvBottomTitle.setText("Please Record your Temp ");
            else if (health_title.equalsIgnoreCase("Blood Pressure"))
                tvBottomTitle.setText("Please Record your BP ");
            else tvBottomTitle.setText("Please Record your "+health_title);
        }

        updateCardsActiveInActive();

        InitGoogleAnalytics();
        mGoogleHelper.SendScreenNameGA(this, GAConstants.GRAPH_SCREEN);
    }

    private boolean checkViewColor(String last_record) {
        if (health_title.equalsIgnoreCase("Blood Pressure") || health_title.equalsIgnoreCase("Sugar")){
            String[] ss = defaultValue.split("/");
            if (health_title.equalsIgnoreCase("Blood Pressure")){
                if (Integer.parseInt(ss[0])<Integer.parseInt(healthrecord.getAbnormal_min()) || Integer.parseInt(ss[0])>Integer.parseInt(healthrecord.getAbnormal_max())){
                    return true;
                }
                else  if (Integer.parseInt(ss[1])<Integer.parseInt(healthrecord2.getAbnormal_min()) || Integer.parseInt(ss[1])>Integer.parseInt(healthrecord2.getAbnormal_max())){
                    return true;
                }
            }else {
                if (Integer.parseInt(ss[0])<Integer.parseInt(healthrecord.getAbnormal_min()) || Integer.parseInt(ss[0])>Integer.parseInt(healthrecord.getAbnormal_max())){
                    return true;
                }
                else  if (Integer.parseInt(ss[1])<Integer.parseInt(healthrecord2.getAbnormal_min()) || Integer.parseInt(ss[1])>Integer.parseInt(healthrecord2.getAbnormal_max())){
                    return true;
                }
            }
        }
        else if (health_title.equalsIgnoreCase("heart rate")){
            if (Integer.parseInt(defaultValue)<Integer.parseInt(healthrecord.getAbnormal_min()) || Integer.parseInt(defaultValue)>Integer.parseInt(healthrecord.getAbnormal_max())){
                return true;
            }
        }
        else {
//            String[] ss = defaultValue.split("\\.");
            if (Float.parseFloat(defaultValue)<Float.parseFloat(healthrecord.getAbnormal_min()) || Float.parseFloat(defaultValue)>Float.parseFloat(healthrecord.getAbnormal_max())){
                return true;
            }
        }
        return false;
    }

    // Google Analytics
    private void InitGoogleAnalytics() {
        mGoogleHelper = new GoogleAnalyticsHelper();
        mGoogleHelper.init(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }


    private void saveGlobalRecords(ArrayList<GlobalConfigResObjModel> globalRecords) {
        for (int i = 0; i < globalRecords.size(); i++){
            if (health_title.equalsIgnoreCase("weight") && globalRecords.get(i).getType().equalsIgnoreCase("weight")) {
                healthrecord = globalRecords.get(i);
                defaultValue = globalRecords.get(i).getMin()+".00";
            } else if (health_title.equalsIgnoreCase("temperature") && globalRecords.get(i).getType().equalsIgnoreCase("temperature")) {
                healthrecord = globalRecords.get(i);
                defaultValue = globalRecords.get(i).getMin()+".00";
            } else if (health_title.equalsIgnoreCase("Blood pressure") && globalRecords.get(i).getType().equalsIgnoreCase("bp_diastolic")) {
                healthrecord2 = globalRecords.get(i);
                defaultValue = defaultValue+"/"+globalRecords.get(i).getMin();
            } else if (health_title.equalsIgnoreCase("Blood pressure") && globalRecords.get(i).getType().equalsIgnoreCase("bp_systolic")) {
                healthrecord = globalRecords.get(i);
                defaultValue = globalRecords.get(i).getMin();
            } else if (health_title.equalsIgnoreCase("sugar") && globalRecords.get(i).getType().equalsIgnoreCase("sugar_fasting")) {
                healthrecord = globalRecords.get(i);
                defaultValue = globalRecords.get(i).getMin();
            } else if (health_title.equalsIgnoreCase("sugar") && globalRecords.get(i).getType().equalsIgnoreCase("sugar_post_prandial")) {
                healthrecord2 = globalRecords.get(i);
                defaultValue = defaultValue+"/"+globalRecords.get(i).getMin();
            } else if (health_title.equalsIgnoreCase("hemoglobin") && globalRecords.get(i).getType().equalsIgnoreCase("hemoglobin")) {
                healthrecord = globalRecords.get(i);
                defaultValue = globalRecords.get(i).getMin()+".00";
            } else if (health_title.equalsIgnoreCase("heart rate") && globalRecords.get(i).getType().equalsIgnoreCase("heart_rate")) {
                healthrecord = globalRecords.get(i);
                defaultValue = globalRecords.get(i).getMin();
            }
        }
    }


    /**
     *
     */
    private void updateCardsActiveInActive() {

        int childCount = SharedPrefsUtils.getChildCount(TrackGraphActivity.this);

        Logger.info(TAG, "childCount-->" + childCount);

        if (childCount == 0) {
            tvChild1.setAlpha(0.3f);
            tvChild2.setAlpha(0.3f);
            tvChild1.setOnClickListener(null);
            tvChild2.setOnClickListener(null);

        } else if (childCount == 1) {
            tvChild2.setAlpha(0.3f);
            tvChild1.setOnClickListener(this);
            tvChild2.setOnClickListener(null);

        } else if (childCount == 2) {
            tvChild1.setOnClickListener(this);
            tvChild2.setOnClickListener(this);
        }
    }

    /**
     *
     */
    private void setPreviousIntentData() {
        health_title = getIntent().getStringExtra(AppConstants.EXTRA_HEALTH_DESC);
        health_id = getIntent().getStringExtra(AppConstants.EXTRA_HEALTH_ID);
        health_unit = getIntent().getStringExtra(AppConstants.EXTRA_HEALTH_UNIT);

        if (health_title != null) {
            tvHeaderTitle.setText("Track " + health_title);
        }
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivAddTrackData:
                runOnUiThread(new HealthTrackPicketDialog(TrackGraphActivity.this, health_title, health_unit, defaultValue, healthrecord, healthrecord2, TrackGraphActivity.this.getLayoutInflater(), new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Logger.info("TAG", "Selected prblm Date-->" + view.getTag());
                        String date = (String) view.getTag();
                        tvLatestRecord.setText("" + date.split("/n")[0] + " " + health_unit);
                        if (health_title.equalsIgnoreCase("Temperature"))
                            tvBottomTitle.setText("Your Temp as on "+date.split("/n")[1]);
                        else if (health_title.equalsIgnoreCase("Blood Pressure"))
                            tvBottomTitle.setText("Your BP as on "+date.split("/n")[1]);
                        else tvBottomTitle.setText("Your " + health_title + " as on "+date.split("/n")[1]);
                       // tvBottomTitle.setText(" Your " + health_title + " is on "+ date.split("/n")[1]);
                        //Post Updated Record
                        postUpdatedRecord("" + date.split("/n")[0],date.split("/n")[1]);
                    }
                }));
                break;

            case R.id.tvMother:
                updateMotherCards(tvMother);
                TYPE = AppConstants.STR_MOTHER;
                break;

            case R.id.tvChild1:
                if (ARR_CHILD_DATA.size() > 0 && ARR_CHILD_DATA.get(0) != null) {
                    CHILD_ID = ARR_CHILD_DATA.get(0).getChildid();

                    updateMotherCards(tvChild1);
                    TYPE = AppConstants.STR_CHILD;
                }
                break;

            case R.id.tvChild2:
                if (ARR_CHILD_DATA.size() > 0 && ARR_CHILD_DATA.get(1) != null) {
                    CHILD_ID = ARR_CHILD_DATA.get(1).getChildid();

                    updateMotherCards(tvChild2);
                    TYPE = AppConstants.STR_CHILD;
                }
                break;

            case R.id.tvWeekly:
                weeklyGraphData();
                break;

            case R.id.tvMonthly:
                monthlyGraphData();

                //datedialog();
                break;

            case R.id.tvCustom:
                Calendar cal = Calendar.getInstance();
                cal.add(Calendar.DATE, -30); // I just want date before 90 days. you can give that you want.
                SimpleDateFormat s = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()); // you can specify your format here...
                //Log.d("DATE", "Date before 7 Days: " + s.format(new Date(cal.getTimeInMillis())));
                fromdate1 = s.format(new Date(cal.getTimeInMillis()));

                DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
                Date date = new Date();
                todate1 = dateFormat.format(date);
                customdatedialog();
                break;

            case R.id.llBack:
                finish();
                break;
        }
    }

    public void customdatedialog() {
        runOnUiThread(new CustomDatePickerDialog(TrackGraphActivity.this, fromdate1,todate1, TrackGraphActivity.this.getLayoutInflater(), new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Logger.info("TAG", "Selected prblm Date-->" + view.getTag());
                String DATE = view.getTag().toString();
                Date fromdate = null, todate = null;
                DateFormat sdf = new SimpleDateFormat("dd MMM yyyy", Locale.getDefault());
                String[] customdtarr = DATE.split("###");
                if (customdtarr[0].equalsIgnoreCase("null")) {
                    showAlert(TrackGraphActivity.this, "Choose From date  ", 1);
                    return;
                } else {
                    DateFormat df = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
                    try {
                        fromdate = df.parse(customdtarr[0]);
                        fromdate1 = sdf.format(fromdate);
                        Date date = new Date();
                        Date today = df.parse(df.format(date));
                        if (fromdate.after(today) && !fromdate.equals(date)) {
                            showAlert(TrackGraphActivity.this, "Choose before date  ", 1);
                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }

                if (customdtarr[1].equalsIgnoreCase("null") ) {
                    showAlert(TrackGraphActivity.this, "Choose To date  ", 1);
                    return;
                } else {
                    DateFormat df = new SimpleDateFormat("dd-MM-yyyy",Locale.getDefault());
                    try {
                        todate = df.parse(customdtarr[1]);
                        todate1 = sdf.format(todate);
                        Date date = new Date();
                        Date today = df.parse(df.format(date));
                        if (todate.after(today) && !todate.equals(date)) {
                            fromdate1 = df.format(fromdate);
                            todate1 = df.format(todate);
                            showAlert(TrackGraphActivity.this, "Choose before date  ", 1);
                        } else {
                            if (todate.before(fromdate)) {
                                fromdate1 = df.format(fromdate);
                                todate1 = df.format(todate);
                                showAlert(TrackGraphActivity.this, "from date must be greater than to date", 1);
                            }
                            else {
                                Logger.info("TAG", "CustomDatePicketDialog Date-->" + DATE);
                                typedt = 2;
                                START_DATE = customdtarr[0];
                                END_DATE = customdtarr[1];
                                getGraphData(customdtarr[0], customdtarr[1]);
                            }
                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }

            }
        }));
    }

    public void datedialog() {
        String defaultdate = "";
        runOnUiThread(new DatePicketDialog(TrackGraphActivity.this, getString(R.string.filter_by_month), "Choose a month", true, 35, 0,defaultdate, TrackGraphActivity.this.getLayoutInflater(), new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Logger.info("TAG", "Selected prblm Date-->" + view.getTag());
                DateFormat df = new SimpleDateFormat("dd MMM yyyy",Locale.getDefault());
                try {
                    Date selecteddate = df.parse("01 " + view.getTag());
                    Date date = new Date();
                    Date today = df.parse(df.format(date));
                    if (selecteddate.after(today) && !selecteddate.equals(date)) {
                        showAlert(TrackGraphActivity.this, "Choose before month", 0);
                    } else {
                        START_DATE = convertDateFormat("01 " + view.getTag());

                        END_DATE = convertDateFormat("31 " + view.getTag());
                        typedt = 1;
                        getGraphData(START_DATE, END_DATE);
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }

            }
        }));
    }

    private void showAlert(TrackGraphActivity ctx, String message, final int type) {
        if (ctx != null) {

            try {
                final AlertDialog.Builder builder = AppUtils.getAlertDialog(ctx);
                View view = getLayoutInflater().inflate(R.layout.pop_alert_date, null);
                builder.setView(view);
                final AlertDialog alert = builder.create();
                alert.show();
                TextView heading = view.findViewById(R.id.tvdateerrorTitle);
                TextView messagetxt = view.findViewById(R.id.tvdateerrorText);
                if (message.equalsIgnoreCase("Choose before month")){
                    heading.setText("Check Month");
                    messagetxt.setText("Choose before date/month");
                }
                else messagetxt.setText(message);
                Button ok = view.findViewById(R.id.datebtnPositive);
                ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alert.cancel();

                         customdatedialog();
                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /*
     *
     */
    private void weeklyGraphData() {
        changeTextLabelsColor(tvWeekly);

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -6); // I just want date before 90 days. you can give that you want.
        SimpleDateFormat s = new SimpleDateFormat("dd-MM-yyyy",Locale.getDefault()); // you can specify your format here...
        //Log.d("DATE", "Date before 7 Days: " + s.format(new Date(cal.getTimeInMillis())));
        START_DATE = s.format(new Date(cal.getTimeInMillis()));

        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy",Locale.getDefault());
        Date date = new Date();
        END_DATE = dateFormat.format(date);
        typedt = 0;
        getGraphData(START_DATE, END_DATE);
    }

    private void monthlyGraphData(){
        changeTextLabelsColor(tvMonthly);
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -30); // I just want date before 90 days. you can give that you want.
        SimpleDateFormat s = new SimpleDateFormat("dd-MM-yyyy",Locale.getDefault()); // you can specify your format here...
        //Log.d("DATE", "Date before 7 Days: " + s.format(new Date(cal.getTimeInMillis())));
        START_DATE = s.format(new Date(cal.getTimeInMillis()));

        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy",Locale.getDefault());
        Date date = new Date();
        END_DATE = dateFormat.format(date);
        typedt = 1;
        getGraphData(START_DATE, END_DATE);
    }

    /*
     * @param tvView
     */
    private void updateMotherCards(TextView tvView) {
        tvMother.setBackgroundColor(getResources().getColor(R.color.white));
        tvChild1.setBackgroundColor(getResources().getColor(R.color.white));
        tvChild2.setBackgroundColor(getResources().getColor(R.color.white));

        tvMother.setTextColor(Color.BLACK);
        tvChild1.setTextColor(Color.BLACK);
        tvChild2.setTextColor(Color.BLACK);

        tvView.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        tvView.setTextColor(Color.WHITE);
    }

    /*
     * @param tvlabel
     */
    private void changeTextLabelsColor(TextView tvlabel) {
        tvWeekly.setTextColor(Color.BLACK);
        tvMonthly.setTextColor(Color.BLACK);
        tvCustom.setTextColor(Color.BLACK);

        tvlabel.setTextColor(Color.parseColor("#c27a9f"));
    }


    /*
     * post updated record
     */
    private void postUpdatedRecord(final String reading, String credate) {

        if (AppUtils.isNetworkConnected(this)) {
            //AppUtils.showDialog(this);
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            PostTrackRecordModel model = new PostTrackRecordModel();
            model.setType(TYPE);

            if (TYPE.equalsIgnoreCase(AppConstants.STR_CHILD)) {
                model.setChildid(CHILD_ID);
            }

            Date curDate = new Date();
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",Locale.getDefault());
            String DateToStr = null;
            DateFormat df = new SimpleDateFormat("dd MMM yyyy",Locale.getDefault());
            try {
                Date selecteddate = df.parse(credate);
                Date date = new Date();
                Date today = df.parse(df.format(date));
                if (selecteddate.before(today) && !selecteddate.equals(date)) {
                    DateToStr = AppUtils.convertDateFormat(credate) + " 23:59:59";

                } else {
                    DateToStr = format.format(curDate);
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
            model.setHealthid(health_id);
            model.setHealthreading(reading);
            model.setUserid(SharedPrefsUtils.getUserID(this));
            model.setUsername(SharedPrefsUtils.getUsername(this));
            model.setDate(DateToStr);
            model.setCreatedat(DateToStr);
            model.setUnit(health_unit);

            Call<ResponseModel> call = apiService.postRecord(SharedPrefsUtils.getToken(getApplicationContext()),model);

            call.enqueue(new retrofit2.Callback<ResponseModel>() {
                @Override
                public void onResponse(Call<ResponseModel> call, retrofit2.Response<ResponseModel> response) {
                    Logger.info(TAG, "status code-->" + response.code());
                    AppUtils.dismissDialog();

                    if (response.code() == 200 && response.body().isSuccess()) {
                        AppUtils.showToast(TrackGraphActivity.this, "Success");
                        getGraphData(START_DATE, END_DATE);
                        last_record = reading;
                        trackGoogle();
                        mGoogleHelper.SendEventGA(TrackGraphActivity.this, GAConstants.GRAPH_SCREEN, GAConstants.I_AM_PREGNANT, "");
                    } else {
                        if (response.body().getMessage()!=null) {
                            if (response.body().getMessage().equalsIgnoreCase("Unauthorized")) {
                                AppUtils.sessionExpired(TrackGraphActivity.this);
                            } else
                                AppUtils.showToast(getApplicationContext(), response.body().getMessage());
                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseModel> call, Throwable t) {
                    AppUtils.dismissDialog();
                }
            });
        }
        else {
            AppUtils.showToast(this,"It looks like your internet connectioin off. Please turn it on and try again.");
        }
    }

    private void trackGoogle() {
        if (health_title.equalsIgnoreCase("weight")) mGoogleHelper.SendEventGA(TrackGraphActivity.this, GAConstants.GRAPH_SCREEN, GAConstants.ADD_WEIGHT, "");
        else if (health_title.equalsIgnoreCase("blood pressure")) mGoogleHelper.SendEventGA(TrackGraphActivity.this, GAConstants.GRAPH_SCREEN, GAConstants.ADD_BP, "");
        else if (health_title.equalsIgnoreCase("sugar")) mGoogleHelper.SendEventGA(TrackGraphActivity.this, GAConstants.GRAPH_SCREEN, GAConstants.ADD_SUGAR, "");
        else if (health_title.equalsIgnoreCase("temperature")) mGoogleHelper.SendEventGA(TrackGraphActivity.this, GAConstants.GRAPH_SCREEN, GAConstants.ADD_TEMP, "");
        else if (health_title.equalsIgnoreCase("hemoglobin")) mGoogleHelper.SendEventGA(TrackGraphActivity.this, GAConstants.GRAPH_SCREEN, GAConstants.ADD_HEMOGLOBIN, "");
        else if (health_title.equalsIgnoreCase("heart rate")) mGoogleHelper.SendEventGA(TrackGraphActivity.this, GAConstants.GRAPH_SCREEN, GAConstants.ADD_HEARTRATE, "");
    }


    /*
     * post updated record
     *
     * @param START_DATE
     * @param END_DATE
     */
    private void getGraphData(String START_DATE, String END_DATE) {

        if (AppUtils.isNetworkConnected(this)) {
            AppUtils.showDialog(this);

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            GraphReqModel graph_model = new GraphReqModel();
            graph_model.setUserid(SharedPrefsUtils.getUserID(this));
            graph_model.setFrom_date(START_DATE);
            graph_model.setTo_date(END_DATE);
            graph_model.setType(TYPE);
            if (TYPE.equalsIgnoreCase(AppConstants.STR_CHILD)) {
                graph_model.setChildid(CHILD_ID);
            }
            graph_model.setHealthid(health_id);

            Call<GraphResponseModel> call = apiService.getGraphData(SharedPrefsUtils.getToken(getApplicationContext()),graph_model);
            call.enqueue(new retrofit2.Callback<GraphResponseModel>() {
                @Override
                public void onResponse(Call<GraphResponseModel> call, retrofit2.Response<GraphResponseModel> response) {
                    AppUtils.dismissDialog();

                    if (response.code() == 200 && response.body().isSuccess()) {
                        ARR_GRAPH_DATA = response.body().getArrGraphRecords();

                        Logger.info(TAG, "--ARR_GRAPH_DATA-->" + ARR_GRAPH_DATA.size());

                        if (ARR_GRAPH_DATA != null && ARR_GRAPH_DATA.size() > 0) {
                            drawGraph(ARR_GRAPH_DATA);

                            chart_container.setVisibility(View.VISIBLE);
                            tvNoData.setVisibility(View.GONE);
                        } else {
                            chart_container.setVisibility(View.GONE);
                            tvNoData.setVisibility(View.VISIBLE);
                        }
                        //drawGraph(ARR_GRAPH_DATA);
                    }

                    else {
                        if (response.body().getMessage()!=null) {
                            if (response.body().getMessage().equalsIgnoreCase("Unauthorized")) {
                                AppUtils.sessionExpired(TrackGraphActivity.this);
                            } else
                                AppUtils.showToast(getApplicationContext(), response.body().getMessage());
                        }
                    }
                }

                @Override
                public void onFailure(Call<GraphResponseModel> call, Throwable t) {
                    AppUtils.dismissDialog();
                }
            });
        }
        else {
            AppUtils.showToast(this,"It looks like your internet connectioin off. Please turn it on and try again.");
        }
    }

    /*
     * @param ARR_GRAPH_DATA
     */
    private void drawGraph(ArrayList<GraphObjtModel> ARR_GRAPH_DATA) {

        String[] mWeek = new String[]{"","Sun","Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};
        int[] x_values;
        int[] y_values;
        int[] y2_values = new int[0];
        ArrayList<int[]> values = new ArrayList<>();
        if (typedt==1){
            x_values = CalculateDates.createMissingCustomDates(START_DATE,END_DATE);
            values = CalculateDates.createMissingValues(ARR_GRAPH_DATA);
            if (values.size()>1){
                y_values = values.get(0);
                y2_values = values.get(1);
            }
            else y_values = values.get(0);
        }
        else if (typedt == 2){
            x_values = CalculateDates.createMissingCustomDates(START_DATE,END_DATE);
            values = CalculateDates.createMissingValues(ARR_GRAPH_DATA);
            if (values.size()>1){
                y_values = values.get(0);
                y2_values = values.get(1);
            }
            else y_values = values.get(0);
            changeTextLabelsColor(tvCustom);
        }
        else {
            x_values = CalculateDates.createMissingDates(START_DATE,END_DATE);
            values = CalculateDates.createMissingValues(ARR_GRAPH_DATA);
            if (values.size()>1){
                y_values = values.get(0);
                y2_values = values.get(1);
            }
            else y_values = values.get(0);
        }

        // Creating an  XYSeries for Expense
        XYSeries expenseSeries = new XYSeries("");
        XYSeries expenseSeries1 = new XYSeries("");
        XYSeries expenseSeries2 = new XYSeries("");

        // Adding data to Expense Series
        for (int i = 0; i < x_values.length; i++) {
            expenseSeries.add(x_values[i], y_values[i]);
            if (y_values[i]!=0){
                expenseSeries2.add(x_values[i],y_values[i]);
            }
            if (y2_values.length>1){
                if (y2_values[i]!=0){
                    expenseSeries1.add(x_values[i], (y2_values[i]+1));
                }
            }


        }

        // Creating a dataset to hold each series
        XYMultipleSeriesDataset xyMultipleSeriesDataset = new XYMultipleSeriesDataset();
        // Adding Expense Series to the dataset
        xyMultipleSeriesDataset.addSeries(0,expenseSeries);
        xyMultipleSeriesDataset.addSeries(1,expenseSeries2);
        if (y2_values.length>1)
            xyMultipleSeriesDataset.addSeries(2,expenseSeries1);

        // Creating XYSeriesRenderer to customize expenseSeries
        XYSeriesRenderer renderer = new XYSeriesRenderer();
        renderer.setColor(Color.parseColor("#00ffffff"));
        renderer.setPointStyle(PointStyle.CIRCLE);
        renderer.setFillPoints(true);
        renderer.setLineWidth(3);
        renderer.setDisplayChartValues(false);
        //renderer.setChartValuesTextSize(20);
        //renderer.setFillBelowLine(false);
        //renderer.setFillBelowLineColor(Color.parseColor("#bbf4ed21"));Ex: fill graph area
        //renderer.setChartValuesSpacing(100f);

        XYSeriesRenderer renderer1 = new XYSeriesRenderer();
        renderer1.setColor(Color.parseColor("#c27a9f"));
        renderer1.setPointStyle(PointStyle.CIRCLE);
        renderer1.setFillPoints(true);
        renderer1.setLineWidth(3);
        renderer1.setDisplayChartValues(false);

        XYSeriesRenderer renderer2 = new XYSeriesRenderer();
        renderer2.setColor(Color.parseColor("#1e90ff"));
        renderer2.setPointStyle(PointStyle.CIRCLE);
        renderer2.setFillPoints(true);
        renderer2.setLineWidth(3);
        renderer2.setDisplayChartValues(false);

        // Creating a XYMultipleSeriesRenderer to customize the whole chart
        XYMultipleSeriesRenderer multiRenderer = new XYMultipleSeriesRenderer();
        multiRenderer.setXLabels(0);
        multiRenderer.setChartTitle("");
        multiRenderer.setXTitle("");
        multiRenderer.setYTitle(health_title+" ("+health_unit+")");
        multiRenderer.setShowLegend(false);


        multiRenderer.setApplyBackgroundColor(true);
        multiRenderer.setBackgroundColor(Color.parseColor("#FFFFFF"));
        multiRenderer.setMarginsColor(Color.parseColor("#FFFFFF"));

        multiRenderer.setPointSize(7);

        multiRenderer.setLabelsTextSize(20);
        multiRenderer.setAxisTitleTextSize(25);

        multiRenderer.setMarginsColor(Color.argb(0, 255, 255, 255));

        multiRenderer.setExternalZoomEnabled(false);
        multiRenderer.setZoomEnabled(false, false);
        multiRenderer.setZoomButtonsVisible(false);
        multiRenderer.setPanEnabled(false, false);
        multiRenderer.setZoomRate(0);

        multiRenderer.setAxesColor(Color.BLACK);
        multiRenderer.setLabelsColor(Color.BLACK);
        multiRenderer.setXLabelsColor(Color.BLACK);
        multiRenderer.setYLabelsColor(0, Color.BLACK);

        multiRenderer.setAxesColor(Color.parseColor("#4d4d4d"));//Ex: Xaxis and Yaxis color
        multiRenderer.setLabelsColor(Color.parseColor("#4d4d4d"));//Ex: weigth-x and KG-y

        multiRenderer.setShowGrid(true);
        multiRenderer.setGridColor(Color.parseColor("#cacaca"));//Ex: Grid color code
        multiRenderer.setShowCustomTextGrid(true);
        multiRenderer.setInScroll(false);
        multiRenderer.setYLabels(12);
        multiRenderer.setYAxisMin(0);
        bottom_graph.setVisibility(View.GONE);
        if (health_title.equalsIgnoreCase("weight")){
            if (healthrecord.getMax()!=null)
                multiRenderer.setYAxisMax(Double.parseDouble(healthrecord.getMax()));
            else multiRenderer.setYAxisMax(110);
        }else if (health_title.equalsIgnoreCase("Blood Pressure")){
            bottom_graph.setVisibility(View.VISIBLE);
            diastolicdot.setText("Diastolic");
            sysdotdot.setText("Systolic");
            if (healthrecord2.getMax()!=null)
                multiRenderer.setYAxisMax(Double.parseDouble(healthrecord.getMax()));
            else multiRenderer.setYAxisMax(200);
        }else if (health_title.equalsIgnoreCase("Sugar")){
            bottom_graph.setVisibility(View.VISIBLE);
            diastolicdot.setText("Fasting");
            sysdotdot.setText("Post Prandial");
            if (healthrecord2.getMax()!=null)
                multiRenderer.setYAxisMax(Double.parseDouble(healthrecord2.getMax()));
            else multiRenderer.setYAxisMax(400);
        }else if (health_title.equalsIgnoreCase("hemoglobin")){
            if (healthrecord.getMax()!=null)
                multiRenderer.setYAxisMax(Double.parseDouble(healthrecord.getMax()));
            else multiRenderer.setYAxisMax(17);
        }else if (health_title.equalsIgnoreCase("Heart Rate")){
            if (healthrecord.getMax()!=null)
                multiRenderer.setYAxisMax(Double.parseDouble(healthrecord.getMax()));
            else multiRenderer.setYAxisMax(100);
        }else if (health_title.equalsIgnoreCase("Temperature")){
            if (healthrecord.getMax()!=null)
                multiRenderer.setYAxisMax(Double.parseDouble(healthrecord.getMax()));
            else multiRenderer.setYAxisMax(110);
        }


//        multiRenderer.setYAxisMin(10, 0);
//        0multiRenderer.setYAxisMax(90, 0);

        multiRenderer.setYLabelsAlign(Paint.Align.LEFT, 0);

        //multiRenderer.setMargins(new int[]{80, 80, 80, 80});
        String[] stdt = START_DATE.split("-");
        int mnt = Integer.parseInt(stdt[1]);
        int rem = x_values.length/3;
        int cons = 0;
        for (int i = 0; i < x_values.length; i++) {
            if (typedt==0){
                DateFormat df = new SimpleDateFormat("yyyy-MM-dd",Locale.getDefault());
                Date date = null;
                try {
                    date = df.parse(CalculateDates.week_dates[i]);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                Calendar c= Calendar.getInstance();
                c.setTime(date);
                Logger.info(TAG, "day of week -->" + c.get(Calendar.DAY_OF_WEEK));
                multiRenderer.addXTextLabel(x_values[i], "" + mWeek[c.get(Calendar.DAY_OF_WEEK)]);
            }
            else if (typedt ==2){

                if(i==0) {
                    multiRenderer.addXTextLabel(x_values[i], "" + CalculateDates.month_dates[i]);
                }
                if (i==1)
                    cons = 1;
                if(cons>0&&cons<3 && rem>0 &&i%rem==0){
                    cons++;
                    multiRenderer.addXTextLabel(x_values[i], "" + CalculateDates.month_dates[i]);
                }
                if (i==x_values.length-1){
                    multiRenderer.addXTextLabel(x_values[i], "" + CalculateDates.month_dates[i]);
                }

//                if (x_values.length<10)
//                    multiRenderer.addXTextLabel(x_values[i], "" + (x_values[i]+1));
//                else if (x_values.length>=10 && x_values.length<150){
//                    if ((i+1)%10==0)
//                        multiRenderer.addXTextLabel(x_values[i], "" + (x_values[i]+1));
//                }
//                else {
//                    if ((i+1)%20==0)
//                        multiRenderer.addXTextLabel(x_values[i], "" + (x_values[i]+1));
//                }
            }
            else  {
                if (i==0)
                    multiRenderer.addXTextLabel(x_values[i], "" + CalculateDates.month_dates[i]);
                if (i==10)
                    multiRenderer.addXTextLabel(x_values[i], "" + CalculateDates.month_dates[i]);
                if (i==20)
                    multiRenderer.addXTextLabel(x_values[i], "" + CalculateDates.month_dates[i]);
                if (i==x_values.length-1)
                    multiRenderer.addXTextLabel(x_values[i], "" + CalculateDates.month_dates[i]);
            }
        }

        // Adding expenseRenderer to multipleRenderer
        multiRenderer.addSeriesRenderer(renderer);
        multiRenderer.addSeriesRenderer(renderer1);
        if (y2_values.length>1)
            multiRenderer.addSeriesRenderer(renderer2);

        // Getting a reference to LinearLayout of the LineChartActivity Layout
        LinearLayout chartContainer = findViewById(R.id.chart_container);
        chartContainer.removeAllViews();
        // Creating a Line Chart
        View chart = ChartFactory.getLineChartView(getBaseContext(), xyMultipleSeriesDataset, multiRenderer);

        // Adding the Line Chart to the LinearLayout
        chartContainer.addView(chart);
    }


    public String convertDateFormat(String inputText1) {
        String outputText = "";
        String inputText = inputText1;
        // "Z" appears not to be supported for some reason.
        DateFormat inputFormat = new SimpleDateFormat("dd MMM yyyy",Locale.getDefault());
        inputFormat.setTimeZone(TimeZone.getTimeZone("IST"));
        DateFormat outputFormat = new SimpleDateFormat("dd-MM-yyyy",Locale.getDefault());
        Date parsed = null;
        try {
            parsed = inputFormat.parse(inputText);
            outputText = outputFormat.format(parsed);
        } catch (android.net.ParseException e) {
            e.printStackTrace();
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return outputText;
        }
    }
}