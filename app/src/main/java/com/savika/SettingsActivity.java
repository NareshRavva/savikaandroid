package com.savika;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Switch;

import com.savika.components.textview.TextView;
import com.savika.utils.SharedPrefsUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SettingsActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.tvHeaderTitle)
    TextView tvHeaderTitle;

    @BindView(R.id.llBack)
    LinearLayout llBack;

    @BindView(R.id.notif)
    Switch notif;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        ButterKnife.bind(this);
        tvHeaderTitle.setText("Settings");

        boolean isnotifOn = SharedPrefsUtils.getNotificationStatus(SettingsActivity.this);

        if (isnotifOn)
            notif.setChecked(true);
        else notif.setChecked(false);

        llBack.setOnClickListener(this);
        notif.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id){
            case R.id.llBack:
                finish();
                break;
            case R.id.notif:
                SharedPrefsUtils.setNotificationStatus(SettingsActivity.this,notif.isChecked());
                break;
        }
    }
}
