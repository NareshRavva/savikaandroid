package com.savika;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.webkit.WebView;
import butterknife.BindView;
import butterknife.ButterKnife;

/*
 * Created by Naresh Ravva on 05/09/17.
 */

public class AboutUs extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.tvHeaderTitle)
    TextView tvHeaderTitle;

    @BindView(R.id.llBack)
    LinearLayout llBack;

    @BindView(R.id.terms_conditions)
    TextView terms_conditions;

    @BindView(R.id.privacy_policy)
    TextView privacy_policy;

    @BindView(R.id.tems_privacy)
    WebView webview;

    @BindView(R.id.main_aboutus)
    LinearLayout main_aboutus;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);

        ButterKnife.bind(this);

        tvHeaderTitle.setText(getString(R.string.menu_about_us));
        terms_conditions.setOnClickListener(this);
        privacy_policy.setOnClickListener(this);
        webview.setWebViewClient(new WebViewClient());

                /*
                    WebSettings
                        Manages settings state for a WebView. When a
                        WebView is first created, it obtains a set
                        of default settings.

                    setJavaScriptEnabled(boolean flag)
                        Tells the WebView to enable JavaScript execution.
                 */
        webview.getSettings().setJavaScriptEnabled(true);
        llBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (main_aboutus.getVisibility() == View.VISIBLE )
                    finish();
                else {
                    webview.clearView();
                    tvHeaderTitle.setText(getString(R.string.menu_about_us));
                    main_aboutus.setVisibility(View.VISIBLE);
                    webview.setVisibility(View.GONE);
                }
            }
        });
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id){
            case R.id.terms_conditions:
                tvHeaderTitle.setText(R.string.terms_conditions);
                main_aboutus.setVisibility(View.GONE);
                webview.loadUrl("file:///android_asset/temscondition.html");
                webview.setVisibility(View.VISIBLE);
                break;
            case R.id.privacy_policy:
                tvHeaderTitle.setText(R.string.privacy_policy);
                main_aboutus.setVisibility(View.GONE);
                webview.loadUrl("file:///android_asset/privacypolicy.html");
                webview.setVisibility(View.VISIBLE);
                break;
        }
    }
}
