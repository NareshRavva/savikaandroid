package com.savika.onBoardScreens;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.savika.R;
import com.savika.homeScreen.HomeActivity;
import com.savika.utils.SharedPrefsUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

/*
 * Created by Naresh Ravva on 01/10/17.
 */

public class WelcomeDetailsScreen extends AppCompatActivity {

    @BindView(R.id.tvUserName)
    TextView tvUserName;

    @BindView(R.id.llProceed)
    LinearLayout llProceed;

    private String TAG = "WelcomeDetailsScreen";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome_details);

        ButterKnife.bind(this);

        tvUserName.setText("Hi, " + SharedPrefsUtils.getUsername(this));

        llProceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent articals = new Intent(WelcomeDetailsScreen.this, HomeActivity.class);
                articals.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                articals.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                articals.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(articals);
            }
        });
    }
}
