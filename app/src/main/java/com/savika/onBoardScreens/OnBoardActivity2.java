package com.savika.onBoardScreens;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.RadioButton;
import android.widget.TextView;

import com.savika.R;
import com.savika.amAMother.AddChildDataActivity;
import com.savika.amAPregnant.AmAPregnant;
import com.savika.constants.AppConstants;
import com.savika.constants.GAConstants;
import com.savika.googleAnalytics.GoogleAnalyticsHelper;
import com.savika.utils.SharedPrefsUtils;


import butterknife.BindView;
import butterknife.ButterKnife;

/*
 * Created by Naresh Ravva on 09/08/17.
 */

public class OnBoardActivity2 extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.rBtnPrgnt)
    RadioButton rBtnPrgnt;

    @BindView(R.id.rBtnMthr)
    RadioButton rBtnMthr;

    @BindView(R.id.tvNxt)
    TextView tvNxt;

    GoogleAnalyticsHelper mGoogleHelper;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_onboard_screen_2);

        ButterKnife.bind(this);

        tvNxt.setOnClickListener(this);

        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Quicksand-Medium.ttf");
        rBtnPrgnt.setTypeface(font);
        rBtnMthr.setTypeface(font);

        rBtnPrgnt.setOnClickListener(this);
        rBtnMthr.setOnClickListener(this);

        InitGoogleAnalytics();

        mGoogleHelper.SendScreenNameGA(this, GAConstants.SIGN_UP_SCREEN);
    }

    // Google Analytics
    private void InitGoogleAnalytics() {
        mGoogleHelper = new GoogleAnalyticsHelper();
        mGoogleHelper.init(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rBtnPrgnt:
                rBtnPrgnt.setTextColor(Color.WHITE);
                rBtnMthr.setTextColor(Color.BLACK);
                SharedPrefsUtils.setMotherORPrgntType(this, AppConstants.PREGNANT);
                mGoogleHelper.SendEventGA(this, GAConstants.SIGN_UP_SCREEN, GAConstants.I_AM_PREGNANT, "");

                Intent pregnt_intent = new Intent(getApplicationContext(), AmAPregnant.class);
                startActivity(pregnt_intent);
                break;

            case R.id.rBtnMthr:
                rBtnPrgnt.setTextColor(Color.BLACK);
                rBtnMthr.setTextColor(Color.WHITE);
                SharedPrefsUtils.setMotherORPrgntType(this, AppConstants.MOTHER);
                mGoogleHelper.SendEventGA(this, GAConstants.SIGN_UP_SCREEN, GAConstants.I_AM_A_MOTHER, "");

                Intent intent = new Intent(getApplicationContext(), AddChildDataActivity.class);
                startActivity(intent);
                break;

        }
    }

}
