package com.savika.components.textview;

import android.content.Context;
import android.util.AttributeSet;


/**
 * TextView with custom font by XML or Code
 * This class provided too a font factory
 *
 * @author odemolliens
 */
public class TextView extends com.savika.components.customtv.textview.TextView {
    public TextView(Context context) {
        super(context);
    }

    public TextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public TextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }
}