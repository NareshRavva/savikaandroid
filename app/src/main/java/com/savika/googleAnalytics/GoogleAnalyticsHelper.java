package com.savika.googleAnalytics;

import android.content.Context;
import android.util.Log;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.savika.constants.AppConstants;

public class GoogleAnalyticsHelper {

    private Tracker mGaTracker = null;
    private static String TAG = "GoogleAnalyticsHelper";
    private static final String PRODUCTION_ID = "UA-107526116-1";
    // private static final String PROPERTY_ID = "UA-69464994-4";

    public void init(Context ctx) {
        try {

            if (mGaTracker == null && ctx != null) {
                mGaTracker = GoogleAnalytics.getInstance(ctx).newTracker(PRODUCTION_ID);
            }
        } catch (Exception e) {
            Log.d(GoogleAnalyticsHelper.TAG, "init, e=" + e);
        }
    }

    /**
     * @param context
     * @param screenName
     */
    public void SendScreenNameGA(Context context, String screenName) {
        init(context);

        // TRACK GA ONLY FOR PRODUCTION ENVIROMMENT
        //if (AppConstants.PLAY_STORE_BUILD) {
            mGaTracker.setScreenName(screenName);
            mGaTracker.send(new HitBuilders.AppViewBuilder().build());
        //}

    }

    public void SendEventGA(Context iCtx, String iCategoryId, String iActionId, String iLabelId) {
        init(iCtx);

        // TRACK GA ONLY FOR PRODUCTION ENVIROMMENT
        // Build and send an Event.
        //if (AppConstants.PLAY_STORE_BUILD) {
            mGaTracker.send(new HitBuilders.EventBuilder().setCategory(iCategoryId).setAction(iActionId)
                    .setLabel(iLabelId).build());
        //}

    }
}
