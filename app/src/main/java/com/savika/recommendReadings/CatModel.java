package com.savika.recommendReadings;


import java.io.Serializable;

/*
 * Created by Naresh
 */
public class CatModel implements Serializable {

    private String catid, catdesc;

    public String getCatid() {
        return catid;
    }

    public void setCatid(String catid) {
        this.catid = catid;
    }

    public String getCatdesc() {
        return catdesc;
    }

    public void setCatdesc(String catdesc) {
        this.catdesc = catdesc;
    }
}


