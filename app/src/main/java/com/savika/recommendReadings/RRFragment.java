package com.savika.recommendReadings;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.ShareCompat;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.savika.AboutUs;
import com.savika.ContactUs;
import com.savika.R;
import com.savika.SavikaApplication;
import com.savika.Splash;
import com.savika.WebView;
import com.savika.constants.AppConstants;
import com.savika.homeScreen.HomeActivity;
import com.savika.logger.Logger;
import com.savika.utils.ImageUtil;
import com.savika.utils.SharedPrefsUtils;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
//7981802159
/*
 * @author naresh
 */
public class RRFragment extends Fragment implements View.OnClickListener, UpdateableFragment {

    private ArrayList<ArticalsModel> data = new ArrayList<>();

    private PagerAdapter mAdapter;

    ArticalsModel model;

    private String TAG = "RRFragment";

    private int pos;

//    public RRFragment(ArrayList<ArticalsModel> modle, int position) {
//        this.data = modle;
//        this.pos = position;
//    }

    public static Fragment newInstance( int position)
    {
        RRFragment myFragment = new RRFragment();
        Bundle args = new Bundle();
        args.putSerializable("position",position);
        myFragment.setArguments(args);
        return myFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.data = SavikaApplication.getData();
        this.pos = getArguments().getInt("position");
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_articals, container, false);
        setHasOptionsMenu(true);

        TextView tvTitle = rootView.findViewById(R.id.tvTitle);
        TextView tvContent = rootView.findViewById(R.id.tvContent);
        TextView tvDate = rootView.findViewById(R.id.tvDate);
        TextView tvReadmore = rootView.findViewById(R.id.tvReadmore);
        ImageView openWeb = rootView.findViewById(R.id.open_web);
        RelativeLayout rlMore = rootView.findViewById(R.id.rlMore);
        LinearLayout llBack = rootView.findViewById(R.id.llBack);

        LinearLayout llArticalData = rootView.findViewById(R.id.llArticalData);
        RelativeLayout rlArticalReachEnd = rootView.findViewById(R.id.rlArticalReachEnd);
        TextView shareapp = rootView.findViewById(R.id.tvShareApp);

        ImageView imgHeader = rootView.findViewById(R.id.img);
        RelativeLayout rlShare = rootView.findViewById(R.id.rlShare);

        model = data.get(pos);

        if (!model.getCatid().equalsIgnoreCase(AppConstants.ARTICALS_REACH_END)) {
            try{
                llArticalData.setVisibility(View.VISIBLE);
                rlArticalReachEnd.setVisibility(View.GONE);

                tvTitle.setText(model.getTitle());
                tvContent.setText(model.getContent());

                Typeface custom_font = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Quicksand-Regular.ttf");
                tvContent.setTypeface(custom_font);

                tvDate.setText(model.getCreatedat());
                //ImageUtil.LoadPicaso(getActivity(), imgHeader, model.getImageurl());

                Picasso.with(getActivity()).load(model.getImageurl().replace(" ", "20%")).placeholder(R.drawable.articals_default_img)
                        .error(R.drawable.articals_default_img).fit().into(imgHeader, new Callback() {

                    @Override
                    public void onSuccess() {
                        // Log.v(TAG, "onSuccess came");
                    }

                    @Override
                    public void onError() {
                        Logger.info(TAG, "picaso image load failed.");
                    }
                });

                tvReadmore.setText(model.getWebsite());

                tvDate.setOnClickListener(this);
                tvReadmore.setOnClickListener(this);
                openWeb.setOnClickListener(this);
                tvTitle.setOnClickListener(this);
                rlShare.setOnClickListener(this);
                rlMore.setOnClickListener(this);
                llBack.setOnClickListener(this);
            }
            catch (Exception e){
                e.printStackTrace();
            }


        } else {
            llBack.setOnClickListener(this);
            llArticalData.setVisibility(View.GONE);
            rlArticalReachEnd.setVisibility(View.VISIBLE);
            shareapp.setOnClickListener(this);

        }


        return rootView;
    }


    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.tvDate:
            case R.id.tvReadmore:
            case R.id.tvTitle:
            case R.id.open_web:
                openWebView(model);
                break;

            case R.id.rlShare:
                shareContent();
                break;

            case R.id.llBack:
                if (getActivity().getIntent().getBooleanExtra("from_notifications",false)){
                    Intent home = new Intent(getActivity(), HomeActivity.class);
                    startActivity(home);
                    getActivity().finish();

                }
                else getActivity().finish();
                break;

            case R.id.rlMore:
                int[] location = new int[2];
//                currentRowId = position;
//                currentRow = v;
//                // Get the x, y location and store it in the location[] array
//                // location[0] = x, location[1] = y.
//                v.getLocationOnScreen(location);

                //Initialize the Point with x, and y positions
                Point point = new Point();
                point.x = location[0];
                point.y = location[1];
                showStatusPopup(getActivity(), point);
                break;
            case R.id.tvShareApp:
                shareApp();
                break;
        }

    }

    private void shareContent() {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);

        StringBuilder sb = new StringBuilder();
        sb.append(model.getTitle());
        sb.append("\n");
        sb.append("\n");
        sb.append("Read more: " + model.getWebsiteurl());

        sendIntent.putExtra(Intent.EXTRA_TEXT, sb.toString());
        sendIntent.setType("text/plain");
        startActivity(sendIntent);
    }

    private void openWebView(ArticalsModel model) {
        if (model.getWebsiteurl() != null && model.getWebsiteurl().length() > 1) {
            Intent webView = new Intent(getActivity(), WebView.class);
            webView.putExtra(AppConstants.WEB_URL, model.getWebsiteurl());
            webView.putExtra(AppConstants.WEB_VIEW_TITLE, model.getTitle());
            getActivity().startActivity(webView);
        }
    }

    @Override
    public void update(ArrayList<ArticalsModel> data) {

        this.data = data;

        mAdapter = new PagerAdapter(getChildFragmentManager(), data);
    }

    // The method that displays the popup.
    private void showStatusPopup(final Activity context, Point p) {

        // Inflate the popup_layout.xml
        RelativeLayout viewGroup = context.findViewById(R.id.llStatusChangePopup);
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = layoutInflater.inflate(R.layout.popup, null);

        TextView tvAboutUs = layout.findViewById(R.id.tvAboutUs);
        TextView tvContactUs = layout.findViewById(R.id.tvContactUs);
        TextView tvShare = layout.findViewById(R.id.tvShare);
        TextView tvRateUs = layout.findViewById(R.id.tvRateApp);
        TextView tvLogout = layout.findViewById(R.id.tvLogout);


        // Creating the PopupWindow
        final PopupWindow changeStatusPopUp = new PopupWindow(context);
        changeStatusPopUp.setContentView(layout);
        changeStatusPopUp.setWidth(LinearLayout.LayoutParams.WRAP_CONTENT);
        changeStatusPopUp.setHeight(LinearLayout.LayoutParams.WRAP_CONTENT);
        changeStatusPopUp.setFocusable(true);

        tvAboutUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                aboutUs();
                changeStatusPopUp.dismiss();
            }
        });

        tvContactUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                contactUs();
                changeStatusPopUp.dismiss();
            }
        });


        tvShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                shareApp();
                changeStatusPopUp.dismiss();
            }
        });

        tvRateUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rateOurApp();
                changeStatusPopUp.dismiss();
            }
        });

        tvLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logout();
                changeStatusPopUp.dismiss();
            }
        });

        //Clear the default translucent background
        changeStatusPopUp.setBackgroundDrawable(new BitmapDrawable());

        int[] location = new int[2];
        // Displaying the popup at the specified location, + offsets.
        changeStatusPopUp.showAtLocation(layout, Gravity.TOP, 0, 0);
    }

    private void logout() {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
        builder1.setMessage(getResources().getString(R.string.are_you_sure_you_want_to_logout_));
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();

                        SharedPrefsUtils.setUserID(getActivity(), null);

                        Intent articals = new Intent(getActivity(), Splash.class);
                        articals.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        articals.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        articals.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(articals);
                        getActivity().finish();
                    }
                });

        builder1.setNegativeButton(
                "No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    public static AlertDialog.Builder getAlertDialog(Context ctx) {

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
            return new AlertDialog.Builder(ctx, R.style.AppCompatAlertDialogStyle);
        } else {
            return new AlertDialog.Builder(ctx);
        }
    }

    private void aboutUs() {
        Intent aboutUs = new Intent(getActivity(), AboutUs.class);
        getActivity().startActivity(aboutUs);
    }

    private void contactUs() {
        Intent aboutUs = new Intent(getActivity(), ContactUs.class);
        getActivity().startActivity(aboutUs);
    }


    private void rateOurApp() {
        String APP_PNAME = "com.savika";
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + APP_PNAME)));
    }

    private void shareApp() {
        ShareCompat.IntentBuilder.from(getActivity())
                .setType("text/plain")
                .setChooserTitle("Savika")
                .setText("Savika is a comprehensive pregnancy and pediatric healthcare app\n Please click on this link to install Savika.\nhttp://play.google.com/store/apps/details?id=" + getActivity().getPackageName())
                .startChooser();
    }
}
