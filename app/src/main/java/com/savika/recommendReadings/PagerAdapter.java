package com.savika.recommendReadings;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.view.ViewGroup;

import com.savika.SavikaApplication;
import com.savika.logger.Logger;

import java.util.ArrayList;

/*
 * Created by Naresh Ravva
 */
public class PagerAdapter extends FragmentStatePagerAdapter {

    private ArrayList<ArticalsModel> data = new ArrayList<>();

    private String TAG = "PagerAdapter";


    public PagerAdapter(FragmentManager fragmentManager, ArrayList<ArticalsModel> modle) {
        super(fragmentManager);
        this.data = modle;
        SavikaApplication.setData(modle);
        //notifyDataSetChanged();
    }

//    @Override
//    public CharSequence getPageTitle(int position) {
//        return data.get(position).getTitle();
//    }

    @Override
    public Fragment getItem(int position) {
        //Logger.info(TAG, "getItem calling-->");
        return RRFragment.newInstance(position);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    public void update(ArrayList<ArticalsModel> data) {
        this.data = data;
        SavikaApplication.setData(data);
    }

    @Override
    public int getItemPosition(Object object) {
        if (object instanceof UpdateableFragment) {
            ((UpdateableFragment) object).update(data);
        }
        //don't return POSITION_NONE, avoid fragment recreation.
//        return super.getItemPosition(object);
        return POSITION_NONE;
    }

//    @Override
//    public void destroyItem(ViewGroup container, int position, Object object) {
//        FragmentManager manager = ((Fragment) object).getFragmentManager();
//        FragmentTransaction trans = manager.beginTransaction();
//        trans.remove((Fragment) object);
//        trans.commit();
//
//        super.destroyItem(container, position, object);
//    }
}
