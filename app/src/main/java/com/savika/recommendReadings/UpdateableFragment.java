package com.savika.recommendReadings;

import java.util.ArrayList;

/*
 * Created by Naresh Ravva on 09/08/17.
 */

public interface UpdateableFragment {
    public void update(ArrayList<ArticalsModel> modle);
}
