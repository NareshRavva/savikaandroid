package com.savika.recommendReadings;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/*
 * Created by Naresh
 */
public class ArticalsResModel implements Serializable {

    public ArticalsResModel() {
    }

    private String status;

    @SerializedName("catid")
    private ArrayList<ArticalsModel> listArticals = new ArrayList<ArticalsModel>();


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ArrayList<ArticalsModel> getListArticals() {
        return listArticals;
    }

    public void setListArticals(ArrayList<ArticalsModel> listArticals) {
        this.listArticals = listArticals;
    }
}


