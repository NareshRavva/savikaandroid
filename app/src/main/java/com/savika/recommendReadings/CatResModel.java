package com.savika.recommendReadings;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/*
 * Created by Naresh
 */
public class CatResModel implements Serializable {

    public CatResModel() {
    }

    private String status;

    @SerializedName("categories")
    private ArrayList<CatModel> listArticals = new ArrayList<CatModel>();

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ArrayList<CatModel> getListArticals() {
        return listArticals;
    }

    public void setListArticals(ArrayList<CatModel> listArticals) {
        this.listArticals = listArticals;
    }
}


