package com.savika.recommendReadings;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.savika.Api.ApiClient;
import com.savika.Api.ApiInterface;
import com.savika.Api.ServiceUrl;
import com.savika.R;
import com.savika.constants.GAConstants;
import com.savika.googleAnalytics.GoogleAnalyticsHelper;
import com.savika.homeScreen.HomeActivity;
import com.savika.homeScreen.notification.NotificationActivity;
import com.savika.logger.Logger;
import com.savika.constants.AppConstants;
import com.savika.utils.AppUtils;
import com.savika.utils.SharedPrefsUtils;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;

/*
 * Created by Naresh Ravva on 05/07/17.
 * https://stackoverflow.com/questions/19099296/set-text-view-ellipsize-and-add-view-more-at-end
 */

public class RecommendReadingsActivity extends AppCompatActivity implements CategoriesAdapter.ItemClickListener, View.OnClickListener {

    @BindView(R.id.rvCategories)
    RecyclerView rvCategories;

    @BindView(R.id.viewPager)
    ViewPager viewPager;

    @BindView(R.id.llScroll)
    LinearLayout llScroll;

    CategoriesAdapter adapter;

    PagerAdapter pagerAdapter;
    LinearLayoutManager layoutManager;

    GoogleAnalyticsHelper mGoogleHelper;

    private int count;

//    public static Activity activity;

    ArrayList<CatModel> LIST_CAT = new ArrayList<>();
    ArrayList<ArticalsModel> LIST_ARTICALS = new ArrayList<>();

    private String TAG = this.getClass().getSimpleName();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recommend_readings);

        ButterKnife.bind(this);

//        activity = RecommendReadingsActivity.this;

        llScroll.setOnClickListener(this);

        //retrive old cat if have
        if (SharedPrefsUtils.getCatData(this) != null) {

            Gson gson = new Gson();
            String json = SharedPrefsUtils.getCatData(this);
            Type type = new TypeToken<ArrayList<CatModel>>() {
            }.getType();
            ArrayList<CatModel> arrayList = gson.fromJson(json, type);
            parseCatData(arrayList);

            getCategoriesData(false);

        } else {
            getCategoriesData(true);
        }

        rvCategories.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                int visibleItemCount = layoutManager.getChildCount();
                int totalItemCount = layoutManager.getItemCount();
                int pastVisibleItems = layoutManager.findFirstCompletelyVisibleItemPosition();

                if (pastVisibleItems + visibleItemCount >= totalItemCount) {
                    // End of the list is here.
                    Log.i(TAG, "End of list");
                    llScroll.setVisibility(View.VISIBLE);
                } else {
                    llScroll.setVisibility(View.INVISIBLE);
                }

                int firstVisiblePosition = layoutManager.findLastVisibleItemPosition();
            }
        });


        InitGoogleAnalytics();

        mGoogleHelper.SendScreenNameGA(this, GAConstants.ARTICLE_SCREEN);
    }

    // Google Analytics
    private void InitGoogleAnalytics() {
        mGoogleHelper = new GoogleAnalyticsHelper();
        mGoogleHelper.init(this);
    }


    private void getCategoriesData(boolean isDisplayDialog) {
        if (AppUtils.isNetworkConnected(this)) {

            if (isDisplayDialog)
                AppUtils.showDialog(this);

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            String url = ServiceUrl.GET_CATEGORIES;

            Call<CatResModel> call = apiService.getCategories(ServiceUrl.AWS_API_KEY);

            call.enqueue(new retrofit2.Callback<CatResModel>() {
                @Override
                public void onResponse(Call<CatResModel> call, retrofit2.Response<CatResModel> response) {

                    AppUtils.dismissDialog();
                    AppUtils.showToast(RecommendReadingsActivity.this, "Swipe UP For Next Articles");

                    try {
                        if (response.code() == 200 && response.body().getStatus().equalsIgnoreCase(getString(R.string.status_ok))) {

                            parseCatData(response.body().getListArticals());

                        } else {
                            JSONObject jObjError = new JSONObject(response.errorBody().string());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<CatResModel> call, Throwable t) {
                    AppUtils.dismissDialog();
                }
            });
        }
        else {
            AppUtils.showToast(this,"It looks like your internet connectioin off. Please turn it on and try again.");
        }
    }

    private void parseCatData(ArrayList<CatModel> listCat) {

        LIST_CAT = listCat;
        //store cat data
        SharedPrefsUtils.setCatData(this, listCat);

        layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, true);
        rvCategories.setLayoutManager(layoutManager);

        Collections.reverse(listCat);

        adapter = new CategoriesAdapter(RecommendReadingsActivity.this, listCat);
        adapter.setClickListener(this);
        rvCategories.setAdapter(adapter);

        rvCategories.scrollToPosition(adapter.getItemCount() - 1);

        int pos = listCat.size() - 1;

        //retrive old cat if have
        if (SharedPrefsUtils.getMotherORPrgntType(this).equalsIgnoreCase(AppConstants.PREGNANT)) {
            if (SharedPrefsUtils.getPregntData(this) != null) {

                Gson gson = new Gson();
                String json = SharedPrefsUtils.getPregntData(this);
                Logger.info(TAG, "json-->" + json);
                Type type = new TypeToken<ArrayList<ArticalsModel>>() {
                }.getType();
                ArrayList<ArticalsModel> arrayList = gson.fromJson(json, type);
                parseArticalsData(arrayList, null);

                getArticals(false, listCat.get(pos).getCatid());

            } else {
                getArticals(true, listCat.get(pos).getCatid());
            }
        }
        if (SharedPrefsUtils.getMotherORPrgntType(this).equalsIgnoreCase(AppConstants.MOTHER)) {
            if (SharedPrefsUtils.getMotherData(this) != null) {

                Gson gson = new Gson();
                String json = SharedPrefsUtils.getMotherData(this);
                Logger.info(TAG, "json-->" + json);
                Type type = new TypeToken<ArrayList<ArticalsModel>>() {
                }.getType();
                ArrayList<ArticalsModel> arrayList = gson.fromJson(json, type);
                parseArticalsData(arrayList, null);

                getArticals(false, listCat.get(pos).getCatid());

            } else {
                getArticals(true, listCat.get(pos).getCatid());
            }
        }
    }

    @Override
    public void onItemClick(View view, String catID, String catName) {
        Log.i(TAG, "You clicked " + " CatID:--> " + catName);
        mGoogleHelper.SendEventGA(this, GAConstants.ARTICLE_SCREEN, GAConstants.MOST_USED_CATEGORIES, catName);

        if (getIntent().getStringExtra("from")!=null && !(getIntent().getStringExtra("from").equalsIgnoreCase("")))
            getIntent().putExtra("from","");
        // get old data
        ArrayList<ArticalsModel> temp_list = new ArrayList<>();

        if (catID.equalsIgnoreCase("0")) {
            if (LIST_ARTICALS.size() > 0) {
                parseArticalsData(LIST_ARTICALS, null);
            } else {
                getArticals(true, catID);
            }
        } else {
            for (int i = 0; i < LIST_ARTICALS.size(); i++) {
                if (LIST_ARTICALS.get(i).getCatid().equalsIgnoreCase(catID)) {
                    temp_list.add(LIST_ARTICALS.get(i));
                }
            }
            if (temp_list.size() > 0) {
                parseArticalsData(temp_list, null);
            } else {
                //other wise get from api
                getArticals(true, catID);
            }
        }
    }


    /*
     * get Articals list
     *
     * @param catid
     */
    private void getArticals(boolean isDisplayDialog, String catid) {

        if (AppUtils.isNetworkConnected(this)) {
            if (isDisplayDialog)
                AppUtils.showDialog(this);

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            if (getIntent().getStringExtra("from") != null && !(getIntent().getStringExtra("from").equalsIgnoreCase(""))) {
                ArticalsModel model = (ArticalsModel) getIntent().getSerializableExtra("article");
                catid = model.getCatid();
                adapter.setSelectedPos(checkposition(model.getCatid()));
                rvCategories.scrollToPosition(checkposition(model.getCatid()));
            }
            String url = ServiceUrl.GET_ARTICALS + SharedPrefsUtils.getMotherORPrgntType(this) + "/" + catid;

            Call<ArticalsResModel> call = apiService.getArticals(ServiceUrl.AWS_API_KEY,url);

            final String finalCatid = catid;
            call.enqueue(new retrofit2.Callback<ArticalsResModel>() {
                @Override
                public void onResponse(Call<ArticalsResModel> call, retrofit2.Response<ArticalsResModel> response) {
                    try {
                        AppUtils.dismissDialog();
                        if (response.code() == 200 && response.body().getStatus().equalsIgnoreCase(getString(R.string.status_ok))) {
                            if (getIntent().getStringExtra("from") != null && !(getIntent().getStringExtra("from").equalsIgnoreCase("")))
                                parseArticalsData(response.body().getListArticals(), "cards");
                            else parseArticalsData(response.body().getListArticals(), null);
                            if (SharedPrefsUtils.getMotherORPrgntType(RecommendReadingsActivity.this).equalsIgnoreCase("1") && finalCatid.equalsIgnoreCase("0")) {
                                SharedPrefsUtils.setPregntData(RecommendReadingsActivity.this, response.body().getListArticals());
                            } else {
                                SharedPrefsUtils.setMotherData(RecommendReadingsActivity.this, response.body().getListArticals());
                            }

                            if (finalCatid.equalsIgnoreCase("0")) {
                                LIST_ARTICALS = response.body().getListArticals();
                            }

                        } else {
                            JSONObject jObjError = new JSONObject(response.errorBody().string());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<ArticalsResModel> call, Throwable t) {
                    AppUtils.dismissDialog();
                }
            });
        }
        else {
            AppUtils.showToast(this,"It looks like your internet connectioin off. Please turn it on and try again.");
        }
    }

    private int checkposition(String catid) {
        for (int i=0; i<LIST_CAT.size();i++){
            if (catid.equalsIgnoreCase(LIST_CAT.get(i).getCatid())){
                return i;
            }
        }
        return 0;
    }

    /*
     * @param listArticals
     * @param cards
     */
    private void parseArticalsData(final ArrayList<ArticalsModel> listArticals, String cards) {

        if (listArticals != null) {
            ArticalsModel last_artical_model = new ArticalsModel();
            last_artical_model.setCatid(AppConstants.ARTICALS_REACH_END);
            if (cards!=null){
                listArticals.add(0,(ArticalsModel) getIntent().getSerializableExtra("article"));
//                if (ServiceUrl.BASE_URL.contains("3001")) {
                    ArticalsModel am = (ArticalsModel) getIntent().getSerializableExtra("article");
                    if (getIntent().getBooleanExtra("from_notifications", false))
                        NotificationActivity.postNotificationsData(RecommendReadingsActivity.this, am.getId(), SharedPrefsUtils.getUserID(RecommendReadingsActivity.this), "article");
//                }
               // getIntent().putExtra("from","");
            }

            listArticals.add(last_artical_model);

            pagerAdapter = new PagerAdapter(getSupportFragmentManager(), listArticals);
            viewPager.setAdapter(pagerAdapter);
            pagerAdapter.update(listArticals);
            viewPager.getAdapter().notifyDataSetChanged();
        }

    }

    @Override
    public void onBackPressed() {
        if (count == 1) {
            super.onBackPressed();
            if (getIntent().getBooleanExtra("from_notifications",false)){
                Intent home = new Intent(RecommendReadingsActivity.this, HomeActivity.class);
                startActivity(home);
                finish();
            }
            else finish();
        } else {
            Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();
        }
        count++;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.llScroll:
                rvCategories.scrollToPosition(0);
                llScroll.setVisibility(View.INVISIBLE);
                break;
        }
    }
}