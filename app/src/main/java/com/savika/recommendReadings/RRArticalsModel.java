package com.savika.recommendReadings;

/*
 * Created by Naresh Ravva on 05/07/17.
 */

public class RRArticalsModel {
    private String title,artical;
    private int image;

    public RRArticalsModel(String title,String artical,int image) {
        this.title = title;
        this.artical = artical;
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getArtical() {
        return artical;
    }

    public void setArtical(String artical) {
        this.artical = artical;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}
