package com.savika.recommendReadings;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.savika.R;

import java.util.ArrayList;

/*
 * Created by Naresh Ravva on 30/06/17.
 */

public class CategoriesAdapter extends RecyclerView.Adapter<CategoriesAdapter.ViewHolder> {

    private LayoutInflater mInflater;
    public ItemClickListener mClickListener;
    private Context context;
    private int selectedPos = 0;

    private ArrayList<CatModel> arrData = new ArrayList<>();

    public CategoriesAdapter(Context context, ArrayList<CatModel> arrData) {
        this.mInflater = LayoutInflater.from(context);
        this.arrData = arrData;
        this.context = context;

        selectedPos = arrData.size() - 1;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.list_item_recommend_readings_bottom, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the textview in each cell
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        CatModel model = arrData.get(position);
        holder.myTextView.setText(model.getCatdesc());

        if (selectedPos == position) {
            holder.myTextView.setTextColor(context.getResources().getColor(R.color.top_bar_color));
        } else {
            holder.myTextView.setTextColor(context.getResources().getColor(R.color.gray_ligth));
        }
    }

    // total number of cells
    @Override
    public int getItemCount() {
        return arrData.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView myTextView;

        public ViewHolder(View itemView) {
            super(itemView);
            myTextView = itemView.findViewById(R.id.tvTitle);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) {
                mClickListener.onItemClick(view, arrData.get(getAdapterPosition()).getCatid(), arrData.get(getAdapterPosition()).getCatdesc());

                selectedPos = getAdapterPosition();

                notifyDataSetChanged();
            }
        }
    }

    public CatModel getItem(int id) {
        return arrData.get(id);
    }

    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, String position, String catName);
    }

    public void setSelectedPos(int pos){
        this.selectedPos = pos;
    }
}