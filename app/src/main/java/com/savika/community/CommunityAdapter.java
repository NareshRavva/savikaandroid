package com.savika.community;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.util.Linkify;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.savika.R;
import com.savika.constants.AppConstants;
import com.savika.firebase.FbConstants;
import com.savika.logger.Logger;
import com.savika.utils.AppUtils;
import com.savika.utils.ImageUtil;
import com.savika.utils.SharedPrefsUtils;

import java.util.ArrayList;
import java.util.HashMap;

/*
 * Created by Naresh Ravva on 30/06/17.
 */

public class CommunityAdapter extends RecyclerView.Adapter<CommunityAdapter.ViewHolder> implements Filterable {

    private LayoutInflater mInflater;

    private ItemClickListener mClickListener;
    private LikeCountClickListener mLikeCntClickListener;
    private CommentItemClickListener mCommentClickListener;

    private CommunityCommentsAdapter cmnts_adapter;

    private Context context;
    private int selectedPos = 0;
    private String TAG = "CommunityAdapter";
    boolean isFilter;

    private ArrayList<CommunityDataModel> arrData = new ArrayList<>();
    private ArrayList<CommunityDataModel> arrData1 = new ArrayList<>();

    public CommunityAdapter(Context context, ArrayList<CommunityDataModel> arrData) {
        this.mInflater = LayoutInflater.from(context);
        this.arrData = arrData;
        this.arrData1 = arrData;
        this.context = context;

        selectedPos = arrData.size() - 1;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.list_item_community, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        CommunityDataModel model = arrData1.get(position);
        holder.tvUserName.setText(model.getUserName());
        holder.tvMsg.setText(model.getPostMsg());
        Linkify.addLinks(holder.tvMsg,Linkify.WEB_URLS);
        if (model.getUpdateTime()!=null && !(model.getUpdateTime().equalsIgnoreCase("")))
            holder.tvTime.setText(AppUtils.convertSimpleDayFormat1(model.getUpdateTime()));
        else holder.tvTime.setText(AppUtils.convertSimpleDayFormat(model.getPostTime()));
        holder.tvLikeCount.setText(model.getLikes_count() + "");
        holder.tvCommentCount.setText(model.getComments_count() + "");
        Glide.with(context).load(model.getUser_pic()).apply(new RequestOptions().placeholder(R.drawable.profile_round_icon)).apply(RequestOptions.circleCropTransform()).into(holder.ivProfile);
//        ImageUtil.LoadPicasoCicular(context, holder.ivProfile, model.getUser_pic());

        if (model.isThisPostAlrdyLiked() == true) {
            holder.tvLike.setTextColor(Color.parseColor("#c27a9f"));
        } else {
            holder.tvLike.setTextColor(Color.parseColor("#95989A"));
        }

        if (model.getCommunity_pic() != null && model.getCommunity_pic().length() > 5) {
            holder.ivCommentImg.setVisibility(View.VISIBLE);
            ImageUtil.LoadPicaso(context, holder.ivCommentImg, model.getCommunity_pic());
        } else {
            holder.ivCommentImg.setVisibility(View.GONE);
        }

        /**
         * HANDLE EDIT/DELETE FOR SLEF POSTS
         */
        if (model.getMobile() != null && model.getMobile().equalsIgnoreCase(SharedPrefsUtils.getMobileNum(context))) {
            holder.ivMore.setVisibility(View.VISIBLE);
        } else {
            holder.ivMore.setVisibility(View.INVISIBLE);
        }

        if (model.getComments_count() > 1) {
            holder.tvComment.setText("Comments");
        } else {
            holder.tvComment.setText("Comment");
        }
        if (model.getLikes_count() > 1) {
            holder.tvLike.setText("Likes");
        } else {
            holder.tvLike.setText("Like");
        }

        setCommentsDataToAdapter(model.getArrData(), holder.rvCommunityCmnts, model);

    }

//    /*
//     * @param community_model
//     * @param rvCommunityCmnts
//     * @param moreComments
//     */
//    private void getCommentsData(final CommunityDataModel community_model, final RecyclerView rvCommunityCmnts, final TextView moreComments) {
//
//        final ArrayList<CommentsResModel> arr_comments = new ArrayList<>();
//
//        DatabaseReference dbPath = FirebaseDatabase.getInstance().getReference().
//                child(FbConstants.TABLE_COMMUNITY).child(community_model.getCommunity_id()).child(FbConstants.TABLE_COMMENTS);
//
//
//        dbPath.addChildEventListener(new ChildEventListener() {
//            @Override
//            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
//                append_comments_conversion(context, dataSnapshot, arr_comments, community_model, rvCommunityCmnts, moreComments);
//            }
//
//            @Override
//            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
//                Logger.info(TAG, "comments onChildChanged-->" + dataSnapshot.getKey());
//            }
//
//            @Override
//            public void onChildRemoved(DataSnapshot dataSnapshot) {
//                Log.v(TAG, "onChildRemoved");
//            }
//
//            @Override
//            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
//                Logger.info(TAG, "comments onChildMoved");
//
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//                Logger.info(TAG, "comments onCancelled");
//
//            }
//        });
//
//    }

//    /*
//     * @param context
//     * @param dataSnapshot
//     * @param moreComments
//     */
//    private void append_comments_conversion(Context context, DataSnapshot dataSnapshot,
//                                            ArrayList<CommentsResModel> arr_comments, CommunityDataModel community_model, RecyclerView rvCommunityCmnts, TextView moreComments) {
//
//        Object value = dataSnapshot.getValue();
//
//        HashMap<String, String> message = (HashMap<String, String>) value;
//
//        CommentsResModel community_model = new CommentsResModel();
//        community_model.setComment_msg(message.get(FbConstants.COMMENT));
//        community_model.setComment_id(dataSnapshot.getKey());
//        community_model.setCommenter_name(message.get(FbConstants.USER_NAME));
//        String time = "" + dataSnapshot.child(FbConstants.POST_MSG_TIME).getValue();
//        community_model.setComment_time(time);
//        community_model.setUser_id(message.get(FbConstants.USER_ID));
//
//        arr_comments.add(community_model);
//
//        //Logger.info(TAG, "--arr_comments-->" + arr_comments.size() + "-->" + community_model.getUserName());
//
//        setCommentsDataToAdapter(arr_comments, rvCommunityCmnts, community_model);
//    }

    /*
     * @param arrListComments
     */
    private void setCommentsDataToAdapter(ArrayList<CommentsResModel> arrListComments,
                                          RecyclerView rvCommunityCmnts, CommunityDataModel community_model) {
        if (arrListComments != null && arrListComments.size() > 0) {

            LinearLayoutManager layoutManager = new LinearLayoutManager(context);
            rvCommunityCmnts.setLayoutManager(layoutManager);

            if (arrListComments.size() >= 2) {// Make sure you really have 3 elements
                ArrayList<CommentsResModel> arrListComments_new = new ArrayList<>();
                arrListComments_new.add(arrListComments.get(arrListComments.size() - 1)); // The last
                arrListComments_new.add(arrListComments.get(arrListComments.size() - 2)); // The one before the last

                cmnts_adapter = new CommunityCommentsAdapter(context, arrListComments_new, false);
            } else {
                cmnts_adapter = new CommunityCommentsAdapter(context, arrListComments, false);
            }
            rvCommunityCmnts.setAdapter(cmnts_adapter);
            rvCommunityCmnts.setVisibility(View.VISIBLE);
        } else {
            rvCommunityCmnts.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return arrData1.size();
    }


    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    arrData1 = arrData;
                } else {
                    ArrayList<CommunityDataModel> filteredList = new ArrayList<>();
                    for (CommunityDataModel row : arrData) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getMobile() != null && row.getMobile().equalsIgnoreCase(charString)) {
                            filteredList.add(row);
                        }
                    }

                    arrData1 = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = arrData1;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                arrData1 = (ArrayList<CommunityDataModel>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    void setFilter(boolean b) {
        isFilter = b;
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvUserName, tvMsg, tvTime, tvLike, tvComment, tvLikeCount, tvCommentCount, moreComments;
        ImageView ivProfile, ivCommentImg, ivMore;
        public RecyclerView rvCommunityCmnts;

        public ViewHolder(View itemView) {
            super(itemView);
            tvUserName = itemView.findViewById(R.id.tvLoginUserName);
            tvMsg = itemView.findViewById(R.id.tvMsg);
            tvTime = itemView.findViewById(R.id.tvTime);
            ivProfile = itemView.findViewById(R.id.img_profile);
            ivCommentImg = itemView.findViewById(R.id.ivCommentImg);
            tvLike = itemView.findViewById(R.id.tvLike);
            tvComment = itemView.findViewById(R.id.tvComment);
            ivMore = itemView.findViewById(R.id.imgMore);
            rvCommunityCmnts = itemView.findViewById(R.id.rvCommunityCmnts);
//            moreComments = (TextView) itemView.findViewById(R.id.more_coments);
            tvLikeCount = itemView.findViewById(R.id.tvLikeCount);
            tvCommentCount = itemView.findViewById(R.id.tvCommentCount);

//            moreComments.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    if (mCommentClickListener != null) {
//                        mCommentClickListener.onItemClick(view, arrData.get(getAdapterPosition()), AppConstants.COMMUNITY_COMMMENT);
//                        selectedPos = getAdapterPosition();
//                        //notifyDataSetChanged();
//                    }
//                }
//            });

            tvLike.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mClickListener != null) {
                        mClickListener.onItemClick(view, arrData.get(getAdapterPosition()), AppConstants.COMMUNITY_LIKE);
                        selectedPos = getAdapterPosition();
                        //notifyDataSetChanged();
                    }
                }
            });

            tvComment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mCommentClickListener != null) {
                        mCommentClickListener.onItemClick(view, arrData.get(getAdapterPosition()), AppConstants.COMMUNITY_COMMMENT);
                        selectedPos = getAdapterPosition();
                        //notifyDataSetChanged();
                    }
                }
            });

            ivMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mLikeCntClickListener != null) {
                        if (isFilter)
                            mLikeCntClickListener.onItemClick(view, arrData1.get(getAdapterPosition()), AppConstants.COMMUNITY_MORE);
                        else mLikeCntClickListener.onItemClick(view, arrData.get(getAdapterPosition()), AppConstants.COMMUNITY_MORE);
                    }
                }
            });
        }

        @Override
        public void onClick(View view) {
        }
    }

    public CommunityDataModel getItem(int id) {
        return arrData.get(id);
    }


    public interface ItemClickListener {
        void onItemClick(View view, CommunityDataModel model, String catName);
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }


    public interface LikeCountClickListener {
        void onItemClick(View view, CommunityDataModel model, String catName);
    }

    void setLikeCntClickListener(LikeCountClickListener mLikeCntClickListener) {
        this.mLikeCntClickListener = mLikeCntClickListener;
    }


    public interface CommentItemClickListener {
        void onItemClick(View view, CommunityDataModel model, String catName);
    }

    void setCommentClickListener(CommentItemClickListener mCommentClickListener) {
        this.mCommentClickListener = mCommentClickListener;
    }


}