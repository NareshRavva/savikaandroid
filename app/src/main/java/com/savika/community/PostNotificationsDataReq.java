package com.savika.community;

/*
 * Created by Naresh Ravva on 25/11/17.
 */

public class PostNotificationsDataReq {

    String n_id, msg, image_url, time, userid, reactuserid, notification_type;

    public String getViewed() {
        return viewed;
    }

    public void setViewed(String viewed) {
        this.viewed = viewed;
    }

    String viewed;

    public String getId() {
        return n_id;
    }

    public void setId(String id) {
        this.n_id = id;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getReactuserid() {
        return reactuserid;
    }

    public void setReactuserid(String reactuserid) {
        this.reactuserid = reactuserid;
    }

    public String getNotification_type() {
        return notification_type;
    }

    public void setNotification_type(String notification_type) {
        this.notification_type = notification_type;
    }
}

