package com.savika.community;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.savika.R;
import com.savika.constants.AppConstants;
import com.savika.logger.Logger;
import com.savika.utils.AppUtils;
import com.savika.utils.ImageUtil;

import java.util.ArrayList;

/*
 * Created by Naresh Ravva on 30/06/17.
 */

public class LikesAdapter extends RecyclerView.Adapter<LikesAdapter.ViewHolder> {

    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    private Context context;
    private int selectedPos = 0;

    private ArrayList<LikesResModel> arrData = new ArrayList<>();

    public LikesAdapter(Context context, ArrayList<LikesResModel> arrData) {
        this.mInflater = LayoutInflater.from(context);
        this.arrData = arrData;
        this.context = context;

        selectedPos = arrData.size() - 1;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.list_item_liked_users, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        LikesResModel model = arrData.get(position);

        holder.tvUserName.setText(model.getName());
        ImageUtil.LoadPicasoCicular(context, holder.img_profile, model.getProfile_pic());
    }

    @Override
    public int getItemCount() {
        return arrData.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvUserName;
        public ImageView img_profile;

        public ViewHolder(View itemView) {
            super(itemView);
            tvUserName = itemView.findViewById(R.id.tvLoginUserName);
            img_profile = itemView.findViewById(R.id.img_profile);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mClickListener != null) {
                        mClickListener.onItemClick(view, arrData.get(getAdapterPosition()).getLike_id(), "");
                    }
                }
            });
        }

        @Override
        public void onClick(View view) {
        }
    }

    public LikesResModel getItem(int id) {
        return arrData.get(id);
    }


    public interface ItemClickListener {
        void onItemClick(View view, String position, String catName);
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }


}