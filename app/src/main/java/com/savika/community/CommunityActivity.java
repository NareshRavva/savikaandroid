package com.savika.community;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.savika.Api.ApiClient;
import com.savika.Api.ApiInterface;
import com.savika.Api.CommonResModel;
import com.savika.constants.GAConstants;
import com.savika.dialogUtils.AlertDialogUtility;
import com.savika.firebase.FBResponseModel;
import com.savika.Api.ServiceUrl;
import com.savika.R;
import com.savika.amAMother.ProfileUploadResModel;
import com.savika.components.Button;
import com.savika.constants.AppConstants;
import com.savika.firebase.Data;
import com.savika.firebase.FbConstants;
import com.savika.googleAnalytics.GoogleAnalyticsHelper;
import com.savika.homeScreen.notification.NotificationActivity;
import com.savika.logger.Logger;
import com.savika.utils.AppUtils;
import com.savika.utils.ImageUtil;
import com.savika.utils.SharedPrefsUtils;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.savika.firebase.SendNotificationModel;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/*
 * Created by Naresh Ravva on 04/11/17.
 */

public class CommunityActivity extends AppCompatActivity implements View.OnClickListener,
        CommunityAdapter.ItemClickListener, CommunityAdapter.CommentItemClickListener,
        CommunityAdapter.LikeCountClickListener, CommunityCommentsAdapter.ItemClickListener {

    @BindView(R.id.tvHeaderTitle)
    TextView tvHeaderTitle;

    @BindView(R.id.llBack)
    LinearLayout llBack;

    @BindView(R.id.rvCommunityRecords)
    RecyclerView rvCommunityRecords;

    @BindView(R.id.ivAddCommunity)
    ImageView ivAddCommunity;

    @BindView(R.id.myposts)
    TextView myposts;

    @BindView(R.id.rlNotifications)
    RelativeLayout rlNotifications;

    @BindView(R.id.tvCount)
    TextView tvCount;

    @BindView(R.id.tvNoRecords)
    TextView tvNoRecords;

    private DatabaseReference root;
    private LinearLayoutManager layoutManager;

    private CommunityAdapter adapter;
    private CommunityCommentsAdapter cmnts_adapter;
    private LikesAdapter likes_adapter;

    RecyclerView rvCommunityCmnts;
    RecyclerView rvCommunityLikes;
    TextView tvNoCommnets;
    ImageView imgExtn;
    FrameLayout flRemoveExtn;

    int likes_count = 0;
    int comments_count = 0;

    String COMMUNITY_PIC = "";

    LayoutInflater inflater;

    private String last_community_id;
    String COMMUNITY_ID;
    boolean isOurPosts = false;

    ArrayList<CommunityDataModel> arrListCommunities = new ArrayList<>();
    ArrayList<CommentsResModel> arr_comments = new ArrayList<>();
    ArrayList<LikesResModel> arr_likes = new ArrayList<>();

    private String CONTENT_TYPE = "application/json";
    private String SERVER_KEY = "key=AAAAdTZ4eQg:APA91bERldk33nwEym-fLdG4sCT7w6IgBxTmane7r05k89HScY8N6BRBQxj_5uEyw-O6PmPO7e6cq0j64Swra3mk2YztRzEQ4Bq71KWWCtnLlow5W_ugMbw8PtxiLdaaARqHI8Ae9tCg";

    private String TAG = this.getClass().getSimpleName();
    private ArrayList<String> community_ids = new ArrayList<>();

    String community_key, community_msg;
    String LAST_COMMUNITY_COMMENT_ID = null;
    boolean isUserTryingToLike = true;

    int likeCount = 0;
    int commentsCount = 0;
    boolean isAlrdyLiked = false;
    ArrayList<CommentsResModel> temp_arr_comments = new ArrayList<>();

    GoogleAnalyticsHelper mGoogleHelper;

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String message = intent.getStringExtra("message");
            Log.d("receiver", "Received Unread Count: " + message);
            updateNotificationCount();
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_community);

        ButterKnife.bind(this);

        inflater = this.getLayoutInflater();

        adapter = new CommunityAdapter(CommunityActivity.this, arrListCommunities);

        tvHeaderTitle.setText(R.string.community);

        ivAddCommunity.setOnClickListener(this);
        llBack.setOnClickListener(this);
        rlNotifications.setVisibility(View.VISIBLE);
        rlNotifications.setOnClickListener(this);
        tvCount.setText(String.format(Locale.getDefault(),"%d", SharedPrefsUtils.getUnReadNoticationsCount(this)));
        myposts.setOnClickListener(this);

        root = FirebaseDatabase.getInstance().getReference();

        firebaseIsConnected();

        InitGoogleAnalytics();
        mGoogleHelper.SendScreenNameGA(this, GAConstants.COMMUNITY_SCREEN);
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
                new IntentFilter(AppConstants.BR_NOTIFICATION_COUNT));
    }

    // Google Analytics
    private void InitGoogleAnalytics() {
        mGoogleHelper = new GoogleAnalyticsHelper();
        mGoogleHelper.init(this);
    }

    private void updateNotificationCount() {
        if(SharedPrefsUtils.getUnReadNoticationsCount(CommunityActivity.this)>0){
            tvCount.setVisibility(View.VISIBLE);

            tvCount.setText(String.format(Locale.getDefault(),"%d", SharedPrefsUtils.getUnReadNoticationsCount(CommunityActivity.this)));
        }
        else {
            tvCount.setVisibility(View.INVISIBLE);
        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.rlNotifications:
                //clear unread count
                SharedPrefsUtils.setUnReadNoticationsCount(this, 0);
                tvCount.setVisibility(View.INVISIBLE);
                Intent notifintent = new Intent(CommunityActivity.this,NotificationActivity.class);
                startActivity(notifintent);

                break;
            case R.id.ivAddCommunity:
                addNewPostToCommunity(null);
                break;

            case R.id.llBack:
                finish();
                break;

            case R.id.myposts:
                if (!isOurPosts) {
                    isOurPosts = true;
                    myposts.setTextColor(getResources().getColor(R.color.colorPrimary));
                    adapter.setFilter(true);
                    adapter.getFilter().filter(SharedPrefsUtils.getMobileNum(CommunityActivity.this));
                } else {
                    isOurPosts = false;
                    adapter.setFilter(false);
                    myposts.setTextColor(getResources().getColor(R.color.black));
                    adapter.getFilter().filter("");
                }
        }
    }

    /*
     *
     */
    private void firebaseIsConnected() {

        AppUtils.showDialog(CommunityActivity.this);

        DatabaseReference connectedRef = FirebaseDatabase.getInstance().getReference(".info/connected");
        connectedRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                boolean connected = snapshot.getValue(Boolean.class);
                if (connected) {
                    System.out.println("connected");
                    updateListner();
                } else {
                    System.out.println("not connected");
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                System.err.println("Listener was cancelled");
            }
        });
    }

    private void updateListner() {

        new Handler().postDelayed(AppUtils::dismissDialog, 2000);

        root = root.child(FbConstants.TABLE_COMMUNITY);

        root.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                int posts_count = (int) dataSnapshot.getChildrenCount();
                if (!community_ids.contains(dataSnapshot.getKey())) {
                    community_ids.add(dataSnapshot.getKey());
                    append_chat_conversation(dataSnapshot);
                }
                //Logger.info(TAG, "onChildAdded--->" + dataSnapshot.getKey());
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                Logger.info(TAG, "onChildChanged-->" + dataSnapshot.getKey());
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                Log.v(TAG, "onChildRemoved");
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                Logger.info(TAG, "onChildMoved");

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Logger.info(TAG, "onCancelled");

            }
        });
    }

    private void append_chat_conversation(DataSnapshot dataSnapshot) {

        Object value = dataSnapshot.getValue();

        HashMap<String, String> message = (HashMap<String, String>) value;

        final CommunityDataModel model = new CommunityDataModel();
        model.setCommunity_id(dataSnapshot.getKey());
        COMMUNITY_ID = dataSnapshot.getKey();
        model.setUserName(message.get(FbConstants.USER_NAME));
        model.setUser_pic(message.get(FbConstants.USER_PROFILE_PIC));

        model.setUserID(message.get(FbConstants.USER_ID));
        model.setCommunity_pic(message.get(FbConstants.POST_MSG_PIC));
        String time = "" + dataSnapshot.child(FbConstants.POST_MSG_TIME).getValue();
        if (dataSnapshot.child(FbConstants.UPDATE_POST_TIME).getValue()!=null)
            model.setUpdateTime(""+dataSnapshot.child(FbConstants.UPDATE_POST_TIME).getValue());
        model.setPostTime(time);
        model.setPostMsg(message.get(FbConstants.POST_MSG));
        model.setMobile(message.get(FbConstants.MOBILE_NUMBER));

        /*
         * check is this post liked
         */
        //isThisPostLiked(dataSnapshot.getKey());

        DatabaseReference dbPath_isLiked = FirebaseDatabase.getInstance().getReference().
                child(FbConstants.TABLE_COMMUNITY).child(dataSnapshot.getKey()).child(FbConstants.TABLE_LIKES);

        dbPath_isLiked.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Object value = dataSnapshot.getValue();

                HashMap<String, String> message = (HashMap<String, String>) value;
                String mobile_num = message.get(FbConstants.MOBILE_NUMBER);

                boolean isLiked = false;
                if (mobile_num.equalsIgnoreCase(SharedPrefsUtils.getMobileNum(CommunityActivity.this))) {
                    isLiked = true;
                    model.setThisPostAlrdyLiked(true);
                }
//
                for (CommunityDataModel model : arrListCommunities) {
                    //Logger.info(TAG, "id-->" + community_model.getCommunity_id() + "---" + key);
                    if (model.getCommunity_id().equalsIgnoreCase(dataSnapshot.getKey())) {
                        model.setThisPostAlrdyLiked(isLiked);
                    }
                }

                adapter.notifyDataSetChanged();
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });

        /*
         * comments data
         */
        DatabaseReference dbPath_comments = FirebaseDatabase.getInstance().getReference().
                child(FbConstants.TABLE_COMMUNITY).child(dataSnapshot.getKey()).child(FbConstants.TABLE_COMMENTS);

        /*
         * Commnets Count
         */
        comments_count = 0;
        dbPath_comments.addValueEventListener(new ValueEventListener() {
            public void onDataChange(DataSnapshot dataSnapshot) {
                comments_count = (int) dataSnapshot.getChildrenCount();
                //System.out.println("In onDataChange, count=" + comments_count);
                String parent = dataSnapshot.getRef().getParent().getKey();
                Logger.info(TAG, "--comid-->" + parent);
                model.setComments_count(comments_count);
                int pos = getPositionOfPost(parent);
                rvCommunityRecords.scrollToPosition(pos);
            }

            public void onCancelled(DatabaseError databaseError) {
            }
        });

        /*
         * Likes Count
         */
        likes_count = 0;
        DatabaseReference dbPath = FirebaseDatabase.getInstance().getReference().
                child(FbConstants.TABLE_COMMUNITY).child(dataSnapshot.getKey()).child(FbConstants.TABLE_LIKES);
        dbPath.addValueEventListener(new ValueEventListener() {
            public void onDataChange(DataSnapshot dataSnapshot) {
                likes_count = (int) dataSnapshot.getChildrenCount();
                model.setLikes_count(likes_count);
                //System.out.println("In onDataChange, count=" + likes_count);
                String parent = dataSnapshot.getRef().getParent().getKey();
                Logger.info(TAG, "--likeid-->" + parent);
                int pos = getPositionOfPost(parent);
                rvCommunityRecords.scrollToPosition(pos);
            }

            public void onCancelled(DatabaseError databaseError) {
            }
        });

        /*
         * get update Name
         */
        DatabaseReference dbPath_name = FirebaseDatabase.getInstance().getReference().
                child(FbConstants.TABLE_USERS).child(message.get(FbConstants.MOBILE_NUMBER)).child(FbConstants.USER_NAME);
        dbPath_name.addValueEventListener(new ValueEventListener() {
            public void onDataChange(DataSnapshot dataSnapshot) {
                model.setUserName("" + dataSnapshot.getValue());
            }

            public void onCancelled(DatabaseError databaseError) {
            }
        });
        /*
         * get update Profile Pic
         */
        DatabaseReference dbPath_profile_pic = FirebaseDatabase.getInstance().getReference().
                child(FbConstants.TABLE_USERS).child(message.get(FbConstants.MOBILE_NUMBER)).child(FbConstants.USER_PROFILE_PIC);
        dbPath_profile_pic.addValueEventListener(new ValueEventListener() {
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot != null)
                    model.setUser_pic("" + dataSnapshot.getValue());

                arrListCommunities.add(model);
//                //Logger.info(TAG, "--arrListCommunities-->" + arrListCommunities.size());
                setDataToAdapter(arrListCommunities);
            }

            public void onCancelled(DatabaseError databaseError) {
            }
        });

        //if (community_model.getComments_count() > 0) {

        DatabaseReference dbPath_community_cmnts = FirebaseDatabase.getInstance().getReference().
                child(FbConstants.TABLE_COMMUNITY).child(model.getCommunity_id()).child(FbConstants.TABLE_COMMENTS);

        dbPath_community_cmnts.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                //append_community_comments_conversion(dataSnapshot, arr_comments, community_model, arrListCommunities);

                ArrayList<CommentsResModel> arr_latest_comments = new ArrayList<>();

                Object value = dataSnapshot.getValue();

                HashMap<String, String> message = (HashMap<String, String>) value;

                CommentsResModel comments_model = new CommentsResModel();
                comments_model.setComment_msg(message.get(FbConstants.COMMENT));
                comments_model.setComment_id(dataSnapshot.getKey());
                comments_model.setCommenter_name(message.get(FbConstants.USER_NAME));
                String time = "" + dataSnapshot.child(FbConstants.POST_MSG_TIME).getValue();
                comments_model.setComment_time(time);
                comments_model.setUser_id(message.get(FbConstants.USER_ID));

                arr_latest_comments.add(comments_model);

                model.setArrData(arr_latest_comments);
                arrListCommunities.add(model);
                Logger.info(TAG, "--arr_latest_comments-->" + COMMUNITY_ID);
                setDataToAdapter(arrListCommunities);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                Logger.info(TAG, "comments onChildChanged-->" + dataSnapshot.getKey());
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                Log.v(TAG, "onChildRemoved");
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                Logger.info(TAG, "comments onChildMoved");
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Logger.info(TAG, "comments onCancelled");
            }
        });
//        }
//        else {
//            arrListCommunities.add(community_model);
//            Logger.info(TAG, "--arrListCommunities-->" + arrListCommunities.size());
//            setDataToAdapter(arrListCommunities);
//        }


    }

    private int getPositionOfPost(String parent) {
        for (int i = 0; i < arrListCommunities.size(); i++) {
            if (arrListCommunities.get(i).getCommunity_id().equalsIgnoreCase(parent)) {
                return i;
            }
        }
        return 0;
    }

    /*
     * @param arrListCommunities
     */
    private void setDataToAdapter(ArrayList<CommunityDataModel> arrListCommunities) {
        if (arrListCommunities != null && arrListCommunities.size() > 0) {
            tvNoRecords.setVisibility(View.GONE);
            rvCommunityRecords.setVisibility(View.VISIBLE);
            myposts.setVisibility(View.VISIBLE);
            removeDuplicates(arrListCommunities);

            rvCommunityRecords.setLayoutManager(new LinearLayoutManager(this));

            //Collections.reverse(arrListCommunities);
            Collections.sort(arrListCommunities, new CommunityDataModel.CustomComparator());
            adapter = new CommunityAdapter(CommunityActivity.this, arrListCommunities);
            adapter.setClickListener(this);
            adapter.setCommentClickListener(this);
            adapter.setLikeCntClickListener(this);
            rvCommunityRecords.setAdapter(adapter);

            removeDuplicates(arrListCommunities);
        }
    }

    void removeDuplicates(ArrayList<CommunityDataModel> list) {
        int size = list.size();
        int out = 0;
        {
            final Set<CommunityDataModel> encountered = new HashSet<>();
            for (int in = 0; in < size; in++) {
                final CommunityDataModel t = list.get(in);
                final boolean first = encountered.add(t);
                if (first) {
                    list.set(out++, t);
                }
            }
        }
        while (out < size) {
            list.remove(--size);
        }

        adapter.notifyDataSetChanged();

    }


    /*
     * add post to community
     *
     * @param model
     */
    private void addNewPostToCommunity(final CommunityDataModel model) {
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.popup_add_community, null);
        alertDialogBuilder.setView(view);
        alertDialogBuilder.setCancelable(true);
        final AlertDialog dialog = alertDialogBuilder.create();
        dialog.show();

        final EditText etvMsg = view.findViewById(R.id.etvMsg);
        final Button btnPost = view.findViewById(R.id.btnPost);
        final Button btnAddPic = view.findViewById(R.id.btnAddPic);
        imgExtn = view.findViewById(R.id.imgExtn);
        flRemoveExtn = view.findViewById(R.id.flRemoveExtn);
        flRemoveExtn.setVisibility(View.GONE);

        community_key = FirebaseDatabase.getInstance().getReference().push().getKey();
        community_msg = etvMsg.getText().toString();
        COMMUNITY_PIC = "";

        /*
         * IF MODEL THERE ITS UPDATE
         */
        if (model != null && model.getCommunity_id() != null) {
            community_key = model.getCommunity_id();
            COMMUNITY_PIC = model.getCommunity_pic();
            community_msg = model.getPostMsg();
            likeCount = model.getLikes_count();
            commentsCount = model.getComments_count();
            isAlrdyLiked = model.isThisPostAlrdyLiked();
            temp_arr_comments = model.getArrData();

            if (COMMUNITY_PIC != null && COMMUNITY_PIC.length() > 10) {
                flRemoveExtn.setVisibility(View.VISIBLE);
                ImageUtil.LoadPicaso(CommunityActivity.this, imgExtn, COMMUNITY_PIC);
            }

            etvMsg.setText(community_msg);
        }

        flRemoveExtn.setOnClickListener(view1 -> {
            flRemoveExtn.setVisibility(View.GONE);
            COMMUNITY_PIC = null;
        });

        btnPost.setOnClickListener(view12 -> {

            if (etvMsg.getText().toString().length() < 1) {
                AppUtils.showToast(CommunityActivity.this, "Enter Message");
                return;
            }
            if (etvMsg.getText().toString().length() < 4) {
                AppUtils.hideKeyBoard(CommunityActivity.this, etvMsg);
                AppUtils.showToast(CommunityActivity.this, "Minimum Characters should be 4");
                return;
            }

            DatabaseReference dbPath = FirebaseDatabase.getInstance().getReference().
                    child(FbConstants.TABLE_COMMUNITY).child(community_key);

            Map<String, Object> map2 = new HashMap<>();

            map2.put(FbConstants.USER_NAME, SharedPrefsUtils.getUsername(CommunityActivity.this));
            map2.put(FbConstants.USER_ID, SharedPrefsUtils.getUserID(CommunityActivity.this));
            map2.put(FbConstants.MOBILE_NUMBER, SharedPrefsUtils.getMobileNum(CommunityActivity.this));
            map2.put(FbConstants.USER_PROFILE_PIC, SharedPrefsUtils.getProfilePic(CommunityActivity.this));

            map2.put(FbConstants.POST_MSG, etvMsg.getText().toString());
            String addorUpdate = "add";
            if (model != null && model.getCommunity_id() != null) {
                map2.put(FbConstants.UPDATE_POST_TIME, ServerValue.TIMESTAMP);
                addorUpdate = "update";
            }
            else map2.put(FbConstants.POST_MSG_TIME, ServerValue.TIMESTAMP);
            map2.put(FbConstants.POST_MSG_PIC, COMMUNITY_PIC);

            dbPath.updateChildren(map2);
            String post_time = "" + System.currentTimeMillis();
            int position =0;
            if (community_ids.contains(community_key)) {
                for (int i = 0; i < arrListCommunities.size(); i++) {
                    if (arrListCommunities.get(i).getCommunity_id().equalsIgnoreCase(community_key)) {
                        post_time = arrListCommunities.get(i).getPostTime();
                        arrListCommunities.remove(i);
                        position =i;
                        adapter.notifyDataSetChanged();
                    }
                }

                //Logger.info(TAG, "Time-->" + community_model.getPostTime());
                CommunityDataModel model1 = new CommunityDataModel();
                model1.setUserName(SharedPrefsUtils.getUsername(CommunityActivity.this));
                model1.setUserID(SharedPrefsUtils.getUserID(CommunityActivity.this));
                model1.setUser_pic(SharedPrefsUtils.getProfilePic(CommunityActivity.this));
                if (addorUpdate.equalsIgnoreCase("update")) {
                    model1.setUpdateTime("" + System.currentTimeMillis());
                    model1.setPostTime(post_time);
                }
                else  model1.setPostTime(post_time);
                model1.setPostMsg(etvMsg.getText().toString());
                model1.setCommunity_pic(COMMUNITY_PIC);
                model1.setCommunity_id(community_key);
                model1.setMobile(SharedPrefsUtils.getMobileNum(CommunityActivity.this));
                model1.setLikes_count(likeCount);
                model1.setComments_count(commentsCount);
                model1.setThisPostAlrdyLiked(isAlrdyLiked);
                model1.setArrData(temp_arr_comments);

                Logger.info(TAG, "size-->" + comments_count);

                arrListCommunities.add(model1);

                setDataToAdapter(arrListCommunities);
                if (position!=0){
                    rvCommunityRecords.scrollToPosition(position);
                }
            }


            // Close dialog
            dialog.dismiss();
        });

        btnAddPic.setOnClickListener(view13 -> startCropImageActivity(null));
    }

    /*
     * Start crop image activity for the given image.
     */
    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri).setGuidelines(CropImageView.Guidelines.ON).
                setMultiTouchEnabled(true).setInitialCropWindowPaddingRatio(0).setAspectRatio(16, 10)
                .start(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateNotificationCount();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);

            if (resultCode == RESULT_OK) {
                Log.v("MainActivityPrblm", "Image Path-->" + result.getUri());

                Log.v("MainActivityPrblm", "Image Path-->" + result.getUri());

                String selectedImagePath = result.getUri().getPath();

                Logger.info(TAG, "--FilePath-->" + selectedImagePath);

                if (selectedImagePath.contains("/external/")) {
                    selectedImagePath = AppUtils.getPath(CommunityActivity.this, data.getData());
                    Logger.info(TAG, "--FilePath-->" + selectedImagePath);
                }

                String fileName = AppUtils.getFileNameWithExtn(selectedImagePath);

                String fileExt = AppUtils.getMimeType(selectedImagePath);

                Logger.info(TAG, "--fileName-->" + fileName + "--fileExt-->" + fileExt);

                uploadToServer(selectedImagePath, fileName);

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Toast.makeText(this, "Cropping failed: " + result.getError(), Toast.LENGTH_LONG).show();
            }
        }
    }

    private void uploadToServer(String selectedImagePath, final String fileName) {

        if (AppUtils.isNetworkConnected(this)) {
            AppUtils.showDialog(CommunityActivity.this);

            try {
                final File file = new File(selectedImagePath);

                Log.d(TAG, "Filename " + file.getName());

                //RequestBody mFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                RequestBody mFile = RequestBody.create(MediaType.parse("image/*"), file);
                MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("doc", file.getName(), mFile);

                RequestBody mobilenumber = RequestBody.create(MultipartBody.FORM, SharedPrefsUtils.getMobileNum(CommunityActivity.this));
                RequestBody userid = RequestBody.create(MultipartBody.FORM, SharedPrefsUtils.getUserID(CommunityActivity.this));
                RequestBody filename = RequestBody.create(MultipartBody.FORM, fileName);

                ApiInterface uploadImage = ApiClient.getClient().create(ApiInterface.class);
                Call<ProfileUploadResModel> fileUpload = uploadImage.uploadDoc(SharedPrefsUtils.getToken(getApplicationContext()), fileToUpload, mobilenumber, userid, filename);

                fileUpload.enqueue(new Callback<ProfileUploadResModel>() {
                    @Override
                    public void onResponse(@NonNull Call<ProfileUploadResModel> call,@NonNull Response<ProfileUploadResModel> response) {
                        Logger.info(TAG, "Response " + response.code());

                        if (response.code() == 200 && response.body().isSuccess() == true) {

                            flRemoveExtn.setVisibility(View.VISIBLE);
                            COMMUNITY_PIC = response.body().getFile_url();
                            Logger.info(TAG, "MEDIA_URL--->" + COMMUNITY_PIC);

                            if (file.exists())
                                file.delete();

                            ImageUtil.LoadPicaso(CommunityActivity.this, imgExtn, COMMUNITY_PIC);
                        }
                        else{
                            if (response.body().getMessage()!=null) {
                                if (response.body().getMessage().equalsIgnoreCase("Unauthorized")) {
                                    AppUtils.sessionExpired(CommunityActivity.this);
                                } else
                                    AppUtils.showToast(getApplicationContext(), response.body().getMessage());
                            }
                        }

                        AppUtils.dismissDialog();

                    }

                    @Override
                    public void onFailure(@NonNull Call<ProfileUploadResModel> call, @NonNull Throwable t) {
                        AppUtils.showToast(CommunityActivity.this, getString(R.string.server_down));
                        Log.d(TAG, "Error " + t.getMessage());
                        AppUtils.dismissDialog();
                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else {
            AppUtils.showToast(this,"It looks like your internet connectioin off. Please turn it on and try again.");
        }

    }


    /*
     * add post to community
     *
     * @param key
     */
    private void addCommentToCommunity(final String key, final CommunityDataModel model) {

        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.popup_add_community_comments, null);
        alertDialogBuilder.setView(view);

        alertDialogBuilder.setCancelable(true);
        final AlertDialog dialog = alertDialogBuilder.create();
        //dialog.show();

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        dialog.show();
        dialog.getWindow().setAttributes(lp);

        final EditText etvMsg = view.findViewById(R.id.etxtMsg);
        final ImageView imgPost = view.findViewById(R.id.ivPost);
        TextView close = view.findViewById(R.id.ivClose);
        rvCommunityCmnts = view.findViewById(R.id.rvCommunityCmnts);
        tvNoCommnets = view.findViewById(R.id.tvNoCommnets);

        getCommentsOfThisCommunity(key);

        close.setOnClickListener(view1 -> dialog.dismiss());

        imgPost.setOnClickListener(view12 -> {

            for (int i = 0; i < arrListCommunities.size(); i++) {
                if (arrListCommunities.get(i).getCommunity_id().equalsIgnoreCase(community_key)) {
                    arrListCommunities.remove(i);
                    adapter.notifyDataSetChanged();
                }
            }

            if (etvMsg.getText().toString().length() < 1) {
                AppUtils.showToast(CommunityActivity.this, getString(R.string.enter_comment));
                return;
            }

            String like_key = FirebaseDatabase.getInstance().getReference().push().getKey();

            DatabaseReference dbPath = FirebaseDatabase.getInstance().getReference().
                    child(FbConstants.TABLE_COMMUNITY).child(key).child(FbConstants.TABLE_COMMENTS).child(like_key);

            Map<String, Object> map2 = new HashMap<>();

            map2.put(FbConstants.USER_ID, SharedPrefsUtils.getUserID(CommunityActivity.this));
            map2.put(FbConstants.COMMENT, etvMsg.getText().toString());
            map2.put(FbConstants.USER_NAME, SharedPrefsUtils.getUsername(CommunityActivity.this));
            map2.put(FbConstants.POST_MSG_TIME, ServerValue.TIMESTAMP);

            dbPath.updateChildren(map2);

            etvMsg.setText("");

            // Close dialog
            dialog.dismiss();

            getPostUserFirebaseID(AppConstants.NOTIFICATION_TYPE_COMMENT, model.getMobile(), key);

            String msgBody = SharedPrefsUtils.getUsername(CommunityActivity.this) + " commented on your post.";

            if (!model.getUserID().equalsIgnoreCase(SharedPrefsUtils.getUserID(CommunityActivity.this))) {
                postNotificationsData(key, msgBody,
                        SharedPrefsUtils.getProfilePic(CommunityActivity.this),
                        "" + System.currentTimeMillis(), model.getUserID(),
                        SharedPrefsUtils.getUserID(CommunityActivity.this), AppConstants.NOTIFICATION_TYPE_LIKE);
            }
        });
    }

    /*
     * @param type
     * @param mobile_num
     */
    private void getPostUserFirebaseID(final String type, String mobile_num, final String communityID) {
        Logger.info(TAG, "mobile Num-->" + mobile_num);
        FirebaseDatabase.getInstance().getReference().child(FbConstants.TABLE_USERS)
                .child(mobile_num).child(FbConstants.FIREBASE_REG_ID).
                addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        if (dataSnapshot.getValue() != null) {
                            Logger.info(TAG, "Firebase REG ID-->" + "" + dataSnapshot.getValue());

                            sendNotification("" + dataSnapshot.getValue(), type, communityID);

                        } else {
                            Logger.info(TAG, "Firebase REG ID NOT FOUND-->");
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }

    private void sendNotification(String firebase_reg_id, String type, String communityID) {

        if (firebase_reg_id != null && firebase_reg_id.length() > 10) {
            Data data = new Data();

            String msgBody;
            if (type.equalsIgnoreCase(AppConstants.NOTIFICATION_TYPE_COMMENT)) {
                msgBody = SharedPrefsUtils.getUsername(CommunityActivity.this) + " commented on your post.";
            } else {
                msgBody = SharedPrefsUtils.getUsername(CommunityActivity.this) + " likes your post.";
            }
            data.setBody(msgBody);

            data.setTitle(getString(R.string.app_name));
            data.setFrom_user_mobile(SharedPrefsUtils.getMobileNum(CommunityActivity.this));
            data.setNotification_type(type);
            data.setCommunity_id(communityID);

            ArrayList<String> arrRegIDs = new ArrayList<>();
            arrRegIDs.add(firebase_reg_id);

            SendNotificationModel notifyModel = new SendNotificationModel();
            notifyModel.setData(data);
            notifyModel.setRegistration_ids(arrRegIDs);
            if (AppUtils.isNetworkConnected(this)) {
                ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                Call<FBResponseModel> call = apiService.sendNotificationToAnotherDevice(ServiceUrl.FIREBASE_URL, CONTENT_TYPE, SERVER_KEY, notifyModel);

                call.enqueue(new Callback<FBResponseModel>() {
                    @Override
                    public void onResponse(@NonNull Call<FBResponseModel> call, @NonNull Response<FBResponseModel> response) {

                        Logger.info(TAG, "status code-->" + response.code());

                        if (response.code() == 200) {
                            if (response.body() != null && response.body().getSuccessCode() == 1) {
                                Logger.info(TAG, "NOTIFICATION API SUCCESS");
                            } else {
                                Logger.error(TAG, "NOTIFICATION API FAILS");
                            }
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<FBResponseModel> call,@NonNull Throwable t) {

                    }
                });
            }
            else {
                AppUtils.showToast(this,"It looks like your internet connectioin off. Please turn it on and try again.");
            }
        } else {
            Logger.error(TAG, "Unable to get Firebase REG id from firebase console");
        }
    }

    private void getCommentsOfThisCommunity(String key) {

        arr_comments = new ArrayList<>();

        LAST_COMMUNITY_COMMENT_ID = key;

        DatabaseReference dbPath = FirebaseDatabase.getInstance().getReference().
                child(FbConstants.TABLE_COMMUNITY).child(key).child(FbConstants.TABLE_COMMENTS);


        dbPath.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                append_comments_conversion(dataSnapshot);
                //Logger.info(TAG, "comments onChildAdded--->" + dataSnapshot.getKey());
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                Logger.info(TAG, "comments onChildChanged-->" + dataSnapshot.getKey());
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                Log.v(TAG, "onChildRemoved");
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                Logger.info(TAG, "comments onChildMoved");

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Logger.info(TAG, "comments onCancelled");

            }
        });
    }

    /*
     * @param communityActivity
     * @param dataSnapshot
     */
    private void append_comments_conversion(DataSnapshot dataSnapshot) {

        Object value = dataSnapshot.getValue();

        HashMap<String, String> message = (HashMap<String, String>) value;

        CommentsResModel model = new CommentsResModel();
        model.setComment_msg(message.get(FbConstants.COMMENT));
        model.setComment_id(dataSnapshot.getKey());
        model.setCommenter_name(message.get(FbConstants.USER_NAME));
        String time = "" + dataSnapshot.child(FbConstants.POST_MSG_TIME).getValue();
        model.setComment_time(time);
        model.setUser_id(message.get(FbConstants.USER_ID));

        arr_comments.add(model);

        Logger.info(TAG, "--arr_comments-->" + arr_comments.size());

        setCommentsDataToAdapter(arr_comments);
    }

    /*
     * @param arrListComments
     */
    private void setCommentsDataToAdapter(ArrayList<CommentsResModel> arrListComments) {
        if (arrListComments != null && arrListComments.size() > 0) {
            rvCommunityCmnts.setLayoutManager(new LinearLayoutManager(this));

            cmnts_adapter = new CommunityCommentsAdapter(CommunityActivity.this, arrListComments, true);
            rvCommunityCmnts.setAdapter(cmnts_adapter);
            cmnts_adapter.setClickListener(this);

            tvNoCommnets.setVisibility(View.GONE);
            rvCommunityCmnts.setVisibility(View.VISIBLE);

            if (arrListComments.size() > 2)
                rvCommunityCmnts.scrollToPosition(arrListComments.size() - 1);
        } else {
            tvNoCommnets.setVisibility(View.VISIBLE);
            rvCommunityCmnts.setVisibility(View.GONE);
        }
    }


    @Override
    public void onItemClick(View view, CommunityDataModel model, String type) {
        //Logger.info(TAG, "position-->" + key + "---type-->" + type);

        if (type.equalsIgnoreCase(AppConstants.COMMUNITY_COMMMENT)) {

            addCommentToCommunity(model.getCommunity_id(), model);
            COMMUNITY_ID = model.getCommunity_id();
            mGoogleHelper.SendEventGA(this, GAConstants.COMMUNITY_SCREEN, GAConstants.ADD_COMMUNITY, "");

        } else if (type.equalsIgnoreCase(AppConstants.COMMUNITY_LIKE)) {
            mGoogleHelper.SendEventGA(this, GAConstants.COMMUNITY_SCREEN, GAConstants.ADD_LIKES, "");
            updateLikeData(model.getCommunity_id(), model.getMobile());

        } else if (type.equalsIgnoreCase(AppConstants.COMMUNITY_LIKE_COUNT)) {

            getLikesData(model.getCommunity_id());
        } else if (type.equalsIgnoreCase(AppConstants.COMMUNITY_MORE)) {

            showEditDeletePopup(view, model);
        }
    }

    /*
     * @param view
     * @param model
     */
    private void showEditDeletePopup(View view, final CommunityDataModel model) {
        final PopupWindow popup = new PopupWindow(this);
        View layout = getLayoutInflater().inflate(R.layout.popup_update_child, null);
        popup.setContentView(layout);
        // Set content width and height
        popup.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        popup.setWidth(WindowManager.LayoutParams.WRAP_CONTENT);
        // Closes the popup window when touch outside of it - when looses focus
        popup.setOutsideTouchable(true);
        popup.setFocusable(true);
        // Show anchored to button
        popup.setBackgroundDrawable(new BitmapDrawable());
        popup.showAsDropDown(view);

        View container ;
        if (android.os.Build.VERSION.SDK_INT > 22) {
            container = (View) popup.getContentView().getParent().getParent();
        }else{
            container = (View) popup.getContentView().getParent();
        }
        WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        WindowManager.LayoutParams p = (WindowManager.LayoutParams) container.getLayoutParams();
// add flag
        p.flags |= WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        p.dimAmount = 0.3f;
        wm.updateViewLayout(container, p);


        LinearLayout llEditChild = layout.findViewById(R.id.llEditChild);
        LinearLayout llDeleteChild = layout.findViewById(R.id.llDeleteChild);
        TextView llEditText = layout.findViewById(R.id.edit_record);
        TextView llDeleteText = layout.findViewById(R.id.delete_record);
        llEditText.setText(R.string.edit_post);
        llDeleteText.setText(R.string.delete_post);

        llEditChild.setOnClickListener(view1 -> {

            popup.dismiss();

            addNewPostToCommunity(model);

        });

        llDeleteChild.setOnClickListener(view12 -> {

            popup.dismiss();

            runOnUiThread(new AlertDialogUtility(CommunityActivity.this, inflater, getResources().getString(R.string.app_name), getResources().getString(R.string.are_you_sure_you_want_to_delete_), "Yes", "No",
                    view1212 -> {

                        for (int i = 0; i < arrListCommunities.size(); i++) {
                            if (arrListCommunities.get(i).getCommunity_id().equalsIgnoreCase(model.getCommunity_id())) {
                                arrListCommunities.remove(i);
                                adapter.notifyDataSetChanged();
                            }
                        }
                        DatabaseReference dbPath = FirebaseDatabase.getInstance().getReference().
                                child(FbConstants.TABLE_COMMUNITY).child(model.getCommunity_id());

                        dbPath.removeValue();
                        AppUtils.showToast(CommunityActivity.this, "Post Deleted Sucessfully");

                        if (isOurPosts) {
                            myposts.setTextColor(getResources().getColor(R.color.colorPrimary));
                            adapter.getFilter().filter(SharedPrefsUtils.getMobileNum(CommunityActivity.this));
                        }

                    }, view121 -> {

                    }, true));
        });
    }

    private void getLikesData(String key) {

        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.popup_community_likes, null);
        alertDialogBuilder.setView(view);

        alertDialogBuilder.setCancelable(true);
        final AlertDialog dialog = alertDialogBuilder.create();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        dialog.show();
        dialog.getWindow().setAttributes(lp);

        rvCommunityLikes = view.findViewById(R.id.rvCommunityCmnts);

        arr_likes = new ArrayList<>();

        DatabaseReference dbPath = FirebaseDatabase.getInstance().getReference().
                child(FbConstants.TABLE_COMMUNITY).child(key).child(FbConstants.TABLE_LIKES);


        dbPath.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                append_likes_conversion(dataSnapshot);
                //Logger.info(TAG, "comments onChildAdded--->" + dataSnapshot.getKey());
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                //Logger.info(TAG, "comments onChildChanged-->" + dataSnapshot.getKey());
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                Log.v(TAG, "onChildRemoved");
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                //Logger.info(TAG, "comments onChildMoved");
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                //Logger.info(TAG, "comments onCancelled");
            }
        });
    }

    /*
     * @param communityActivity
     * @param dataSnapshot
     */
    private void append_likes_conversion(DataSnapshot dataSnapshot) {
        Object value = dataSnapshot.getValue();

        HashMap<String, String> message = (HashMap<String, String>) value;

        final LikesResModel model = new LikesResModel();

        String mobile_num = message.get(FbConstants.MOBILE_NUMBER);

        FirebaseDatabase.getInstance().getReference().child(FbConstants.TABLE_USERS)
                .child(mobile_num).child(FbConstants.USER_PROFILE_PIC).
                addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        if (dataSnapshot.getValue() != null) {
                            model.setProfile_pic("" + dataSnapshot.getValue());
                        } else {
                            model.setProfile_pic("");
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

        FirebaseDatabase.getInstance().getReference().child(FbConstants.TABLE_USERS)
                .child(mobile_num).child(FbConstants.USER_NAME).
                addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        if (dataSnapshot.getValue() != null) {
                            model.setName("" + dataSnapshot.getValue());
                        } else {
                            model.setName("Unknown User");
                        }
                        arr_likes.add(model);
                        setLikesDataToAdapter(arr_likes);
                        //Logger.info(TAG, "to_user_lastseen-->" + dataSnapshot.getValue());
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

    }

    /*
     * @param arr_likes
     */
    private void setLikesDataToAdapter(ArrayList<LikesResModel> arr_likes) {
        if (arr_likes != null && arr_likes.size() > 0) {
            rvCommunityLikes.setLayoutManager(new LinearLayoutManager(this));

            likes_adapter = new LikesAdapter(CommunityActivity.this, arr_likes);
            rvCommunityLikes.setAdapter(likes_adapter);
        }
    }

    /*
     * @param key
     */
    private void updateLikeData(String key, final String mobile) {
        for (CommunityDataModel model : arrListCommunities) {
            if (model.getCommunity_id().equalsIgnoreCase(key)) {
                //if this not liked previously
                if (!model.isThisPostAlrdyLiked()) {
                    isUserTryingToLike = true;
                    String like_key = FirebaseDatabase.getInstance().getReference().push().getKey();

                    DatabaseReference dbPath = FirebaseDatabase.getInstance().getReference().
                            child(FbConstants.TABLE_COMMUNITY).child(key).child(FbConstants.TABLE_LIKES).child(like_key);

                    Map<String, Object> map2 = new HashMap<String, Object>();

                    map2.put(FbConstants.USER_ID, SharedPrefsUtils.getUserID(CommunityActivity.this));
                    map2.put(FbConstants.MOBILE_NUMBER, SharedPrefsUtils.getMobileNum(CommunityActivity.this));

                    dbPath.updateChildren(map2);

                    getPostUserFirebaseID(AppConstants.NOTIFICATION_TYPE_LIKE, mobile, model.getCommunity_id());

                    String msgBody = SharedPrefsUtils.getUsername(CommunityActivity.this) + " likes your post.";

                    if (!model.getUserID().equalsIgnoreCase(SharedPrefsUtils.getUserID(CommunityActivity.this))) {
                        postNotificationsData(key, msgBody,
                                SharedPrefsUtils.getProfilePic(CommunityActivity.this),
                                "" + System.currentTimeMillis(), model.getUserID(),
                                SharedPrefsUtils.getUserID(CommunityActivity.this), AppConstants.NOTIFICATION_TYPE_LIKE);
                    }
                } else {
                    isUserTryingToLike = false;
                    unLike(key);

                }
            }
        }
    }

    /*
     * @param communityiD
     */
    private void unLike(final String communityiD) {
        final DatabaseReference dbPath_unlike = FirebaseDatabase.getInstance().getReference().
                child(FbConstants.TABLE_COMMUNITY).child(communityiD).child(FbConstants.TABLE_LIKES);

        dbPath_unlike.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                Logger.info(TAG, "--isUserTryingToLike-->" + isUserTryingToLike);


                Object value = dataSnapshot.getValue();
                HashMap<String, String> message = (HashMap<String, String>) value;
                String mobile_num = message.get(FbConstants.MOBILE_NUMBER);


                if (mobile_num.equalsIgnoreCase(SharedPrefsUtils.getMobileNum(CommunityActivity.this))) {
                    /*
                     * Remove direct from firebase DB
                     */
                    if (isUserTryingToLike == false)
                        dbPath_unlike.child(dataSnapshot.getKey()).removeValue();
                }

                /*
                 * remove From local
                 */

                DatabaseReference dbPath = FirebaseDatabase.getInstance().getReference().
                        child(FbConstants.TABLE_COMMUNITY).child(communityiD).child(FbConstants.TABLE_LIKES);
                dbPath.addValueEventListener(new ValueEventListener() {
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        likes_count = (int) dataSnapshot.getChildrenCount();

                        if (isUserTryingToLike == false) {
                            for (CommunityDataModel model : arrListCommunities) {
                                if (model.getCommunity_id().equalsIgnoreCase(communityiD)) {
                                    model.setLikes_count(likes_count);
                                    model.setThisPostAlrdyLiked(false);
                                    adapter.notifyDataSetChanged();
                                }
                            }
                        }
                    }

                    public void onCancelled(DatabaseError databaseError) {
                    }
                });


            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                //Logger.info(TAG, "comments onChildChanged-->" + dataSnapshot.getKey());
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                //Logger.info(TAG, "comments onChildMoved");
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                //Logger.info(TAG, "comments onCancelled");
            }
        });
    }

    /*
     * @param key
     */
    private void isThisPostLiked(final String key) {

        DatabaseReference dbPath_isLiked = FirebaseDatabase.getInstance().getReference().
                child(FbConstants.TABLE_COMMUNITY).child(key).child(FbConstants.TABLE_LIKES);

        dbPath_isLiked.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Object value = dataSnapshot.getValue();

                HashMap<String, String> message = (HashMap<String, String>) value;
                String mobile_num = message.get(FbConstants.MOBILE_NUMBER);

                boolean isLiked = false;
                if (mobile_num.equalsIgnoreCase(SharedPrefsUtils.getMobileNum(CommunityActivity.this))) {
                    isLiked = true;
                }

                for (CommunityDataModel model : arrListCommunities) {
                    //Logger.info(TAG, "id-->" + community_model.getCommunity_id() + "---" + key);
                    if (model.getCommunity_id().equalsIgnoreCase(key)) {
                        model.setThisPostAlrdyLiked(isLiked);
                    }
                }

                adapter.notifyDataSetChanged();
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }

    @Override
    public void onItemClick(View view, final CommentsResModel model) {

        final String commentId = model.getComment_id();

        Logger.info(TAG, "Comment id-->" + commentId);

        if (model != null && arr_comments.size() > 0 && model.getUser_id().equalsIgnoreCase(SharedPrefsUtils.getUserID(this))) {

            runOnUiThread(new AlertDialogUtility(CommunityActivity.this, inflater, getResources().getString(R.string.app_name), getResources().getString(R.string.are_you_sure_you_want_to_delete_), "Yes", "No",
                    view1 -> {

                        //Remove from local
                        for (int i = 0; i < arr_comments.size(); i++) {
                            if (arr_comments.get(i).getComment_id().equalsIgnoreCase(commentId)) {
                                arr_comments.remove(i);
                            }
                        }
                        cmnts_adapter.notifyDataSetChanged();

                        if (arr_comments != null && arr_comments.size() == 0) {
                            tvNoCommnets.setVisibility(View.VISIBLE);
                        }

                        // Remove from firebase DB
                        DatabaseReference remove_cmnt_dbPath = FirebaseDatabase.getInstance().getReference().
                                child(FbConstants.TABLE_COMMUNITY).child(LAST_COMMUNITY_COMMENT_ID).child(FbConstants.TABLE_COMMENTS).child(commentId);
                        remove_cmnt_dbPath.removeValue();
                        for (CommunityDataModel community_model : arrListCommunities) {
                            if (community_model.getCommunity_id().equalsIgnoreCase(COMMUNITY_ID)) {
                                Logger.info(TAG, "id-->" + community_model.getCommunity_id() + "--" + COMMUNITY_ID);
                                community_model.setArrData(arr_comments);
                                community_model.setComments_count(arr_comments.size());
                            }
                        }
                        adapter.notifyDataSetChanged();

                    }, view12 -> {

                    }, true));
        }
    }

    /*
     * post updated record
     */
    private void postNotificationsData(String id, String msg, String imgUrl, String time,
                                       String userID, String reactedUserid, String type) {

        if (AppUtils.isNetworkConnected(this)) {
            //AppUtils.showDialog(this);
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            PostNotificationsDataReq model = new PostNotificationsDataReq();
            model.setId(id);
            model.setMsg(msg);
            model.setImage_url(imgUrl);
            model.setTime(time);
            model.setUserid(userID);
            model.setReactuserid(reactedUserid);
            model.setNotification_type(type);

            Call<CommonResModel> call = apiService.postNotificationsData(SharedPrefsUtils.getToken(getApplicationContext()),model);

            call.enqueue(new Callback<CommonResModel>() {
                @Override
                public void onResponse(@NonNull Call<CommonResModel> call, @NonNull Response<CommonResModel> response) {
                    Logger.info(TAG, "status code-->" + response.code());
                    AppUtils.dismissDialog();

                    if (response.code() == 200 && response.body().isSuccess() == true) {
                        //AppUtils.showToast(CommunityActivity.this, "Success");
                    }
                    else{
                        if (response.body().getMessage()!=null) {
                            if (response.body().getMessage().equalsIgnoreCase("Unauthorized")) {
                                AppUtils.sessionExpired(CommunityActivity.this);
                            } else
                                AppUtils.showToast(getApplicationContext(), response.body().getMessage());
                        }
                    }
                }

                @Override
                public void onFailure(@NonNull Call<CommonResModel> call, @NonNull Throwable t) {
                    AppUtils.dismissDialog();
                }
            });
        }
        else {
            AppUtils.showToast(this,"It looks like your internet connectioin off. Please turn it on and try again.");
        }
    }
}
