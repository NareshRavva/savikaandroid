package com.savika.community;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.savika.R;
import com.savika.logger.Logger;
import com.savika.utils.AppUtils;
import com.savika.utils.SharedPrefsUtils;

import java.util.ArrayList;

/*
 * Created by Naresh Ravva on 30/06/17.
 */

public class CommunityCommentsAdapter extends RecyclerView.Adapter<CommunityCommentsAdapter.ViewHolder> {

    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    private Context context;
    private int selectedPos = 0;
    private boolean hideCloseMark = true;

    private ArrayList<CommentsResModel> arrData = new ArrayList<>();

    public CommunityCommentsAdapter(Context context, ArrayList<CommentsResModel> arrData, boolean hideCloseMark) {
        this.mInflater = LayoutInflater.from(context);
        this.arrData = arrData;
        this.context = context;
        this.hideCloseMark = hideCloseMark;

        selectedPos = arrData.size() - 1;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.list_item_comments, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        CommentsResModel model = arrData.get(position);
        holder.tvComntName.setText(model.getCommenter_name() + ":");
        holder.tvComntDesc.setText(model.getComment_msg());

        if (AppUtils.timeAgo(Long.parseLong(model.getComment_time())) != null) {
            holder.tvTimeAgo.setText(AppUtils.timeAgo(Long.parseLong(model.getComment_time())));
        } else {
            holder.tvTimeAgo.setText("just now");
        }

        if (hideCloseMark == true && model.getUser_id() != null && model.getUser_id().equalsIgnoreCase(SharedPrefsUtils.getUserID(context))) {
            holder.ivCmntDelete.setVisibility(View.VISIBLE);
        } else {
//            holder.tvComntName.setTextColor(context.getResources().getColor(R.color.calnder_blue));
            holder.ivCmntDelete.setVisibility(View.INVISIBLE);
        }

    }

    @Override
    public int getItemCount() {
        return arrData.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvComntName, tvComntDesc, tvTimeAgo;
        ImageView ivCmntDelete;

        public ViewHolder(View itemView) {
            super(itemView);
            tvComntName = itemView.findViewById(R.id.tvComntName);
            tvComntDesc = itemView.findViewById(R.id.tvComntDesc);
            tvTimeAgo = itemView.findViewById(R.id.tvTimeAgo);
            ivCmntDelete = itemView.findViewById(R.id.ivCmntDelete);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) {
                mClickListener.onItemClick(view, arrData.get(getAdapterPosition()));

                selectedPos = getAdapterPosition();

                notifyDataSetChanged();
            }
        }
    }

    public CommentsResModel getItem(int id) {
        return arrData.get(id);
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    public interface ItemClickListener {
        void onItemClick(View view, CommentsResModel model);
    }
}