package com.savika.community;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.savika.Api.ApiClient;
import com.savika.Api.ApiInterface;
import com.savika.Api.CommonResModel;
import com.savika.Api.ServiceUrl;
import com.savika.R;
import com.savika.constants.AppConstants;
import com.savika.dialogUtils.AlertDialogUtility;
import com.savika.firebase.Data;
import com.savika.firebase.FBResponseModel;
import com.savika.firebase.FbConstants;
import com.savika.firebase.SendNotificationModel;
import com.savika.homeScreen.HomeActivity;
import com.savika.homeScreen.notification.NotificationActivity;
import com.savika.logger.Logger;
import com.savika.utils.AppUtils;
import com.savika.utils.ImageUtil;
import com.savika.utils.SharedPrefsUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/*
 * Created by Naresh Ravva on 26/11/17.
 */

public class CommunityDetailsScreen extends AppCompatActivity implements View.OnClickListener, CommunityCommentsAdapter.ItemClickListener {

    @BindView(R.id.tvHeaderTitle)
    TextView tvHeaderTitle;

    @BindView(R.id.llBack)
    LinearLayout llBack;

    @BindView(R.id.img_profile)
    ImageView img_profile;

    @BindView(R.id.tvLoginUserName)
    TextView tvLoginUserName;

    @BindView(R.id.tvTime)
    TextView tvTime;

    @BindView(R.id.tvMsg)
    TextView tvMsg;

    @BindView(R.id.ivCommentImg)
    ImageView ivCommentImg;

    @BindView(R.id.tvLikeCount)
    TextView tvLikeCount;

    @BindView(R.id.tvLike)
    TextView tvLike;

    @BindView(R.id.tvCommentCount)
    TextView tvCommentCount;

    @BindView(R.id.tvComment)
    TextView tvComment;

    @BindView(R.id.rvCommunityCmnts)
    RecyclerView rvCommunityCmnts;

    @BindView(R.id.tvNoCommnets)
    TextView tvNoCommnets;

    @BindView(R.id.etxtMsg)
    EditText etxtMsg;

    @BindView(R.id.ivPost)
    ImageView ivPost;

    int likes_count = 0;
    int comments_count = 0;

    String ALREADY_LIKED = "already_liked";
    String NOT_LIKED = "not_liked";

    LayoutInflater inflater;

    private CommunityCommentsAdapter cmnts_adapter;

    private String TAG = this.getClass().getSimpleName();

    String COMMUNITY_ID = null;

    CommunityDataModel community_model;

    ArrayList<CommentsResModel> arr_latest_comments;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_community_detail);

        ButterKnife.bind(this);
        tvHeaderTitle.setText(R.string.community);

        inflater = this.getLayoutInflater();

        if (getIntent().getStringExtra("nid") != null) {
            COMMUNITY_ID = getIntent().getStringExtra("nid");
                if (getIntent().getBooleanExtra("from_notifications", false)){
                    NotificationActivity.postNotificationsData(CommunityDetailsScreen.this,COMMUNITY_ID,SharedPrefsUtils.getUserID(this),"community_like");
                }
            Logger.info(TAG, "n_id-->" + getIntent().getStringExtra("nid"));
        }


        if (COMMUNITY_ID == null && COMMUNITY_ID.length() < 1) {
            finish();
            return;
        }

        //get community all data
        getParticularCommunityData(COMMUNITY_ID);

        tvLike.setOnClickListener(this);
        ivPost.setOnClickListener(this);
        llBack.setOnClickListener(this);
    }

    private void getParticularCommunityData(String communityID) {

        DatabaseReference dbPath = FirebaseDatabase.getInstance().getReference().
                child(FbConstants.TABLE_COMMUNITY).child(communityID);

        dbPath.addValueEventListener(new ValueEventListener() {
            public void onDataChange(DataSnapshot dataSnapshot) {

                Object value = dataSnapshot.getValue();
                HashMap<String, String> message = (HashMap<String, String>) value;

                community_model = new CommunityDataModel();
                community_model.setCommunity_id(dataSnapshot.getKey());
                community_model.setUserName(message.get(FbConstants.USER_NAME));
                community_model.setUser_pic(message.get(FbConstants.USER_PROFILE_PIC));

                community_model.setUserID(message.get(FbConstants.USER_ID));
                community_model.setCommunity_pic(message.get(FbConstants.POST_MSG_PIC));
                String time = "" + dataSnapshot.child(FbConstants.POST_MSG_TIME).getValue();
                community_model.setPostTime(time);
                community_model.setPostMsg(message.get(FbConstants.POST_MSG));
                community_model.setMobile(message.get(FbConstants.MOBILE_NUMBER));

                /*
                 * check is this post liked
                 */
                isThisPostLiked(COMMUNITY_ID);

                /*
                 * comments data
                 */
                DatabaseReference dbPath_comments = FirebaseDatabase.getInstance().getReference().
                        child(FbConstants.TABLE_COMMUNITY).child(dataSnapshot.getKey()).child(FbConstants.TABLE_COMMENTS);

                /*
                 * Commnets Count
                 */
                comments_count = 0;
                dbPath_comments.addValueEventListener(new ValueEventListener() {
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        comments_count = (int) dataSnapshot.getChildrenCount();
                        if (comments_count>1){
                            tvComment.setText(R.string.comments);
                        }
                        else tvComment.setText(R.string.comment);
                        //System.out.println("In onDataChange, count=" + comments_count);
                        community_model.setComments_count(comments_count);
                    }

                    public void onCancelled(DatabaseError databaseError) {
                    }
                });

                /*
                 * Likes Count
                 */
                likes_count = 0;
                DatabaseReference dbPath = FirebaseDatabase.getInstance().getReference().
                        child(FbConstants.TABLE_COMMUNITY).child(dataSnapshot.getKey()).child(FbConstants.TABLE_LIKES);
                dbPath.addValueEventListener(new ValueEventListener() {
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        likes_count = (int) dataSnapshot.getChildrenCount();
                        community_model.setLikes_count(likes_count);
                        //System.out.println("In onDataChange, count=" + likes_count);
                    }

                    public void onCancelled(DatabaseError databaseError) {
                    }
                });

                /*
                 * get update Name
                 */
                DatabaseReference dbPath_name = FirebaseDatabase.getInstance().getReference().
                        child(FbConstants.TABLE_USERS).child(message.get(FbConstants.MOBILE_NUMBER)).child(FbConstants.USER_NAME);
                dbPath_name.addValueEventListener(new ValueEventListener() {
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        community_model.setUserName("" + dataSnapshot.getValue());
                    }

                    public void onCancelled(DatabaseError databaseError) {
                    }
                });
                /*
                 * get update Profile Pic
                 */
                DatabaseReference dbPath_profile_pic = FirebaseDatabase.getInstance().getReference().
                        child(FbConstants.TABLE_USERS).child(message.get(FbConstants.MOBILE_NUMBER)).child(FbConstants.USER_PROFILE_PIC);
                dbPath_profile_pic.addValueEventListener(new ValueEventListener() {
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot != null)
                            community_model.setUser_pic("" + dataSnapshot.getValue());

                        setDataToView(community_model);
                    }

                    public void onCancelled(DatabaseError databaseError) {
                    }
                });


            }

            public void onCancelled(DatabaseError databaseError) {
            }
        });

        getCommentsData();
    }

    private void getCommentsData() {
        arr_latest_comments = new ArrayList<>();

        DatabaseReference dbPath_community_cmnts = FirebaseDatabase.getInstance().getReference().
                child(FbConstants.TABLE_COMMUNITY).child(COMMUNITY_ID).child(FbConstants.TABLE_COMMENTS);

        dbPath_community_cmnts.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                Object value = dataSnapshot.getValue();

                HashMap<String, String> message = (HashMap<String, String>) value;

                CommentsResModel comments_model = new CommentsResModel();
                comments_model.setComment_msg(message.get(FbConstants.COMMENT));
                comments_model.setComment_id(dataSnapshot.getKey());
                comments_model.setCommenter_name(message.get(FbConstants.USER_NAME));
                String time = "" + dataSnapshot.child(FbConstants.POST_MSG_TIME).getValue();
                comments_model.setComment_time(time);
                comments_model.setUser_id(message.get(FbConstants.USER_ID));

                arr_latest_comments.add(comments_model);

                Logger.info(TAG, "--arr_latest_comments-->" + arr_latest_comments.size());

                setCommentsDataToAdapter(arr_latest_comments);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                Logger.info(TAG, "comments onChildChanged-->" + dataSnapshot.getKey());
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                Log.v(TAG, "onChildRemoved");
                String key = dataSnapshot.getKey();
                for (int i=0; i<arr_latest_comments.size(); ++i){
                    if (arr_latest_comments.get(i).getComment_id().equalsIgnoreCase(key)) {
                        arr_latest_comments.remove(i);
                        break;
                    }
                }
                cmnts_adapter.notifyDataSetChanged();


            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                Logger.info(TAG, "comments onChildMoved");
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Logger.info(TAG, "comments onCancelled");
            }
        });
    }

    /*
     * @param arrListComments
     */
    private void setCommentsDataToAdapter(ArrayList<CommentsResModel> arrListComments) {
        if (arrListComments != null && arrListComments.size() > 0) {

            LinearLayoutManager layoutManager = new LinearLayoutManager(this);
            rvCommunityCmnts.setLayoutManager(layoutManager);

            cmnts_adapter = new CommunityCommentsAdapter(this, arrListComments, true);
            rvCommunityCmnts.setAdapter(cmnts_adapter);
            cmnts_adapter.setClickListener(this);

            rvCommunityCmnts.setVisibility(View.VISIBLE);
            tvNoCommnets.setVisibility(View.GONE);
        } else {
            rvCommunityCmnts.setVisibility(View.GONE);
            tvNoCommnets.setVisibility(View.VISIBLE);
        }
    }

    /*
     * @param model
     */
    private void setDataToView(CommunityDataModel model) {

        tvLoginUserName.setText(model.getUserName());
        ImageUtil.LoadPicasoCicular(this, img_profile, model.getUser_pic());
        tvTime.setText(AppUtils.convertSimpleDayFormat(model.getPostTime()));

        tvMsg.setText(model.getPostMsg());

        tvLikeCount.setText(String.format(Locale.getDefault(),"%d", model.getLikes_count()));
        tvCommentCount.setText(String.format(Locale.getDefault(),"%d", model.getComments_count()));
        if (model.getLikes_count()>1){
            tvLike.setText(R.string.likes);
        }
        else tvLike.setText(R.string.like);
        if (model.getComments_count()>1){
            tvComment.setText(R.string.comments);
        }
        else tvComment.setText(R.string.comment);

        if (model.getCommunity_pic() != null && model.getCommunity_pic().length() > 5) {
            ivCommentImg.setVisibility(View.VISIBLE);
            ImageUtil.LoadPicaso(this, ivCommentImg, model.getCommunity_pic());
        } else {
            ivCommentImg.setVisibility(View.GONE);
        }
    }

    /*
     * @param key
     */
    private void isThisPostLiked(final String key) {

        DatabaseReference dbPath = FirebaseDatabase.getInstance().getReference().
                child(FbConstants.TABLE_COMMUNITY).child(key).child(FbConstants.TABLE_LIKES);

        dbPath.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Object value = dataSnapshot.getValue();

                HashMap<String, String> message = (HashMap<String, String>) value;
                String mobile_num = message.get(FbConstants.MOBILE_NUMBER);

                com.savika.logger.Logger.info(TAG, "--mobile_num-->" + mobile_num);

                if (mobile_num.equalsIgnoreCase(SharedPrefsUtils.getMobileNum(CommunityDetailsScreen.this))) {
                    tvLike.setTextColor(Color.parseColor("#c27a9f"));
                    tvLike.setTag(ALREADY_LIKED);
                }

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.tvLike:
                //Logger.info(TAG, "TAG-->" + tvLike.getTag().toString());
                if (tvLike.getTag() != null && tvLike.getTag().toString().equalsIgnoreCase(ALREADY_LIKED)) {
                    doUnLike();
                    tvLike.setTag(NOT_LIKED);
                    if (likes_count > 0)
                        tvLikeCount.setText(String.format(Locale.getDefault(),"%d", likes_count - 1));
                    if ((likes_count -1)>1){
                        tvLike.setText(R.string.likes);
                    }
                    else tvLike.setText(R.string.like);
                } else {
                    doLike();
                    tvLikeCount.setText(String.format(Locale.getDefault(),"%d", likes_count + 1));
                    tvLike.setTag(ALREADY_LIKED);
                }
                break;

            case R.id.llBack:
                if (getIntent().getBooleanExtra("from_notifications",false)){
                    Intent home = new Intent(this, HomeActivity.class);
                    startActivity(home);
                    finish();
                }
                else finish();
                break;


            case R.id.ivPost:
                postNewCommentToCommunity();
                break;
        }

    }

    /*
     *
     */
    private void postNewCommentToCommunity() {
        if (etxtMsg.getText().toString().length() < 1) {
            AppUtils.showToast(CommunityDetailsScreen.this, getString(R.string.enter_comment));
            return;
        }

        String like_key = FirebaseDatabase.getInstance().getReference().push().getKey();

        DatabaseReference dbPath = FirebaseDatabase.getInstance().getReference().
                child(FbConstants.TABLE_COMMUNITY).child(COMMUNITY_ID).child(FbConstants.TABLE_COMMENTS).child(like_key);

        Map<String, Object> map2 = new HashMap<>();

        map2.put(FbConstants.USER_ID, SharedPrefsUtils.getUserID(CommunityDetailsScreen.this));
        map2.put(FbConstants.COMMENT, etxtMsg.getText().toString());
        map2.put(FbConstants.USER_NAME, SharedPrefsUtils.getUsername(CommunityDetailsScreen.this));
        map2.put(FbConstants.POST_MSG_TIME, ServerValue.TIMESTAMP);

        dbPath.updateChildren(map2);

        etxtMsg.setText("");

        getPostUserFirebaseID(AppConstants.NOTIFICATION_TYPE_COMMENT, community_model.getMobile(), COMMUNITY_ID);

        AppUtils.hideKeyBoard(this, etxtMsg);

        String msgBody = SharedPrefsUtils.getUsername(CommunityDetailsScreen.this) + " commented on your post.";

        if (!community_model.getUserID().equalsIgnoreCase(SharedPrefsUtils.getUserID(CommunityDetailsScreen.this))) {
            postNotificationsData(COMMUNITY_ID, msgBody,
                    SharedPrefsUtils.getProfilePic(CommunityDetailsScreen.this),
                    "" + System.currentTimeMillis(), community_model.getUserID(),
                    SharedPrefsUtils.getUserID(CommunityDetailsScreen.this), AppConstants.NOTIFICATION_TYPE_LIKE);
        }


    }


    /*
     * post updated record
     */
    private void postNotificationsData(String id, String msg, String imgUrl, String time,
                                       String userID, String reactedUserid, String type) {

        if (AppUtils.isNetworkConnected(this)) {
            //AppUtils.showDialog(this);
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            PostNotificationsDataReq model = new PostNotificationsDataReq();
            model.setId(id);
            model.setMsg(msg);
            model.setImage_url(imgUrl);
            model.setTime(time);
            model.setUserid(userID);
            model.setReactuserid(reactedUserid);
            model.setNotification_type(type);

            Call<CommonResModel> call = apiService.postNotificationsData(SharedPrefsUtils.getToken(getApplicationContext()),model);

            call.enqueue(new retrofit2.Callback<CommonResModel>() {
                @Override
                public void onResponse(@NonNull Call<CommonResModel> call, @NonNull retrofit2.Response<CommonResModel> response) {
                    Logger.info(TAG, "status code-->" + response.code());
                    AppUtils.dismissDialog();

                    if (response.code() == 200 && response.body().isSuccess() == true) {
                        //AppUtils.showToast(CommunityActivity.this, "Success");
                    }  else{
                        if (response.body().getMessage()!=null) {
                            if (response.body().getMessage().equalsIgnoreCase("Unauthorized")) {
                                AppUtils.sessionExpired(CommunityDetailsScreen.this);
                            } else
                                AppUtils.showToast(getApplicationContext(), response.body().getMessage());
                        }
                    }
                }

                @Override
                public void onFailure(@NonNull Call<CommonResModel> call, @NonNull Throwable t) {
                    AppUtils.dismissDialog();
                }
            });
        }
        else {
            AppUtils.showToast(this,"It looks like your internet connectioin off. Please turn it on and try again.");
        }
    }

    private void doLike() {

        String like_key = FirebaseDatabase.getInstance().getReference().push().getKey();

        DatabaseReference dbPath = FirebaseDatabase.getInstance().getReference().
                child(FbConstants.TABLE_COMMUNITY).child(COMMUNITY_ID).child(FbConstants.TABLE_LIKES).child(like_key);

        Map<String, Object> map2 = new HashMap<>();

        map2.put(FbConstants.USER_ID, SharedPrefsUtils.getUserID(CommunityDetailsScreen.this));
        map2.put(FbConstants.MOBILE_NUMBER, SharedPrefsUtils.getMobileNum(CommunityDetailsScreen.this));

        dbPath.updateChildren(map2);

        tvLike.setTextColor(Color.parseColor("#c27a9f"));

        getPostUserFirebaseID(AppConstants.NOTIFICATION_TYPE_LIKE, community_model.getMobile(), community_model.getCommunity_id());
    }

    private void doUnLike() {
        final DatabaseReference dbPath_unlike = FirebaseDatabase.getInstance().getReference().
                child(FbConstants.TABLE_COMMUNITY).child(COMMUNITY_ID).child(FbConstants.TABLE_LIKES);

        dbPath_unlike.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                Object value = dataSnapshot.getValue();
                HashMap<String, String> message = (HashMap<String, String>) value;
                String mobile_num = message.get(FbConstants.MOBILE_NUMBER);


                if (mobile_num.equalsIgnoreCase(SharedPrefsUtils.getMobileNum(CommunityDetailsScreen.this))) {
                    /*
                     * Remove direct from firebase DB
                     */
                    if (tvLike.getTag() != null && tvLike.getTag().toString().equalsIgnoreCase(NOT_LIKED)){
                        dbPath_unlike.child(dataSnapshot.getKey()).removeValue();
                        tvLike.setTextColor(Color.parseColor("#95989A"));
                    }

                }


            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                //Logger.info(TAG, "comments onChildChanged-->" + dataSnapshot.getKey());
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                //Logger.info(TAG, "comments onChildMoved");
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                //Logger.info(TAG, "comments onCancelled");
            }
        });
    }

    /*
     * @param type
     * @param mobile_num
     */
    private void getPostUserFirebaseID(final String type, String mobile_num, final String communityID) {
        Logger.info(TAG, "mobile Num-->" + mobile_num);
        FirebaseDatabase.getInstance().getReference().child(FbConstants.TABLE_USERS)
                .child(mobile_num).child(FbConstants.FIREBASE_REG_ID).
                addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        if (dataSnapshot.getValue() != null) {
                            Logger.info(TAG, "Firebase REG ID-->" + "" + dataSnapshot.getValue());

                            sendNotification("" + dataSnapshot.getValue(), type, communityID);

                        } else {
                            Logger.info(TAG, "Firebase REG ID NOT FOUND-->");
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }

    private void sendNotification(String firebase_reg_id, String type, String communityID) {

        if (firebase_reg_id != null && firebase_reg_id.length() > 10) {
            Data data = new Data();

            String msgBody;
            if (type.equalsIgnoreCase(AppConstants.NOTIFICATION_TYPE_COMMENT)) {
                msgBody = SharedPrefsUtils.getUsername(CommunityDetailsScreen.this) + " commented on your post.";
            } else {
                msgBody = SharedPrefsUtils.getUsername(CommunityDetailsScreen.this) + " likes your post.";
            }
            data.setBody(msgBody);

            data.setTitle(getString(R.string.app_name));
            data.setFrom_user_mobile(SharedPrefsUtils.getMobileNum(CommunityDetailsScreen.this));
            data.setNotification_type(type);
            data.setCommunity_id(communityID);

            ArrayList<String> arrRegIDs = new ArrayList<>();
            arrRegIDs.add(firebase_reg_id);

            SendNotificationModel notifyModel = new SendNotificationModel();
            notifyModel.setData(data);
            notifyModel.setRegistration_ids(arrRegIDs);

            if (AppUtils.isNetworkConnected(this)) {
                ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                String CONTENT_TYPE = "application/json";
                String SERVER_KEY = "key=AAAAdTZ4eQg:APA91bERldk33nwEym-fLdG4sCT7w6IgBxTmane7r05k89HScY8N6BRBQxj_5uEyw-O6PmPO7e6cq0j64Swra3mk2YztRzEQ4Bq71KWWCtnLlow5W_ugMbw8PtxiLdaaARqHI8Ae9tCg";
                Call<FBResponseModel> call = apiService.sendNotificationToAnotherDevice(ServiceUrl.FIREBASE_URL, CONTENT_TYPE, SERVER_KEY, notifyModel);

                call.enqueue(new Callback<FBResponseModel>() {
                    @Override
                    public void onResponse(@NonNull Call<FBResponseModel> call, @NonNull Response<FBResponseModel> response) {

                        Logger.info(TAG, "status code-->" + response.code());

                        if (response.code() == 200) {
                            if (response.body() != null && response.body().getSuccessCode() == 1) {
                                Logger.info(TAG, "NOTIFICATION API SUCCESS");
                            } else {
                                Logger.error(TAG, "NOTIFICATION API FAILS");
                            }
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<FBResponseModel> call,@NonNull Throwable t) {

                    }
                });
            }
            else {
                AppUtils.showToast(this,"It looks like your internet connectioin off. Please turn it on and try again.");
            }
        } else {
            Logger.error(TAG, "Unable to get Firebase REG id from firebase console");
        }
    }

    @Override
    public void onItemClick(View view, final CommentsResModel model) {

        final String commentId = model.getComment_id();

        Logger.info(TAG, "Comment id-->" + commentId);

        if (model != null && model.getUser_id().equalsIgnoreCase(SharedPrefsUtils.getUserID(this))) {

            runOnUiThread(new AlertDialogUtility(CommunityDetailsScreen.this, inflater, getResources().getString(R.string.app_name), getResources().getString(R.string.are_you_sure_you_want_to_delete_), "Yes", "No",
                    view12 -> {

                        //Remove from local
                        for (int i = 0; i < arr_latest_comments.size(); i++) {
                            if (arr_latest_comments.get(i).getComment_id().equalsIgnoreCase(commentId)) {
                                arr_latest_comments.remove(i);
                            }
                        }
                        cmnts_adapter.notifyDataSetChanged();

                        if (arr_latest_comments != null && arr_latest_comments.size() == 0) {
                            tvNoCommnets.setVisibility(View.VISIBLE);
                        }

                        // Remove from firebase DB
                        DatabaseReference remove_cmnt_dbPath = FirebaseDatabase.getInstance().getReference().
                                child(FbConstants.TABLE_COMMUNITY).child(COMMUNITY_ID).child(FbConstants.TABLE_COMMENTS).child(commentId);
                        remove_cmnt_dbPath.removeValue();

                    }, view1 -> {

                    }, true));
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (getIntent().getBooleanExtra("from_notifications",false)){
            Intent home = new Intent(this, HomeActivity.class);
            startActivity(home);
            finish();
        }
        else finish();

    }
}
