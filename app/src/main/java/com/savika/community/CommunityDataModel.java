package com.savika.community;

import java.util.ArrayList;
import java.util.Comparator;

/*
 * Created by Naresh Ravva on 04/11/17.
 */

public class CommunityDataModel {

    private String userName, userID, user_pic, postTime, postMsg, community_pic, community_id, mobile;

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    private String updateTime;
    private int likes_count, comments_count;
    private boolean isThisPostAlrdyLiked;
    private ArrayList<CommentsResModel> arrData = new ArrayList<>();


    public ArrayList<CommentsResModel> getArrData() {
        return arrData;
    }

    public void setArrData(ArrayList<CommentsResModel> arrData) {
        this.arrData = arrData;
    }

    public boolean isThisPostAlrdyLiked() {
        return isThisPostAlrdyLiked;
    }

    public void setThisPostAlrdyLiked(boolean thisPostAlrdyLiked) {
        isThisPostAlrdyLiked = thisPostAlrdyLiked;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public int getLikes_count() {
        return likes_count;
    }

    public void setLikes_count(int likes_count) {
        this.likes_count = likes_count;
    }

    public int getComments_count() {
        return comments_count;
    }

    public void setComments_count(int comments_count) {
        this.comments_count = comments_count;
    }

    public String getCommunity_id() {
        return community_id;
    }

    public void setCommunity_id(String community_id) {
        this.community_id = community_id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getUser_pic() {
        return user_pic;
    }

    public void setUser_pic(String user_pic) {
        this.user_pic = user_pic;
    }

    public String getPostTime() {
        return postTime;
    }

    public void setPostTime(String postTime) {
        this.postTime = postTime;
    }

    public String getPostMsg() {
        return postMsg;
    }

    public void setPostMsg(String postMsg) {
        this.postMsg = postMsg;
    }

    public String getCommunity_pic() {
        return community_pic;
    }

    public void setCommunity_pic(String community_pic) {
        this.community_pic = community_pic;
    }


    public static class CustomComparator implements Comparator<CommunityDataModel> {
        @Override
        public int compare(CommunityDataModel o1, CommunityDataModel o2) {
            return o2.getPostTime().compareTo(o1.getPostTime());
        }
    }
}
