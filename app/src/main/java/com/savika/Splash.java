package com.savika;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Handler;
import android.util.Base64;
import android.util.Log;
import android.view.Display;

import com.crashlytics.android.Crashlytics;
import com.facebook.appevents.AppEventsLogger;
import com.google.firebase.FirebaseApp;
import com.savika.Api.ApiClient;
import com.savika.Api.ApiInterface;
import com.savika.constants.AppConstants;
import com.savika.demoSliding.DemoSlidingActivity;
import com.savika.homeScreen.HomeActivity;
import com.savika.onBoardScreens.OnBoardActivity1;
import com.savika.utils.AppUtils;
import com.savika.utils.SharedPrefsUtils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

import io.fabric.sdk.android.Fabric;
import io.fabric.sdk.android.Logger;
import retrofit2.Call;

/*
 * Created by Naresh Ravva on 10/08/17.
 */

public class Splash extends Activity {

    /**
     * Duration of wait
     **/
    AppEventsLogger logger;
    private final int SPLASH_DISPLAY_LENGTH = 2000;
    int count =0;
    int count1 =0;


    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);

        setContentView(R.layout.activity_splashscreen);

        FirebaseApp.initializeApp(this);
        Fabric.with(this, new Crashlytics());
        logger = AppEventsLogger.newLogger(Splash.this);


        getDeviceWidthAndHeigth();
        getGlobalConfig();

        //getSHA1Key();

    }

    private void getGlobalConfig() {
//        AppUtils.showDialog(this);
        if (AppUtils.isNetworkConnected(Splash.this)) {

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);


            Call<GlobalConfigResMOdel> call = apiService.getGlobalConfig();
            call.enqueue(new retrofit2.Callback<GlobalConfigResMOdel>() {
                @Override
                public void onResponse(Call<GlobalConfigResMOdel> call, retrofit2.Response<GlobalConfigResMOdel> response) {
//                AppUtils.dismissDialog();

                    if (response.code() == 200 && response.body().isSuccess().equalsIgnoreCase("ok")) {
                        SharedPrefsUtils.setGlobalConfig(Splash.this, response.body().getGlobalRecords());
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if (SharedPrefsUtils.getUserID(Splash.this) == null || SharedPrefsUtils.getMobileNum(Splash.this) == null) {
                                    Intent mainIntent = new Intent(Splash.this, DemoSlidingActivity.class);
                                    Splash.this.startActivity(mainIntent);
                                    Splash.this.finish();
                                } else {
                                    Intent mainIntent = new Intent(Splash.this, HomeActivity.class);
                                    Splash.this.startActivity(mainIntent);
                                    Splash.this.finish();
                                }
                            }
                        }, SPLASH_DISPLAY_LENGTH);
                    } else {
//                    AppUtils.dismissDialog();
                        AppUtils.showToast(Splash.this, String.valueOf(response.code()));
                    }
                }

                @Override
                public void onFailure(Call<GlobalConfigResMOdel> call, Throwable t) {
                    AppUtils.dismissDialog();
                    if (t.getMessage().contains("Failed to connect")){
                        count1 = count1+1;
                        if (count1<2)
                            getGlobalConfig();
                        else AppUtils.showToast(Splash.this,"Check your Internet connection");
                    }
                }

            });
        }
        else {
            count = count+1;
            AppUtils.showToast(Splash.this,"It looks like your internet connectioin off. Please turn it on and try again.");
            if (count <2)
                new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    getGlobalConfig();
                }
            },2000);


        }
    }



    private void getDeviceWidthAndHeigth() {
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = AppConstants.DEVICE_DISPLAY_WIDTH = size.x;
        int height = size.y;
    }

    private void getSHA1Key() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
        } catch (NoSuchAlgorithmException e) {
        }
    }
}
