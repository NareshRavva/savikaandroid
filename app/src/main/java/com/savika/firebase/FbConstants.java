package com.savika.firebase;

import android.content.Context;

import com.savika.utils.SharedPrefsUtils;

/**
 * Created by Naresh Ravva on 03/11/17.
 */

public class FbConstants {

    public static String TABLE_USERS = "Users";
    public static String DEVELOPMENT = "Community_Development";
    public static String PRODUCTION = "Community_Production";
    public static String TABLE_COMMUNITY = PRODUCTION;
//    public static String TABLE_COMMUNITY = "Community";

    public static String TABLE_LIKES = "Likes";
    public static String TABLE_COMMENTS = "Comments";


    /**
     * User Details
     */
    public static String USER_NAME = "name";
    public static String LASTSEEN = "lastSeen";
    public static String FIREBASE_REG_ID = "fID";
    public static String MOBILE_OS = "OS";
    public static String DEVICE_ID = "deviceID";
    public static String USER_PROFILE_PIC = "profilePic";
    public static String ANDROID = "ANDROID";
    public static int PRESENCE_ONLINE = 0;
    public static String USER_ID = "user_id";
    public static String MOBILE_NUMBER = "mobile_num";
    public static String USER_TYPE = "user_type";
    public static String POST_MSG_TIME = "post_time";
    public static String UPDATE_POST_TIME = "update_time";
    public static String POST_MSG = "community_msg";
    public static String POST_MSG_PIC = "community_pic";
    public static String COMMENT = "comment";


    /**
     * @param ctx
     * @return
     */
    public static String userIDcolumn(Context ctx) {
        return SharedPrefsUtils.getMobileNum(ctx) + "_" + SharedPrefsUtils.getUsername(ctx);
    }

}
