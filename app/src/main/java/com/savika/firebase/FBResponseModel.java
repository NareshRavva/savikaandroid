package com.savika.firebase;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Naresh Ravva on 14/07/17.
 */

public class FBResponseModel implements Serializable {

    @SerializedName("success")
    private int successCode;

    public int getSuccessCode() {
        return successCode;
    }

    public void setSuccessCode(int successCode) {
        this.successCode = successCode;
    }
}
