package com.savika.firebase;

/**
 * Created by Naresh Ravva on 09/11/17.
 */

public class Data {

    public String body, title, from_user_mobile, notification_type, community_id;

    public String getCommunity_id() {
        return community_id;
    }

    public void setCommunity_id(String community_id) {
        this.community_id = community_id;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFrom_user_mobile() {
        return from_user_mobile;
    }

    public void setFrom_user_mobile(String from_user_mobile) {
        this.from_user_mobile = from_user_mobile;
    }

    public String getNotification_type() {
        return notification_type;
    }

    public void setNotification_type(String notification_type) {
        this.notification_type = notification_type;
    }
}
