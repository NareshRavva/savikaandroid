package com.savika.firebase;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Naresh Ravva on 14/07/17.
 */

public class SendNotificationModel {

    @SerializedName("data")
    Data data;

    @SerializedName("registration_ids")
    private ArrayList<String> registration_ids = new ArrayList<String>();

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public ArrayList<String> getRegistration_ids() {
        return registration_ids;
    }

    public void setRegistration_ids(ArrayList<String> registration_ids) {
        this.registration_ids = registration_ids;
    }
}


