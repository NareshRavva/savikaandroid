package com.savika.firebase;

/**
 * Created by Naresh R on 06-10-2016.
 */

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.savika.R;
import com.savika.calendar.DetailCalenderRecordActivity;
import com.savika.community.CommunityDetailsScreen;
import com.savika.constants.AppConstants;
import com.savika.homeScreen.CardsDataModel;
import com.savika.homeScreen.HomeActivity;
import com.savika.homeScreen.cards.HomeCardModel;
import com.savika.logger.Logger;
import com.savika.recommendReadings.ArticalsModel;
import com.savika.recommendReadings.RecommendReadingsActivity;
import com.savika.utils.SharedPrefsUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Map;


/**
 * Created by Belal on 5/27/2016.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";

    public static ArrayList<String> arr_user_names = new ArrayList<>();
    public static ArrayList<String> arr_msg_body = new ArrayList<>();

    private String MESSAGE_BODY = "message";
    private String MESSAGE_FROM = "username";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Map<String, String> data = remoteMessage.getData();

        Gson gson = new Gson();
        String json = gson.toJson(data);

        Logger.info(TAG, "json response-->" + json);

        try {
            JSONObject objt = new JSONObject(json);

            String received_msg_mobileNum = objt.optString("from_user_mobile");

            if (received_msg_mobileNum != null && received_msg_mobileNum.equalsIgnoreCase(SharedPrefsUtils.getMobileNum(this))) {
                //STOP SELF NOTIFICATIONS
            } else {
                //send broadcast
                sendBroadCast();

                boolean isnotifOn = SharedPrefsUtils.getNotificationStatus(this);
                String nid = null;
                String notification_type = null;
                if (objt.optString("notification_type")!=null && !(objt.optString("notification_type").equalsIgnoreCase("") ))
                    notification_type = objt.optString("notification_type");
                if (objt.optString("community_id")!=null && !(objt.optString("community_id").equalsIgnoreCase("") ))
                    nid = objt.optString("community_id");
                if (isnotifOn)
                    sendNotification(MyFirebaseMessagingService.this, "Savika", objt.optString("body") ,nid,notification_type,objt);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    private void sendBroadCast() {
        //update count
        int current_count = SharedPrefsUtils.getUnReadNoticationsCount(MyFirebaseMessagingService.this);
        SharedPrefsUtils.setUnReadNoticationsCount(MyFirebaseMessagingService.this, current_count + 1);

        Intent intent = new Intent(AppConstants.BR_NOTIFICATION_COUNT);
        intent.putExtra("message", "This is my message!");
        LocalBroadcastManager.getInstance(MyFirebaseMessagingService.this).sendBroadcast(intent);
    }

    public static void sendNotification(Context ctx, String title, String msg, String nid, String notification_type,JSONObject obj) {
        NotificationManager mNotificationManager = (NotificationManager) ctx.getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.BigTextStyle notiStyle = new NotificationCompat.BigTextStyle();

        notiStyle.bigText(msg);

        /**
         * Default Screen if all cases fails
         */
        Intent resultIntent;
        // This ensures that the back button follows the recommended convention for the back key.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(ctx);

        if (notification_type!=null && !(notification_type.equalsIgnoreCase(""))){
            if (notification_type.equalsIgnoreCase("Community_like") || notification_type.equalsIgnoreCase("community_commment")){
                resultIntent = new Intent(ctx, CommunityDetailsScreen.class);
                if (nid!=null && !(nid.equalsIgnoreCase("")))
                    resultIntent.putExtra("nid",nid);
                resultIntent.putExtra("notif_type","community_like");
                resultIntent.putExtra("from_notifications",true);
                // Adds the back stack for the Intent (but not the Intent itself).
                stackBuilder.addParentStack(CommunityDetailsScreen.class);
            }
            else if (notification_type.equalsIgnoreCase("article")){
                ArticalsModel articalsModel = new ArticalsModel();
                articalsModel.setCatid(obj.optString("catid").trim());
                articalsModel.setId(obj.optString("n_id").trim());
                articalsModel.setContent(obj.optString("content"));
                articalsModel.setTitle(obj.optString("body"));
                articalsModel.setImageurl(obj.optString("main_picture"));
                articalsModel.setWebsite(obj.optString("website"));
                articalsModel.setWebsiteurl(obj.optString("websiteurl"));
                articalsModel.setCreatedat(obj.optString("date"));
                resultIntent = new Intent(ctx, RecommendReadingsActivity.class);
                resultIntent.putExtra("from_notifications",true);
                resultIntent.putExtra("from","card");
                resultIntent.putExtra("article",articalsModel);
                // Adds the back stack for the Intent (but not the Intent itself).
                stackBuilder.addParentStack(RecommendReadingsActivity.class);
            }
            else if (notification_type.equalsIgnoreCase("reminder")){
                HomeCardModel cardModel = new HomeCardModel();
                cardModel.setUserid(obj.optString("userid").trim());
                cardModel.setTitle(obj.optString("body"));
                cardModel.setDescription(obj.optString("content"));
                cardModel.setTime(obj.optString("time"));
                cardModel.setImage_url(obj.optString("main_picture"));
                cardModel.setC_id(obj.optString("n_id").trim());
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                try {
                    cardModel.setDate(sdf.format(sdf.parse(obj.optString("date"))));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                cardModel.setRemind_before(obj.optString("remind_before"));
                cardModel.setCard_type(notification_type);
                resultIntent = new Intent(ctx, DetailCalenderRecordActivity.class);
                resultIntent.putExtra("comingfrom", "card");
                resultIntent.putExtra("from_notifications",true);
                resultIntent.putExtra(AppConstants.EXTRA_HEALTH_RECORD, (Serializable) cardModel);
                stackBuilder.addParentStack(DetailCalenderRecordActivity.class);
            }
            else {
                resultIntent = new Intent(ctx, HomeActivity.class);
                resultIntent.putExtra("from_notifications",true);
                resultIntent.putExtra("n_id",obj.optString("n_id").trim());
                resultIntent.putExtra("userID",obj.optString("userid").trim());
                resultIntent.putExtra("notif_type",notification_type);
                // Adds the back stack for the Intent (but not the Intent itself).
                stackBuilder.addParentStack(HomeActivity.class);
            }
        }
        else {
            resultIntent = new Intent(ctx, HomeActivity.class);
            // Adds the back stack for the Intent (but not the Intent itself).
            stackBuilder.addParentStack(HomeActivity.class);
        }






        // Adds the Intent that starts the Activity to the top of the stack.
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        Bitmap icon = BitmapFactory.decodeResource(ctx.getResources(),
                R.drawable.savika_icon);
        Notification noti = new NotificationCompat.Builder(ctx)
                .setSmallIcon(R.drawable.notifications_small_icon)
                .setAutoCancel(true)
                .setLargeIcon(icon)
                .setContentIntent(resultPendingIntent)
                .setContentTitle(title)
                .setContentText(msg)
                .setStyle(notiStyle).setPriority(Notification.PRIORITY_MAX).build();


        noti.defaults |= Notification.DEFAULT_LIGHTS;
        //noti.defaults |= Notification.DEFAULT_VIBRATE;
        noti.defaults |= Notification.DEFAULT_SOUND;
        noti.flags |= Notification.FLAG_ONLY_ALERT_ONCE;
        int notify_id = (int) System.currentTimeMillis();
        mNotificationManager.notify(notify_id, noti);
    }


}
