package com.savika.firebase;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.iid.FirebaseInstanceId;
import com.savika.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Naresh Ravva on 21/08/17.
 */

public class TestFirebase extends AppCompatActivity {

    @BindView(R.id.tvId)
    TextView tvId;

    @BindView(R.id.btnShare)
    Button btnShare;

    private String TAG = this.getClass().getSimpleName();


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_firebase);

        ButterKnife.bind(this);

        updateFirebaseRegID();

        btnShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, tvId.getText().toString());
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
            }
        });
    }


    /**
     * Reg with firebase get fb token for push notifications
     */
    private void updateFirebaseRegID() {
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();

        Log.d(TAG, "refreshedToken-->" + refreshedToken);

        tvId.setText(refreshedToken);
    }
}
