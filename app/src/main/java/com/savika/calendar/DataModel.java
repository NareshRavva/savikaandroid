package com.savika.calendar;

/*
 * Created by Naresh Ravva on 15/10/17.
 */

public class DataModel {
    private boolean isSelected;
    private String data;

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
