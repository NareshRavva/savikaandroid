package com.savika.calendar;

/*
 * Created by Naresh Ravva on 11/11/17.
 */

public class CalendarRecordUpdateReqModel {


    private String calender_type, recordid, userid, childid, title, milestoneid, milestonehappened, date,
            description, media_url, add_to_memory, time, remind_before, type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getRemind_before() {
        return remind_before;
    }

    public void setRemind_before(String remind_before) {
        this.remind_before = remind_before;
    }

    public String getMilestonehappened() {
        return milestonehappened;
    }

    public void setMilestonehappened(String milestonehappened) {
        this.milestonehappened = milestonehappened;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getCalender_type() {
        return calender_type;
    }

    public void setCalender_type(String calender_type) {
        this.calender_type = calender_type;
    }

    public String getRecordid() {
        return recordid;
    }

    public void setRecordid(String recordid) {
        this.recordid = recordid;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getChildid() {
        return childid;
    }

    public void setChildid(String childid) {
        this.childid = childid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMilestoneid() {
        return milestoneid;
    }

    public void setMilestoneid(String milestoneid) {
        this.milestoneid = milestoneid;
    }

    public String getMilestonehappend() {
        return milestonehappened;
    }

    public void setMilestonehappend(String milestonehappend) {
        this.milestonehappened = milestonehappend;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMedia_url() {
        return media_url;
    }

    public void setMedia_url(String media_url) {
        this.media_url = media_url;
    }

    public String getAdd_to_memory() {
        return add_to_memory;
    }

    public void setAdd_to_memory(String add_to_memory) {
        this.add_to_memory = add_to_memory;
    }
}
