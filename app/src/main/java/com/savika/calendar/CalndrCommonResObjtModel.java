package com.savika.calendar;

import java.io.Serializable;

/*
 * Created by Naresh Ravva on 09/11/17.
 */

public class CalndrCommonResObjtModel implements Serializable {

    private String id, userid, childid, title, date, description, media_url, milestone_id, calender_type,
            milestoneid, time, remind_before, rangefrom, rangeto, type, username, add_to_memory,
            milestonehappened, week_range, milestonedesc;

    public String getRangefrom() {
        return rangefrom;
    }

    public void setRangefrom(String rangefrom) {
        this.rangefrom = rangefrom;
    }

    public String getRangeto() {
        return rangeto;
    }

    public void setRangeto(String rangeto) {
        this.rangeto = rangeto;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
    public String getAdd_to_memory() {
        return add_to_memory;
    }

    public void setAdd_to_memory(String add_to_memory) {
        this.add_to_memory = add_to_memory;
    }

    public String getMilestonehappened() {
        return milestonehappened;
    }

    public void setMilestonehappened(String milestonehappened) {
        this.milestonehappened = milestonehappened;
    }


    public String getWeek_range() {
        return week_range;
    }

    public void setWeek_range(String week_range) {
        this.week_range = week_range;
    }



    public String getMilestonedesc() {
        return milestonedesc;
    }

    public void setMilestonedesc(String milestonedesc) {
        this.milestonedesc = milestonedesc;
    }

    public String getMilestoneid() {
        return milestoneid;
    }

    public void setMilestoneid(String milestoneid) {
        this.milestoneid = milestoneid;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getRemind_before() {
        return remind_before;
    }

    public void setRemind_before(String remind_before) {
        this.remind_before = remind_before;
    }

    public String getCalender_type() {
        return calender_type;
    }

    public void setCalender_type(String calender_type) {
        this.calender_type = calender_type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getChildid() {
        return childid;
    }

    public void setChildid(String childid) {
        this.childid = childid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMedia_url() {
        return media_url;
    }

    public void setMedia_url(String media_url) {
        this.media_url = media_url;
    }

    public String getMilestone_id() {
        return milestone_id;
    }

    public void setMilestone_id(String milestone_id) {
        this.milestone_id = milestone_id;
    }
}
