package com.savika.calendar.milestones;

import com.google.gson.annotations.SerializedName;
import com.savika.trackHealthRecords.HRDocTypeObjtModel;

import java.util.ArrayList;

/*
 * Created by nexivo on 8/11/17.
 */

public class MilestoneResponseModel {

    private boolean success;

    @SerializedName("milestones_list")
    private ArrayList<MilestoneObjModel> arrMilestoneDocTypes = new ArrayList<>();


    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public ArrayList<MilestoneObjModel> getArrHealthDocTypes() {
        return arrMilestoneDocTypes;
    }

    public void setArrHealthDocTypes(ArrayList<MilestoneObjModel> arrMilestoneDocTypes) {
        this.arrMilestoneDocTypes = arrMilestoneDocTypes;
    }
}
