package com.savika.calendar.milestones;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.savika.R;
import com.savika.calendar.CalendarActivity;
import com.savika.calendar.DetailCalenderRecordActivity;
import com.savika.constants.AppConstants;

import java.util.ArrayList;

/*
 * Created by nexivo on 9/11/17.
 */

public class MileStoneRecordsAdapter extends RecyclerView.Adapter<MileStoneRecordsAdapter.ViewHolder> {
    private LayoutInflater mInflater;
    private MileStoneRecordsAdapter.ItemClickListener mClickListener;
    private CalendarActivity context;
    private int selectedPos = 0;

    private ArrayList<AddMLReqModel> arrData = new ArrayList<>();

    public MileStoneRecordsAdapter(CalendarActivity context, ArrayList<AddMLReqModel> arrData) {
        this.mInflater = LayoutInflater.from(context);
        this.arrData = arrData;
        this.context = context;

        selectedPos = arrData.size() - 1;
    }

    @Override
    public MileStoneRecordsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.list_item_calender_record, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MileStoneRecordsAdapter.ViewHolder holder, int position) {
        final AddMLReqModel model = arrData.get(position);

        holder.tvDocTitle.setText(model.getMilestonedesc());

        holder.tvDesc.setText(model.getMLdesc());
        holder.tvMonth.setText(model.getDate()!=null?CalendarActivity.arr_months[Integer.parseInt(model.getDate().split("-")[1])-1]:"week");
        holder.tvDate.setText(model.getDate()!=null?model.getDate().split("-")[2]:model.getWeek_range());
//        if (model.getMedia_url() != null && model.getMedia_url().length() > 15) {
//            ImageUtil.LoadPicaso(context, holder.ivProfile, model.getMedia_url());
//            holder.ivProfile.setPadding(0, 0, 0, 0);
//        } else {
//            holder.ivProfile.setImageResource(R.drawable.no_img);
//            if (model.getCalender_type().equalsIgnoreCase(AppConstants.MILESTONES)) {
//                holder.ivProfile.setImageResource(R.drawable.calndr_memories);
//            }
//            int valueInPixels = (int) context.getResources().getDimension(R.dimen.profile_lables_size);
//            holder.ivProfile.setPadding(valueInPixels, valueInPixels, valueInPixels, valueInPixels);
//        }
        holder.record_det.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent detail_screen = new Intent(context, DetailCalenderRecordActivity.class);
                detail_screen.putExtra("comingfrom","milestone");
                detail_screen.putExtra(AppConstants.EXTRA_HEALTH_RECORD, model);
                context.startActivityForResult(detail_screen,13);
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrData.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvDocTitle, tvDoctorName, tvDate, tvMonth, tvDesc;
        public ImageView ivMore;
        //ivProfile;
        FrameLayout record_det;

        public ViewHolder(View itemView) {
            super(itemView);
            tvDocTitle = itemView.findViewById(R.id.tvName);
            tvDesc = itemView.findViewById(R.id.tvDob);
            ivMore = itemView.findViewById(R.id.imgMore);
            tvDate = itemView.findViewById(R.id.item_cal_date);
            ivMore.setOnClickListener(this);
            tvMonth = itemView.findViewById(R.id.item_cal_month);
            //ivProfile = (ImageView) itemView.findViewById(R.id.ivProfile);

            record_det = itemView.findViewById(R.id.record_det);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) {
                mClickListener.onItemClick(view, arrData.get(getAdapterPosition()).getId(), arrData.get(getAdapterPosition()).getCalender_type());

                selectedPos = getAdapterPosition();

                notifyDataSetChanged();
            }
        }
    }

    public AddMLReqModel getItem(int id) {
        return arrData.get(id);
    }

    public void setClickListener(MileStoneRecordsAdapter.ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    public interface ItemClickListener {
        void onItemClick(View view, String ID, String type);
    }
}
