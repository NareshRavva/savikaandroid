package com.savika.calendar.milestones;
/*
 * Created by nexivo on 8/11/17.
 */

public class MilestoneObjModel {
    private String id;
    private String milestonedesc;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getHealthdocdesc() {
        return milestonedesc;
    }

    public void setHealthdocdesc(String healthdocdesc) {
        this.milestonedesc = healthdocdesc;
    }
}
