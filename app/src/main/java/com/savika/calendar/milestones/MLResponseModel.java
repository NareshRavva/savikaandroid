package com.savika.calendar.milestones;

import com.google.gson.annotations.SerializedName;
import com.savika.trackHealthRecords.AddHRReqModel;

import java.util.ArrayList;

/*
 * Created by nexivo on 9/11/17.
 */

public class MLResponseModel {
    private boolean success;

    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @SerializedName("milestone_details")
    private ArrayList<AddMLReqModel> arr_health_records = new ArrayList<>();

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public ArrayList<AddMLReqModel> getArr_health_records() {
        return arr_health_records;
    }

    public void setArr_health_records(ArrayList<AddMLReqModel> arr_health_records) {
        this.arr_health_records = arr_health_records;
    }
}
