package com.savika.calendar.milestones;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
/**
 * Created by nexivo on 9/11/17.
 */

public class AddMLReqModel implements Serializable {

    private String id, userid, milestoneid, title, date, add_to_memory, media_url, username, type, childid,
            description, calender_type, milestonedesc, time, remind_before, milestonehappened, week_range;

    public String getWeek_range() {
        return week_range;
    }

    public void setWeek_range(String week_range) {
        this.week_range = week_range;
    }

    public String getMilestonehappened() {
        return milestonehappened;
    }

    public void setMilestonehappened(String milestonehappened) {
        this.milestonehappened = milestonehappened;
    }

    public String getRemind_before() {
        return remind_before;
    }

    public void setRemind_before(String remind_before) {
        this.remind_before = remind_before;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setMilestonedesc(String milestonedesc) {
        this.milestonedesc = milestonedesc;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public void setmilestonedesc(String milestonedesc) {
        this.milestonedesc = milestonedesc;
    }

    public String getMilestonedesc() {
        return milestonedesc;
    }

    public String getMilestoneid() {
        return milestoneid;
    }

    public void setMilestoneid(String milestoneid) {
        this.milestoneid = milestoneid;
    }

    public String getMLdesc() {
        return description;
    }

    public void setMLdesc(String description) {
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getChildid() {
        return childid;
    }

    public void setChildid(String childid) {
        this.childid = childid;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    public String getAdd_to_memory() {
        return add_to_memory;
    }

    public void setAdd_to_memory(String add_to_memory) {
        this.add_to_memory = add_to_memory;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCalender_type() {
        return calender_type;
    }

    public void setCalender_type(String calender_type) {
        this.calender_type = calender_type;
    }

    public String getMedia_url() {
        return media_url;
    }

    public void setMedia_url(String docurl) {
        this.media_url = docurl;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

}
