package com.savika.calendar;

import com.google.gson.annotations.SerializedName;
import com.savika.calendar.CalndrCommonResObjtModel;
import com.savika.calendar.milestones.MilestoneObjModel;

import java.util.ArrayList;

/*
 * Created by nexivo on 8/11/17.
 */

public class CalndrCommonResModel {

    private boolean success;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    private String message;

    public ArrayList<CalndrCommonResObjtModel> getArrMilestones() {
        return arrMilestones;
    }

    public void setArrMilestones(ArrayList<CalndrCommonResObjtModel> arrMilestones) {
        this.arrMilestones = arrMilestones;
    }

    @SerializedName("milestone_details")
    private ArrayList<CalndrCommonResObjtModel> arrMilestones = new ArrayList<>();

    @SerializedName("memory_details")
    private ArrayList<CalndrCommonResObjtModel> arrMemories = new ArrayList<>();

    @SerializedName("note_details")
    private ArrayList<CalndrCommonResObjtModel> arrNotes = new ArrayList<>();

    @SerializedName("reminder_details")
    private ArrayList<CalndrCommonResObjtModel> arrReminders = new ArrayList<>();

    public ArrayList<CalndrCommonResObjtModel> getAllrecords() {
        return allrecords;
    }

    public void setAllrecords(ArrayList<CalndrCommonResObjtModel> allrecords) {
        this.allrecords = allrecords;
    }

    @SerializedName("values")
    private ArrayList<CalndrCommonResObjtModel> allrecords = new ArrayList<>();

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public ArrayList<CalndrCommonResObjtModel> getArrMemories() {
        return arrMemories;
    }

    public void setArrMemories(ArrayList<CalndrCommonResObjtModel> arrMemories) {
        this.arrMemories = arrMemories;
    }

    public ArrayList<CalndrCommonResObjtModel> getArrNotes() {
        return arrNotes;
    }

    public void setArrNotes(ArrayList<CalndrCommonResObjtModel> arrNotes) {
        this.arrNotes = arrNotes;
    }

    public ArrayList<CalndrCommonResObjtModel> getArrReminders() {
        return arrReminders;
    }

    public void setArrReminders(ArrayList<CalndrCommonResObjtModel> arrReminders) {
        this.arrReminders = arrReminders;
    }
}
