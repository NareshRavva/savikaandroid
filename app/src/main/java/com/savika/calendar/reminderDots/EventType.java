package com.savika.calendar.reminderDots;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/*
 * Created by Naresh Ravva on 09/11/17.
 */

public class EventType {

    @SerializedName("milestones")
    private ArrayList<EventsTypeObjtModel> arrMilestones = new ArrayList<>();

    private @SerializedName("memories")
    ArrayList<EventsTypeObjtModel> arrMemories = new ArrayList<>();

    @SerializedName("notes")
    private ArrayList<EventsTypeObjtModel> arrNotes = new ArrayList<>();

    @SerializedName("remainders")
    private ArrayList<EventsTypeObjtModel> arrRemainders = new ArrayList<>();

    public ArrayList<EventsTypeObjtModel> getArrMilestones() {
        return arrMilestones;
    }

    public void setArrMilestones(ArrayList<EventsTypeObjtModel> arrMilestones) {
        this.arrMilestones = arrMilestones;
    }

    public ArrayList<EventsTypeObjtModel> getArrMemories() {
        return arrMemories;
    }

    public void setArrMemories(ArrayList<EventsTypeObjtModel> arrMemories) {
        this.arrMemories = arrMemories;
    }

    public ArrayList<EventsTypeObjtModel> getArrNotes() {
        return arrNotes;
    }

    public void setArrNotes(ArrayList<EventsTypeObjtModel> arrNotes) {
        this.arrNotes = arrNotes;
    }

    public ArrayList<EventsTypeObjtModel> getArrRemainders() {
        return arrRemainders;
    }

    public void setArrRemainders(ArrayList<EventsTypeObjtModel> arrRemainders) {
        this.arrRemainders = arrRemainders;
    }
}
