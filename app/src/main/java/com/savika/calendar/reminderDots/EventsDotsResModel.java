package com.savika.calendar.reminderDots;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/*
 * Created by Naresh Ravva on 09/11/17.
 */

public class EventsDotsResModel {

    private boolean success;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    private String message;

    @SerializedName("calendar_remainders")
    private ArrayList<EventType> arrCalendar_remainders = new ArrayList<>();

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public ArrayList<EventType> getArrCalendar_remainders() {
        return arrCalendar_remainders;
    }

    public void setArrCalendar_remainders(ArrayList<EventType> arrCalendar_remainders) {
        this.arrCalendar_remainders = arrCalendar_remainders;
    }
}




