package com.savika.calendar.reminderDots;

/*
 * Created by Naresh Ravva on 09/11/17.
 */

public class EventsTypeObjtModel {

    private String id, date;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
