package com.savika.calendar;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.savika.R;
import com.savika.constants.AppConstants;
import com.savika.utils.AppUtils;
import com.savika.utils.ImageUtil;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/*
 * Created by nexivo on 9/11/17.
 */

public class CalndrCommonRecordsAdapter extends RecyclerView.Adapter<CalndrCommonRecordsAdapter.ViewHolder> {
    private LayoutInflater mInflater;
    private CalndrCommonRecordsAdapter.ItemClickListener mClickListener;
    private CalendarActivity context;
    private int selectedPos = 0;
    private String all;

    private ArrayList<CalndrCommonResObjtModel> arrData = new ArrayList<>();

    public CalndrCommonRecordsAdapter(CalendarActivity context, ArrayList<CalndrCommonResObjtModel> arrData) {
        this.mInflater = LayoutInflater.from(context);
        this.arrData = arrData;
        this.context = context;

        selectedPos = arrData.size() - 1;
    }

    public CalndrCommonRecordsAdapter(CalendarActivity context, ArrayList<CalndrCommonResObjtModel> arrData, String all) {
        this.mInflater = LayoutInflater.from(context);
        this.arrData = arrData;
        this.context = context;
        this.all = all;
        selectedPos = arrData.size() - 1;
    }

    @Override
    public CalndrCommonRecordsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.list_item_calender_record, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CalndrCommonRecordsAdapter.ViewHolder holder, int position) {
        final CalndrCommonResObjtModel model = arrData.get(position);


        if (model.getMilestonedesc()!=null){
            holder.tvDocTitle.setText(model.getMilestonedesc());
        }
        else {
            holder.tvDocTitle.setText(model.getTitle());
        }

        holder.tvDesc.setText(model.getDescription());
        holder.tvMonth.setText(model.getDate()!=null?CalendarActivity.arr_months[Integer.parseInt(model.getDate().split("-")[1])-1]:"week");
        holder.tvDate.setText(model.getDate()!=null?model.getDate().split("-")[2]:model.getRangefrom()+"-"+model.getRangeto());
        if (model.getCalender_type().equalsIgnoreCase(AppConstants.MILESTONES)){
            holder.ivMore.setVisibility(View.VISIBLE);
            if (model.getMilestonehappened().equalsIgnoreCase("completed"))
                holder.ivMore.setImageResource(R.drawable.milestonehappendyes);
            else holder.ivMore.setImageResource(R.drawable.milestonehappendno);
        }
        if (model.getCalender_type().equalsIgnoreCase(AppConstants.REMINDERS)){
            holder.tvTime.setVisibility(View.VISIBLE);
            holder.tvTime.setText(AppUtils.get12HRSTime(model.getTime().split(":")[0]+":"+model.getTime().split(":")[1]));
        }
        if (all!=null && all.equalsIgnoreCase(AppConstants.ALL)){

            if (model.getCalender_type().equalsIgnoreCase(AppConstants.MILESTONES)){
                holder.border.setBackgroundColor(context.getResources().getColor(R.color.calnder_blue));
            }
            else if (model.getCalender_type().equalsIgnoreCase(AppConstants.MEMORIES)){
                holder.border.setBackgroundColor(context.getResources().getColor(R.color.calnder_green));
            }
            else if (model.getCalender_type().equalsIgnoreCase(AppConstants.NOTES)){
                holder.border.setBackgroundColor(context.getResources().getColor(R.color.calnder_yelw));
            }
            else if (model.getCalender_type().equalsIgnoreCase(AppConstants.REMINDERS)){
                holder.border.setBackgroundColor(context.getResources().getColor(R.color.calnder_red));
            }
            holder.border.setVisibility(View.VISIBLE);
        }
        holder.record_det.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent detail_screen = new Intent(context, DetailCalenderRecordActivity.class);
                detail_screen.putExtra("comingfrom","common");
                detail_screen.putExtra(AppConstants.EXTRA_HEALTH_RECORD, model);
                context.startActivityForResult(detail_screen,13);
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrData.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvDocTitle, tvDoctorName, tvDate, tvTime, tvDesc;
        TextView tvMonth, btnSendMail;
        public ImageView ivMore;
        View border;
//                ivProfile;
        FrameLayout record_det;

        public ViewHolder(View itemView) {
            super(itemView);
            tvDocTitle = itemView.findViewById(R.id.tvName);
            tvDesc = itemView.findViewById(R.id.tvDob);
            ivMore = itemView.findViewById(R.id.imgMore);
            ivMore.setOnClickListener(this);
            tvDate = itemView.findViewById(R.id.item_cal_date);
            tvMonth = itemView.findViewById(R.id.item_cal_month);
            tvTime = itemView.findViewById(R.id.item_cal_time);
//            ivProfile = (ImageView) itemView.findViewById(R.id.ivProfile);
            record_det = itemView.findViewById(R.id.record_det);
            border = itemView.findViewById(R.id.allborder);

        }

        @Override
        public void onClick(View view) {
                if (mClickListener != null) {
                    mClickListener.onItemClick(view, arrData.get(getAdapterPosition()));

                    selectedPos = getAdapterPosition();

                    notifyDataSetChanged();
                }
        }
    }

    public CalndrCommonResObjtModel getItem(int id) {
        return arrData.get(id);
    }

    public void setClickListener(CalndrCommonRecordsAdapter.ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    public interface ItemClickListener {
        void onItemClick(View view, CalndrCommonResObjtModel model);
    }
}
