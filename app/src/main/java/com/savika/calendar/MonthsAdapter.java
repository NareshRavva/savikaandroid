package com.savika.calendar;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.savika.R;

import java.util.ArrayList;

/*
 * Created by Naresh Ravva on 30/06/17.
 */

public class MonthsAdapter extends RecyclerView.Adapter<MonthsAdapter.ViewHolder> {

    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    private Context context;
    private int selectedPos = 0;

    private ArrayList<DataModel> arrData = new ArrayList<>();

    public MonthsAdapter(Context context, ArrayList<DataModel> arrData) {
        this.mInflater = LayoutInflater.from(context);
        this.arrData = arrData;
        this.context = context;

        selectedPos = arrData.size() - 1;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.list_item_calendar_month, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        DataModel model = arrData.get(position);

        holder.tvTitle.setText(model.getData());

        if (model.isSelected() == true) {
            holder.tvTitle.setTextColor(Color.parseColor("#c27a9f"));
            holder.tvTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.calndr_label_selected_txt_size));
            setfontStyleAsBold(holder.tvTitle);
        } else {
            holder.tvTitle.setTextColor(Color.parseColor("#636363"));
            holder.tvTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.calndr_label_txt_size));
            setfontStyleAsMedium(holder.tvTitle);
        }

        //Logger.info("TAG", "Reading-->" + common_model.getData());
    }

    /*
     * @param tvTitle
     */
    private void setfontStyleAsBold(TextView tvTitle) {
        Typeface face = Typeface.createFromAsset(context.getAssets(),
                "fonts/Quicksand-Bold.ttf");

        tvTitle.setTypeface(face);
    }

    /*
     * @param tvTitle
     */
    private void setfontStyleAsMedium(TextView tvTitle) {
        Typeface face = Typeface.createFromAsset(context.getAssets(),
                "fonts/Quicksand-Medium.ttf");

        tvTitle.setTypeface(face);
    }

    @Override
    public int getItemCount() {
        return arrData.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView tvTitle;

        public ViewHolder(View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.tvTitle);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) {
                mClickListener.onItemClick(view, arrData.get(getAdapterPosition()).getData(), 0);

                selectedPos = getAdapterPosition();

                notifyDataSetChanged();
            }
        }
    }

    public DataModel getItem(int id) {
        return arrData.get(id);
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    public interface ItemClickListener {
        void onItemClick(View view, String position, int Type);
    }
}