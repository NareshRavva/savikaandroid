package com.savika.calendar;

import android.graphics.Color;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.savika.R;
import com.savika.calendar.memories.AddMemoriesFragment;
import com.savika.calendar.milestones.AddMilestonesFragment;
import com.savika.calendar.notes.AddNotesFragment;
import com.savika.calendar.reminder.AddReminderFragment;
import com.savika.constants.AppConstants;
import com.savika.homeScreen.cards.HomeCardModel;
import com.savika.logger.Logger;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AddCalenderHealthRecord extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.llBack)
    LinearLayout llBack;

    @BindView(R.id.addMilestones)
    LinearLayout addMilestone;

    @BindView(R.id.addMemories)
    LinearLayout addMemories;

    @BindView(R.id.addNotes)
    LinearLayout addNotes;

    @BindView(R.id.addReminders)
    LinearLayout addReminders;

    @BindView(R.id.tvMilestones)
    TextView tvMilestones;

    @BindView(R.id.tvMemories)
    TextView tvMemories;

    @BindView(R.id.tvNotes)
    TextView tvNotes;

    @BindView(R.id.tvReminders)
    TextView tvReminders;

    @BindView(R.id.tvHeaderTitle)
    TextView tvHeaderTitle;

    @BindView(R.id.cvLables)
    CardView fragments_card;

    String TAG = this.getClass().getSimpleName();

    CalndrCommonResObjtModel common_model;
    HomeCardModel card_model;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_health_record2);

        ButterKnife.bind(this);

        llBack.setOnClickListener(this);
        addMilestone.setOnClickListener(this);
        addMemories.setOnClickListener(this);
        addNotes.setOnClickListener(this);
        addReminders.setOnClickListener(this);

        updateCardsMilesStoneCards(tvMilestones, addMilestone);

        common_model = (CalndrCommonResObjtModel) getIntent().getSerializableExtra(AppConstants.CALNDAR_MODEL_2);
        card_model = (HomeCardModel) getIntent().getSerializableExtra("cards");
        fragments_card.setVisibility(View.VISIBLE);
        if (getIntent().getStringExtra(AppConstants.CALNDAR_TYPE) != null) {
            String addtype = getIntent().getStringExtra(AppConstants.CALNDAR_TYPE);

            Logger.info(TAG, "addtype-->" + addtype);

            if (addtype.equalsIgnoreCase(AppConstants.MILESTONES)) {
              if (card_model!=null){
                    tvHeaderTitle.setText(AppConstants.EDIT_MILESTONE);
                    fragments_card.setVisibility(View.GONE);
                    showFragment(AddMilestonesFragment.newInstance( null,card_model));
                }
                else if (common_model != null){
                    tvHeaderTitle.setText(AppConstants.EDIT_MILESTONE);
                    fragments_card.setVisibility(View.GONE);
                    showFragment(AddMilestonesFragment.newInstance(common_model,null));
                }
                else {
                    tvHeaderTitle.setText(AppConstants.ADD_MILESTONE);
                    showFragment(AddMilestonesFragment.newInstance( common_model,null));
                }
                updateCardsMilesStoneCards(tvMilestones, addMilestone);
            } else if (addtype.equalsIgnoreCase(AppConstants.MEMORIES)) {
                if (common_model!=null){
                    tvHeaderTitle.setText(AppConstants.EDIT_MEMORIES);
                    fragments_card.setVisibility(View.GONE);
                    showFragment(AddMemoriesFragment.newInstance(common_model));
                }else {
                    tvHeaderTitle.setText(AppConstants.ADD_MEMORIES);
                    showFragment(AddMemoriesFragment.newInstance(common_model));
                }
                updateCardsMilesStoneCards(tvMemories, addMemories);
            } else if (addtype.equalsIgnoreCase(AppConstants.NOTES)) {
                if (common_model!=null){
                    tvHeaderTitle.setText(AppConstants.EDIT_NOTES);
                    fragments_card.setVisibility(View.GONE);
                    showFragment(AddNotesFragment.newInstance(common_model));
                }else {
                    tvHeaderTitle.setText(AppConstants.ADD_NOTES);
                    showFragment(AddNotesFragment.newInstance(common_model));
                }
                updateCardsMilesStoneCards(tvNotes, addNotes);
            } else if (addtype.equalsIgnoreCase(AppConstants.REMINDERS)) {
                showFragment(AddReminderFragment.newInstance(common_model,null));
                if (common_model!=null){
                    tvHeaderTitle.setText(AppConstants.EDIT_REMINDERS);
                    fragments_card.setVisibility(View.GONE);
                    showFragment(AddReminderFragment.newInstance(common_model,null));
                }else if (card_model!=null){
                    tvHeaderTitle.setText(AppConstants.EDIT_REMINDERS);
                    fragments_card.setVisibility(View.GONE);
                    showFragment(AddReminderFragment.newInstance(common_model,card_model));
//                    showFragment(new AddReminderFragment(common_model,card_model));
                }
                else {
                    tvHeaderTitle.setText(AppConstants.ADD_REMINDERS);
                    showFragment(AddReminderFragment.newInstance(common_model,null));
                }
                updateCardsMilesStoneCards(tvReminders, addReminders);
            }
        } else {
            showFragment(AddMilestonesFragment.newInstance( common_model,null));
            updateCardsMilesStoneCards(tvMilestones, addMilestone);
        }
    }

    private void showFragment(Fragment fragment) {
        String TAG = fragment.getClass().getSimpleName();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.mainContainer, fragment, TAG);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commitAllowingStateLoss();
    }

    protected void backstackFragment() {
        Log.d("Stack count", getSupportFragmentManager().getBackStackEntryCount() + "");
        if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
            finish();
        }
        getSupportFragmentManager().popBackStack();
        removeCurrentFragment();
    }

    private void removeCurrentFragment() {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        Fragment currentFrag = getSupportFragmentManager()
                .findFragmentById(R.id.mainContainer);

        if (currentFrag != null) {
            transaction.remove(currentFrag);
        }
        transaction.commitAllowingStateLoss();
    }

    private void updateCardsMilesStoneCards(TextView tvText, LinearLayout llLayout) {
        addMilestone.setBackgroundColor(getResources().getColor(R.color.white));
        addMemories.setBackgroundColor(getResources().getColor(R.color.white));
        addReminders.setBackgroundColor(getResources().getColor(R.color.white));
        addNotes.setBackgroundColor(getResources().getColor(R.color.white));

        tvMilestones.setTextColor(Color.BLACK);
        tvMemories.setTextColor(Color.BLACK);
        tvNotes.setTextColor(Color.BLACK);
        tvReminders.setTextColor(Color.BLACK);

        tvText.setTextColor(Color.WHITE);

        if (tvText == tvMilestones) {
            llLayout.setBackgroundColor(getResources().getColor(R.color.calnder_blue));
        } else if (tvText == tvMemories) {
            llLayout.setBackgroundColor(getResources().getColor(R.color.calnder_green));
        } else if (tvText == tvNotes) {
            llLayout.setBackgroundColor(getResources().getColor(R.color.calnder_yelw));
        } else if (tvText == tvReminders) {
            llLayout.setBackgroundColor(getResources().getColor(R.color.calnder_red));
        }

    }


    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.llBack:
                finish();
                break;
            case R.id.addMilestones:
                backstackFragment();
                updateCardsMilesStoneCards(tvMilestones, addMilestone);
                tvHeaderTitle.setText(AppConstants.ADD_MILESTONE);
                showFragment(AddMilestonesFragment.newInstance(null, null));
                break;
            case R.id.addMemories:
                backstackFragment();
                updateCardsMilesStoneCards(tvMemories, addMemories);
                tvHeaderTitle.setText(AppConstants.ADD_MEMORIES);
                showFragment(AddMemoriesFragment.newInstance(null));
                break;
            case R.id.addNotes:
                backstackFragment();
                updateCardsMilesStoneCards(tvNotes, addNotes);
                tvHeaderTitle.setText(AppConstants.ADD_NOTES);
                showFragment(AddNotesFragment.newInstance(null));
                break;
            case R.id.addReminders:
                backstackFragment();
                updateCardsMilesStoneCards(tvReminders, addReminders);
                tvHeaderTitle.setText(AppConstants.ADD_REMINDERS);
                showFragment(AddReminderFragment.newInstance(null,null));
                break;
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
            finish();
        }
        else {
            finish();
        }
    }

}
