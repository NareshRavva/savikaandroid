package com.savika.calendar;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.savika.Api.ApiClient;
import com.savika.Api.ApiInterface;
import com.savika.Api.CommonResModel;
import com.savika.R;
import com.savika.amAMother.ChildModel;
import com.savika.calendar.milestones.AddMLReqModel;
import com.savika.calendar.milestones.MLResponseModel;
import com.savika.calendar.milestones.MileStoneRecordsAdapter;
import com.savika.calendar.reminderDots.EventsDotsResModel;
import com.savika.calendar.reminderDots.EventsTypeObjtModel;
import com.savika.constants.AppConstants;
import com.savika.constants.GAConstants;
import com.savika.dialogUtils.AlertDialogUtility;
import com.savika.dialogUtils.DatePicketDialog;
import com.savika.googleAnalytics.GoogleAnalyticsHelper;
import com.savika.homeScreen.cards.HomeCardModel;
import com.savika.logger.Logger;
import com.savika.utils.AppUtils;
import com.savika.utils.SharedPrefsUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;

/*
 * Created by Naresh Ravva on 15/10/17.
 */

public class CalendarActivity extends AppCompatActivity implements YearsAdapter.ItemClickListener,
        MonthsAdapter.ItemClickListener, View.OnClickListener, CalndrCommonRecordsAdapter.ItemClickListener {

    @BindView(R.id.calendar)
    GridView calendarView;

    @BindView(R.id.rvYears)
    RecyclerView rvYears;

    @BindView(R.id.rvMonths)
    RecyclerView rvMonths;

    @BindView(R.id.tvHeaderTitle)
    TextView tvHeaderTitle;

    @BindView(R.id.llBack)
    LinearLayout llBack;

    @BindView(R.id.cvLables)
    CardView cvMotherChildLables;

    @BindView(R.id.tvMother)
    TextView tvMother;

    @BindView(R.id.tvChild1)
    TextView tvChild1;

    @BindView(R.id.tvChild2)
    TextView tvChild2;

    @BindView(R.id.tvNoRecords)
    TextView tvNoRecords;

    @BindView(R.id.tvAll)
    TextView tvAll;

    @BindView(R.id.tvMilestones)
    TextView tvMilestones;

    @BindView(R.id.tvMemories)
    TextView tvMemories;

    @BindView(R.id.tvNotes)
    TextView tvNotes;

    @BindView(R.id.tvReminders)
    TextView tvReminders;

    @BindView(R.id.llAll)
    LinearLayout llAll;

    @BindView(R.id.llMilestones)
    LinearLayout llMilestones;

    @BindView(R.id.llMemories)
    LinearLayout llMemories;

    @BindView(R.id.llNotes)
    LinearLayout llNotes;

    @BindView(R.id.llReminders)
    LinearLayout llReminders;

    @BindView(R.id.calendar_change)
    LinearLayout calendar_change;

    @BindView(R.id.calender_month)
    TextView calender_month;

    @BindView(R.id.ivAddHealthRecrds)
    ImageView ivAddHealthRecrds;

    @BindView(R.id.rvHealthRecords)
    RecyclerView rvHealthRecords;

    @BindView(R.id.day_in_calender)
    TextView day_in_calender;
    String datesel;
    boolean isDateSelected = false;

    ArrayList<EventsTypeObjtModel> arr_milestones, arr_memories, arr_notes, arr_remainders = new ArrayList<>();

    private LinearLayoutManager layoutManager;
    private YearsAdapter years_adapter;
    private MonthsAdapter months_adapter;

    ArrayList<CalndrCommonResObjtModel> ARR_CALNDR_RECORDS_MILESTONES = new ArrayList<>();
    ArrayList<CalndrCommonResObjtModel> ARR_CALNDR_RECORDS_MEMORIES = new ArrayList<>();
    ArrayList<CalndrCommonResObjtModel> ARR_CALNDR_RECORDS_NOTES = new ArrayList<>();
    ArrayList<CalndrCommonResObjtModel> ARR_CALNDR_RECORDS_REMINDERS = new ArrayList<>();
    ArrayList<CalndrCommonResObjtModel> ARR_CALNDR_ALL_RECORDS = new ArrayList<>();
    ArrayList<DataModel> ARR_MONTHS_DATA = new ArrayList<>();
    ArrayList<DataModel> ARR_YEARS_DATA = new ArrayList<>();

    GridCellAdapter grid_cell_adapter;
    MileStoneRecordsAdapter ml_adapter;
    Calendar _calendar;
    private String selectmonth, selectyear;
    private final DateFormat dateFormatter = new DateFormat();
    private static final String dateTemplate = "MMMM yyyy";

    ArrayList<ChildModel> ARR_CHILD_DATA = new ArrayList<>();

    private String SELECTED_DATE;
    private String TYPE;
    private String ADD_TYPE;
    private String CHILD_ID = null;

    private String CALENDAR_TYPE;

    public static String[] arr_months = new String[]{"Jan", "Feb", "Mar", "Apr",
            "May", "Jun", "Jul", "Aug", "Sep",
            "Oct", "Nov", "Dec"};

    private final String[] arr_monthsFull = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};

    String[] arr_days = new String[]{"Sunday", "Monday", "Tuesday", "Wednesday",
            "Thursday", "Friday", "Saturday"};

    LayoutInflater inflater;
    int YEAR, MONTH;

    private String TAG = this.getClass().getSimpleName();

    GoogleAnalyticsHelper mGoogleHelper;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar);

        ButterKnife.bind(this);

        inflater = this.getLayoutInflater();

        TYPE = AppUtils.getMotherOrPregnantType(this);
        SELECTED_DATE = "" + Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
        CALENDAR_TYPE = AppConstants.ALL;

        YEAR = Calendar.getInstance().get(Calendar.YEAR);
        selectyear =  String.valueOf(Calendar.getInstance().get(Calendar.YEAR));
        selectmonth = String.valueOf((Calendar.getInstance().get(Calendar.MONTH)+1));
        Calendar calendar = Calendar.getInstance();
//        calendar.set(Calendar.YEAR, YEAR);
//        calendar.set(Calendar.MONTH, MONTH);
        int maxWeeknumber = getWeeksInaMonth(calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH),calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        setGridHeight(maxWeeknumber);
        calender_month.setText(new SimpleDateFormat("MMM yyyy",Locale.getDefault()).format(Calendar.getInstance().getTime()));
        MONTH = Calendar.getInstance().get(Calendar.MONTH);
        datesel  = YEAR+ "-" + MONTH + "-" + SELECTED_DATE;
        tvHeaderTitle.setText(R.string.calendar);

        initializeCustomCalendar();

        if (SharedPrefsUtils.getMotherORPrgntType(this).equalsIgnoreCase("1")) {
            cvMotherChildLables.setVisibility(View.GONE);
        } else {
            List<String> child = new ArrayList<>((SharedPrefsUtils.getChildIds(this)));
            if (getIntent().getSerializableExtra(AppConstants.EXTRA_CHILD_DATA) != null)
                ARR_CHILD_DATA = (ArrayList<ChildModel>) getIntent().getSerializableExtra(AppConstants.EXTRA_CHILD_DATA);
        }

        llBack.setOnClickListener(this);
        tvMother.setOnClickListener(this);

        llAll.setOnClickListener(this);
        llMilestones.setOnClickListener(this);
        llMemories.setOnClickListener(this);
        llNotes.setOnClickListener(this);
        llReminders.setOnClickListener(this);
        ivAddHealthRecrds.setOnClickListener(this);
        calendar_change.setOnClickListener(this);

        updateCardsActiveInActive();

        tvNoRecords.setVisibility(View.VISIBLE);

        updateCardsMilesStoneCards(tvAll, llAll);

        InitGoogleAnalytics();
        mGoogleHelper.SendScreenNameGA(this, GAConstants.CALENDAR_SCREEN);

    }

    private int getWeeksInaMonth(int year,int month,int no_of_days){
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DATE, 1);
        cal.set(Calendar.MONTH, month);
        cal.set(Calendar.YEAR, year);
        Date firstDayOfMonth = cal.getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("EEEEEEEE",Locale.getDefault());
        System.out.println("First Day of Month: " + sdf.format(firstDayOfMonth));
        cal.set(Calendar.DATE, no_of_days);
        Date firstDayOfMonth1 = cal.getTime();
        String first = sdf.format(firstDayOfMonth);
        String last = sdf.format(firstDayOfMonth1);
        if(first.equalsIgnoreCase("FRI") && last.equalsIgnoreCase("SUN")){
            return 6;
        }
        else if((first.equalsIgnoreCase("SAT") && last.equalsIgnoreCase("SUN"))|| (first.equalsIgnoreCase("SAT") && last.equalsIgnoreCase("MON"))){
            return 6;
        }
        else if(first.equalsIgnoreCase("SUN") && last.equalsIgnoreCase("SATUR")){
            return 4;
        }
        else return 5;
    }

    private void setGridHeight(int maxWeeknumber) {
        if (maxWeeknumber==5){
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, (int) getResources().getDimension(R.dimen._210sdp));
            calendarView.setLayoutParams(params);
        }else if (maxWeeknumber == 6){
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, (int) getResources().getDimension(R.dimen._250sdp));
            calendarView.setLayoutParams(params);
        }
    }

    // Google Analytics
    private void InitGoogleAnalytics() {
        mGoogleHelper = new GoogleAnalyticsHelper();
        mGoogleHelper.init(this);
    }

    /**
     *
     */
    private void updateCardsActiveInActive() {

        int childCount = SharedPrefsUtils.getChildCount(CalendarActivity.this);

        Logger.info(TAG, "childCount-->" + childCount);

        if (childCount == 0) {
            tvChild1.setAlpha(0.3f);
            tvChild2.setAlpha(0.3f);
            tvChild1.setOnClickListener(null);
            tvChild2.setOnClickListener(null);

        } else if (childCount == 1) {
            tvChild2.setAlpha(0.3f);
            tvChild1.setOnClickListener(this);
            tvChild2.setOnClickListener(null);

        } else if (childCount == 2) {
            tvChild1.setOnClickListener(this);
            tvChild2.setOnClickListener(this);
        }
    }


    private void initializeCustomCalendar() {
        _calendar = Calendar.getInstance(Locale.getDefault());
        MONTH = _calendar.get(Calendar.MONTH) + 1;
        YEAR = _calendar.get(Calendar.YEAR);
        Logger.info(TAG, "Calendar " + "Month: " + MONTH + " " + "Year: " + YEAR);


        // Initialised
        grid_cell_adapter = new GridCellAdapter(this, R.id.calendar_day_gridcell, MONTH, YEAR);
        grid_cell_adapter.notifyDataSetChanged();
        calendarView.setAdapter(grid_cell_adapter);
    }

    /*
     * @param month
     * @param year
     */
    private void setGridCellAdapterToDate(int month, int year) {
        grid_cell_adapter = new GridCellAdapter(getApplicationContext(), R.id.calendar_day_gridcell, month + 1, year);
        _calendar.set(year, month - 1, _calendar.get(Calendar.DAY_OF_MONTH));
        //currentMonth.setText(dateFormatter.format(dateTemplate, _calendar.getTime()));
        grid_cell_adapter.notifyDataSetChanged();
        calendarView.setAdapter(grid_cell_adapter);

        MONTH = month + 1;
        YEAR = year;

        getCalndrReminders(TYPE);
    }

    @Override
    public void onItemClick(View view, String data, int Type) {

        Logger.info(TAG, "--selected Item-->" + view);

        SELECTED_DATE = null;

        if (Type == 0) {

            highlightMonth(data);

            for (int i = 0; i < arr_months.length; i++) {
                if (arr_months[i].equalsIgnoreCase(data)) {
                    MONTH = i + 1;
                }
            }

        } else {
            highlightYear(data);
            YEAR = Integer.parseInt(data);
        }

        clear();
        setGridCellAdapterToDate(MONTH, YEAR);
    }

    /*
     * @param data
     */
    private void highlightMonth(String data) {
        for (DataModel model : ARR_MONTHS_DATA) {
            if (data.equalsIgnoreCase(model.getData())) {
                model.setSelected(true);
            } else {
                model.setSelected(false);
            }
        }

        months_adapter.notifyDataSetChanged();
    }

    /*
     * @param data
     */
    private void highlightYear(String data) {
        for (DataModel model : ARR_YEARS_DATA) {
            if (data.equalsIgnoreCase(model.getData())) {
                model.setSelected(true);
            } else {
                model.setSelected(false);
            }
        }

        years_adapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.llBack:
                finish();
                break;

            case R.id.tvMother:
                updateMotherCards(tvMother);
                TYPE = AppConstants.STR_MOTHER;
//                getMLRecords(CALENDAR_TYPE);
                break;

            case R.id.tvChild1:
                if (ARR_CHILD_DATA.size() > 0 && ARR_CHILD_DATA.get(0) != null) {
                    CHILD_ID = ARR_CHILD_DATA.get(0).getChildid();

                    updateMotherCards(tvChild1);
                    TYPE = AppConstants.STR_CHILD;
//                    getMLRecords(CALENDAR_TYPE);
                }
                break;

            case R.id.tvChild2:
                if (ARR_CHILD_DATA.size() > 0 && ARR_CHILD_DATA.get(1) != null) {
                    CHILD_ID = ARR_CHILD_DATA.get(1).getChildid();

                    updateMotherCards(tvChild2);
                    TYPE = AppConstants.STR_CHILD;
//                    getMLRecords(CALENDAR_TYPE);
                }

                break;

            case R.id.llAll:
                updateCardsMilesStoneCards(tvAll, llAll);
                CALENDAR_TYPE = AppConstants.ALL;
                getAllRecords(datesel, selectmonth, selectyear);
                break;

            case R.id.llMilestones:
                updateCardsMilesStoneCards(tvMilestones, llMilestones);
                CALENDAR_TYPE = AppConstants.MILESTONES;
                getMemoriesNotesReminders(CALENDAR_TYPE);
                break;

            case R.id.llMemories:
                updateCardsMilesStoneCards(tvMemories, llMemories);
                CALENDAR_TYPE = AppConstants.MEMORIES;
                getMemoriesNotesReminders(CALENDAR_TYPE);
                break;

            case R.id.llNotes:
                updateCardsMilesStoneCards(tvNotes, llNotes);
                CALENDAR_TYPE = AppConstants.NOTES;
                getMemoriesNotesReminders(CALENDAR_TYPE);
                break;

            case R.id.llReminders:
                updateCardsMilesStoneCards(tvReminders, llReminders);
                CALENDAR_TYPE = AppConstants.REMINDERS;
                getMemoriesNotesReminders(CALENDAR_TYPE);
                break;

            case R.id.ivAddHealthRecrds:
                addHeadlthRecord();
                break;

            case R.id.calendar_change:
                runOnUiThread(new DatePicketDialog(CalendarActivity.this, getString(R.string.select_date), getString(R.string.please_pic_the_date), true, CalendarActivity.this.getLayoutInflater(), new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        calender_month.setText("" + view.getTag());
                        String mstring = "" + view.getTag();
                        String[] month_year = mstring.split(" ");
                        selectyear =  month_year[1];
                        Calendar cal = Calendar.getInstance();
                        cal.set(Calendar.YEAR, Integer.parseInt(selectyear));
                        cal.set(Calendar.MONTH, (Arrays.asList(arr_months).indexOf(month_year[0])));
                        int maxWeeknumber = getWeeksInaMonth(cal.get(Calendar.YEAR),cal.get(Calendar.MONTH),cal.getActualMaximum(Calendar.DAY_OF_MONTH));
                        setGridHeight(maxWeeknumber);
                        isDateSelected = false;
                        selectmonth = String.valueOf((Arrays.asList(arr_months).indexOf(month_year[0])+1));
                        clear();
                        setGridCellAdapterToDate(getMonth(month_year[0]), Integer.parseInt(month_year[1]));

//
//
                    }
                }));
                break;

        }
    }

    private void getAllRecords(String datesel, String month, String year) {
        if (AppUtils.isNetworkConnected(this)) {
            AppUtils.showDialog(this);

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            String userID = SharedPrefsUtils.getUserID(this);

            String child_ID = null;
            if (TYPE.equalsIgnoreCase(AppConstants.STR_CHILD)) {
                child_ID = CHILD_ID;
            }
            datesel = "";

            Call<CalndrCommonResModel> call = apiService.getAllRecords(SharedPrefsUtils.getToken(getApplicationContext()),userID, datesel, month, year, child_ID);

            call.enqueue(new retrofit2.Callback<CalndrCommonResModel>() {
                @Override
                public void onResponse(Call<CalndrCommonResModel> call, retrofit2.Response<CalndrCommonResModel> response) {

                    AppUtils.dismissDialog();

                    if (response.code() == 200 && response.body().isSuccess() == true) {

                        ARR_CALNDR_ALL_RECORDS = response.body().getAllrecords();
                        setCommonDataToAdapter(ARR_CALNDR_ALL_RECORDS);
                        updateCardsMilesStoneCards(tvAll, llAll);

                    } else {
                        tvNoRecords.setVisibility(View.VISIBLE);
                        rvHealthRecords.setVisibility(View.GONE);
                        if (response.body().getMessage()!=null) {
                            if (response.body().getMessage().equalsIgnoreCase("Unauthorized")) {
                                AppUtils.sessionExpired(CalendarActivity.this);
                            }
                            else if (!response.body().getMessage().contains("No records found")){
                                AppUtils.showToast(getApplicationContext(),response.body().getMessage());
                            }
                        }

                    }
                }

                @Override
                public void onFailure(Call<CalndrCommonResModel> call, Throwable t) {
                    AppUtils.dismissDialog();
                }
            });
        }
        else {
            AppUtils.showToast(this,"It looks like your internet connectioin off. Please turn it on and try again.");
        }
    }

    private int getMonth(String s) {
        for (int i = 0; i < arr_months.length; ++i) {
            if (s.equalsIgnoreCase(arr_months[i])) {
                return i;
            }
        }
        return 1;
    }

    private void addHeadlthRecord() {
        Intent addHealthRecord = new Intent(CalendarActivity.this, AddCalenderHealthRecord.class);
        if (CALENDAR_TYPE.equalsIgnoreCase( AppConstants.ALL)){
            ADD_TYPE = AppConstants.MILESTONES;
        }

        addHealthRecord.putExtra(AppConstants.CALNDAR_TYPE, ADD_TYPE);
        startActivity(addHealthRecord);
    }

    /*
     * @param tvView
     */
    private void updateMotherCards(TextView tvView) {
        tvMother.setBackgroundColor(getResources().getColor(R.color.white));
        tvChild1.setBackgroundColor(getResources().getColor(R.color.white));
        tvChild2.setBackgroundColor(getResources().getColor(R.color.white));

        tvMother.setTextColor(Color.BLACK);
        tvChild1.setTextColor(Color.BLACK);
        tvChild2.setTextColor(Color.BLACK);

        tvView.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        tvView.setTextColor(Color.WHITE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        clear();
        getCalndrReminders(TYPE);


    }


    private void getMemoriesNotesReminders(final String type) {

        if (AppUtils.isNetworkConnected(this)) {
            AppUtils.showDialog(this);

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            String userID = SharedPrefsUtils.getUserID(this);

            String child_ID = null;
            if (TYPE.equalsIgnoreCase(AppConstants.STR_CHILD)) {
                child_ID = CHILD_ID;
            }

            String datesel1 = checkDate(datesel);
            String month1 = selectmonth;
            String year1 = selectyear;
            Call<CalndrCommonResModel> call;
            if (isDateSelected) {
                if (type.equalsIgnoreCase(AppConstants.MILESTONES)) {
                    boolean isFound = false;
                    for (int i = 0; i < arr_milestones.size(); i++) {
                        if (datesel1.equalsIgnoreCase(arr_milestones.get(i).getDate())) {
                            isFound = true;
                            break;
                        }
                    }
                    if (isFound)
                        call = apiService.getCalenderRecordswithDate(SharedPrefsUtils.getToken(getApplicationContext()),userID, type, datesel1, child_ID);
                    else
                        call = apiService.getCalenderRecordswithMonth(SharedPrefsUtils.getToken(getApplicationContext()),userID, type, month1, year1, child_ID);
                } else
                    call = apiService.getCalenderRecordswithDate(SharedPrefsUtils.getToken(getApplicationContext()),userID, type, datesel1, child_ID);
            } else
                call = apiService.getCalenderRecordswithMonth(SharedPrefsUtils.getToken(getApplicationContext()),userID, type, month1, year1, child_ID);

            call.enqueue(new retrofit2.Callback<CalndrCommonResModel>() {
                @Override
                public void onResponse(Call<CalndrCommonResModel> call, retrofit2.Response<CalndrCommonResModel> response) {

                    AppUtils.dismissDialog();

                    if (response.code() == 200 && response.body().isSuccess() == true) {

                        if (type.equalsIgnoreCase(AppConstants.MILESTONES)) {
                            ARR_CALNDR_RECORDS_MILESTONES = response.body().getArrMilestones();
                            setCommonDataToAdapter(ARR_CALNDR_RECORDS_MILESTONES);
                        }

                        if (type.equalsIgnoreCase(AppConstants.MEMORIES)) {
                            ARR_CALNDR_RECORDS_MEMORIES = response.body().getArrMemories();
                            setCommonDataToAdapter(ARR_CALNDR_RECORDS_MEMORIES);

                        } else if (type.equalsIgnoreCase(AppConstants.NOTES)) {
                            ARR_CALNDR_RECORDS_NOTES = response.body().getArrNotes();
                            setCommonDataToAdapter(ARR_CALNDR_RECORDS_NOTES);

                        } else if (type.equalsIgnoreCase(AppConstants.REMINDERS)) {
                            ARR_CALNDR_RECORDS_REMINDERS = response.body().getArrReminders();
                            setCommonDataToAdapter(ARR_CALNDR_RECORDS_REMINDERS);
                        }

                    } else {
                        tvNoRecords.setVisibility(View.VISIBLE);
                        rvHealthRecords.setVisibility(View.GONE);
                        if (response.body().getMessage()!=null) {
                            if (response.body().getMessage().equalsIgnoreCase("Unauthorized")) {
                                AppUtils.sessionExpired(CalendarActivity.this);
                            }
                            else if (!response.body().getMessage().contains("No records found")){
                                AppUtils.showToast(getApplicationContext(),response.body().getMessage());
                            }
                        }

                    }
                }

                @Override
                public void onFailure(Call<CalndrCommonResModel> call, Throwable t) {
                    AppUtils.dismissDialog();
                }
            });
        }
        else {
            AppUtils.showToast(this,"It looks like your internet connectioin off. Please turn it on and try again.");
        }
    }

    private String checkDate(String datesel) {
        String[] splitdate = datesel.split("-");
        String day = "";
        if (Integer.parseInt(splitdate[2])<10){
            day = "0"+Integer.parseInt(splitdate[2]);
        }
        else day = splitdate[2];
        return splitdate[0]+"-"+splitdate[1]+"-"+day;
    }

    private void getCalndrReminders(final String type) {

        //AppUtils.showDialog(this);
        if (AppUtils.isNetworkConnected(this)) {

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            String userID = SharedPrefsUtils.getUserID(this);

            String child_ID = null;
            if (TYPE.equalsIgnoreCase(AppConstants.STR_CHILD)) {
                child_ID = CHILD_ID;
            }
            Call<EventsDotsResModel> call = apiService.getCalndrReminders(SharedPrefsUtils.getToken(getApplicationContext()),userID, child_ID, MONTH, YEAR);

            call.enqueue(new retrofit2.Callback<EventsDotsResModel>() {
                @Override
                public void onResponse(Call<EventsDotsResModel> call, retrofit2.Response<EventsDotsResModel> response) {

                    AppUtils.dismissDialog();

                    if (response.code() == 200 && response.body().isSuccess() == true) {

                        Logger.info(TAG, "milesStrones prblm-->" + response.body().getArrCalendar_remainders().size());

                        arr_milestones = response.body().getArrCalendar_remainders().get(0).getArrMilestones();
                        arr_memories = response.body().getArrCalendar_remainders().get(1).getArrMemories();
                        arr_notes = response.body().getArrCalendar_remainders().get(2).getArrNotes();
                        arr_remainders = response.body().getArrCalendar_remainders().get(3).getArrRemainders();

                        Logger.info(TAG, "arr_milestones-->" + arr_milestones.size());
                        Logger.info(TAG, "arr_memories-->" + arr_memories.size());
                        Logger.info(TAG, "arr_notes-->" + arr_notes.size());
                        Logger.info(TAG, "arr_remainders-->" + arr_remainders.size());
                        if (!CALENDAR_TYPE.equalsIgnoreCase(AppConstants.ALL)) {
                            getMemoriesNotesReminders(CALENDAR_TYPE);
                        } else {
                            getAllRecords(datesel, selectmonth, selectyear);
                        }

                        grid_cell_adapter.notifyDataSetChanged();

                    } else {
                        tvNoRecords.setVisibility(View.VISIBLE);
                        rvHealthRecords.setVisibility(View.GONE);
                        if (response.body().getMessage()!=null) {
                            if (response.body().getMessage().equalsIgnoreCase("Unauthorized")) {
                                AppUtils.sessionExpired(CalendarActivity.this);
                            } else
                                AppUtils.showToast(getApplicationContext(), response.body().getMessage());
                        }
                    }
                }

                @Override
                public void onFailure(Call<EventsDotsResModel> call, Throwable t) {
                    AppUtils.dismissDialog();
                }
            });
        }
        else {
            AppUtils.showToast(this,"It looks like your internet connectioin off. Please turn it on and try again.");
        }
    }

    /*
     * @param tvText
     * @param llLayout
     */

    private void clear(){
        arr_milestones = new ArrayList<>();
        arr_memories = new ArrayList<>();
        arr_notes = new ArrayList<>();
        arr_remainders = new ArrayList<>();

        Logger.info(TAG, "arr_milestones-->" + arr_milestones.size());
        Logger.info(TAG, "arr_memories-->" + arr_memories.size());
        Logger.info(TAG, "arr_notes-->" + arr_notes.size());
        Logger.info(TAG, "arr_remainders-->" + arr_remainders.size());

        grid_cell_adapter.notifyDataSetChanged();
    }
    private void updateCardsMilesStoneCards(TextView tvText, LinearLayout llLayout) {
        llAll.setBackgroundColor(getResources().getColor(R.color.white));
        llMilestones.setBackgroundColor(getResources().getColor(R.color.white));
        llMemories.setBackgroundColor(getResources().getColor(R.color.white));
        llNotes.setBackgroundColor(getResources().getColor(R.color.white));
        llReminders.setBackgroundColor(getResources().getColor(R.color.white));

        tvAll.setTextColor(Color.BLACK);
        tvMilestones.setTextColor(Color.BLACK);
        tvMemories.setTextColor(Color.BLACK);
        tvNotes.setTextColor(Color.BLACK);
        tvReminders.setTextColor(Color.BLACK);

        tvText.setTextColor(Color.WHITE);

        if (tvText == tvAll) {
            ADD_TYPE = "all";
            llAll.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        } else if (tvText == tvMilestones) {
            ADD_TYPE = "milestones";
            llLayout.setBackgroundColor(getResources().getColor(R.color.calnder_blue));
        } else if (tvText == tvMemories) {
            ADD_TYPE = "memories";
            llLayout.setBackgroundColor(getResources().getColor(R.color.calnder_green));
        } else if (tvText == tvNotes) {
            ADD_TYPE = "notes";
            llLayout.setBackgroundColor(getResources().getColor(R.color.calnder_yelw));
        } else if (tvText == tvReminders) {
            ADD_TYPE = "reminders";
            llLayout.setBackgroundColor(getResources().getColor(R.color.calnder_red));
        }

    }

    public class GridCellAdapter extends BaseAdapter implements View.OnClickListener {
        private static final String tag = "GridCellAdapter";
        private final Context _context;

        private final List<String> list;
        private static final int DAY_OFFSET = 1;
        private final String[] weekdays = new String[]{"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};
        private final String[] months = {"01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"};
        //private final String[] arr_months = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};

        private final int[] daysOfMonth = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
        private final int month, year;
        private int daysInMonth, prevMonthDays;
        private int currentDayOfMonth;
        private int currentWeekDay;
        private TextView gridcell;
        private RelativeLayout rlCaldrItemMain;
        private final HashMap eventsPerMonthMap;
        private final SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MMM-yyyy",Locale.getDefault());

        private ImageView ivMilestone, ivMemories, ivNotes, ivReminder;


        // Days in Current Month
        public GridCellAdapter(Context context, int textViewResourceId, int month, int year) {
            super();
            this._context = context;
            this.list = new ArrayList<String>();
            this.month = month;
            this.year = year;

            Log.d(tag, "==> Passed in Date FOR Month: " + month + " " + "Year: " + year);
            Calendar calendar = Calendar.getInstance();
            setCurrentDayOfMonth(calendar.get(Calendar.DAY_OF_MONTH));
            setCurrentWeekDay(calendar.get(Calendar.DAY_OF_WEEK));
            Log.d(tag, "New Calendar:= " + calendar.getTime().toString());
            Log.d(tag, "CurrentDayOfWeek :" + getCurrentWeekDay());
            Log.d(tag, "CurrentDayOfMonth :" + getCurrentDayOfMonth());

            Calendar cal = Calendar.getInstance();
            SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy",Locale.getDefault());
            try {
                Date date = df.parse(SELECTED_DATE + "-" + this.month + "-" + this.year);
                cal.setTime(date);
                datesel  = this.year+ "-" + this.month + "-" + SELECTED_DATE;
                day_in_calender.setText(arr_days[cal.get(Calendar.DAY_OF_WEEK) - 1]);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            // Print Month
            printMonth(month, year);

            // Find Number of Events
            eventsPerMonthMap = findNumberOfEventsPerMonth(year, month);
        }

        private String getMonthAsString(int i) {
            return months[i];
        }

        private String getWeekDayAsString(int i) {
            return weekdays[i];
        }

        private int getNumberOfDaysOfMonth(int i) {
            return daysOfMonth[i];
        }

        public String getItem(int position) {
            return list.get(position);
        }

        @Override
        public int getCount() {
            return list.size();
        }

        /*
         * Prints Month
         *
         * @param mm
         * @param yy
         */
        private void printMonth(int mm, int yy) {
            Log.d(tag, "==> printMonth: mm: " + mm + " " + "yy: " + yy);
            // The number of days to leave blank at
            // the start of this MONTH.
            int trailingSpaces = 0;
            int leadSpaces = 0;
            int daysInPrevMonth = 0;
            int prevMonth = 0;
            int prevYear = 0;
            int nextMonth = 0;
            int nextYear = 0;

            int currentMonth = mm - 1;
            String currentMonthName = getMonthAsString(currentMonth);
            daysInMonth = getNumberOfDaysOfMonth(currentMonth);

            Log.d(tag, "Current Month: " + " " + currentMonthName + " having " + daysInMonth + " days.");

            // Gregorian Calendar : MINUS 1, set to FIRST OF MONTH
            GregorianCalendar cal = new GregorianCalendar(yy, currentMonth, 1);
            Log.d(tag, "Gregorian Calendar:= " + cal.getTime().toString());

            if (currentMonth == 11) {
                prevMonth = currentMonth - 1;
                daysInPrevMonth = getNumberOfDaysOfMonth(prevMonth);
                nextMonth = 0;
                prevYear = yy;
                nextYear = yy + 1;
                Log.d(tag, "*->PrevYear: " + prevYear + " PrevMonth:" + prevMonth + " NextMonth: " + nextMonth + " NextYear: " + nextYear);
            } else if (currentMonth == 0) {
                prevMonth = 11;
                prevYear = yy - 1;
                nextYear = yy;
                daysInPrevMonth = getNumberOfDaysOfMonth(prevMonth);
                nextMonth = 1;
                Log.d(tag, "**--> PrevYear: " + prevYear + " PrevMonth:" + prevMonth + " NextMonth: " + nextMonth + " NextYear: " + nextYear);
            } else {
                prevMonth = currentMonth - 1;
                nextMonth = currentMonth + 1;
                nextYear = yy;
                prevYear = yy;
                daysInPrevMonth = getNumberOfDaysOfMonth(prevMonth);
                Log.d(tag, "***---> PrevYear: " + prevYear + " PrevMonth:" + prevMonth + " NextMonth: " + nextMonth + " NextYear: " + nextYear);
            }

            // Compute how much to leave before before the first day of the
            // MONTH.
            // getDay() returns 0 for Sunday.
            int currentWeekDay = cal.get(Calendar.DAY_OF_WEEK) - 1;
            trailingSpaces = currentWeekDay;

            Log.d(tag, "Week Day:" + currentWeekDay + " is " + getWeekDayAsString(currentWeekDay));
            Log.d(tag, "No. Trailing space to Add: " + trailingSpaces);
            Log.d(tag, "No. of Days in Previous Month: " + daysInPrevMonth);

            if (cal.isLeapYear(cal.get(Calendar.YEAR)) && mm == 1) {
                ++daysInMonth;
            }

            // Trailing Month days
            for (int i = 0; i < trailingSpaces; i++) {
                Log.d(tag, "PREV MONTH:= " + prevMonth + " => " + getMonthAsString(prevMonth) + " " + String.valueOf((daysInPrevMonth - trailingSpaces + DAY_OFFSET) + i));
                list.add(String.valueOf((daysInPrevMonth - trailingSpaces + DAY_OFFSET) + i) + "-GREY" + "-" + getMonthAsString(prevMonth) + "-" + prevYear);
            }

            // Current Month Days
            for (int i = 1; i <= daysInMonth; i++) {
                Log.d(currentMonthName, String.valueOf(i) + " " + getMonthAsString(currentMonth) + " " + yy);
                if (i == getCurrentDayOfMonth()) {
                    list.add(String.valueOf(i) + "-BLUE" + "-" + getMonthAsString(currentMonth) + "-" + yy);
                } else {
                    list.add(String.valueOf(i) + "-WHITE" + "-" + getMonthAsString(currentMonth) + "-" + yy);
                }
            }

            //Logger.info(TAG,"getCurrentDayOfMonth-->"+getCurrentDayOfMonth());

            // Leading Month days
            for (int i = 0; i < list.size() % 7; i++) {
                Log.d(tag, "NEXT MONTH:= " + getMonthAsString(nextMonth));
                list.add(String.valueOf(i + 1) + "-GREY" + "-" + getMonthAsString(nextMonth) + "-" + nextYear);
            }
        }

        /*
         * NOTE: YOU NEED TO IMPLEMENT THIS PART Given the YEAR, MONTH, retrieve
         * ALL entries from a SQLite database for that MONTH. Iterate over the
         * List of All entries, and get the dateCreated, which is converted into
         * day.
         *
         * @param year
         * @param month
         * @return
         */
        private HashMap findNumberOfEventsPerMonth(int year, int month) {
            HashMap map = new HashMap<String, Integer>();
            return map;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View row = convertView;
            if (row == null) {
                LayoutInflater inflater = (LayoutInflater) _context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                row = inflater.inflate(R.layout.calendar_day_gridcell, parent, false);
            }

            // Get a reference to the Day gridcell
            gridcell = row.findViewById(R.id.calendar_day_gridcell);
            rlCaldrItemMain = row.findViewById(R.id.rlCaldrItemMain);

            ivMilestone = row.findViewById(R.id.ivMilestone);
            ivMemories = row.findViewById(R.id.ivMemories);
            ivNotes = row.findViewById(R.id.ivNotes);
            ivReminder = row.findViewById(R.id.ivReminder);

            // ACCOUNT FOR SPACING

            //Log.d(tag, "Current Day: " + getCurrentDayOfMonth());
            String[] day_color = list.get(position).split("-");
            final String theday = day_color[0];
            final String themonth = day_color[2];
            final String theyear = day_color[3];

            // Set the Day GridCell
            gridcell.setText(theday);
            gridcell.setTag(theday + "-" + themonth + "-" + theyear);
            //Log.d(TAG, "Setting GridCell " + theyear + "-" + themonth + "-" + theday);

            String new_date = theday;
            if (new_date.length() == 1) {
                new_date = "0" + new_date;
            }

            String new_month = themonth;
            if (new_month.length() == 1) {
                new_month = "0" + new_month;
            }

            String formate_date = theyear + "-" + new_month + "-" + new_date;
            //Log.d(TAG, "formate_date---> " + formate_date);

            if (arr_milestones != null && arr_milestones.size() > 0) {
                for (EventsTypeObjtModel model : arr_milestones) {
                    if (model.getDate().equalsIgnoreCase(formate_date)) {
                        ivMilestone.setVisibility(View.VISIBLE);
                    } else {
                        //ivMilestone.setVisibility(View.INVISIBLE);
                    }
                }
            }
            if (arr_memories != null && arr_memories.size() > 0) {
                for (EventsTypeObjtModel model : arr_memories) {
                    if (model.getDate().equalsIgnoreCase(formate_date)) {
                        ivMemories.setVisibility(View.VISIBLE);
                    } else {
                        //ivMilestone.setVisibility(View.INVISIBLE);
                    }
                }
            }
            if (arr_notes != null && arr_notes.size() > 0) {
                for (EventsTypeObjtModel model : arr_notes) {
                    if (model.getDate().equalsIgnoreCase(formate_date)) {
                        ivNotes.setVisibility(View.VISIBLE);
                    } else {
                        //ivMilestone.setVisibility(View.INVISIBLE);
                    }
                }
            }
            if (arr_remainders != null && arr_remainders.size() > 0) {
                for (EventsTypeObjtModel model : arr_remainders) {
                    if (model.getDate().equalsIgnoreCase(formate_date)) {
                        ivReminder.setVisibility(View.VISIBLE);
                    } else {
                        //ivMilestone.setVisibility(View.INVISIBLE);
                    }
                }
            }

            if (SELECTED_DATE != null && theday.equalsIgnoreCase(SELECTED_DATE)) {
                rlCaldrItemMain.setBackgroundColor(Color.parseColor("#50E5E5E5"));
            } else {
                rlCaldrItemMain.setBackgroundColor(Color.parseColor("#FFFFFF"));
            }

            if (day_color[1].equals("GREY")) {
                //gridcell.setTextColor(Color.LTGRAY);
                gridcell.setVisibility(View.INVISIBLE);
            }
            if (day_color[1].equals("WHITE")) {
                //gridcell.setTextColor(Color.BLACK);
                gridcell.setVisibility(View.VISIBLE);
            }
            if (day_color[1].equals("BLUE")) {
                //gridcell.setTextColor(getResources().getColor(R.color.static_text_color));
                gridcell.setVisibility(View.VISIBLE);
            }

            String date_formate = theyear + "-" + themonth + "-" + theday;

            // ####################################################################

            rlCaldrItemMain.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    //Logger.info(TAG, "book status-->" + gridcell.getTag().toString());

                    String[] day_color = list.get(position).split("-");
                    final String theday = SELECTED_DATE = day_color[0];
                    Calendar cal = Calendar.getInstance();
                    SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy",Locale.getDefault());
                    try {
                        Date date = df.parse(day_color[0] + "-" + day_color[2] + "-" + day_color[3]);
                        cal.setTime(date);
                        datesel = day_color[3] + "-" + day_color[2] + "-" + day_color[0];
                        selectmonth = day_color[2];
                        selectyear = day_color[3];
                        isDateSelected = true;
                        if (!CALENDAR_TYPE.equalsIgnoreCase(AppConstants.ALL)){
                                getMemoriesNotesReminders(CALENDAR_TYPE);
                        }
                        else {
                            getAllRecords("", day_color[2], day_color[3]);
                        }



                        day_in_calender.setText(arr_days[cal.get(Calendar.DAY_OF_WEEK) - 1]);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    Logger.info(TAG, "book status theday-->" + theday);

//                    /**
//                     * don't do any thing if selected day is less then current date
//                     */
//                    if (Integer.parseInt(theday) < getCurrentDayOfMonth()) {
//                        return;
//                    }

                    grid_cell_adapter.notifyDataSetChanged();
                }
            });
            return row;
        }

        @Override
        public void onClick(View view) {
            String date_month_year = (String) view.getTag();
            Logger.info(TAG, "--date_month_year--->" + date_month_year);
            //selectedDayMonthYearButton.setText("Selected: " + date_month_year);
        }

        public int getCurrentDayOfMonth() {
            return currentDayOfMonth;
        }

        private void setCurrentDayOfMonth(int currentDayOfMonth) {
            this.currentDayOfMonth = currentDayOfMonth;
        }

        public void setCurrentWeekDay(int currentWeekDay) {
            this.currentWeekDay = currentWeekDay;
        }

        public int getCurrentWeekDay() {
            return currentWeekDay;
        }
    }

    private void setCommonDataToAdapter(ArrayList<CalndrCommonResObjtModel> arr_health_records) {

        if (arr_health_records != null && arr_health_records.size() > 0) {
            rvHealthRecords.setLayoutManager(new LinearLayoutManager(this));
            CalndrCommonRecordsAdapter common_adapter;
            if (CALENDAR_TYPE.equalsIgnoreCase(AppConstants.ALL)){
                common_adapter = new CalndrCommonRecordsAdapter(CalendarActivity.this, arr_health_records,AppConstants.ALL);
            }
            else
                common_adapter = new CalndrCommonRecordsAdapter(CalendarActivity.this, arr_health_records);
            common_adapter.setClickListener(this);
            rvHealthRecords.setAdapter(common_adapter);

            tvNoRecords.setVisibility(View.GONE);
            rvHealthRecords.setVisibility(View.VISIBLE);
        } else {
            tvNoRecords.setVisibility(View.VISIBLE);
            rvHealthRecords.setVisibility(View.GONE);
        }
    }

    @Override
    public void onItemClick(View view, CalndrCommonResObjtModel model) {
        HomeCardModel cardModel = null;
        Intent addHealthRecord = new Intent(CalendarActivity.this, AddCalenderHealthRecord.class);
        addHealthRecord.putExtra(AppConstants.CALNDAR_TYPE, model.getCalender_type());
        addHealthRecord.putExtra(AppConstants.CALNDAR_MODEL_2, model);
        addHealthRecord.putExtra("from","calendar");
        addHealthRecord.putExtra("cards",cardModel);
        startActivityForResult(addHealthRecord,12);
    }

    private void removeLocalData(String type, String id) {
        removeCalendar(type,id);
        if (ADD_TYPE.equalsIgnoreCase(AppConstants.ALL)){
            type = AppConstants.ALL;
        }


        if (type.equalsIgnoreCase(AppConstants.MILESTONES)) {
            for (int i = 0; i < ARR_CALNDR_RECORDS_MILESTONES.size(); i++) {
                if (ARR_CALNDR_RECORDS_MILESTONES.get(i).getId().equalsIgnoreCase(id)) {
                    ARR_CALNDR_RECORDS_MILESTONES.remove(i);
                    setCommonDataToAdapter(ARR_CALNDR_RECORDS_MILESTONES);
                }
            }
        } else if (type.equalsIgnoreCase(AppConstants.MEMORIES)) {
            for (int i = 0; i < ARR_CALNDR_RECORDS_MEMORIES.size(); i++) {
                if (ARR_CALNDR_RECORDS_MEMORIES.get(i).getId().equalsIgnoreCase(id)) {
                    ARR_CALNDR_RECORDS_MEMORIES.remove(i);
                    setCommonDataToAdapter(ARR_CALNDR_RECORDS_MEMORIES);
                }
            }
        } else if (type.equalsIgnoreCase(AppConstants.REMINDERS)) {
            for (int i = 0; i < ARR_CALNDR_RECORDS_REMINDERS.size(); i++) {
                if (ARR_CALNDR_RECORDS_REMINDERS.get(i).getId().equalsIgnoreCase(id)) {
                    ARR_CALNDR_RECORDS_REMINDERS.remove(i);
                    setCommonDataToAdapter(ARR_CALNDR_RECORDS_REMINDERS);
                }
            }
        } else if (type.equalsIgnoreCase(AppConstants.NOTES)) {
            for (int i = 0; i < ARR_CALNDR_RECORDS_NOTES.size(); i++) {
                if (ARR_CALNDR_RECORDS_NOTES.get(i).getId().equalsIgnoreCase(id)) {
                    ARR_CALNDR_RECORDS_NOTES.remove(i);
                    setCommonDataToAdapter(ARR_CALNDR_RECORDS_NOTES);
                }
            }
        } else if (type.equalsIgnoreCase(AppConstants.ALL)){
            for (int i = 0; i < ARR_CALNDR_ALL_RECORDS.size(); i++) {
                if (ARR_CALNDR_ALL_RECORDS.get(i).getId().equalsIgnoreCase(id)) {
                    ARR_CALNDR_ALL_RECORDS.remove(i);
                    setCommonDataToAdapter(ARR_CALNDR_ALL_RECORDS);
                }
            }
        }
    }

    private void removeCalendar(String type, String id) {
        if (type.equalsIgnoreCase(AppConstants.MILESTONES)) {
            for (int i = 0; i < arr_milestones.size(); i++) {
                if (arr_milestones.get(i).getId().equalsIgnoreCase(id)) {
                    arr_milestones.remove(i);
                }
            }
        } else if (type.equalsIgnoreCase(AppConstants.MEMORIES)) {
            for (int i = 0; i < arr_memories.size(); i++) {
                if (arr_memories.get(i).getId().equalsIgnoreCase(id)) {
                    arr_memories.remove(i);
                }
            }
        } else if (type.equalsIgnoreCase(AppConstants.REMINDERS)) {
            for (int i = 0; i < arr_remainders.size(); i++) {
                if (arr_remainders.get(i).getId().equalsIgnoreCase(id)) {
                    arr_remainders.remove(i);
                }
            }
        } else if (type.equalsIgnoreCase(AppConstants.NOTES)) {
            for (int i = 0; i < arr_notes.size(); i++) {
                if (arr_notes.get(i).getId().equalsIgnoreCase(id)) {
                    arr_notes.remove(i);
                }
            }
        }
        grid_cell_adapter.notifyDataSetChanged();
    }

    /*
     * @param type
     */
    private void deleteCalndarItems(final String type, final String recordID) {

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<CommonResModel> call = apiService.deleteCalandrItem(SharedPrefsUtils.getToken(getApplicationContext()),type, recordID, SharedPrefsUtils.getUserID(CalendarActivity.this), CHILD_ID);

        call.enqueue(new retrofit2.Callback<CommonResModel>() {
            @Override
            public void onResponse(Call<CommonResModel> call, final retrofit2.Response<CommonResModel> response) {
                AppUtils.dismissDialog();
                if (response.code() == 200 && response.body().isSuccess() == true) {
                    AppUtils.showToast(CalendarActivity.this, response.body().getMessage());
                    clear();
                    setGridCellAdapterToDate(MONTH-1,YEAR);
                    removeLocalData(type, recordID);
                } else {
                    if (response.body().getMessage()!=null) {
                        if (response.body().getMessage().equalsIgnoreCase("Unauthorized")) {
                            AppUtils.sessionExpired(CalendarActivity.this);
                        } else
                            AppUtils.showToast(getApplicationContext(), response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<CommonResModel> call, Throwable t) {
                AppUtils.dismissDialog();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode==2){
            clear();
            setGridCellAdapterToDate(MONTH-1,YEAR);
            removeLocalData(data.getStringExtra("type"), data.getStringExtra("recordid"));
        }
        else if (resultCode==3){
            clear();
            setGridCellAdapterToDate(MONTH-1,YEAR);
        }
    }
}
