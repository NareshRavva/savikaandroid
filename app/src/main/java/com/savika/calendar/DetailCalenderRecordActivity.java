package com.savika.calendar;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Layout;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.savika.Api.ApiClient;
import com.savika.Api.ApiInterface;
import com.savika.Api.CommonResModel;
import com.savika.Api.ServiceUrl;
import com.savika.R;
import com.savika.calendar.milestones.AddMLReqModel;
import com.savika.constants.AppConstants;
import com.savika.dialogUtils.AlertDialogUtility;
import com.savika.homeScreen.HomeActivity;
import com.savika.homeScreen.cards.HomeCardModel;
import com.savika.homeScreen.notification.NotificationActivity;
import com.savika.logger.Logger;
import com.savika.trackHealthRecords.AddHRReqModel;
import com.savika.trackHealthRecords.DetailHealthRecordActivity;
import com.savika.utils.AppUtils;
import com.savika.utils.ImageUtil;
import com.savika.utils.SharedPrefsUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;

/*
 * Created by Naresh Ravva on 21/10/17.
 */

public class DetailCalenderRecordActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.tvTitle)
    TextView tvTitle;

    @BindView(R.id.tvDoctorName)
    TextView tvDoctorName;

    @BindView(R.id.tvcalender_type)
    TextView tvReportName;

    @BindView(R.id.tvDate)
    TextView tvDate;

    @BindView(R.id.tvDesc)
    TextView tvDesc;

    @BindView(R.id.tvHeaderTitle)
    TextView tvHeaderTitle;

    @BindView(R.id.cal_webview)
    ImageView webView;

    @BindView(R.id.llBack)
    LinearLayout llBack;

    @BindView(R.id.ivMore)
    ImageView ivMore;

    @BindView(R.id.fullscreen)
    ImageView fullscreen;

    @BindView(R.id.ivShare)
    ImageView ivShare;
    CalndrCommonResObjtModel health_record_model;
    HomeCardModel cardModel;
    String type , ID, CHILD_ID;
    int REQUEST = 1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_calender_record);

        ButterKnife.bind(this);
        isStoragePermissionGranted();
        ivMore.setOnClickListener(this);
        llBack.setOnClickListener(this);
        ivShare.setOnClickListener(this);
        if (getIntent().getStringExtra("comingfrom").equalsIgnoreCase("card")){
           if (getIntent().getSerializableExtra(AppConstants.EXTRA_HEALTH_RECORD) != null) {
                cardModel = (HomeCardModel) getIntent().getSerializableExtra(AppConstants.EXTRA_HEALTH_RECORD);
                type = cardModel.getCard_type();
                ID = cardModel.getC_id();
                CHILD_ID = cardModel.getChildid();
//                if (ServiceUrl.BASE_URL.contains("3001")) {
                    if (getIntent().getBooleanExtra("from_notifications", false))
                        NotificationActivity.postNotificationsData(DetailCalenderRecordActivity.this, cardModel.getC_id(), cardModel.getUserid(), cardModel.getCard_type());
//                }
           } else {
                AppUtils.showToast(this, "Unable to show detail health records");
                finish();
            }
            displayDatacard(cardModel);
            if (cardModel.getImage_url()!=null && cardModel.getImage_url().length()>10)
                fullscreen.setVisibility(View.VISIBLE);
            else fullscreen.setVisibility(View.INVISIBLE);
            fullscreen.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (cardModel.getTitle()!=null){
                        if (cardModel.getImage_url()!=null)
                            openWebViewC(cardModel);
                    }

                }
            });
        }
        else {
            if (getIntent().getSerializableExtra(AppConstants.EXTRA_HEALTH_RECORD) != null) {
                health_record_model = (CalndrCommonResObjtModel) getIntent().getSerializableExtra(AppConstants.EXTRA_HEALTH_RECORD);
                type = health_record_model.getCalender_type();
                ID = health_record_model.getId();
                CHILD_ID = health_record_model.getChildid();
            } else {
                AppUtils.showToast(this, "Unable to show detail health records");
                finish();
            }
            displayData(health_record_model);
            if (health_record_model.getMedia_url()!=null && health_record_model.getMedia_url().length()>10)
                fullscreen.setVisibility(View.VISIBLE);
            else fullscreen.setVisibility(View.INVISIBLE);
            fullscreen.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (health_record_model.getMilestonedesc()!=null){
                        if (health_record_model.getMedia_url()!=null)
                            openWebView(health_record_model);
                    }
                    else {
                        if (health_record_model.getMedia_url()!=null)
                            openWebView(health_record_model);
                    }

                }
            });
        }



    }

    public boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v("", "Permission is granted");
                return true;
            } else {
                Log.v("", "Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.v("", "Permission is granted");
            return true;
        }
    }


    private void displayDatacard(HomeCardModel cardModel) {
        tvTitle.setText(cardModel.getTitle());
//        tvDoctorName.setText(health_record_model.);
        String type = cardModel.getCard_type();
        tvReportName.setText(type.substring(0, 1).toUpperCase() + type.substring(1));
        tvDate.setText(AppUtils.getDisplayDate(cardModel.getDate()));
        tvDesc.setText(cardModel.getDescription());

        tvHeaderTitle.setText(cardModel.getTitle());
        if (cardModel.getCard_type().equalsIgnoreCase(AppConstants.MILESTONES)){
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            webView.setLayoutParams(layoutParams);
            ImageUtil.LoadPicaso(this,webView,cardModel.getImage_url());
        }
        else {
            ImageUtil.LoadPicaso(this,webView,cardModel.getImage_url());
        }
    }

    private void openWebViewC(HomeCardModel model) {
        if (model.getImage_url() != null && model.getImage_url().length() > 1) {
            Intent webView = new Intent(DetailCalenderRecordActivity.this, com.savika.WebView.class);
            webView.putExtra(AppConstants.WEB_URL, model.getImage_url());
            webView.putExtra(AppConstants.WEB_VIEW_TITLE, model.getTitle());
            startActivity(webView);
        }
    }

    private void openWebView(CalndrCommonResObjtModel model) {
        if (model.getMedia_url() != null && model.getMedia_url().length() > 1) {
            Intent webView = new Intent(DetailCalenderRecordActivity.this, com.savika.WebView.class);
            webView.putExtra(AppConstants.WEB_URL, model.getMedia_url());
            webView.putExtra(AppConstants.WEB_VIEW_TITLE, model.getTitle());
            startActivity(webView);
        }
        else {
            Intent webView = new Intent(DetailCalenderRecordActivity.this, com.savika.WebView.class);
            webView.putExtra(AppConstants.WEB_URL, "milestone"+model.getMilestoneid());
            webView.putExtra(AppConstants.WEB_VIEW_TITLE, model.getMilestonedesc());
            startActivity(webView);
        }
    }

    /*
     * @param health_record_model
     */
    private void displayData(CalndrCommonResObjtModel health_record_model) {
        if (health_record_model.getMilestonedesc()!=null){
            tvTitle.setText(health_record_model.getMilestonedesc());
            tvHeaderTitle.setText(health_record_model.getMilestonedesc());
        }
        else {
            tvTitle.setText(health_record_model.getTitle());
            tvHeaderTitle.setText(health_record_model.getTitle());
        }
//        tvDoctorName.setText(health_record_model.);
        String type = health_record_model.getCalender_type();
        tvReportName.setText(type.substring(0, 1).toUpperCase() + type.substring(1));
        tvDate.setText(AppUtils.getDisplayDate(health_record_model.getDate()));
        tvDesc.setText(health_record_model.getDescription());

        if (health_record_model.getCalender_type().equalsIgnoreCase(AppConstants.MILESTONES)){
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            webView.setLayoutParams(layoutParams);
            ImageUtil.LoadPicaso(this,webView,health_record_model.getMedia_url());
        }
        else {
            ImageUtil.LoadPicaso(this,webView,health_record_model.getMedia_url());
        }


    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.llBack:
                if (getIntent().getBooleanExtra("from_notifications",false)){
                    Intent home = new Intent(DetailCalenderRecordActivity.this, HomeActivity.class);
                    startActivity(home);
                    finish();
                }
                else finish();
                break;
            case R.id.ivShare:
                if (isStoragePermissionGranted())
                    shareContent(view);
                else AppUtils.showToast(DetailCalenderRecordActivity.this,"Please grant the permission");
                break;
            case R.id.ivMore:


                final PopupWindow popup = new PopupWindow(this);
                View layout = getLayoutInflater().inflate(R.layout.popup_update_child, null);
                popup.setContentView(layout);
                // Set content width and height
                popup.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
                popup.setWidth(WindowManager.LayoutParams.WRAP_CONTENT);
                // Closes the popup window when touch outside of it - when looses focus
                popup.setOutsideTouchable(true);
                popup.setFocusable(true);
                // Show anchored to button
                popup.setBackgroundDrawable(new BitmapDrawable());
                popup.showAsDropDown(view);
                View container ;
                WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
                if (android.os.Build.VERSION.SDK_INT > 22) {
                    container = (View) popup.getContentView().getParent().getParent();
                }else{
                    container = (View) popup.getContentView().getParent();
                }
                WindowManager.LayoutParams p = (WindowManager.LayoutParams) container.getLayoutParams();
// add flag
                p.flags |= WindowManager.LayoutParams.FLAG_DIM_BEHIND;
                p.dimAmount = 0.3f;
                wm.updateViewLayout(container, p);


                LinearLayout llEditChild = layout.findViewById(R.id.llEditChild);
                LinearLayout llDeleteChild = layout.findViewById(R.id.llDeleteChild);
                TextView llEditText = layout.findViewById(R.id.edit_record);
                TextView llDeleteText = layout.findViewById(R.id.delete_record);
                if (type.equalsIgnoreCase(AppConstants.MILESTONES)){
                    llEditText.setText(AppConstants.EDIT_MILESTONE);
                    llDeleteText.setText(AppConstants.DELETE_MILESTONE);
                } else if (type.equalsIgnoreCase(AppConstants.MEMORIES)){
                    llEditText.setText(AppConstants.EDIT_MEMORIES);
                    llDeleteText.setText(AppConstants.DELETE_MEMORIES);
                } else if (type.equalsIgnoreCase(AppConstants.NOTES)){
                    llEditText.setText(AppConstants.EDIT_NOTES);
                    llDeleteText.setText(AppConstants.DELETE_NOTES);
                } else if (type.equalsIgnoreCase(AppConstants.REMINDERS)){
                    llEditText.setText(AppConstants.EDIT_REMINDERS);
                    llDeleteText.setText(AppConstants.DELETE_REMINDERS);
                }

                llEditChild.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        popup.dismiss();

                        if (health_record_model!=null && health_record_model.getCalender_type().equalsIgnoreCase(AppConstants.MEMORIES)) {
                            try {
                                if (health_record_model.getMilestone_id() != null && !(health_record_model.getMilestone_id().equalsIgnoreCase("")))
                                    AppUtils.showToast(DetailCalenderRecordActivity.this, "This memory is a milestone ");
                                else {
                                    Intent addHealthRecord = new Intent(DetailCalenderRecordActivity.this, AddCalenderHealthRecord.class);
                                    addHealthRecord.putExtra(AppConstants.CALNDAR_TYPE, type);
                                    addHealthRecord.putExtra(AppConstants.CALNDAR_MODEL_2, health_record_model);
                                    addHealthRecord.putExtra("cards",cardModel);
                                    startActivityForResult(addHealthRecord,12);
                                }
                            }
                            catch (Exception e){
                                e.printStackTrace();
                            }
                        }
                        else {
                            Intent addHealthRecord = new Intent(DetailCalenderRecordActivity.this, AddCalenderHealthRecord.class);
                            addHealthRecord.putExtra(AppConstants.CALNDAR_TYPE, type);
                            addHealthRecord.putExtra(AppConstants.CALNDAR_MODEL_2, health_record_model);
                            addHealthRecord.putExtra("cards",cardModel);
                            startActivityForResult(addHealthRecord,12);
                        }

                    }
                });

                llDeleteChild.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        popup.dismiss();

                        runOnUiThread(new AlertDialogUtility(DetailCalenderRecordActivity.this, getLayoutInflater(), getResources().getString(R.string.app_name), getResources().getString(R.string.are_you_sure_you_want_to_delete_), "Yes", "No", new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                if (SharedPrefsUtils.getUserID(DetailCalenderRecordActivity.this) != null) {
                                    if (health_record_model!=null && health_record_model.getCalender_type().equalsIgnoreCase(AppConstants.MEMORIES)) {
                                        try {
                                            if (health_record_model.getMilestone_id() != null && !(health_record_model.getMilestone_id().equalsIgnoreCase("")))
                                                AppUtils.showToast(DetailCalenderRecordActivity.this, "This memory is a milestone ");
                                            else deleteCalndarItems(type, ID);
                                        }
                                        catch (Exception e){
                                            e.printStackTrace();
                                        }
                                    }
                                    else deleteCalndarItems(type, ID);
                                }

                            }
                        }, new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                            }
                        }, true));
                    }
                });
                break;
        }
    }

    private void deleteCalndarItems(final String type, final String recordID) {
        if (AppUtils.isNetworkConnected(this)) {

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            Call<CommonResModel> call = apiService.deleteCalandrItem(SharedPrefsUtils.getToken(getApplicationContext()),type, recordID, SharedPrefsUtils.getUserID(DetailCalenderRecordActivity.this), CHILD_ID);

            call.enqueue(new retrofit2.Callback<CommonResModel>() {
                @Override
                public void onResponse(Call<CommonResModel> call, retrofit2.Response<CommonResModel> response) {

                    AppUtils.dismissDialog();

                    if (response.code() == 200 && response.body().isSuccess() == true) {
                        AppUtils.showToast(DetailCalenderRecordActivity.this, response.body().getMessage());
//                    removeLocalData(type, recordID);

                        Intent i = new Intent(DetailCalenderRecordActivity.this, CalendarActivity.class);
                        i.putExtra("type", type);
                        i.putExtra("recordid", recordID);
                        setResult(2, i);
                        if (getIntent().getBooleanExtra("from_notifications", false)) {
                            Intent home = new Intent(DetailCalenderRecordActivity.this, HomeActivity.class);
                            startActivity(home);
                            finish();
                        } else finish();


                    } else {
                        if (response.body().getMessage()!=null) {
                            if (response.body().getMessage().equalsIgnoreCase("Unauthorized")) {
                                AppUtils.showToast(getApplicationContext(), "Session Expired");
                            } else
                                AppUtils.showToast(DetailCalenderRecordActivity.this, response.body().getMessage());
                        }
                    }

                }

                @Override
                public void onFailure(Call<CommonResModel> call, Throwable t) {
                    AppUtils.dismissDialog();
                }
            });
        }
        else {
            AppUtils.showToast(this,"It looks like your internet connectioin off. Please turn it on and try again.");
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode){
            case 12:
                if (resultCode == 2){
                    if (data!=null && data.getBooleanExtra("success",false)){
                        if (getIntent().getBooleanExtra("from_notifications",false)){
                            Intent home1 = new Intent(DetailCalenderRecordActivity.this, HomeActivity.class);
                            startActivity(home1);
                            finish();
                        }
                        else{
                            Intent home = new Intent(DetailCalenderRecordActivity.this,CalendarActivity.class);
                            setResult(3,home);
                            finish();
                        }
                    }

                }
                break;

        }
    }

    private void shareContent(View view) {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        View rootview = view.getRootView();
        rootview.setDrawingCacheEnabled(true);
        Bitmap bitmap = rootview.getDrawingCache();
        String bitmapPath = MediaStore.Images.Media.insertImage(getContentResolver(), bitmap,"title", null);
        Uri bitmapUri = Uri.parse(bitmapPath);
        StringBuilder sb = new StringBuilder();
        if (health_record_model!=null){
            if (health_record_model.getCalender_type().equalsIgnoreCase(AppConstants.MILESTONES)){
                sb.append(health_record_model.getMilestonedesc());
            }
            else
                sb.append(health_record_model.getTitle());
            sb.append("\n");
            sb.append("\n");
            if (health_record_model.getMedia_url()!=null && health_record_model.getMedia_url().length()>10)
                sb.append("Image_URl:"+health_record_model.getMedia_url().replace(" ","%20"));
        }
        else if (cardModel!=null){
            if (cardModel.getCard_type().equalsIgnoreCase(AppConstants.MILESTONES)){
                sb.append(cardModel.getTitle());
            }
            else
                sb.append(cardModel.getTitle());
            sb.append("\n");
            sb.append("\n");
            if (cardModel.getImage_url()!=null && cardModel.getImage_url().length()>10)
                sb.append("Image_URl:"+cardModel.getImage_url().replace(" ","%20"));
        }

        sendIntent.setType("image/png");
        sendIntent.putExtra(Intent.EXTRA_STREAM,bitmapUri);
        sendIntent.putExtra(Intent.EXTRA_TEXT, sb.toString());
        startActivity(sendIntent);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (getIntent().getBooleanExtra("from_notifications",false)){
            Intent home = new Intent(DetailCalenderRecordActivity.this, HomeActivity.class);
            startActivity(home);
            finish();
        }
        else finish();
    }
}
