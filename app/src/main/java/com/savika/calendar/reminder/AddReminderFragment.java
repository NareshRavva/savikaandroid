package com.savika.calendar.reminder;


import android.app.AlertDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.savika.Api.ApiClient;
import com.savika.Api.ApiInterface;
import com.savika.Api.CommonResModel;
import com.savika.R;
import com.savika.amAMother.ProfileUploadResModel;
import com.savika.calendar.CalendarRecordUpdateReqModel;
import com.savika.calendar.CalndrCommonResObjtModel;
import com.savika.calendar.DetailCalenderRecordActivity;
import com.savika.calendar.milestones.AddMLReqModel;
import com.savika.components.Button;
import com.savika.constants.AppConstants;
import com.savika.constants.GAConstants;
import com.savika.dialogUtils.DatePicketDialog;
import com.savika.googleAnalytics.GoogleAnalyticsHelper;
import com.savika.homeScreen.cards.HomeCardModel;
import com.savika.logger.Logger;
import com.savika.profile.ProfileEditActivity;
import com.savika.utils.AppUtils;
import com.savika.utils.ImageUtil;
import com.savika.utils.SharedPrefsUtils;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static android.content.ContentValues.TAG;
import static com.google.android.gms.internal.zzahn.runOnUiThread;


/*
 * A simple {@link Fragment} subclass.
 */
public class AddReminderFragment extends Fragment implements View.OnClickListener {

    @BindView(R.id.spnrmot_preg)
    Spinner spnrmot_preg;

    @BindView(R.id.spneRmndr)
    Spinner spneRmndr;

    @BindView(R.id.rlFilePickup)
    RelativeLayout rlFilePickup;

    @BindView(R.id.flRemoveExtn)
    FrameLayout flRemoveExtn;

    @BindView(R.id.imgExtn)
    ImageView imgExtn;

    @BindView(R.id.llProceed)
    LinearLayout llProceed;

    @BindView(R.id.rlDate)
    RelativeLayout rlDate;

    @BindView(R.id.tvAddDate)
    TextView tvAddDate;

    @BindView(R.id.etvTitle)
    EditText etvTitle;

    @BindView(R.id.etvDesc)
    EditText etvDesc;

    @BindView(R.id.rlTime)
    RelativeLayout rlTime;

    @BindView(R.id.tvAddTime)
    TextView tvAddTime;

    @BindView(R.id.tvProceed)
    TextView tvProceed;

    @BindView(R.id.mother_child)
    RelativeLayout mother_child;

    @BindView(R.id.tvAddDoc)
    TextView tvAddDoc;

    Set keys;

    private String TAG = "AddReminderFragment";

    String[] TYPEARR;

    String DATE_DISPLAY, DATE, TIME, DOC_NAME, MEDIA_URL, TYPE, CHILD_ID, REMAINDER_BEFORE, RECORD_ID;
    String[] REMAINDR_HRS = {"1 Hour", "6 Hours", "12 Hours", "Day"};

    private CalndrCommonResObjtModel common_model;
    HomeCardModel card_model;
    GoogleAnalyticsHelper mGoogleHelper;

//    public AddReminderFragment(CalndrCommonResObjtModel common_model) {
//        this.common_model = common_model;
//    }

    public static Fragment newInstance(CalndrCommonResObjtModel common_model, HomeCardModel cardmodel)
    {
        AddReminderFragment myFragment = new AddReminderFragment();
        Bundle args = new Bundle();
        args.putSerializable("common",common_model);
        args.putSerializable("card",cardmodel);
        myFragment.setArguments(args);
        return myFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.common_model = (CalndrCommonResObjtModel) getArguments().getSerializable("common");
        this.card_model = (HomeCardModel) getArguments().getSerializable("card");
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View addMemories = inflater.inflate(R.layout.fragment_add_reminder, container, false);

        ButterKnife.bind(this, addMemories);

        AppUtils.getDeviceWidthAndHeigth(getActivity());

        TYPE = AppUtils.getMotherOrPregnantType(getActivity());

        if (TYPE.equalsIgnoreCase(AppConstants.STR_PREGNANT)) {
            mother_child.setVisibility(View.GONE);
        }
        rlFilePickup.setOnClickListener(this);
        flRemoveExtn.setOnClickListener(this);
        llProceed.setOnClickListener(this);

        keys = SharedPrefsUtils.getChildIds(getActivity());
        if (keys.size() == 1) {
            TYPEARR = new String[]{"Mother", "Child1"};
        } else {
            TYPEARR = new String[]{"Mother", "Child1", "Child2"};
        }

        setSpinnerData(spnrmot_preg);
        setSpinnerData(spneRmndr);
        Date date =new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy", Locale.getDefault());
        tvAddDate.setText(sdf.format(date));
        DATE = AppUtils.convertDateFormat(sdf.format(date));
        rlDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                datedialog(tvAddDate);

            }
        });

        rlTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar mcurrentTime = Calendar.getInstance();
                int hour;
                int minute;
                if (tvAddTime!=null && tvAddTime.length()>3){
                    String time = AppUtils.get24HRSTime(tvAddTime.getText().toString());
                    hour = Integer.parseInt(time.split(":")[0]);
                    minute = Integer.parseInt(time.split(":")[1]);
                }
                else {
                    hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                    minute = mcurrentTime.get(Calendar.MINUTE);
                }

                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        tvAddTime.setText(AppUtils.get12HRSTime(selectedHour + ":" + selectedMinute));
                        TIME = selectedHour + ":" + selectedMinute;
                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();
            }
        });
        InitGoogleAnalytics();
        updateExistingData();

        return addMemories;
    }

    // Google Analytics
    private void InitGoogleAnalytics() {
        mGoogleHelper = new GoogleAnalyticsHelper();
        mGoogleHelper.init(getActivity());
    }

    private void updateExistingData() {
        if (common_model != null) {
            etvTitle.setText(common_model.getTitle());
            DateFormat df =  new SimpleDateFormat("yyyy-MM-dd",Locale.getDefault());
            DateFormat sdf = new SimpleDateFormat("dd MMM yyyy",Locale.getDefault());
            try {
                Date selecteddate = df.parse(common_model.getDate());
                tvAddDate.setText(sdf.format(selecteddate));
            }catch (Exception e){
                e.printStackTrace();
                tvAddDate.setText(common_model.getDate());
            }
            tvAddTime.setText(AppUtils.get12HRSTime(common_model.getTime().split(":")[0]+":"+common_model.getTime().split(":")[1]));
            etvDesc.setText(common_model.getDescription());

            DATE = common_model.getDate();
            MEDIA_URL = common_model.getMedia_url();
            RECORD_ID = common_model.getId();
            CHILD_ID = common_model.getChildid();
            TIME = common_model.getTime();
            REMAINDER_BEFORE = common_model.getRemind_before();

            if (MEDIA_URL != null && MEDIA_URL.length() > 10) {
                String fileName = MEDIA_URL.substring(MEDIA_URL.lastIndexOf('/') + 1);
                if (fileName.split("-",2).length>1)
                    tvAddDoc.setText(fileName.split("-",2)[1]);
                else tvAddDoc.setText(fileName);
                flRemoveExtn.setVisibility(View.VISIBLE);
                ImageUtil.LoadPicaso(getActivity(), imgExtn, MEDIA_URL);
            }

            if (common_model.getRemind_before().equalsIgnoreCase("1")) {
                spneRmndr.setSelection(0);
            } else if (common_model.getRemind_before().equalsIgnoreCase("6")) {
                spneRmndr.setSelection(1);
            } else if (common_model.getRemind_before().equalsIgnoreCase("12")) {
                spneRmndr.setSelection(2);
            } else if (common_model.getRemind_before().equalsIgnoreCase("24")) {
                spneRmndr.setSelection(3);
            }

            // VALIDATE IF USER IS MOTHER
            if (!SharedPrefsUtils.getMotherORPrgntType(getActivity()).equalsIgnoreCase("1")) {
                List<String> arr_child = new ArrayList<>(keys);
                if (arr_child.get(0).equalsIgnoreCase(CHILD_ID)) {
                    spnrmot_preg.setSelection(1);
                } else if (arr_child.get(1).equalsIgnoreCase(CHILD_ID)) {
                    spnrmot_preg.setSelection(2);
                }
            }

            tvProceed.setText("Update");
        }
        else if (card_model!=null){
            etvTitle.setText(card_model.getTitle());
            DateFormat df =  new SimpleDateFormat("yyyy-MM-dd",Locale.getDefault());
            DateFormat sdf = new SimpleDateFormat("dd MMM yyyy",Locale.getDefault());
            try {
                Date selecteddate = df.parse(card_model.getDate());
                tvAddDate.setText(sdf.format(selecteddate));
            }catch (Exception e){
                e.printStackTrace();
                tvAddDate.setText(card_model.getDate());
            }
            tvAddTime.setText(AppUtils.get12HRSTime(card_model.getTime().split(":")[0]+":"+card_model.getTime().split(":")[1]));
            etvDesc.setText(card_model.getDescription());

            DATE = card_model.getDate();
            MEDIA_URL = card_model.getImage_url();
            RECORD_ID = card_model.getC_id();
            CHILD_ID = card_model.getChildid();
            TIME = card_model.getTime();
            REMAINDER_BEFORE = card_model.getRemind_before();

            if (MEDIA_URL != null && MEDIA_URL.length() > 10) {
                String fileName = MEDIA_URL.substring(MEDIA_URL.lastIndexOf('/') + 1);
                if (fileName.split("-",2).length>1)
                    tvAddDoc.setText(fileName.split("-",2)[1]);
                else tvAddDoc.setText(fileName);
                flRemoveExtn.setVisibility(View.VISIBLE);
                ImageUtil.LoadPicaso(getActivity(), imgExtn, MEDIA_URL);
            }

            if (card_model.getRemind_before().equalsIgnoreCase("1")) {
                spneRmndr.setSelection(0);
            } else if (card_model.getRemind_before().equalsIgnoreCase("6")) {
                spneRmndr.setSelection(1);
            } else if (card_model.getRemind_before().equalsIgnoreCase("12")) {
                spneRmndr.setSelection(2);
            } else if (card_model.getRemind_before().equalsIgnoreCase("24")) {
                spneRmndr.setSelection(3);
            }

            // VALIDATE IF USER IS MOTHER
            if (!SharedPrefsUtils.getMotherORPrgntType(getActivity()).equalsIgnoreCase("1")) {
                List<String> arr_child = new ArrayList<>(keys);
                if (arr_child.get(0).equalsIgnoreCase(CHILD_ID)) {
                    spnrmot_preg.setSelection(1);
                } else if (arr_child.get(1).equalsIgnoreCase(CHILD_ID)) {
                    spnrmot_preg.setSelection(2);
                }
            }

            tvProceed.setText(R.string.update);
        }
    }

    private void setSpinnerData(final Spinner spinner) {
        spinner.setAdapter(new StaticSpinner(getActivity(), spinner));
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                spinner.setSelection(position);
                String strDocType = (String) spinner.getSelectedItem();

                if (spinner == spnrmot_preg) {
                    ArrayList<String> list = new ArrayList<String>(keys);
                    if (strDocType.equalsIgnoreCase("Child1")) {
                        CHILD_ID = list.get(0);
                        TYPE = AppConstants.STR_CHILD;
                    } else if (strDocType.equalsIgnoreCase("Child2")) {
                        CHILD_ID = list.get(1);
                        TYPE = AppConstants.STR_CHILD;
                    }
                } else if (spinner == spneRmndr) {
                    if (strDocType.equalsIgnoreCase("1 Hour")) {
                        REMAINDER_BEFORE = "1";
                    } else if (strDocType.equalsIgnoreCase("6 Hour")) {
                        REMAINDER_BEFORE = "6";
                    } else if (strDocType.equalsIgnoreCase("12 Hour")) {
                        REMAINDER_BEFORE = "12";
                    } else {
                        REMAINDER_BEFORE = "24";
                    }
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private class StaticSpinner extends BaseAdapter {

        private LayoutInflater mInflater;
        String[] AdArr;

        public StaticSpinner(FragmentActivity con, Spinner spinner) {
            mInflater = LayoutInflater.from(con);
            int id = spinner.getId();
            switch (id) {
                case R.id.spnrmot_preg:
                    AdArr = TYPEARR;
                    break;
                case R.id.spneRmndr:
                    AdArr = REMAINDR_HRS;
                    break;
            }
        }

        @Override
        public int getCount() {
            return AdArr.length;
        }

        @Override
        public Object getItem(int position) {
            return AdArr[position];
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup viewGroup) {
            final ListContent holder;
            View v = convertView;
            if (v == null) {
                v = mInflater.inflate(R.layout.list_item_hr_doc_types, null);
                holder = new ListContent();

                holder.name = v.findViewById(R.id.tvSpinner);

                v.setTag(holder);
            } else {
                holder = (ListContent) v.getTag();
            }

            holder.name.setText(AdArr[position]);

            return v;
        }
    }

    static class ListContent {
        TextView name;
    }


    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.rlFilePickup:
                startCropImageActivity(null);
                break;

            case R.id.flRemoveExtn:
                flRemoveExtn.setVisibility(View.GONE);
                MEDIA_URL = null;
                tvAddDoc.setText("");
                break;

            case R.id.llProceed:

                if (etvTitle.getText().toString().length() < 1) {
                    AppUtils.showToast(getActivity(), "Add Title");
                    return;
                }
                if (etvTitle.getText().toString().length() < 5) {
                    AppUtils.showToast(getActivity(), "Title must be 5 characters");
                    return;
                }
                if (tvAddDate.getText().toString().length() < 1) {
                    AppUtils.showToast(getActivity(), "Add Date");
                    return;
                }
                if (tvAddTime.getText().toString().length() < 1) {
                    AppUtils.showToast(getActivity(), "Add Time");
                    return;
                }

                if (common_model != null || card_model!=null) {
                    updateServiceCall();
                } else {
                    addNewReminderServiceCall();
                }
                break;

        }
    }

    /*
     * update child record
     */
    private void updateServiceCall() {

        CalendarRecordUpdateReqModel model = new CalendarRecordUpdateReqModel();
        model.setCalender_type(AppConstants.REMINDERS);
        model.setRecordid(RECORD_ID);
        model.setUserid(SharedPrefsUtils.getUserID(getActivity()));
        model.setChildid(CHILD_ID);
        model.setType(TYPE);
        if (CHILD_ID != null && CHILD_ID.length() > 0) {
            model.setType(AppConstants.STR_CHILD);
        }
        model.setTitle(etvTitle.getText().toString());
        model.setDate(DATE);
        model.setTime(TIME);
        model.setRemind_before(REMAINDER_BEFORE);
        model.setDescription(etvDesc.getText().toString());
        model.setMedia_url(MEDIA_URL);

        if (AppUtils.isNetworkConnected(getActivity())) {
            AppUtils.showDialog(getActivity());

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            Call<CommonResModel> call = apiService.updateCalndrRecord(SharedPrefsUtils.getToken(getActivity()),model);

            call.enqueue(new Callback<CommonResModel>() {
                @Override
                public void onResponse(Call<CommonResModel> call, Response<CommonResModel> response) {

                    AppUtils.dismissDialog();

                    if (response.code() == 200 && response.body().isSuccess() == true) {
                        AppUtils.showToast(getActivity(), response.body().getMessage());
                        Intent intent = new Intent(getActivity(), DetailCalenderRecordActivity.class);
                        intent.putExtra("success", true);
                        getActivity().setResult(2, intent);
                        getActivity().finish();
                    } else {
                        if (response.body().getMessage()!=null) {
                            if (response.body().getMessage().equalsIgnoreCase("Unauthorized")) {
                                AppUtils.sessionExpired(getActivity());
                            } else AppUtils.showToast(getActivity(), response.body().getMessage());
                        }
                    }
                }

                @Override
                public void onFailure(Call<CommonResModel> call, Throwable t) {
                    AppUtils.dismissDialog();
                }
            });
        }
        else {
            AppUtils.showToast(getActivity(),"It looks like your internet connectioin off. Please turn it on and try again.");
        }
    }

    /*
     * Start crop image activity for the given image.
     *
     * @param imageUri
     */
    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri).setGuidelines(CropImageView.Guidelines.ON).
                setMultiTouchEnabled(true).setInitialCropWindowPaddingRatio(0).setAspectRatio(16, 10)
                .start(getContext(), this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);

            if (resultCode == RESULT_OK) {
                Log.v("MainActivityPrblm", "Image Path-->" + result.getUri());

                String selectedImagePath = result.getUri().getPath();

                Logger.info(TAG, "--FilePath-->" + selectedImagePath);

                if (selectedImagePath.contains("/external/")) {
                    selectedImagePath = AppUtils.getPath(getActivity(), data.getData());
                    Logger.info(TAG, "--FilePath-->" + selectedImagePath);
                }

                String fileName = AppUtils.getFileNameWithExtn(selectedImagePath);

                String fileExt = AppUtils.getMimeType(selectedImagePath);

                Logger.info(TAG, "--fileName-->" + fileName + "--fileExt-->" + fileExt);

                uploadToServer(selectedImagePath, fileName, fileExt);

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Toast.makeText(getActivity(), "Cropping failed: " + result.getError(), Toast.LENGTH_LONG).show();
            }


        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void uploadToServer(String selectedImagePath, final String fileName, final String fileExt) {

        if (AppUtils.isNetworkConnected(getActivity())) {
            AppUtils.showDialog(getActivity());

            try {
                final File file = new File(selectedImagePath);

                Log.d(TAG, "Filename " + file.getName());

                //RequestBody mFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                RequestBody mFile = RequestBody.create(MediaType.parse("image/*"), file);
                MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("image", file.getName(), mFile);

                RequestBody type = RequestBody.create(okhttp3.MultipartBody.FORM, "records");
                RequestBody user_id = RequestBody.create(okhttp3.MultipartBody.FORM, SharedPrefsUtils.getUserID(getActivity()));
                ApiInterface uploadImage = ApiClient.getClient().create(ApiInterface.class);
                Call<ProfileUploadResModel> fileUpload = uploadImage.uploadFile(SharedPrefsUtils.getToken(getActivity()),fileToUpload, type, user_id, null);

                fileUpload.enqueue(new Callback<ProfileUploadResModel>() {
                    @Override
                    public void onResponse(Call<ProfileUploadResModel> call, Response<ProfileUploadResModel> response) {
                        Logger.info(TAG, "Response " + response.code());

                        if (response.code() == 200 && response.body().getProfileurl() != null) {
                            Logger.info(TAG, "profile URL--->" + response.body().getProfileurl());
                            flRemoveExtn.setVisibility(View.VISIBLE);
                            MEDIA_URL = response.body().getProfileurl();
                            Logger.info(TAG, "MEDIA_URL--->" + MEDIA_URL);
                            tvAddDoc.setText(fileName);
                            if (file.exists())
                                file.delete();

                            ImageUtil.LoadPicaso(getActivity(), imgExtn, MEDIA_URL);
                        }
                        else {
                            if (response.body().getMessage()!=null) {
                                if (response.body().getMessage().equalsIgnoreCase("Unauthorized")) {
                                    AppUtils.sessionExpired(getActivity());
                                } else
                                    AppUtils.showToast(getActivity(), response.body().getMessage());
                            }
                        }

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                AppUtils.dismissDialog();
                            }
                        }, 500);

                    }

                    @Override
                    public void onFailure(Call<ProfileUploadResModel> call, Throwable t) {
                        AppUtils.showToast(getActivity(), getString(R.string.server_down));
                        Log.d(TAG, "Error " + t.getMessage());
                        AppUtils.dismissDialog();
                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else {
            AppUtils.showToast(getActivity(),"It looks like your internet connectioin off. Please turn it on and try again.");
        }

    }

    private void addNewReminderServiceCall() {

        AddMLReqModel model = new AddMLReqModel();
        model.setUserid(SharedPrefsUtils.getUserID(getActivity()));
        model.setCalender_type(AppConstants.REMINDERS);
        model.setTitle(etvTitle.getText().toString());
        model.setDate(DATE);
        model.setTime(TIME);
        model.setRemind_before(REMAINDER_BEFORE);
        if (MEDIA_URL != null && !MEDIA_URL.equalsIgnoreCase("")) {
            model.setMedia_url(MEDIA_URL);
        }
        model.setUsername(SharedPrefsUtils.getUsername(getActivity()));
        model.setType(TYPE);
        model.setDescription(etvDesc.getText().toString());


        if (TYPE.equalsIgnoreCase(AppConstants.STR_CHILD)) {
            model.setChildid(CHILD_ID);
        }
        if (AppUtils.isNetworkConnected(getActivity())) {
            AppUtils.showDialog(getActivity());

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            Call<CommonResModel> call = apiService.AddNewMileStone(SharedPrefsUtils.getToken(getActivity()),model);

            call.enqueue(new Callback<CommonResModel>() {
                @Override
                public void onResponse(Call<CommonResModel> call, Response<CommonResModel> response) {

                    AppUtils.dismissDialog();

                    if (response.code() == 200 && response.body().isSuccess() == true) {
                        AppUtils.showToast(getActivity(), response.body().getMessage());
                        mGoogleHelper.SendEventGA(getActivity(), GAConstants.CALENDAR_SCREEN, GAConstants.ADD_REMINDER, "");
                        getActivity().finish();
                    }
                    else{
                        if (response.body().getMessage()!=null) {
                            if (response.body().getMessage().equalsIgnoreCase("Unauthorized")) {
                                AppUtils.sessionExpired(getActivity());
                            } else AppUtils.showToast(getActivity(), response.body().getMessage());
                        }
                    }
                }

                @Override
                public void onFailure(Call<CommonResModel> call, Throwable t) {
                    AppUtils.dismissDialog();
                }
            });
        }
        else {
            AppUtils.showToast(getActivity(),"It looks like your internet connectioin off. Please turn it on and try again.");
        }
    }

    public void datedialog(final TextView tvDate)
    {
        String defaultdate = tvAddDate.getText().toString().trim();
        runOnUiThread(new DatePicketDialog(getActivity(), getString(R.string.select_date), getString(R.string.please_pic_the_date), false, 0, 5,defaultdate, getActivity().getLayoutInflater(), new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Logger.info("TAG", "Selected prblm Date-->" + view.getTag());
                DateFormat df = new SimpleDateFormat("dd MMM yyyy",Locale.getDefault());
                try {
                    Date selecteddate = df.parse((String) view.getTag());
                    Date date = new Date();
                    Date today = df.parse(df.format(date));
                    if (selecteddate.before(today) && !selecteddate.equals(date) ){
                        showAlert(getActivity(),"Choose date after today",tvDate);
                        //tvDate.setText("");
                    }
                    else {
                        Logger.info("TAG", "Selected prblm Date-->" + view.getTag());

                        tvAddDate.setText("" + view.getTag());

                        DATE_DISPLAY = "" + view.getTag();

                        DATE = AppUtils.convertDateFormat("" + view.getTag());
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }

            }
        }));
    }
    private void showAlert(FragmentActivity ctx, String message,final TextView tvDate) {
        if (ctx != null) {

            try {
                final AlertDialog.Builder builder = AppUtils.getAlertDialog(ctx);
                View view  = getActivity().getLayoutInflater().inflate(R.layout.pop_alert_date,null);
                builder.setView(view);
                final AlertDialog alert = builder.create();
                alert.show();
                TextView messagetxt = view.findViewById(R.id.tvdateerrorText);
                messagetxt.setText(message);
                Button ok = view.findViewById(R.id.datebtnPositive);
                ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alert.cancel();
                        datedialog(tvDate);
                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


}
