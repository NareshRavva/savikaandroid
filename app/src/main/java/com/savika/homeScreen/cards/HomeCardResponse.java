package com.savika.homeScreen.cards;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/*
 * Created by nexivo on 13/11/17.
 */

public class HomeCardResponse {

    private boolean success;

    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @SerializedName("cards")
    private
    ArrayList<HomeCardModel> arr_card_records = new ArrayList<>();

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public ArrayList<HomeCardModel> getArr_card_records() {
        return arr_card_records;
    }

    public void setArr_health_records(ArrayList<HomeCardModel> arr_health_records) {
        this.arr_card_records = arr_health_records;
    }
}
