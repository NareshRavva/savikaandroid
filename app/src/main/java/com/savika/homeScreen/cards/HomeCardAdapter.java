package com.savika.homeScreen.cards;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.savika.R;
import com.savika.calendar.CalendarActivity;
import com.savika.constants.AppConstants;
import com.savika.utils.AppUtils;
import com.savika.utils.ImageUtil;

import java.util.ArrayList;

/*
 * Created by nexivo on 13/11/17.
 */

public class HomeCardAdapter extends RecyclerView.Adapter<HomeCardAdapter.ViewHolder>{
    private LayoutInflater mInflater;
    private HomeCardAdapter.ItemClickListener mClickListener;
    private Context context;
//    private int selectedPos = 0;

    private ArrayList<HomeCardModel> arrData = new ArrayList<>();

    public HomeCardAdapter(Context context, ArrayList<HomeCardModel> arrData) {
        this.mInflater = LayoutInflater.from(context);
        this.arrData = arrData;
        this.context = context;

//        selectedPos = arrData.size() - 1;
    }

    @Override
    public HomeCardAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.list_item_home_tips, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(HomeCardAdapter.ViewHolder holder, int position) {
        HomeCardModel model = arrData.get(position);
        if (model.getModule().equalsIgnoreCase("TYPE_BANNER")){
            holder.tips.setVisibility(View.GONE);
            holder.wishes.setVisibility(View.GONE);
            holder.banner.setVisibility(View.VISIBLE);
            ImageUtil.LoadPicaso(context, holder.banner_img, model.getImage_url());
        }
        else if (model.getModule().equalsIgnoreCase("TYPE_WISH_MSG")){
            holder.tips.setVisibility(View.GONE);
            holder.wishes.setVisibility(View.VISIBLE);
            holder.banner.setVisibility(View.GONE);
            holder.wishtext.setText(model.getTitle());
        }
        else if (model.getModule().equalsIgnoreCase("TYPE_TIP")){
            holder.tips.setVisibility(View.VISIBLE);
            holder.wishes.setVisibility(View.GONE);
            holder.banner.setVisibility(View.GONE);
            holder.tvTitle.setText(model.getTitle());
            holder.tvMsg.setText(model.getDescription());
            if (model.getCard_type().equalsIgnoreCase(AppConstants.REMINDERS)){
                holder.ivImg.setVisibility(View.GONE);
                holder.tvCardRem.setVisibility(View.VISIBLE);
                holder.item_card_date.setText(model.getDate()!=null?model.getDate().split("-")[2]:"");
                holder.item_card_month.setText(model.getDate()!=null? CalendarActivity.arr_months[Integer.parseInt(model.getDate().split("-")[1])-1]:"");
                holder.item_card_time.setText(AppUtils.get12HRSTime(model.getTime().split(":")[0]+":"+model.getTime().split(":")[1]));
            }
            else {
//                if (model.getCard_type().equalsIgnoreCase("articles"))
//                    ImageUtil.LoadPicaso(context, holder.ivImg, model.getMedia_url());
//                else
                    ImageUtil.LoadPicaso(context, holder.ivImg, model.getImage_url());
            }
        }


    }

    @Override
    public int getItemCount() {
        return arrData.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvTitle, tvMsg,wishtext,item_card_date,item_card_month,item_card_time;
        ImageView ivImg,banner_img;
        LinearLayout tvCardRem;
        CardView banner,tips,wishes;

        public ViewHolder(View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.tvTitle);
            tvMsg = itemView.findViewById(R.id.tvDesc);
            ivImg = itemView.findViewById(R.id.ivImg);
            banner_img = itemView.findViewById(R.id.ivImg1);
            wishtext = itemView.findViewById(R.id.tvDesc1);
            banner = itemView.findViewById(R.id.card_view1);
            wishes = itemView.findViewById(R.id.card_view2);
            tvCardRem = itemView.findViewById(R.id.tvCardRem);
            item_card_date = itemView.findViewById(R.id.item_card_date);
            item_card_month = itemView.findViewById(R.id.item_card_month);
            item_card_time = itemView.findViewById(R.id.item_card_time);
            tips = itemView.findViewById(R.id.card_view);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) {
                mClickListener.onItemClick(view, arrData.get(getAdapterPosition()));

//                selectedPos = getAdapterPosition();

                notifyDataSetChanged();
            }
        }
    }

    public HomeCardModel getItem(int id) {
        return arrData.get(id);
    }

    public void setClickListener(HomeCardAdapter.ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    public interface ItemClickListener {
        void onItemClick(View view, HomeCardModel cardModel);
    }
}
