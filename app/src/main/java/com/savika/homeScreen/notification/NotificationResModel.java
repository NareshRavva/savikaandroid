package com.savika.homeScreen.notification;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/*
 * Created by nexivo on 26/11/17.
 */

public class NotificationResModel {

    private boolean success;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    private String message;


    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    ArrayList<NotificationResObjModel> getArrNotifData() {
        return arrNotifData;
    }

    public void setArrNotifData(ArrayList<NotificationResObjModel> arrNotifData) {
        this.arrNotifData = arrNotifData;
    }

    @SerializedName("notification_list")
    private
    ArrayList<NotificationResObjModel> arrNotifData = new ArrayList<>();
}
