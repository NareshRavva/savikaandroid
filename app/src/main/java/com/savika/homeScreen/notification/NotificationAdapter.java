package com.savika.homeScreen.notification;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.savika.R;
import com.savika.utils.AppUtils;
import com.savika.utils.ImageUtil;

import java.util.ArrayList;

/*
 * Created by nexivo on 26/11/17.
 */


public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.ViewHolder> {

    private LayoutInflater mInflater;
    private ArrayList<NotificationResObjModel> arrData = new ArrayList<>();
    private Context context;
    private ItemClickListener mClickListener;

    NotificationAdapter(Context context, ArrayList<NotificationResObjModel> arrData) {
        this.mInflater = LayoutInflater.from(context);
        this.arrData = arrData;
        this.context = context;

    }




    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.list_item_notification, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final NotificationResObjModel model = arrData.get(position);
        if (model.getView().equalsIgnoreCase("no")) holder.card_view.setBackgroundResource(R.drawable.commnets_bg);
        holder.tvMsg.setText(model.getMsg());
        holder.tvTime.setText(AppUtils.timeAgo(Long.parseLong((model.getTime()))));
        ImageUtil.LoadPicasoCicular(context, holder.profie, model.getImage_url());

    }

    @Override
    public int getItemCount() {
        return arrData.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView tvMsg, tvTime;
        ImageView profie;
        public LinearLayout card_view;

        public ViewHolder(View itemView) {
            super(itemView);
            tvMsg = itemView.findViewById(R.id.tvMsg);
            tvTime = itemView.findViewById(R.id.tvTime);
            profie = itemView.findViewById(R.id.img_profile);
            card_view = itemView.findViewById(R.id.card_view);
            card_view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) {
                mClickListener.onItemClick(view, arrData.get(getAdapterPosition()));
                notifyDataSetChanged();
            }
        }
    }

    public NotificationResObjModel getItem(int id) {
        return arrData.get(id);
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    public interface ItemClickListener {
        void onItemClick(View view, NotificationResObjModel model);
    }
}
