package com.savika.homeScreen.notification;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;

import com.savika.Api.ApiClient;
import com.savika.Api.ApiInterface;
import com.savika.Api.CommonResModel;
import com.savika.R;
import com.savika.calendar.DetailCalenderRecordActivity;
import com.savika.community.CommunityDetailsScreen;
import com.savika.community.PostNotificationsDataReq;
import com.savika.components.customtv.textview.TextView;
import com.savika.constants.AppConstants;
import com.savika.homeScreen.HomeActivity;
import com.savika.homeScreen.cards.HomeCardModel;
import com.savika.logger.Logger;
import com.savika.recommendReadings.ArticalsModel;
import com.savika.recommendReadings.RecommendReadingsActivity;
import com.savika.utils.AppUtils;
import com.savika.utils.SharedPrefsUtils;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationActivity extends AppCompatActivity implements NotificationAdapter.ItemClickListener {

    ArrayList<NotificationResObjModel> ARR_NOTIFICATION = new ArrayList<>();

    @BindView(R.id.tvNoRecords)
    TextView tvNoRecords;

    @BindView(R.id.rvNotifications)
    RecyclerView rvNotifications;

    @BindView(R.id.llBack)
    LinearLayout llBack;

    @BindView(R.id.tvHeaderTitle)
    TextView tvHeaderTitle;

    NotificationAdapter adapter;

    static NotificationActivity notificationActivity;

    public String TAG = "Notifications";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        ButterKnife.bind(this);
        tvHeaderTitle.setText(TAG);
        llBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        notificationActivity = this;
    }

    @Override
    protected void onResume() {
        super.onResume();
        getNotifications();
    }

    private void getNotifications() {
        if (AppUtils.isNetworkConnected(this)) {
            AppUtils.showDialog(this);

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            String userid = SharedPrefsUtils.getUserID(this);
            Call<NotificationResModel> call = apiService.getNotifications(SharedPrefsUtils.getToken(getApplicationContext()),userid);
            call.enqueue(new retrofit2.Callback<NotificationResModel>() {
                @Override
                public void onResponse(Call<NotificationResModel> call, retrofit2.Response<NotificationResModel> response) {
                    AppUtils.dismissDialog();
                    if (response.code() == 200 && response.body().isSuccess()) {
                        ARR_NOTIFICATION = response.body().getArrNotifData();
                        setDataToAdapter(ARR_NOTIFICATION);
                    } else {
                        tvNoRecords.setVisibility(View.VISIBLE);
                        rvNotifications.setVisibility(View.GONE);
                        if (response.body().getMessage()!=null){
                            if (response.body().getMessage().equalsIgnoreCase("Unauthorized")){
                                AppUtils.sessionExpired(NotificationActivity.this);
                            }
                            else AppUtils.showToast(getApplicationContext(),response.body().getMessage());
                        }
                    }
                }

                @Override
                public void onFailure(Call<NotificationResModel> call, Throwable t) {
                    AppUtils.dismissDialog();
                }
            });
        }
        else {
            AppUtils.showToast(this,"It looks like your internet connectioin off. Please turn it on and try again.");
        }
    }

    @Override
    public void onItemClick(View view, NotificationResObjModel model) {
        postNotificationsData(NotificationActivity.this,model.getN_id(),model.getUserid(),model.getNotification_type());
        if (model.getNotification_type().contains("community")) {
            Intent notifIntent = new Intent(this, CommunityDetailsScreen.class);
            notifIntent.putExtra("nid", model.getId());
            notifIntent.putExtra("from_notifications", false);
            startActivity(notifIntent);
        }
        else if (model.getNotification_type().equalsIgnoreCase("article")){
            if (model.getA_date()!=null)
                openSpecificArticle(model);
        }
        else if (model.getNotification_type().equalsIgnoreCase("reminder")){
            gotoReminder(model);
        }
        else if (model.getNotification_type().equalsIgnoreCase("wish")){
            Intent ho = new Intent(this,HomeActivity.class);
            startActivity(ho);
            finishAffinity();
        }
    }

    private void gotoReminder(NotificationResObjModel model) {
        HomeCardModel cardModel = new HomeCardModel();
        cardModel.setUserid(model.getUserid());
        cardModel.setTitle(model.getMsg());
        cardModel.setDescription(model.getDescription());
        cardModel.setTime(model.getC_time());
        cardModel.setImage_url(model.getImage_url());
        cardModel.setC_id(model.getN_id());
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        Date date = new Date();
        date.setTime(Long.parseLong(model.getTime().trim()));
        cardModel.setDate(sdf.format(date));
        cardModel.setRemind_before(model.getRemind_before());
        cardModel.setCard_type(AppConstants.REMINDERS);
        Intent detail = new Intent(NotificationActivity.this, DetailCalenderRecordActivity.class);
        detail.putExtra("comingfrom", "card");
        detail.putExtra(AppConstants.EXTRA_HEALTH_RECORD, cardModel);
        startActivity(detail);

    }

    private void openSpecificArticle(NotificationResObjModel cardModel) {
        ArticalsModel articalsModel = new ArticalsModel();
        articalsModel.setCatid(cardModel.getCatid());
        articalsModel.setId(cardModel.getN_id());
        articalsModel.setContent(cardModel.getDescription());
        articalsModel.setTitle(cardModel.getMsg());
        articalsModel.setImageurl(cardModel.getImage_url());
        articalsModel.setWebsite(cardModel.getWebsite());
        articalsModel.setWebsiteurl(cardModel.getWebsiteurl());
        articalsModel.setCreatedat(cardModel.getA_date());
        Intent articleintent  = new Intent(NotificationActivity.this,RecommendReadingsActivity.class);
        articleintent.putExtra("from","card");
        articleintent.putExtra("article",articalsModel);
        startActivity(articleintent);
    }

    private void setDataToAdapter(ArrayList<NotificationResObjModel> arr_notification) {

        if (arr_notification != null && arr_notification.size() > 0) {
            rvNotifications.setLayoutManager(new LinearLayoutManager(this));

            adapter = new NotificationAdapter(NotificationActivity.this, arr_notification);
            adapter.setClickListener(this);
            rvNotifications.setAdapter(adapter);

            tvNoRecords.setVisibility(View.GONE);
            rvNotifications.setVisibility(View.VISIBLE);
        } else {
            tvNoRecords.setVisibility(View.VISIBLE);
            rvNotifications.setVisibility(View.GONE);
        }
    }

    public static void postNotificationsData(final AppCompatActivity context, String id, String userID, String notification_type) {

        if (AppUtils.isNetworkConnected(context)) {
            //AppUtils.showDialog(this);
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            PostNotificationsDataReq model = new PostNotificationsDataReq();
            model.setId(id);
            model.setUserid(userID);
            model.setNotification_type(notification_type);

            Call<CommonResModel> call = apiService.updatePostNotificationsData(SharedPrefsUtils.getToken(context),model);

            call.enqueue(new Callback<CommonResModel>() {
                @Override
                public void onResponse(Call<CommonResModel> call, Response<CommonResModel> response) {
                    Logger.info("TAG", "status code-->" + response.code());
                    AppUtils.dismissDialog();

                    if (response.body().getMessage()!=null){
                        if (response.body().getMessage().equalsIgnoreCase("Unauthorized")){
                            AppUtils.sessionExpired(context);
                        }
                        else AppUtils.showToast(context,response.body().getMessage());
                    }
                }

                @Override
                public void onFailure(Call<CommonResModel> call, Throwable t) {
                    AppUtils.dismissDialog();
                }
            });
        }
        else {
            AppUtils.showToast(context,"It looks like your internet connectioin off. Please turn it on and try again.");
        }
    }
}
