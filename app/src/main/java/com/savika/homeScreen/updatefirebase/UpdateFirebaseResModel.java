package com.savika.homeScreen.updatefirebase;

/*
 * Created by nexivo on 1/12/17.
 */

public class UpdateFirebaseResModel {

    private boolean success;
    private String message;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
