package com.savika.homeScreen.updatefirebase;

/*
 * Created by nexivo on 1/12/17.
 */

public class UpdateFirebaseResObjModel {

    private String firebaseid, userid;

    public String getFirebaseid() {
        return firebaseid;
    }

    public void setFirebaseid(String firebaseid) {
        this.firebaseid = firebaseid;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }


}
