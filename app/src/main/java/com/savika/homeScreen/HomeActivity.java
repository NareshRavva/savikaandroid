package com.savika.homeScreen;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ShareCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.savika.AboutUs;
import com.savika.Api.ApiClient;
import com.savika.Api.ApiInterface;
import com.savika.Api.CommonResModel;
import com.savika.FAQActivity;
import com.savika.GlobalConfigResObjModel;
import com.savika.pregnant.PregnancyActivity;
import com.savika.SettingsActivity;
import com.savika.WebView;
import com.savika.calendar.CalendarActivity;
import com.savika.ContactUs;
import com.savika.R;

import com.savika.Splash;
import com.savika.amAMother.AddChildDataActivity;
import com.savika.amAMother.ChildModel;
import com.savika.calendar.DetailCalenderRecordActivity;
import com.savika.community.CommunityActivity;
import com.savika.constants.AppConstants;
import com.savika.firebase.FbConstants;
import com.savika.homeScreen.CustomProgress.CustomProgressBar;
import com.savika.homeScreen.CustomProgress.ProgressItem;
import com.savika.homeScreen.cards.HomeCardAdapter;
import com.savika.homeScreen.cards.HomeCardModel;
import com.savika.homeScreen.navDrawer.HomeMenuAdapter;
import com.savika.homeScreen.navDrawer.MenuModel;
import com.savika.homeScreen.notification.NotificationActivity;
import com.savika.homeScreen.updatefirebase.UpdateFirebaseResModel;
import com.savika.homeScreen.updatefirebase.UpdateFirebaseResObjModel;
import com.savika.profile.ProfileEditActivity;
import com.savika.recommendReadings.ArticalsModel;
import com.savika.tracking.Track;
import com.savika.logger.Logger;
import com.savika.recommendReadings.RecommendReadingsActivity;
import com.savika.dialogUtils.AlertDialogUtility;
import com.savika.utils.AppUtils;
import com.savika.utils.ImageUtil;
import com.savika.utils.SharedPrefsUtils;

import java.io.IOException;
import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

public class HomeActivity extends AppCompatActivity implements View.OnClickListener, HomeCardAdapter.ItemClickListener {

    @BindView(R.id.listView)
    ListView listView;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.drawer_layout)
    DrawerLayout drawer_layout;

    @BindView(R.id.nav_view)
    NavigationView navigationView;

    @BindView(R.id.tvLoginUserName)
    TextView tvLoginUserName;

    @BindView(R.id.tvEditProfile)
    TextView tvEditProfile;

    @BindView(R.id.img_profile)
    ImageView imgProfile;

    @BindView(R.id.btnReadings)
    TextView btnReadings;

    @BindView(R.id.btnCalndr)
    TextView btnCalndr;

    @BindView(R.id.btnTrack)
    TextView btnTrack;

    @BindView(R.id.btnCommunity)
    TextView btnCommunity;

    @BindView(R.id.rvHealthCards)
    RecyclerView rvHealthTips;

    @BindView(R.id.llCardMother)
    LinearLayout llCardMother;

    @BindView(R.id.llCardChild1)
    LinearLayout llCardChild1;

    @BindView(R.id.llCardChild2)
    LinearLayout llCardChild2;

    @BindView(R.id.tvMother)
    TextView tvMother;

    @BindView(R.id.tvTrimester)
    TextView tvTrimester;

    @BindView(R.id.tvTrimesterData)
    TextView tvTrimesterData;

    @BindView(R.id.tvChild1)
    TextView tvChild1;

    @BindView(R.id.tvWeeksPasd)
    TextView tvWeeksPasd;

    @BindView(R.id.tvWeeksPasdData)
    TextView tvWeeksPasdData;

    @BindView(R.id.tvChild2)
    TextView tvChild2;

    @BindView(R.id.tvDaysDue)
    TextView tvDaysDue;

    @BindView(R.id.tvDaysDueData)
    TextView tvDaysDueData;

    @BindView(R.id.tvTodayDate)
    TextView tvTodayDate;

    @BindView(R.id.tvWishMsg)
    TextView tvWishMsg;

    @BindView(R.id.tvTrimesterDatasuff)
    TextView tvTrimesterDatasuff;

    @BindView(R.id.tvTrimesterDataLayout)
    LinearLayout tvTrimesterDataLayout;

    @BindView(R.id.progresshome)
    CustomProgressBar progressmonths;

    @BindView(R.id.rlNotifications)
    RelativeLayout rlNotifications;

//    @BindView(R.id.tvHealthMsg)
//    TextView tvHealthMsg;

    @BindView(R.id.ivNavMenu)
    ImageView ivNavMenu;

    @BindView(R.id.rlProfile)
    RelativeLayout rlProfile;

    @BindView(R.id.tvCount)
    TextView tvCount;

    public LinearLayoutManager layoutManager;
    public HomeCardAdapter adapter;
    private Address locationaddress;
    private String usercity;

    private String tracker="";
    private String mandatory = "false";

    ArrayList<ChildModel> ARR_CHILD_DATA = new ArrayList<>();
    ArrayList<HomeCardModel> ARR_CARD_DATA = new ArrayList<>();

    private String TAG = this.getClass().getSimpleName();

    public LayoutInflater inflater;

    private String wishMsg;

    private DatabaseReference root;

    String TYPE;
    private String CHILD_ID = null;

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String message = intent.getStringExtra("message");
            Log.d("receiver", "Received Unread Count: " + message);
            updateNotificationCount();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        inflater = this.getLayoutInflater();

        root = FirebaseDatabase.getInstance().getReference();

        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        tvCount.setText(String.format(Locale.getDefault(),"%d", SharedPrefsUtils.getUnReadNoticationsCount(this)));

        TYPE = AppUtils.getMotherOrPregnantType(this);

        Logger.info(TAG, "--getUserID-->" + SharedPrefsUtils.getUserID(this));

        loadNavMenu();

        setUpNavigationView();

        setToolbarTitle("");
        progressmonths.getThumb().mutate().setAlpha(0);
        initDataToSeekbar(4);

        btnReadings.setOnClickListener(this);
        btnCalndr.setOnClickListener(this);
        btnTrack.setOnClickListener(this);
        btnCommunity.setOnClickListener(this);
        llCardMother.setOnClickListener(this);
        ivNavMenu.setOnClickListener(this);
        rlProfile.setOnClickListener(this);
        tvEditProfile.setOnClickListener(this);
        tvLoginUserName.setOnClickListener(this);
        imgProfile.setOnClickListener(this);
        rlNotifications.setOnClickListener(this);
        handleFirstCardsDependsUponType();

        tvTodayDate.setText(getTodatDate());
        tvWishMsg.setText(String.format(Locale.getDefault(),"%s, %s", getWishMsg(), SharedPrefsUtils.getUsername(this)));

        if (SharedPrefsUtils.getUserID(this) == null || SharedPrefsUtils.getUserID(this).length() <= 0 || SharedPrefsUtils.getUsername(this).length() <= 1) {
            logout();
        }

        updateProfileAtFirebase();

        updateUserPresence();
        if (SharedPrefsUtils.getGlobalConfig(this) != null) {

            Gson gson = new Gson();
            String json = SharedPrefsUtils.getGlobalConfig(this);
            Type type = new TypeToken<ArrayList<GlobalConfigResObjModel>>() {
            }.getType();
            ArrayList<GlobalConfigResObjModel> arrayList = gson.fromJson(json, type);
            saveGlobalRecords(arrayList);

        }
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            requestGpsPermission();

        } else {
            requestLocationUpdates();
        }


        //register broadcast
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
                new IntentFilter(AppConstants.BR_NOTIFICATION_COUNT));

            if (getIntent().getBooleanExtra("from_notifications", false)) {
               NotificationActivity.postNotificationsData(HomeActivity.this,getIntent().getStringExtra("n_id"),getIntent().getStringExtra("userID"),getIntent().getStringExtra("notif_type"));
            }



    }

    private void requestLocationUpdates() {
        LocationRequest request = new LocationRequest();
        request.setInterval(10000);
        request.setFastestInterval(5000);
        request.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        FusedLocationProviderClient client = LocationServices.getFusedLocationProviderClient(this);
        int permission = ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION);
        if (permission == PackageManager.PERMISSION_GRANTED) {
            // Request location updates and when an update is
            // received, store the location in Firebase
            client.requestLocationUpdates(request, new LocationCallback() {
                @Override
                public void onLocationResult(LocationResult locationResult) {
                    Location location = locationResult.getLastLocation();
                    if (location != null) {
                        Log.d(TAG, "location update " + location);
                        Geocoder gcd = new Geocoder(getApplicationContext(), Locale.getDefault());
                        List<Address> addresses;
                        try {
                            addresses = gcd.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
                            if (addresses.size() > 0) {
                                System.out.println(addresses);
                                locationaddress  = addresses.get(0);
                                tracker = addresses.get(0).getLocality();
                                if (usercity!=null && !usercity.equalsIgnoreCase(tracker)){
                                    updateUserTracking();
                                    client.removeLocationUpdates(this);
                                }
                            }
                        } catch (IOException e) {
                                e.printStackTrace();
                        }
                    }
                }
            }, null);
        }
    }

    private void requestGpsPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.ACCESS_FINE_LOCATION) && ActivityCompat.shouldShowRequestPermissionRationale(this,Manifest.permission.ACCESS_COARSE_LOCATION)){
            Snackbar.make(btnReadings, "Gps permission is needed to show the Gps Location.",
                    Snackbar.LENGTH_INDEFINITE)
                    .setAction("Ok", view -> ActivityCompat.requestPermissions(HomeActivity.this,
                            new String[]{Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION},
                            123))
                    .show();
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION},
                    123);
        }
    }

    @Override
    protected void onDestroy() {
        // Unregister since the activity is about to be closed.
        //LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
        super.onDestroy();
    }


    private void initDataToSeekbar(int j) {
        ArrayList<ProgressItem> progressItemList = new ArrayList<>();
        // red span
        String[] defcolors = {"#ffffff","#fafbfc","#f8f9fa","#f1f4f6","#e9edf0","#e0e6ea","#d6dde3","#cbd2d9","#b6bcc1"};
        String[] filledcolors = {"#ffdc76","#ffcd3e","#febd01","#ff953e","#fe7301","#d86100","#91dea8","#64d084","#33c15d"};
        for (int i = 1; i < 10; i++) {
            ProgressItem mProgressItem = new ProgressItem();
            mProgressItem.progressItemPercentage = 10.3f;
            if (i < j) {
                mProgressItem.color = filledcolors[i-1];
            } else {
                mProgressItem.color = defcolors[i-1];
            }

            progressItemList.add(mProgressItem);
            if (i == 3|| i==6) {
                mProgressItem = new ProgressItem();
                mProgressItem.progressItemPercentage = 3.5f;
                mProgressItem.color = defcolors[0];
                progressItemList.add(mProgressItem);
            }
        }
        //white span
        progressmonths.initData(progressItemList);
        progressmonths.invalidate();
    }

    @Override
    protected void onResume() {
        tvLoginUserName.setText(SharedPrefsUtils.getUsername(this));
        ImageUtil.LoadPicasoCicular(this, imgProfile, SharedPrefsUtils.getProfilePic(this));
        tvWishMsg.setText(String.format(Locale.getDefault(),"%s, %s", getWishMsg(), SharedPrefsUtils.getUsername(this)));
        updateNotificationCount();
        getDashBoardData();
        super.onResume();
    }

    private void updateNotificationCount() {
        if(SharedPrefsUtils.getUnReadNoticationsCount(HomeActivity.this)>0){
            tvCount.setVisibility(View.VISIBLE);

            tvCount.setText(String.format(Locale.getDefault(),"%d", SharedPrefsUtils.getUnReadNoticationsCount(HomeActivity.this)));
        }
        else {
            tvCount.setVisibility(View.INVISIBLE);
        }

    }

    private void saveGlobalRecords(ArrayList<GlobalConfigResObjModel> globalRecords) {
        int k = 0;
        for (int i=0;i<globalRecords.size();i++){
            if (globalRecords.get(i).getType().equalsIgnoreCase("version_control")){
                k=i;
                mandatory = globalRecords.get(i).getMandatory();
                break;
            }
        }
        try {
            if (!getPackageManager().getPackageInfo(getPackageName(), 0).versionName.equalsIgnoreCase(globalRecords.get(k).getVersion()) && globalRecords.get(k).getStatus().equalsIgnoreCase("active")) {
                if (mandatory.equalsIgnoreCase("true")){
                    showUpdateMandatoryDialog();
                }
                else showUpdateDialog();
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void showUpdateMandatoryDialog() {
        runOnUiThread(new AlertDialogUtility(HomeActivity.this, inflater, "Update Available", "New Version Available. Please Update Application", "Update", view -> {
            // Toast.makeText(HomeActivity.this, "yes", Toast.LENGTH_SHORT).show();
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + getPackageName())));
        }, false));
    }


    private void showUpdateDialog() {
        runOnUiThread(new AlertDialogUtility(HomeActivity.this, inflater, "Update Available", "New Version Available. Please Update Application", "Update", "Later", view -> {
           // Toast.makeText(HomeActivity.this, "yes", Toast.LENGTH_SHORT).show();
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + getPackageName())));
        }, view -> {
        }, true));
    }


    /**
     * PREGNANT = "1";
     * MOTHER = "2";
     */
    private void handleFirstCardsDependsUponType() {
        String type = SharedPrefsUtils.getMotherORPrgntType(this);
        Logger.info(TAG, "Type-->" + type);

        if (type.equalsIgnoreCase(AppConstants.PREGNANT)) {
            tvTrimester.setVisibility(View.VISIBLE);
            tvTrimesterDataLayout.setVisibility(View.VISIBLE);

            tvWeeksPasd.setVisibility(View.VISIBLE);
            tvWeeksPasdData.setVisibility(View.VISIBLE);

            tvDaysDue.setVisibility(View.VISIBLE);
            tvDaysDueData.setVisibility(View.VISIBLE);
        } else {
            tvMother.setVisibility(View.VISIBLE);
            tvChild1.setVisibility(View.VISIBLE);
            tvChild2.setVisibility(View.VISIBLE);

            setCardBg(llCardMother, tvMother);
            TYPE = AppConstants.STR_MOTHER;
        }
    }

    /*
     * @param arrTips
     */
    private void setTipsData(ArrayList<HomeCardModel> arrTips) {
        layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, true);
        layoutManager.setReverseLayout(true);
        layoutManager.setStackFromEnd(true);
        rvHealthTips.setLayoutManager(layoutManager);


        adapter = new HomeCardAdapter(HomeActivity.this, arrTips);
        adapter.setClickListener(this);
        rvHealthTips.setAdapter(adapter);
    }

    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.rlNotifications:
                //clear unread count
                SharedPrefsUtils.setUnReadNoticationsCount(this, 0);
                tvCount.setVisibility(View.INVISIBLE);
                Intent notifintent = new Intent(HomeActivity.this,NotificationActivity.class);
                startActivity(notifintent);

                break;
            case R.id.rlProfile:
                drawer_layout.closeDrawer(navigationView);
                break;

            case R.id.btnReadings:
                Intent articalsScreen = new Intent(HomeActivity.this, RecommendReadingsActivity.class);
                startActivity(articalsScreen);
                break;

            case R.id.btnCalndr:
                Intent calndr = new Intent(HomeActivity.this, CalendarActivity.class);
                calndr.putExtra(AppConstants.EXTRA_CHILD_DATA, ARR_CHILD_DATA);
                startActivity(calndr);
                break;

            case R.id.btnTrack:
                Intent track = new Intent(HomeActivity.this, Track.class);
                track.putExtra(AppConstants.EXTRA_CHILD_DATA, ARR_CHILD_DATA);
                startActivity(track);
                break;

            case R.id.btnCommunity:
                Intent cmunty = new Intent(HomeActivity.this, CommunityActivity.class);
                startActivity(cmunty);
                break;

            case R.id.llCardMother:
                setCardBg(llCardMother, tvMother);
                TYPE = AppConstants.STR_MOTHER;
                break;

            case R.id.llCardChild1:
                setCardBg(llCardChild1, tvChild1);

                TYPE = AppConstants.STR_CHILD;

                if (ARR_CHILD_DATA.size() > 0 && ARR_CHILD_DATA.get(0) != null)
                    CHILD_ID = ARR_CHILD_DATA.get(0).getChildid();
                break;

            case R.id.llCardChild2:
                setCardBg(llCardChild2, tvChild2);

                TYPE = AppConstants.STR_CHILD;

                if (ARR_CHILD_DATA.size() > 0 && ARR_CHILD_DATA.get(0) != null)
                    CHILD_ID = ARR_CHILD_DATA.get(0).getChildid();

                break;

            case R.id.ivNavMenu:
                if (drawer_layout.isDrawerOpen(navigationView)) {
                    drawer_layout.closeDrawer(navigationView);
                } else {
                    drawer_layout.openDrawer(navigationView);
                }
                break;
            case R.id.tvEditProfile:
                Intent profile_edit = new Intent(HomeActivity.this, ProfileEditActivity.class);
                startActivity(profile_edit);
                break;
            case R.id.img_profile:
                Intent profile_edit1 = new Intent(HomeActivity.this, ProfileEditActivity.class);
                startActivity(profile_edit1);
                break;
            case R.id.tvLoginUserName:
                Intent profile_edit2 = new Intent(HomeActivity.this, ProfileEditActivity.class);
                startActivity(profile_edit2);
                break;
        }
    }

    /*
     * @param llCard
     * @param tvTxt
     */
    private void setCardBg(LinearLayout llCard, TextView tvTxt) {
        llCardMother.setBackgroundResource(R.drawable.pink_home_cards_in_active);
        llCardChild1.setBackgroundResource(R.drawable.pink_home_cards_in_active);
        llCardChild2.setBackgroundResource(R.drawable.pink_home_cards_in_active);

        tvMother.setTextColor(getResources().getColor(R.color.gray));
        tvChild1.setTextColor(getResources().getColor(R.color.gray));
        tvChild2.setTextColor(getResources().getColor(R.color.gray));

        llCard.setBackgroundResource(R.drawable.pink_home_cards_active);
        tvTxt.setTextColor(getResources().getColor(R.color.white));
    }


    private void loadNavMenu() {

        final ArrayList<MenuModel> arrData = new ArrayList<>();

        arrData.add(new MenuModel(getString(R.string.menu_pregnancy), R.drawable.menu_pregnancy));
        arrData.add(new MenuModel(getString(R.string.menu_settings), R.drawable.menu_settings));
        if (TYPE.equalsIgnoreCase(AppConstants.STR_MOTHER))
            arrData.add(new MenuModel(getString(R.string.menu_child_info), R.drawable.menu_child_info));
        arrData.add(new MenuModel(getString(R.string.menu_about_us), R.drawable.menu_about));
        arrData.add(new MenuModel(getString(R.string.menu_contact_us), R.drawable.menu_contacts_us));
        arrData.add(new MenuModel(getString(R.string.menu_chat_with_savika), R.drawable.menu_chat));
        arrData.add(new MenuModel(getString(R.string.menu_share_app), R.drawable.menu_share_app));
        arrData.add(new MenuModel(getString(R.string.menu_rate_us), R.drawable.menu_rate_us));
        arrData.add(new MenuModel(getString(R.string.menu_feedback), R.drawable.menu_feedback));
        arrData.add(new MenuModel(getString(R.string.menu_faq), R.drawable.menu_faq));
        arrData.add(new MenuModel(getString(R.string.menu_logout), R.drawable.menu_logout));

        HomeMenuAdapter homeMenuAdapter = new HomeMenuAdapter(this, arrData);
        listView.setAdapter(homeMenuAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                drawer_layout.closeDrawers();

                switch (arrData.get(i).getTitle()) {

                    case "Pregnancy":
                        Intent pregnancy = new Intent(HomeActivity.this, PregnancyActivity.class);
                        startActivity(pregnancy);
                        break;

                    case "Settings":
                        Intent addSetting = new Intent(HomeActivity.this, SettingsActivity.class);
                        startActivity(addSetting);
                        break;

                    case "Child Info":
                        Intent addChild = new Intent(HomeActivity.this, AddChildDataActivity.class);
                        startActivity(addChild);
                        break;

                    case "About Us":
                        aboutUs();
                        break;

                    case "Contact Us":
                        contactUs();
                        break;

                    case "Chat with Savika":
                        chatWithSavika();
                        break;

                    case "Share App":
                        shareApp();
                        break;

                    case "Rate Us":
                        rateOurApp();
                        break;

                    case "Feedback":
                        Intent intent = new Intent(Intent.ACTION_SEND);
                        intent.setType("message/rfc822");
                        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{"hello@psynapce.com"});
                        intent.putExtra(Intent.EXTRA_SUBJECT, "Feedback from "+SharedPrefsUtils.getUsername(HomeActivity.this));
                        try {
                            intent.putExtra(Intent.EXTRA_TEXT, "\n\n\n\n\nApp Name:" + getResources().getString(R.string.app_name) +
                                    "\nVersion Name:" + getPackageManager().getPackageInfo(getPackageName(), 0).versionName + "\nVersion Code:" + getPackageManager().getPackageInfo(getPackageName(), 0).versionCode);
                        } catch (PackageManager.NameNotFoundException e) {
                            e.printStackTrace();
                        }
                        intent.setPackage("com.google.android.gm");
                        if (intent.resolveActivity(getPackageManager()) != null)
                            startActivity(intent);
                        break;

                    case "FAQ\'s":
                        Intent faqs = new Intent(HomeActivity.this, FAQActivity.class);
                        startActivity(faqs);
                        break;

                    case "Logout":
                        logout();
                        break;

                    default:
                        System.out.println("INVALID TITLE CLICKED");
                }
            }
        });
    }

    /**
     * chat with savika
     */
    private void chatWithSavika() {
        try {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("fb://messaging/" + "118722698712380")));
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(this, "Facebook App is not installed", Toast.LENGTH_SHORT).show();
        }
    }

    /*
     * @param title
     */
    private void setToolbarTitle(String title) {
        getSupportActionBar().setTitle(title);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#ffffff")));
    }

    private void setUpNavigationView() {
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawer_layout, toolbar, R.string.openDrawer, R.string.closeDrawer) {

            @Override
            public void onDrawerClosed(View drawerView) {
                // Code here will be triggered once the drawer_layout closes as we dont want anything to happen so we leave this blank
                super.onDrawerClosed(drawerView);
                loadNavMenu();
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                // Code here will be triggered once the drawer_layout open as we dont want anything to happen so we leave this blank
                super.onDrawerOpened(drawerView);
            }
        };

        //Setting the actionbarToggle to drawer_layout layout
        drawer_layout.setDrawerListener(actionBarDrawerToggle);

        //calling sync state is necessary or else your hamburger icon wont show up
        actionBarDrawerToggle.syncState();
    }

    @Override
    public void onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawers();
            return;
        }

        super.onBackPressed();
    }

    @Override
    public void onItemClick(View view, HomeCardModel cardModel) {
        String card_type = cardModel.getModule();
        if (card_type.equalsIgnoreCase("TYPE_TIP")) {
            if (cardModel.getClick_action().equalsIgnoreCase("true")) {
                if (cardModel.getCard_type().equalsIgnoreCase(AppConstants.REMINDERS) || cardModel.getCard_type().equalsIgnoreCase(AppConstants.MILESTONES)) {
                    Intent detail_screen = new Intent(HomeActivity.this, DetailCalenderRecordActivity.class);
                    detail_screen.putExtra("comingfrom", "card");
                    detail_screen.putExtra(AppConstants.EXTRA_HEALTH_RECORD, cardModel);
                    startActivity(detail_screen);
                } else if (cardModel.getCard_type().equalsIgnoreCase("articles")) {
                    openSpecificArticle(cardModel);
                }
                Logger.info(TAG, "position-->" + cardModel.getId() + "---catName--->" + cardModel.getTitle());
            }
        } else if (card_type.equalsIgnoreCase("TYPE_BANNER")) {
            openWebView(cardModel);
            Logger.info(TAG, "position-->" + cardModel.getId() + "---catName--->" + cardModel.getTitle());
        } else {
            Logger.info(TAG, "position-->" + cardModel.getId() + "---catName--->" + cardModel.getTitle());
        }
    }

    private void openWebView(HomeCardModel model) {
        if (model.getImage_url() != null && model.getImage_url().length() > 1) {
            Intent webView = new Intent(this, WebView.class);
            webView.putExtra(AppConstants.WEB_URL, model.getImage_url());
            webView.putExtra(AppConstants.WEB_VIEW_TITLE, model.getTitle());
            startActivity(webView);
        }
    }

    private void openSpecificArticle(HomeCardModel cardModel) {
        ArticalsModel articalsModel = new ArticalsModel();
        articalsModel.setCatid(cardModel.getCatid());
        articalsModel.setId(cardModel.getC_id());
        articalsModel.setContent(cardModel.getDescription());
        articalsModel.setTitle(cardModel.getTitle());
        articalsModel.setImageurl(cardModel.getImage_url());
        articalsModel.setWebsite(cardModel.getWebsite());
        articalsModel.setWebsiteurl(cardModel.getWebsiteurl());
        articalsModel.setCreatedat(cardModel.getCreatedat());
        Intent articleintent  = new Intent(HomeActivity.this,RecommendReadingsActivity.class);
        articleintent.putExtra("from","card");
        articleintent.putExtra("article",articalsModel);
        startActivity(articleintent);
    }


    /*
     * post updated record
     */
    private void getDashBoardData() {

        //AppUtils.showDialog(this);
        if (AppUtils.isNetworkConnected(HomeActivity.this)) {
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            DashBoardRequestModel model = new DashBoardRequestModel();
            model.setType(TYPE);
            if (TYPE.equalsIgnoreCase(AppConstants.STR_CHILD)) {
                model.setChildid(CHILD_ID);
            }
            model.setUserid(SharedPrefsUtils.getUserID(this));
            model.setDate(AppUtils.getTodatDate());
            model.setModule("2");

            Call<DashBoardResModel> call = apiService.getDashBoardData(SharedPrefsUtils.getToken(this),model);

            call.enqueue(new retrofit2.Callback<DashBoardResModel>() {
                @Override
                public void onResponse(@NonNull Call<DashBoardResModel> call, @NonNull retrofit2.Response<DashBoardResModel> response) {

                    AppUtils.dismissDialog();

                    if (response.code() == 200 && response.body().isSuccess()) {

                        if (response.body().getMonths() != null) {
                            initDataToSeekbar((Integer.parseInt(response.body().getMonths())) + 1);
                        }
                        ARR_CHILD_DATA = new ArrayList<>();
                        ARR_CHILD_DATA = response.body().arrChildData;
                        ARR_CARD_DATA = new ArrayList<>();
                        ARR_CARD_DATA = response.body().getArrTipsRecords().getArr_card_records();
                        setTipsData(ARR_CARD_DATA);
                        Logger.info(TAG, "--profile img-->" + response.body().userDetails.getProfileimg());
                        if (response.body().getUserDetails().getFirebaseid()!=null){
                            if (!FirebaseInstanceId.getInstance().getToken().equalsIgnoreCase(response.body().getUserDetails().getFirebaseid())) {
                                updateFirebaseId(FirebaseInstanceId.getInstance().getToken());
                            }
                        }
                        else updateFirebaseId(FirebaseInstanceId.getInstance().getToken());
                        if (ARR_CHILD_DATA.size() > 0) {
                            Set<String> childIds = new HashSet<>();
                            for (ChildModel model : ARR_CHILD_DATA) {
                                childIds.add(model.getChildid());
                            }
                            SharedPrefsUtils.setChildIds(HomeActivity.this, childIds);
                        }
                        SharedPrefsUtils.setChildCount(HomeActivity.this, ARR_CHILD_DATA.size());
                        SharedPrefsUtils.setProfilePic(HomeActivity.this, response.body().userDetails.getProfileimg());
                        SharedPrefsUtils.setUsername(HomeActivity.this, response.body().userDetails.getUsername());
                        if (response.body().getUserDetails().getCity()!=null && !response.body().getUserDetails().getCity().isEmpty()){
                          usercity = response.body().getUserDetails().getCity();
                        }
                        else usercity = "";

                        tvLoginUserName.setText(SharedPrefsUtils.getUsername(HomeActivity.this));
                        ImageUtil.LoadPicasoCicular(HomeActivity.this, imgProfile, SharedPrefsUtils.getProfilePic(HomeActivity.this));
                        updateCardsActiveInActive(ARR_CHILD_DATA);

                        if (response.body().userDetails != null && response.body().userDetails.getTypeid().equalsIgnoreCase(AppConstants.PREGNANT)) {
                            setPregnantData(response.body().getPrgntModel());
                        }

                        if (response.body().getDevice_history()!=null){
                            if (response.body().getDevice_history().size() != 0){
                                for (int i=0;i<response.body().getDevice_history().size();i++){
                                    if ((Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID).equalsIgnoreCase(response.body().getDevice_history().get(i).getUserid())) && (getDeviceName().equalsIgnoreCase(response.body().getDevice_history().get(i).getDevicemodel())) && (android.os.Build.VERSION.RELEASE.equalsIgnoreCase(response.body().getDevice_history().get(i).getOsversion()))) {
                                        addDeviceDetails();
                                        break;
                                    }
                                }
                            }
                            else {
                                addDeviceDetails();
                            }
                        }
                    }
                    else{
                        if (response!=null && response.body()!=null && response.body().getMessage()!=null) {
                            if (response.body().getMessage().equalsIgnoreCase("Unauthorized")) {
                                drawer_layout.closeDrawers();
                                AppUtils.sessionExpired(HomeActivity.this);
                            }
                        }
                    }
                }

                @Override
                public void onFailure(@NonNull Call<DashBoardResModel> call, @NonNull Throwable t) {
                    AppUtils.dismissDialog();
                }
            });
        }
         else {
            AppUtils.showToast(HomeActivity.this,"It looks like your internet connectioin off. Please turn it on and try again.");
        }
    }

    private void updateUserTracking() {
        if (AppUtils.isNetworkConnected(HomeActivity.this)) {
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            UpdateUserTrackReqModel model = new UpdateUserTrackReqModel();
            model.setUserid(SharedPrefsUtils.getUserID(this));
            model.setCity(locationaddress.getLocality());
            model.setState(locationaddress.getAdminArea());
            model.setCountry(locationaddress.getCountryName());
            model.setLat(locationaddress.getLatitude());
            model.setLon(locationaddress.getLongitude());
            model.setLocality(locationaddress.getFeatureName());
            model.setPincode(locationaddress.getPostalCode());

            Call<CommonResModel> call = apiService.updateUserTracking(SharedPrefsUtils.getToken(this),model);

            call.enqueue(new retrofit2.Callback<CommonResModel>() {
                @Override
                public void onResponse(Call<CommonResModel> call, retrofit2.Response<CommonResModel> response) {


                    if (response.code() == 200 && response.body().isSuccess()) {

                    }
                    else{
                        if (response.body().getMessage()!=null) {
                            if (response.body().getMessage().equalsIgnoreCase("Unauthorized")) {
                                AppUtils.sessionExpired(HomeActivity.this);
                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call<CommonResModel> call, Throwable t) {
                    // AppUtils.dismissDialog();
                }
            });
        }
        else {
            AppUtils.showToast(HomeActivity.this,"It looks like your internet connectioin off. Please turn it on and try again.");
        }
    }

    private void addDeviceDetails() {
        if (AppUtils.isNetworkConnected(HomeActivity.this)) {
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            DeviceReqModel model = new DeviceReqModel();
            model.setUserid(SharedPrefsUtils.getUserID(this));
            model.setDeviceid(Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID));
            model.setDevicemodel(getDeviceName());
            model.setOsversion(android.os.Build.VERSION.RELEASE);

            Call<CommonResModel> call = apiService.addDeviceDetails(SharedPrefsUtils.getToken(this),model);

            call.enqueue(new retrofit2.Callback<CommonResModel>() {
                @Override
                public void onResponse(@NonNull Call<CommonResModel> call, @NonNull retrofit2.Response<CommonResModel> response) {


                    if (response.code() == 200 && response.body().isSuccess()) {
//                        AppUtils.showToast(getApplication(),response.body().getMessage());
                    }
                    else{
                        if (response.body().getMessage()!=null) {
                            if (response.body().getMessage().equalsIgnoreCase("Unauthorized")) {
                                AppUtils.sessionExpired(HomeActivity.this);
                            }
                        }
                    }
                }

                @Override
                public void onFailure(@NonNull Call<CommonResModel> call,@NonNull Throwable t) {

                }
            });
        }
        else {
            AppUtils.showToast(HomeActivity.this,"It looks like your internet connectioin off. Please turn it on and try again.");
        }
    }

    public String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        } else {
            return capitalize(manufacturer) + " " + model;
        }
    }

    private String capitalize(String s) {
        if (s == null || s.length() == 0) {
            return "";
        }
        char first = s.charAt(0);
        if (Character.isUpperCase(first)) {
            return s;
        } else {
            return Character.toUpperCase(first) + s.substring(1);
        }
    }

    /*
     * @param prgntModel
     */
    private void setPregnantData(PregnantDataModel prgntModel) {
        tvTrimesterData.setText(prgntModel.getTrimester());
        tvTrimesterDatasuff.setText(getDayNumberSuffix(Integer.parseInt(prgntModel.getTrimester())));
        tvWeeksPasdData.setText(prgntModel.getWeeks_Passed());
        tvDaysDueData.setText(prgntModel.getDays_due_in());

        llCardMother.setBackgroundResource(R.drawable.pink_home_cards_in_active);
        llCardChild1.setBackgroundResource(R.drawable.pink_home_cards_in_active);
        llCardChild2.setBackgroundResource(R.drawable.pink_home_cards_in_active);

        llCardMother.setOnClickListener(null);
        llCardChild1.setOnClickListener(null);
        llCardChild2.setOnClickListener(null);
    }

    private String getDayNumberSuffix(int day) {
        if (day >= 11 && day <= 13) {
            return "th";
        }
        switch (day % 10) {
            case 1:
                return "st";
            case 2:
                return "nd";
            case 3:
                return "rd";
            default:
                return "th";
        }
    }

    /*
     * @param arrChildData
     */
    private void updateCardsActiveInActive(ArrayList<ChildModel> arrChildData) {

        if (arrChildData.size() == 1) {
            llCardChild2.setAlpha(0.4f);
            llCardChild1.setOnClickListener(this);
            llCardChild2.setOnClickListener(null);

        } else if (arrChildData.size() == 2) {
            llCardChild1.setOnClickListener(this);
            llCardChild2.setOnClickListener(this);
        }
    }

    /*
     * get wish message
     *
     * @return
     */
    private String getWishMsg() {
        Calendar c = Calendar.getInstance();
        int timeOfDay = c.get(Calendar.HOUR_OF_DAY);

        if (timeOfDay >= 0 && timeOfDay < 12) {
            wishMsg = "Good Morning";
        } else if (timeOfDay >= 12 && timeOfDay < 16) {
            wishMsg = "Good Afternoon";
        } else if (timeOfDay >= 16 && timeOfDay < 21) {
            wishMsg = "Good Evening";
        } else if (timeOfDay >= 21 && timeOfDay < 24) {
            wishMsg = "Good Night";
        }
        return wishMsg;
    }

    /*
     * get today date
     *
     * @return
     */
    public static String getTodatDate() {
        DateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy", Locale.getDefault());
        Date date = new Date();
        System.out.println(dateFormat.format(date));
        return "" + dateFormat.format(date);
    }

    /**
     * Logout
     */
    private void logout() {
        runOnUiThread(new AlertDialogUtility(HomeActivity.this, inflater, getResources().getString(R.string.app_name), getResources().getString(R.string.are_you_sure_you_want_to_logout_), "OK", "Cancel", new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                drawer_layout.closeDrawers();
                SharedPrefsUtils.setUserID(HomeActivity.this, null);
                SharedPrefsUtils.setToken(getApplicationContext(),"");
                SharedPrefsUtils.setFirebaseToken(HomeActivity.this,"");

                Intent articals = new Intent(HomeActivity.this, Splash.class);
                articals.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                articals.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                articals.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(articals);
                finish();

            }
        }, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer_layout.closeDrawers();
            }
        }, true));
    }

    private void aboutUs() {
        Intent aboutUs = new Intent(HomeActivity.this, AboutUs.class);
        startActivity(aboutUs);
    }

    private void contactUs() {
        Intent aboutUs = new Intent(HomeActivity.this, ContactUs.class);
        startActivity(aboutUs);
    }


    private void rateOurApp() {
        String APP_PNAME = "com.savika";
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + APP_PNAME)));
    }

    private void shareApp() {
        ShareCompat.IntentBuilder.from(HomeActivity.this)
                .setType("text/plain")
                .setChooserTitle("Savika")
                .setText("Savika is a comprehensive pregnancy and pediatric healthcare app\n Please click on this link to install Savika.\nhttp://play.google.com/store/apps/details?id=" + getPackageName())
                .startChooser();
    }

    /**
     *
     */
    private void updateProfileAtFirebase() {

        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);
        if (SharedPrefsUtils.getFirebaseToken(this)!=null && (!SharedPrefsUtils.getFirebaseToken(this).equalsIgnoreCase(""))){
            if (!(SharedPrefsUtils.getFirebaseToken(HomeActivity.this).equalsIgnoreCase(refreshedToken))){
                updateFirebaseId(refreshedToken);
            }
        }
        else {
            updateFirebaseId(refreshedToken);
        }


        DatabaseReference dbPath = FirebaseDatabase.getInstance().getReference().
                child(FbConstants.TABLE_USERS).child(SharedPrefsUtils.getMobileNum(this));

        Map<String, Object> map2 = new HashMap<>();

        map2.put(FbConstants.FIREBASE_REG_ID, refreshedToken);

        map2.put(FbConstants.MOBILE_OS, FbConstants.ANDROID);

        String android_id = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
        map2.put(FbConstants.DEVICE_ID, android_id);

        map2.put(FbConstants.USER_NAME, SharedPrefsUtils.getUsername(this));

        map2.put(FbConstants.USER_PROFILE_PIC, SharedPrefsUtils.getProfilePic(this));

        map2.put(FbConstants.USER_TYPE, AppUtils.getMotherOrPregnantType(this));

        map2.put(FbConstants.USER_ID, SharedPrefsUtils.getUserID(this));

        map2.put(FbConstants.MOBILE_NUMBER, SharedPrefsUtils.getMobileNum(this));

        dbPath.updateChildren(map2);

    }

    private void updateFirebaseId(final String refreshedToken) {
        if (AppUtils.isNetworkConnected(HomeActivity.this)) {
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            UpdateFirebaseResObjModel model = new UpdateFirebaseResObjModel();
            model.setUserid(SharedPrefsUtils.getUserID(this));
            model.setFirebaseid(refreshedToken);

            Call<UpdateFirebaseResModel> call = apiService.updateFirebaseId(SharedPrefsUtils.getToken(getApplicationContext()),model);

            call.enqueue(new retrofit2.Callback<UpdateFirebaseResModel>() {
                @Override
                public void onResponse(@NonNull Call<UpdateFirebaseResModel> call, @NonNull retrofit2.Response<UpdateFirebaseResModel> response) {
                    if (response.code() == 200 && response.body().isSuccess()) {
                        Logger.info(TAG, "--updated token-->" + response.body().getMessage());
                        SharedPrefsUtils.setFirebaseToken(HomeActivity.this, refreshedToken);
                    } else {
                        Logger.info(TAG, "--updatetoken failed-->" + response.body().getMessage());
                        if (response.body().getMessage()!=null) {
                            if (response.body().getMessage().equalsIgnoreCase("Unauthorized")) {
                                AppUtils.sessionExpired(HomeActivity.this);
                            }
                        }
                    }
                }

                @Override
                public void onFailure(@NonNull Call<UpdateFirebaseResModel> call, @NonNull Throwable t) {
                    AppUtils.dismissDialog();
                }
            });
        }
        else {
            AppUtils.showToast(HomeActivity.this,"It looks like your internet connectioin off. Please turn it on and try again.");
        }
    }

    /**
     * update user presence Ex: Online,Offline..
     */
    private void updateUserPresence() {
        //Update token to firebase db
        DatabaseReference dbPath = root.child(FbConstants.TABLE_USERS).child(SharedPrefsUtils.getMobileNum(this));

        //SET VALUE AS '0' WHEN HE IS ONLINE
        dbPath.child(FbConstants.LASTSEEN).setValue(FbConstants.PRESENCE_ONLINE);

        //SET LASTSEEN TIME IN MILLESEC WHEN USER KILLS/NOT USING OUR APP
        dbPath.child(FbConstants.LASTSEEN).onDisconnect().setValue(ServerValue.TIMESTAMP);
    }


}
