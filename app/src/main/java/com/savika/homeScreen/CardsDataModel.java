package com.savika.homeScreen;

import com.google.gson.annotations.SerializedName;
import com.savika.homeScreen.cards.HomeCardModel;

import java.util.ArrayList;

/*
 * Created by nexivo on 6/12/17.
 */

public class CardsDataModel {


    ArrayList<HomeCardModel> getArr_card_records() {
        return arr_card_records;
    }

    public void setArr_card_records(ArrayList<HomeCardModel> arr_card_records) {
        this.arr_card_records = arr_card_records;
    }
    @SerializedName("cards")
    private
    ArrayList<HomeCardModel> arr_card_records = new ArrayList<>();
}
