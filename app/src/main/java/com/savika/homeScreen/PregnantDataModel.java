package com.savika.homeScreen;

import com.google.gson.annotations.SerializedName;

/*
 * Created by Naresh Ravva on 11/10/17.
 */

public class PregnantDataModel {

    @SerializedName("Trimester")
    private
    String Trimester;

    @SerializedName("Weeks Passed")
    private
    String Weeks_Passed;

    @SerializedName("Days Due in")
    private
    String days_due_in;

    public String getTrimester() {
        return Trimester;
    }

    public void setTrimester(String trimester) {
        Trimester = trimester;
    }

    public String getWeeks_Passed() {
        return Weeks_Passed;
    }

    public void setWeeks_Passed(String weeks_Passed) {
        Weeks_Passed = weeks_Passed;
    }

    public String getDays_due_in() {
        return days_due_in;
    }

    public void setDays_due_in(String days_due_in) {
        this.days_due_in = days_due_in;
    }
}
