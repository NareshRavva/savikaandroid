package com.savika.homeScreen.tips;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.savika.R;
import com.savika.utils.ImageUtil;

import java.util.ArrayList;

/*
 * Created by Naresh Ravva on 30/06/17.
 */

public class HomeTipsCardAdapter extends RecyclerView.Adapter<HomeTipsCardAdapter.ViewHolder> {

    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    private Context context;

    private ArrayList<HomeTipsCardModel> arrData = new ArrayList<>();

    public HomeTipsCardAdapter(Context context, ArrayList<HomeTipsCardModel> arrData) {
        this.mInflater = LayoutInflater.from(context);
        this.arrData = arrData;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.list_item_home_tips, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        HomeTipsCardModel model = arrData.get(position);
        holder.tvTitle.setText(model.getTipstitle());
        holder.tvMsg.setText(model.getTipsdescription());

        ImageUtil.LoadPicaso(context, holder.ivImg, model.getImgurl());

    }

    @Override
    public int getItemCount() {
        return arrData.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView tvTitle, tvMsg;
        ImageView ivImg;

        public ViewHolder(View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.tvTitle);
            tvMsg = itemView.findViewById(R.id.tvDesc);
            ivImg = itemView.findViewById(R.id.ivImg);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) {
                mClickListener.onItemClick(view, arrData.get(getAdapterPosition()).getIdtips(), arrData.get(getAdapterPosition()).getTipstitle());
                notifyDataSetChanged();
            }
        }
    }

    public HomeTipsCardModel getItem(int id) {
        return arrData.get(id);
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    public interface ItemClickListener {
        void onItemClick(View view, String position, String catName);
    }
}