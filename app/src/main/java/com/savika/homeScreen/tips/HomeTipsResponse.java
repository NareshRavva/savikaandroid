package com.savika.homeScreen.tips;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/*
 * Created by Naresh Ravva on 08/10/17.
 */

public class HomeTipsResponse implements Serializable {

    @SerializedName("tips")
    private
    ArrayList<HomeTipsCardModel> arrTips = new ArrayList<>();

    public ArrayList<HomeTipsCardModel> getArrTips() {
        return arrTips;
    }

    public void setArrTips(ArrayList<HomeTipsCardModel> arrTips) {
        this.arrTips = arrTips;
    }
}
