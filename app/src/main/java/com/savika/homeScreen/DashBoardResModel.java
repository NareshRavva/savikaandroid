package com.savika.homeScreen;

import com.google.gson.annotations.SerializedName;
import com.savika.amAMother.ChildModel;

import java.util.ArrayList;

/*
 * Created by Naresh Ravva on 10/10/17.
 */

public class DashBoardResModel {

    private boolean success;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    private String message;

    String getMonths() {
        return months;
    }

    public void setMonths(String months) {
        this.months = months;
    }

    private String months;

    @SerializedName("user_details")
    UserDetails userDetails;

    @SerializedName("pregnant_details")
    private
    PregnantDataModel prgntModel;

    @SerializedName("child")
    ArrayList<ChildModel> arrChildData = new ArrayList<>();

    @SerializedName("card_details")
    private
    CardsDataModel arrTipsRecords ;

    CardsDataModel getArrTipsRecords() {
        return arrTipsRecords;
    }

    public void setArrTipsRecords(CardsDataModel arrTipsRecords) {
        this.arrTipsRecords = arrTipsRecords;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    PregnantDataModel getPrgntModel() {
        return prgntModel;
    }

    public void setPrgntModel(PregnantDataModel prgntModel) {
        this.prgntModel = prgntModel;
    }

    public UserDetails getUserDetails() {
        return userDetails;
    }

    public void setUserDetails(UserDetails userDetails) {
        this.userDetails = userDetails;
    }

    public ArrayList<ChildModel> getArrChildData() {
        return arrChildData;
    }

    public void setArrChildData(ArrayList<ChildModel> arrChildData) {
        this.arrChildData = arrChildData;
    }

    public ArrayList<DeviceResModel> getDevice_history() {
        return device_history;
    }

    public void setDevice_history(ArrayList<DeviceResModel> device_history) {
        this.device_history = device_history;
    }

    ArrayList<DeviceResModel> device_history = new ArrayList<>();

}
