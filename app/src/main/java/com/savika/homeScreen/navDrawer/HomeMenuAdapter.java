package com.savika.homeScreen.navDrawer;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.savika.R;


/*
 * Notifications Adapter
 *
 * @author Naresh
 */
public class HomeMenuAdapter extends BaseAdapter {
    private Context context;
    private List<MenuModel> data;
    private String TAG = "HomeMenuAdapter";

    public HomeMenuAdapter(Context context, List<MenuModel> arraData) {
        this.context = context;
        this.data = arraData;
    }

    public void refreshListView(List<MenuModel> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ViewHolder holder;

        if (row == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(R.layout.home_menu_list_item, parent, false);

            holder = new ViewHolder();
            holder.txtTitle = row.findViewById(R.id.tvTitle);
            holder.ivIcon = row.findViewById(R.id.ivIcon);
            row.setTag(holder);
        } else {
            holder = (ViewHolder) row.getTag();

        }

        final MenuModel item = data.get(position);

        holder.txtTitle.setText(item.getTitle());
        holder.ivIcon.setImageResource(item.getIcon());

        return row;

    }

    static class ViewHolder {
        private TextView txtTitle;
        private ImageView ivIcon;
    }

    @Override
    public int getCount() {
        if (data != null) {
            return data.size();
        }

        return 0;
    }

    @Override
    public MenuModel getItem(int position) {
        if (data != null) {
            return data.get(position);
        } else {
            return null;
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

}