package com.savika.homeScreen.navDrawer;

/*
 * Created by Naresh Ravva on 07/03/17.
 */

public class MenuModel {
    private String title;
    private int icon;

    public MenuModel(String title, int icon) {
        super();
        this.title = title;
        this.icon = icon;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }
}