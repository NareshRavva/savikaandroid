package com.savika;

import com.google.gson.annotations.SerializedName;
import com.savika.calendar.CalndrCommonResObjtModel;

import java.util.ArrayList;

/*
 * Created by nexivo on 22/11/17.
 */

public class GlobalConfigResMOdel {
    private String status;


    ArrayList<GlobalConfigResObjModel> getGlobalRecords() {
        return globalrecords;
    }

    public void setGlobalRecords(ArrayList<GlobalConfigResObjModel> allrecords) {
        this.globalrecords = allrecords;
    }

    @SerializedName("global_config")
    private ArrayList<GlobalConfigResObjModel> globalrecords = new ArrayList<>();

    public String isSuccess() {
        return status;
    }

    public void setSuccess(String success) {
        this.status = success;
    }

}
