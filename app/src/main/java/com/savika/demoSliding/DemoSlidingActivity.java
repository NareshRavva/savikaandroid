package com.savika.demoSliding;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.rd.PageIndicatorView;
import com.savika.BaseActivity;
import com.savika.R;
import com.savika.components.textview.TextView;
import com.savika.onBoardScreens.OnBoardActivity1;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/*
 * Created by Naresh Ravva on 06/07/17.
 * https://github.com/romandanylyk/PageIndicatorView
 */

public class DemoSlidingActivity extends BaseActivity implements View.OnClickListener {

    @BindView(R.id.viewPager)
    ViewPager pager;

    @BindView(R.id.skip)
    TextView skip;

    @BindView(R.id.next)
    TextView next;

    private PageIndicatorView pageIndicatorView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demo_sliding);

        ButterKnife.bind(this);
        next.setOnClickListener(this);
        skip.setOnClickListener(this);

        initViews();
    }

    private void initViews() {

        DemoSlidingAdapter adapter = new DemoSlidingAdapter(this, this.getLayoutInflater());
        adapter.setData(createPageList());

        pager.setAdapter(adapter);

        pageIndicatorView = (PageIndicatorView) findViewById(R.id.pageIndicatorView);
        pageIndicatorView.setViewPager(pager);
    }

    @NonNull
    private List<View> createPageList() {
        List<View> pageList = new ArrayList<>();
        pageList.add(createPageView(R.color.white));
        pageList.add(createPageView(R.color.white));
        pageList.add(createPageView(R.color.white));
        pageList.add(createPageView(R.color.white));
        pageList.add(createPageView(R.color.white));

        return pageList;
    }

    @NonNull
    private View createPageView(int color) {
        View view = new View(this);
        view.setBackgroundColor(getResources().getColor(color));

        return view;
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id){
            case R.id.skip:
                Intent homeScreen = new Intent(this, OnBoardActivity1.class);
                startActivity(homeScreen);
                finish();
                break;
            case R.id.next:
                if (pager.getCurrentItem() >= 4){
                    Intent homeScreen1 = new Intent(this, OnBoardActivity1.class);
                    startActivity(homeScreen1);
                    finish();
                }
                else {
                    pager.setCurrentItem(pager.getCurrentItem() + 1, true);
                }
                break;
        }
    }
}

