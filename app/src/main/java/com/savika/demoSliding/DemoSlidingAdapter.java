package com.savika.demoSliding;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.savika.R;
import com.savika.homeScreen.HomeActivity;

import java.util.ArrayList;
import java.util.List;

class DemoSlidingAdapter extends PagerAdapter {

    private List<View> viewList;
    private LayoutInflater inflater;
    int currentImage = 0;
    private DemoSlidingActivity ctx;

    DemoSlidingAdapter(DemoSlidingActivity ctx, LayoutInflater layoutInflater) {
        this.viewList = new ArrayList<>();
        this.inflater = layoutInflater;
        this.ctx = ctx;
    }

    @Override
    public Object instantiateItem(@NonNull ViewGroup collection, int position) {

        View view = viewList.get(position);

        FrameLayout rl = null, rlInfopage1 = null, rlInfopage2 = null, rlInfopage3 = null, rlInfopage4 = null, rlInfopage5 = null, rlInfopage = null;

        switch (position) {
            case 0:
                rlInfopage1 = (FrameLayout) inflater.inflate(R.layout.fragment_sliding_tracking, null);
                rl = rlInfopage1;
                break;

            case 1:
                rlInfopage2 = (FrameLayout) inflater.inflate(R.layout.fragment_sliding_articles, null);
//                    ctx.next.setText("Next");
                rl = rlInfopage2;
                break;

            case 2:
                rlInfopage3 = (FrameLayout) inflater.inflate(R.layout.fragment_sliding_calendar, null);
//                    ctx.next.setText("Next");
                rl = rlInfopage3;
                break;

            case 3:
                rlInfopage4 = (FrameLayout) inflater.inflate(R.layout.fragment_sliding_community, null);
//                    ctx.next.setText("Get Started");
                rl = rlInfopage4;
                break;

            case 4:
                rlInfopage5 = (FrameLayout) inflater.inflate(R.layout.fragment_sliding_motherhood, null);
//                    ctx.next.setText("Get Started");
                rl = rlInfopage5;
                break;




        }
        collection.addView(rl);

        return rl;

    }

    @Override
    public void destroyItem(@NonNull ViewGroup collection, int position, @NonNull Object view) {
        collection.removeView((View) view);
    }

    @Override
    public int getCount() {
        return viewList.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        return POSITION_NONE;
    }

    void setData(@Nullable List<View> list) {
        this.viewList.clear();
        if (list != null && !list.isEmpty()) {
            this.viewList.addAll(list);
        }

        notifyDataSetChanged();
    }

    @NonNull
    List<View> getData() {
        if (viewList == null) {
            viewList = new ArrayList<>();
        }

        return viewList;
    }
}