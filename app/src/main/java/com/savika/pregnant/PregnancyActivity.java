package com.savika.pregnant;

import android.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.savika.Api.ApiClient;
import com.savika.Api.ApiInterface;
import com.savika.Api.CommonResModel;
import com.savika.R;
import com.savika.components.Button;
import com.savika.components.textview.TextView;
import com.savika.dialogUtils.DatePicketDialog;
import com.savika.homeScreen.CustomProgress.CustomProgressBar;
import com.savika.homeScreen.CustomProgress.ProgressItem;
import com.savika.logger.Logger;
import com.savika.utils.AppUtils;
import com.savika.utils.ImageUtil;
import com.savika.utils.SharedPrefsUtils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;

import static android.content.ContentValues.TAG;

public class PregnancyActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.tvHeaderTitle)
    TextView tvHeaderTitle;

    @BindView(R.id.img_profile)
    ImageView imgProfile;

    @BindView(R.id.duedate)
    TextView duedate;

    @BindView(R.id.llBack)
    LinearLayout llBack;

    @BindView(R.id.weeks_passed)
    TextView weeks_passed;

    @BindView(R.id.progresshome)
    CustomProgressBar progressmonths;

    @BindView(R.id.helpin)
    TextView helpin;

    String DUE_DATE,LAST_DATE;
    private ArrayList<ProgressItem> progressItemList;
    private ProgressItem mProgressItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pregnancy);
        ButterKnife.bind(this);
        tvHeaderTitle.setText("Pregnancy");
        helpin.setOnClickListener(this);
        duedate.setOnClickListener(this);
        llBack.setOnClickListener(this);
        progressmonths.getThumb().mutate().setAlpha(0);
        initDataToSeekbar(4);

        ImageUtil.LoadPicasoCicular(this, imgProfile, SharedPrefsUtils.getProfilePic(this));
    }

    private void initDataToSeekbar(int j) {
        progressItemList = new ArrayList<ProgressItem>();
        // red span

        String[] defcolors = {"#ffffff","#fafbfc","#f8f9fa","#f1f4f6","#e9edf0","#e0e6ea","#d6dde3","#cbd2d9","#b6bcc1"};
        String[] filledcolors = {"#ffdc76","#ffcd3e","#febd01","#ff953e","#fe7301","#d86100","#91dea8","#64d084","#33c15d"};
        for (int i = 1; i < 10; i++) {
            mProgressItem = new ProgressItem();
            mProgressItem.progressItemPercentage = 10.3f;
            if (i < j) {
                mProgressItem.color = filledcolors[i-1];
            } else {
                mProgressItem.color = defcolors[i-1];
            }

            progressItemList.add(mProgressItem);
            if (i == 3|| i==6) {
                mProgressItem = new ProgressItem();
                mProgressItem.progressItemPercentage = 3.5f;
                mProgressItem.color = defcolors[0];
                progressItemList.add(mProgressItem);
            }
        }
//


        //white span


        progressmonths.initData(progressItemList);
        progressmonths.invalidate();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getPregnantData();
    }

    private void getPregnantData() {
        if (AppUtils.isNetworkConnected(this)) {
            AppUtils.showDialog(PregnancyActivity.this);

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            Call<PregnantDueResModel> call = apiService.getPregnantDueData(SharedPrefsUtils.getToken(getApplicationContext()),SharedPrefsUtils.getUserID(this));

            call.enqueue(new retrofit2.Callback<PregnantDueResModel>() {
                @Override
                public void onResponse(Call<PregnantDueResModel> call, retrofit2.Response<PregnantDueResModel> response) {

                    AppUtils.dismissDialog();

                    if (response.code() == 200 && response.body().isSuccess()) {
                        try {
                            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
                            String dateInString = response.body().getArr_health_records().get(0).getLastperioddate();
                            Date date = (Date) formatter.parse(dateInString);
                            Date today = new Date();
                            long diff = today.getTime() - date.getTime();
                            float weeksint = diff / (1000 * 60 * 60 * 24 * 7.0f);
                            int days = (int) ((weeksint - (int) weeksint) * 7);
                            weeks_passed.setText((int) weeksint + " weeks and " + days + " days Pregnant");
                            if (response.body().getArr_health_records().get(0).getMonths() != null) {
                                initDataToSeekbar((Integer.parseInt(response.body().getArr_health_records().get(0).getMonths())) + 1);
                            }
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        DUE_DATE = AppUtils.getDisplayDate(response.body().getArr_health_records().get(0).getDuedate());
                        LAST_DATE = AppUtils.getDisplayDate(response.body().getArr_health_records().get(0).getLastperioddate());
                        duedate.setText(DUE_DATE);
                        helpin.setText(LAST_DATE);

                    }
                    else {
                        if (response.body().getMessage()!=null) {
                            if (response.body().getMessage().equalsIgnoreCase("Unauthorized")) {
                                AppUtils.sessionExpired(PregnancyActivity.this);
                            } else
                                AppUtils.showToast(getApplicationContext(), response.body().getMessage());
                        }
                    }
                }

                @Override
                public void onFailure(Call<PregnantDueResModel> call, Throwable t) {
                    AppUtils.dismissDialog();
                }
            });
        }
        else {
            AppUtils.showToast(this,"It looks like your internet connectioin off. Please turn it on and try again.");
        }
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id){
            case R.id.helpin:
                calPregnantWeeks();
                break;
            case R.id.llBack:
                finish();
                break;
            case R.id.duedate:
                updateDueDate();
        }
    }

    private void updateDueDate() {
        String defaultDate = duedate.getText().toString().trim();
        runOnUiThread(new DatePicketDialog(PregnancyActivity.this, "Due Date", "Choose Due Date", false, 1, 8,defaultDate, PregnancyActivity.this.getLayoutInflater(), new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Logger.info("TAG", "Selected prblm Date-->" + view.getTag());

                DateFormat df = new SimpleDateFormat("dd MMM yyyy",Locale.getDefault());
                try {
                    Date selecteddate = df.parse((String) view.getTag());
                    Date date = new Date();
                    Date today = df.parse(df.format(date));
                    Calendar cal = Calendar.getInstance();
                    cal.add(Calendar.MONTH, 9);
                    Date m9 = cal.getTime();

                    if (selecteddate.before(today) && !selecteddate.equals(date)){
                        showAlert(PregnancyActivity.this,"Choose date after today","due");
                    }
                    else if (selecteddate.after(m9)){
                        showAlert(PregnancyActivity.this,"Invalid date","due");
                    }
                    else {
                        PregnantDueReqModel reqModel = new PregnantDueReqModel();
                        reqModel.setUserid(SharedPrefsUtils.getUserID(PregnancyActivity.this));
                        reqModel.setDueDate(AppUtils.convertDateFormat((String) view.getTag()));
//                        if (ServiceUrl.BASE_URL.contains("3001"))
                            reqModel.setLastperioddate(AppUtils.convertDateFormat(helpin.getText().toString()));
                        updateDueDateServer(reqModel);
                        duedate.setText((String) view.getTag());
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }

            }
        }));
    }

    private void updateDueDateServer(final PregnantDueReqModel reqModel) {
        if (AppUtils.isNetworkConnected(this)) {
            AppUtils.showDialog(PregnancyActivity.this);

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            Call<CommonResModel> call = apiService.updatePregnantDueData(SharedPrefsUtils.getToken(getApplicationContext()),reqModel);

            call.enqueue(new retrofit2.Callback<CommonResModel>() {
                @Override
                public void onResponse(Call<CommonResModel> call, retrofit2.Response<CommonResModel> response) {

                    AppUtils.dismissDialog();

                    if (response.code() == 200 && response.body().isSuccess()) {
                        if (reqModel.getLastperioddate() != null)
                            getPregnantData();
                        AppUtils.showToast(PregnancyActivity.this, response.body().getMessage());
                    } else {
                        if (response.body().getMessage()!=null) {
                            if (response.body().getMessage().equalsIgnoreCase("Unauthorized")) {
                                AppUtils.sessionExpired(PregnancyActivity.this);
                            } else
                                AppUtils.showToast(getApplicationContext(), response.body().getMessage());
                        }
                        if (reqModel.getLastperioddate() != null)
                            helpin.setText(LAST_DATE);
                        else duedate.setText(DUE_DATE);

                    }
                }

                @Override
                public void onFailure(Call<CommonResModel> call, Throwable t) {
                    AppUtils.dismissDialog();
                }
            });
        }
        else {
            AppUtils.showToast(this,"It looks like your internet connectioin off. Please turn it on and try again.");
        }
    }

    private void calPregnantWeeks() {
        datedialog();
    }

    public void datedialog()
    {
        String defaultdate = helpin.getText().toString().trim();;
        runOnUiThread(new DatePicketDialog(PregnancyActivity.this, "Last Period Date", "Choose Last Period Date", false, 8, 1,defaultdate, PregnancyActivity.this.getLayoutInflater(), new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Logger.info("TAG", "Selected prblm Date-->" + view.getTag());

                DateFormat df = new SimpleDateFormat("dd MMM yyyy",Locale.getDefault());
                try {
                    Date selecteddate = df.parse((String) view.getTag());
                    Date date = new Date();
                    Date today = df.parse(df.format(date));
                    Calendar cal = Calendar.getInstance();
                    cal.add(Calendar.MONTH, -9);
                    Date m9 = cal.getTime();

                    if (selecteddate.after(today) && !selecteddate.equals(date)){
                        showAlert(PregnancyActivity.this,"Choose date before today","lpd");
                    }
                    else if (selecteddate.before(m9)){
                        showAlert(PregnancyActivity.this,"Invalid date","lpd");
                    }
                    else {
                        PregnantDueReqModel reqModel = new PregnantDueReqModel();
                        reqModel.setUserid(SharedPrefsUtils.getUserID(PregnancyActivity.this));
                        reqModel.setLastperioddate(AppUtils.convertDateFormat((String) view.getTag()));
//                        if (ServiceUrl.BASE_URL.contains("3001"))
                            reqModel.setDueDate(addMonths((String) view.getTag(),9));
                        updateDueDateServer(reqModel);
                        //duedate.setText(AppUtils.convertDateFormat((String) view.getTag()));
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                //fgzdfgdgvdf
                /*DateFormat df = new SimpleDateFormat("dd MMM yyyy");
                try {
                    Date selecteddate = df.parse((String) view.getTag());
                    Date date = new Date();
                    Date today = df.parse(df.format(date));
                    if (selecteddate.after(today) && !selecteddate.equals(date)){
                        showAlert(PregnancyActivity.this,"Choose date before today");
                    }
                    else {
                        String DATE_OF_LAST_PERIOD = AmAPregnant.convertDateFormat("" + view.getTag());
                        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                        Date date1 = null;
                        try {
                            date1 = (Date)formatter.parse(DATE_OF_LAST_PERIOD);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        Date today1 = new Date();
                        long diff = today1.getTime() - date1.getTime();
                        float weeksint = diff/(1000*60*60*24*7.0f);
                        int days = (int) ((weeksint - (int)weeksint)*7);
                        weeks_passed.setText((int) weeksint+" weeks and "+days + " days Pregnant");
                        try {
                            duedate.setText(""+addMonths("" + view.getTag(), 9));
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }*/

            }
        }));
    }
    private void showAlert(PregnancyActivity ctx, String s, final String message) {
        if (ctx != null) {

            try {
                final AlertDialog.Builder builder = AppUtils.getAlertDialog(ctx);
                View view  = getLayoutInflater().inflate(R.layout.pop_alert_date,null);
                builder.setView(view);
                final AlertDialog alert = builder.create();
                alert.show();
                android.widget.TextView messagetxt = (android.widget.TextView) view.findViewById(R.id.tvdateerrorText);
                messagetxt.setText(s);
                Button ok = (Button) view.findViewById(R.id.datebtnPositive);
                ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alert.cancel();
                        if (message.equalsIgnoreCase("due"))
                            updateDueDate();
                        else
                        datedialog();
                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public String addMonths(String dateAsString, int nbMonths) throws ParseException {
        String format = "dd MMM yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        SimpleDateFormat display_format = new SimpleDateFormat("yyyy-MM-dd",Locale.getDefault());
        Date dateAsObj = sdf.parse(dateAsString);
        Calendar cal = Calendar.getInstance();
        cal.setTime(dateAsObj);
        cal.add(Calendar.MONTH, nbMonths);
        Date dateAsObjAfterAMonth = cal.getTime();
        System.out.println(sdf.format(dateAsObjAfterAMonth));

        String DISPLAY_DUE_DATE = sdf.format(dateAsObjAfterAMonth);
        String DUE_DATE = display_format.format(dateAsObjAfterAMonth);

        Logger.info(TAG,"DISPLAY_DUE_DATE-->"+DISPLAY_DUE_DATE+"--DUE_DATE-->"+DUE_DATE);


        return display_format.format(dateAsObjAfterAMonth);
    }
}
