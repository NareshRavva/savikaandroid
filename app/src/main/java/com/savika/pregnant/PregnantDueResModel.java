package com.savika.pregnant;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/*
 * Created by nexivo on 11/11/17.
 */

public class PregnantDueResModel {

    private boolean success;

    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @SerializedName("pregnant_details")
    private ArrayList<PregnantDueReqModel> arr_health_records = new ArrayList<>();

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public ArrayList<PregnantDueReqModel> getArr_health_records() {
        return arr_health_records;
    }

    public void setArr_health_records(ArrayList<PregnantDueReqModel> arr_health_records) {
        this.arr_health_records = arr_health_records;
    }
}
