package com.savika.pregnant;

/**
 * Created by nexivo on 11/11/17.
 */

public class PregnantDueReqModel {

    private String pregid, duedate, lastperioddate, createdby, createdat, userid, months;

    public String getMonths() {
        return months;
    }

    public void setMonths(String months) {
        this.months = months;
    }


    public void setPregid(String pregid) {
        this.pregid = pregid;
    }

    public void setDueDate(String duedate) {
        this.duedate = duedate;
    }

    public void setLastperioddate(String lastperioddate) {
        this.lastperioddate = lastperioddate;
    }

    public void setCreatedby(String createdby) {
        this.createdby = createdby;
    }

    public void setCreatedat(String createdat) {
        this.createdat = createdat;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getCreatedat() {
        return createdat;
    }

    public String getCreatedby() {
        return createdby;
    }

    public String getDuedate() {
        return duedate;
    }

    public String getLastperioddate() {
        return lastperioddate;
    }

    public String getPregid() {
        return pregid;
    }

    public String getUserid() {
        return userid;
    }
}
