package com.savika.Api;

/*
 * Created by Naresh Ravva on 13/10/17.
 */

public class CommonResModel {

    private boolean success;
    private String message;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
