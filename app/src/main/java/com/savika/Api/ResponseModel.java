package com.savika.Api;

/*
 * Created by Naresh Ravva on 20/09/17.
 */

public class ResponseModel {

    private boolean success;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    private String token;

    public String getAlready_exists() {
        return already_exists;
    }

    public void setAlready_exists(String already_exists) {
        this.already_exists = already_exists;
    }

    private String already_exists,userid, message, otpvalue, mobilenumber, username, emailid, profileimg, typeid, Notification_title, Notification_msg;

    public String getNotification_title() {
        return Notification_title;
    }

    public void setNotification_title(String notification_title) {
        Notification_title = notification_title;
    }

    public String getNotification_msg() {
        return Notification_msg;
    }

    public void setNotification_msg(String notification_msg) {
        Notification_msg = notification_msg;
    }

    public String getTypeid() {
        return typeid;
    }

    public void setTypeid(String typeid) {
        this.typeid = typeid;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getOtpvalue() {
        return otpvalue;
    }

    public void setOtpvalue(String otpvalue) {
        this.otpvalue = otpvalue;
    }

    public String getMobilenumber() {
        return mobilenumber;
    }

    public void setMobilenumber(String mobilenumber) {
        this.mobilenumber = mobilenumber;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmailid() {
        return emailid;
    }

    public void setEmailid(String emailid) {
        this.emailid = emailid;
    }

    public String getProfileimg() {
        return profileimg;
    }

    public void setProfileimg(String profileimg) {
        this.profileimg = profileimg;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
