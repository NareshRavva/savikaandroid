package com.savika.Api;

/*
 * Created by nisum on 22/02/17.
 */

public class ServiceUrl {

    public static final String BASE_URL = "http://ec2-13-126-40-224.ap-south-1.compute.amazonaws.com:3000/";

//    static final String BASE_URL = "http://ec2-13-126-40-224.ap-south-1.compute.amazonaws.com:3001/";

    private static final String SERVER_VERSION = "api/v2/";

    static final String ADD_OR_UPDATE_DEVICE = SERVER_VERSION+"device_history";

    public static String FIREBASE_URL = "https://fcm.googleapis.com/fcm/send";

    public static final String AWS_API_KEY = "uQZIlwPJ1YZneJYLOono9P4Iig1KG599o7fUeef7";

    //*** main ***//
//    public static final String GET_ARTICALS = "https://23tc22br29.execute-api.ap-south-1.amazonaws.com/prod/v1/categories/";
//
//    public static final String GET_CATEGORIES = "https://cywygnqpfb.execute-api.ap-south-1.amazonaws.com/prod/v1/categories/all";

    /*** test ***/
    public static final String GET_ARTICALS = "https://zv1xfckxmj.execute-api.ap-south-1.amazonaws.com/prod/v1/categories/";
    //
    public static final String GET_CATEGORIES = "https://n1q25rxfi6.execute-api.ap-south-1.amazonaws.com/prod/v1/categories/all";
//end test//
    static final String SIGN_UP = SERVER_VERSION+"signup";

    static final String OTP = SERVER_VERSION+"otp";

    static final String SIGN_IN = SERVER_VERSION+"signin";

    static final String SIGN_IN_WITH_FB = SERVER_VERSION+"oauthsignin";

    static final String RESEND_OTP = SERVER_VERSION+"resendotp";

    static final String UPDATE_PROFILE_DATA = SERVER_VERSION+"update_user_data";

    static final String IMAGE_UPLOAD = SERVER_VERSION+"imageupload";

    static final String HEALTH = SERVER_VERSION+"health";

    static final String HEALTH_TIPS = SERVER_VERSION+"tips";

    static final String TRACK_LAST_RECORDS = SERVER_VERSION+"get_tracking_details";

    static final String POST_RECORD = SERVER_VERSION+"track";

    static final String GET_DASHBOARD_DATA = SERVER_VERSION+"get_dashboard_details";

    static final String GET_GRAPH_DATA = SERVER_VERSION+"get_graph_records";

    static final String GET_CHILD_DATA = SERVER_VERSION+"get_child_data";

    static final String ADD_NEW_CHILD_DATA = SERVER_VERSION+"add_child_data";

    static final String UPDATE_CHILD_DATA = SERVER_VERSION+"update_child_data";

    static final String DELETE_CHILD_DATA = SERVER_VERSION+"delete_child_data";

    static final String GET_HEALTH_DOC_TYPE = SERVER_VERSION+"get_health_doctype";

    static final String AWS_UPLOAD_FILE = SERVER_VERSION+"aws_upload";

    static final String ADD_NEW_HEALTH_RECORD = SERVER_VERSION+"add_userhealth_records";

    static final String GET_HEALTH_RECORD = SERVER_VERSION+"get_userhealth_records";

    static final String DELETE_HEALTH_RECORDS = SERVER_VERSION+"delete_userhealth_records";

    static final String UPDATE_HEALTH_RECORD = SERVER_VERSION+"update_userhealth_records";

    static final String GET_PROFILE_DATA = SERVER_VERSION+"get_user_data";

    static final String GET_MILESTONE_DATA = SERVER_VERSION+"milestones_list";

    static final String CALENDER = SERVER_VERSION+"add_to_calender";

    static final String GET_CALENDER_RECORD = SERVER_VERSION+"get_from_calender";

    static final String GET_CALENDER_REMINDERS = SERVER_VERSION+"get_calender_reminders";

    static final String DELETE_CALNDAR_DATA = SERVER_VERSION+"delete_from_calender";

    static final String UPDATE_CALNDR_RECORD_DATA = SERVER_VERSION+"update_to_calender";

    static final String GET_PREGNANTDUE = SERVER_VERSION+"get_pregnant_data";

    static final String PUT_PREGNANTDUE = SERVER_VERSION+"update_pregnant_details";

    static final String GET_DASHBOARD_CARD_DATA = SERVER_VERSION+"cards";

    static final String GET_GLOBAL_CONFIG = SERVER_VERSION+"global_config";

    static final String ADD_NOTIFICATIONS_DATA = SERVER_VERSION+"add_notification_logs";

    static final String UPDATE_NOTIFICATIONS_DATA = SERVER_VERSION+"update_notification_view";

    static final String GET_NOTIFICATIONS_DATA  = SERVER_VERSION+"get_notification_logs";

    static final String UPDATE_FIREBASE_ID = SERVER_VERSION+"update_firebaseid";

    static final String UPDATE_USER_TRACKING = SERVER_VERSION+"track_location";
}