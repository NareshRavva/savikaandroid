package com.savika.Api;


import com.savika.GlobalConfigResMOdel;
import com.savika.pregnant.PregnantDueReqModel;
import com.savika.pregnant.PregnantDueResModel;
import com.savika.amAMother.ChildModel;
import com.savika.amAMother.GetChildDataRes;
import com.savika.amAMother.ProfileUploadResModel;
import com.savika.authentication.FBReqModel;
import com.savika.authentication.OtpReqModel;
import com.savika.calendar.CalendarRecordUpdateReqModel;
import com.savika.calendar.CalndrCommonResModel;
import com.savika.calendar.milestones.AddMLReqModel;
import com.savika.calendar.reminderDots.EventsDotsResModel;
import com.savika.community.PostNotificationsDataReq;
import com.savika.firebase.FBResponseModel;
import com.savika.firebase.SendNotificationModel;
import com.savika.homeScreen.DashBoardRequestModel;
import com.savika.homeScreen.DashBoardResModel;
import com.savika.homeScreen.DeviceReqModel;
import com.savika.homeScreen.UpdateUserTrackReqModel;
import com.savika.homeScreen.notification.NotificationResModel;
import com.savika.homeScreen.updatefirebase.UpdateFirebaseResModel;
import com.savika.homeScreen.updatefirebase.UpdateFirebaseResObjModel;
import com.savika.profile.ProfileResModel;
import com.savika.profile.ProfileUpdateReqModel;
import com.savika.recommendReadings.ArticalsResModel;
import com.savika.recommendReadings.CatResModel;
import com.savika.authentication.SignUpRequestModel;
import com.savika.trackHealthRecords.AddHRReqModel;
import com.savika.trackHealthRecords.HRDocTypeResModel;
import com.savika.trackHealthRecords.HRResponseModel;
import com.savika.tracking.HealthResponse;
import com.savika.tracking.PostTrackRecordModel;
import com.savika.tracking.graph.GraphReqModel;
import com.savika.tracking.graph.GraphResponseModel;
import com.savika.tracking.lastRecords.LastRecordResponse;
import com.savika.tracking.lastRecords.LastRecordsRequest;
import com.savika.tracking.tips.TipsResponse;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface ApiInterface {

    @POST
    Call<FBResponseModel> sendNotificationToAnotherDevice(@Url String url, @Header("Content-Type") String content_type, @Header("Authorization") String auth, @Body SendNotificationModel model);

    @GET()
    Call<ArticalsResModel> getArticals(@Header("x-api-key")String token, @Url String url);

    @GET(ServiceUrl.GET_CATEGORIES)
    Call<CatResModel> getCategories(@Header("x-api-key") String token);

    @POST(ServiceUrl.SIGN_UP)
    Call<ResponseModel> signUp(@Body SignUpRequestModel model);

    @POST(ServiceUrl.BASE_URL + ServiceUrl.SIGN_IN)
    Call<ResponseModel> signin(@Body OtpReqModel model);

    @POST(ServiceUrl.BASE_URL + ServiceUrl.OTP)
    Call<ResponseModel> validateOTP(@Body OtpReqModel model);

    @POST(ServiceUrl.BASE_URL + ServiceUrl.RESEND_OTP)
    Call<ResponseModel> resendOTP(@Body OtpReqModel model);

    @POST(ServiceUrl.BASE_URL + ServiceUrl.SIGN_IN_WITH_FB)
    Call<ResponseModel> signinWithFb(@Body FBReqModel model);

    @Multipart
    @POST(ServiceUrl.BASE_URL + ServiceUrl.IMAGE_UPLOAD)
    Call<ProfileUploadResModel> uploadFile(@Header("Authorization") String token, @Part MultipartBody.Part file, @Part("type")
            RequestBody type, @Part("userid") RequestBody userID, @Part("childid") RequestBody childid);

    @GET(ServiceUrl.BASE_URL + ServiceUrl.HEALTH)
    Call<HealthResponse> getHealthData(@Header("Authorization") String token);

    @GET(ServiceUrl.BASE_URL + ServiceUrl.HEALTH_TIPS)
    Call<TipsResponse> getHealthTips(@Header("Authorization") String token, @Query("userID") String userID, @Query("typeID") String typeID);

    @POST(ServiceUrl.BASE_URL + ServiceUrl.TRACK_LAST_RECORDS)
    Call<LastRecordResponse> lastRecords(@Header("Authorization") String token, @Body LastRecordsRequest model);

    @POST(ServiceUrl.BASE_URL + ServiceUrl.POST_RECORD)
    Call<ResponseModel> postRecord(@Header("Authorization") String token, @Body PostTrackRecordModel model);

    @POST(ServiceUrl.BASE_URL + ServiceUrl.GET_GRAPH_DATA)
    Call<GraphResponseModel> getGraphData(@Header("Authorization") String token, @Body GraphReqModel model);

    @POST(ServiceUrl.BASE_URL + ServiceUrl.GET_DASHBOARD_DATA)
    Call<DashBoardResModel> getDashBoardData(@Header("Authorization") String token, @Body DashBoardRequestModel model);

    @GET(ServiceUrl.BASE_URL + ServiceUrl.GET_CHILD_DATA)
    Call<GetChildDataRes> getChildData(@Header("Authorization") String token, @Query("userID") String userID);

    @POST(ServiceUrl.BASE_URL + ServiceUrl.ADD_NEW_CHILD_DATA)
    Call<CommonResModel> addChild(@Header("Authorization") String token, @Body ChildModel model);

    @PUT(ServiceUrl.BASE_URL + ServiceUrl.UPDATE_CHILD_DATA)
    Call<CommonResModel> updateChild(@Header("Authorization") String token, @Body ChildModel model);

    @DELETE(ServiceUrl.BASE_URL + ServiceUrl.DELETE_CHILD_DATA)
    Call<CommonResModel> deleteChild(@Header("Authorization") String token, @Query("userID") String userID, @Query("childID") String childid);

    @GET(ServiceUrl.BASE_URL + ServiceUrl.GET_HEALTH_DOC_TYPE)
    Call<HRDocTypeResModel> getHealthDocType(@Header("Authorization") String token, @Query("typeID") String typeID);

    @Multipart
    @POST(ServiceUrl.BASE_URL + ServiceUrl.AWS_UPLOAD_FILE)
    Call<ProfileUploadResModel> uploadDoc(@Header("Authorization") String token, @Part MultipartBody.Part file, @Part("mobilenumber") RequestBody mobilenumber,
                                          @Part("userid") RequestBody userid, @Part("filename") RequestBody filename);

    @POST(ServiceUrl.BASE_URL + ServiceUrl.ADD_NEW_HEALTH_RECORD)
    Call<CommonResModel> AddNewHealthRecord(@Header("Authorization") String token, @Body AddHRReqModel model);

    @GET(ServiceUrl.BASE_URL + ServiceUrl.GET_HEALTH_RECORD)
    Call<HRResponseModel> getHealthRecords(@Header("Authorization") String token, @Query("userID") String userID, @Query("childID") String childid);

    @DELETE(ServiceUrl.BASE_URL + ServiceUrl.DELETE_HEALTH_RECORDS)
    Call<CommonResModel> deleteHealthRecord(@Header("Authorization") String token, @Query("userID") String userID,
                                            @Query("recordID") String recordID, @Query("childID") String childid);
    @PUT(ServiceUrl.BASE_URL + ServiceUrl.UPDATE_HEALTH_RECORD)
    Call<CommonResModel> updateHealthRecord(@Header("Authorization") String token, @Body AddHRReqModel model);

    @PUT(ServiceUrl.BASE_URL + ServiceUrl.UPDATE_PROFILE_DATA)
    Call<CommonResModel> updateProfile(@Header("Authorization") String token, @Body ProfileUpdateReqModel model);

    @GET(ServiceUrl.BASE_URL + ServiceUrl.GET_PROFILE_DATA)
    Call<ProfileResModel> getProfileData(@Header("Authorization") String token, @Query("userID") String userID);

    @POST(ServiceUrl.BASE_URL + ServiceUrl.CALENDER)
    Call<CommonResModel> AddNewMileStone(@Header("Authorization") String token, @Body AddMLReqModel model);

    @GET(ServiceUrl.BASE_URL + ServiceUrl.GET_CALENDER_RECORD)
    Call<CalndrCommonResModel> getAllRecords(@Header("Authorization") String token, @Query("userID") String userID, @Query("date") String date, @Query("month") String month, @Query("year") String year, @Query("childID") String childid);

    @GET(ServiceUrl.BASE_URL + ServiceUrl.GET_CALENDER_RECORD)
    Call<CalndrCommonResModel> getCalenderRecordswithDate(@Header("Authorization") String  token, @Query("userID") String userID, @Query("calender_type") String calender_type, @Query("date") String date, @Query("childID") String childid);

    @GET(ServiceUrl.BASE_URL + ServiceUrl.GET_CALENDER_RECORD)
    Call<CalndrCommonResModel> getCalenderRecordswithMonth(@Header("Authorization") String token, @Query("userID") String userID, @Query("calender_type") String calender_type, @Query("month") String month, @Query("year") String year, @Query("childID") String childid);

    @GET(ServiceUrl.BASE_URL + ServiceUrl.GET_CALENDER_REMINDERS)
    Call<EventsDotsResModel> getCalndrReminders(@Header("Authorization") String token, @Query("userID") String userID, @Query("childID") String childid, @Query("month") int month, @Query("year") int year);

    @DELETE(ServiceUrl.BASE_URL + ServiceUrl.DELETE_CALNDAR_DATA)
    Call<CommonResModel> deleteCalandrItem(@Header("Authorization") String token, @Query("calender_type") String calender_type, @Query("recordID") String recordID, @Query("userID") String userID, @Query("childID") String childid);

    @PUT(ServiceUrl.BASE_URL + ServiceUrl.UPDATE_CALNDR_RECORD_DATA)
    Call<CommonResModel> updateCalndrRecord(@Header("Authorization") String token, @Body CalendarRecordUpdateReqModel model);

    @GET(ServiceUrl.BASE_URL + ServiceUrl.GET_PREGNANTDUE)
    Call<PregnantDueResModel> getPregnantDueData(@Header("Authorization") String token, @Query("userID") String userID);

    @PUT(ServiceUrl.BASE_URL + ServiceUrl.PUT_PREGNANTDUE)
    Call<CommonResModel> updatePregnantDueData(@Header("Authorization") String token, @Body PregnantDueReqModel model);

    @GET(ServiceUrl.BASE_URL + ServiceUrl.GET_GLOBAL_CONFIG)
    Call<GlobalConfigResMOdel> getGlobalConfig();

    @POST(ServiceUrl.BASE_URL + ServiceUrl.ADD_NOTIFICATIONS_DATA)
    Call<CommonResModel> postNotificationsData(@Header("Authorization") String token, @Body PostNotificationsDataReq model);

    @PUT(ServiceUrl.BASE_URL + ServiceUrl.UPDATE_NOTIFICATIONS_DATA)
    Call<CommonResModel> updatePostNotificationsData(@Header("Authorization") String token, @Body PostNotificationsDataReq model);

    @GET(ServiceUrl.BASE_URL + ServiceUrl.GET_NOTIFICATIONS_DATA)
    Call<NotificationResModel> getNotifications(@Header("Authorization") String token, @Query("userID") String userid);

    @PUT(ServiceUrl.BASE_URL + ServiceUrl.UPDATE_FIREBASE_ID)
    Call<UpdateFirebaseResModel> updateFirebaseId(@Header("Authorization") String token, @Body UpdateFirebaseResObjModel model);

    @POST(ServiceUrl.BASE_URL + ServiceUrl.ADD_OR_UPDATE_DEVICE)
    Call<CommonResModel> addDeviceDetails(@Header("Authorization") String token, @Body DeviceReqModel model);

    @PUT(ServiceUrl.BASE_URL + ServiceUrl.UPDATE_USER_TRACKING)
    Call<CommonResModel> updateUserTracking(@Header("Authorization") String token, @Body UpdateUserTrackReqModel model);
}
