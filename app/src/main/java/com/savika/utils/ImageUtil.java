package com.savika.utils;

import android.content.Context;
import android.widget.ImageView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.savika.R;
import com.savika.logger.Logger;

/*
 * Created by Naresh Ravva on 01/07/17.
 */

public class ImageUtil {

    static String TAG = "ImageUtil";

    public static void LoadPicaso(final Context context, final ImageView imgView, String orgininalImg) {

        if (orgininalImg != null && orgininalImg.length() > 1) {
            Picasso.with(context).load(orgininalImg.replace(" ", "20%")).placeholder(R.drawable.no_img)
                    .error(R.drawable.no_img).fit().into(imgView, new Callback() {

                @Override
                public void onSuccess() {
                    // Log.v(TAG, "onSuccess came");
                }

                @Override
                public void onError() {
                    Logger.info(TAG, "picaso image load failed.");
                }
            });
        }
    }

    public static void LoadPicasoWithOutFit(final Context context, final ImageView imgView, String orgininalImg) {

        if (orgininalImg != null && orgininalImg.length() > 1) {
            Picasso.with(context).load(orgininalImg.replace(" ", "20%")).placeholder(R.drawable.no_img)
                    .error(R.drawable.no_img).into(imgView, new Callback() {

                @Override
                public void onSuccess() {
                    // Log.v(TAG, "onSuccess came");
                }

                @Override
                public void onError() {
                    Logger.info(TAG, "picaso image load failed.");
                }
            });
        } else {
            imgView.setImageResource(R.drawable.no_img);
        }
    }

    public static void LoadPicasoCicular(final Context context, final ImageView imgView, String orgininalImg) {
        if (orgininalImg != null && orgininalImg.trim().length() > 1) {
            Picasso.with(context).load(orgininalImg.replace(" ", "%20")).transform(new CircleTransform(70, 4))
                    .resize(130, 130).into(imgView, new Callback() {

                @Override
                public void onSuccess() {
                    // TODO Auto-generated method stub

                }

                @Override
                public void onError() {
                    imgView.setImageResource(R.drawable.profile_round_icon);

                }
            });
        } else {
            imgView.setImageResource(R.drawable.profile_round_icon);
        }
    }

    public static void LoadPicasoRoundCorners(final Context context, final ImageView imgView, String orgininalImg) {
        if (orgininalImg != null && orgininalImg.trim().length() > 1) {
            Picasso.with(context).load(orgininalImg.replace(" ", "%20")).placeholder(R.drawable.no_img)
                    .error(R.drawable.no_img).transform(new SqaureTransform(8, 0)).resize(100, 100)
                    .into(imgView);
        } else {
            imgView.setImageResource(R.drawable.no_img);
        }
    }
}
