package com.savika.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Point;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import java.io.File;
import java.io.FileDescriptor;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import com.savika.R;
import com.savika.Splash;
import com.savika.constants.AppConstants;
import com.savika.dialogUtils.AlertDialogUtility;
import com.savika.logger.Logger;

/*
 * Created by nisum on 22/02/17.
 */

public class AppUtils {
    protected static ProgressDialog progressDialog = null;

    public static String getISODateConvertdTestUTC(String inputText1) {
        String outputText = "";
        String inputText = inputText1;
        // "Z" appears not to be supported for some reason.
        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.getDefault());
        inputFormat.setTimeZone(TimeZone.getTimeZone("IST"));
        DateFormat outputFormat = new SimpleDateFormat("dd-MM-yyyy",Locale.getDefault());
        Date parsed = null;
        try {
            parsed = inputFormat.parse(inputText);

            outputText = outputFormat.format(parsed);

        } catch (android.net.ParseException e) {
            e.printStackTrace();
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {

            return outputText;
        }

    }

    /*
     * close keyboard
     *
     * @param myEditText
     */
    public static void hideKeyBoard(Context ctx, EditText myEditText) {
        InputMethodManager imm = (InputMethodManager) ctx.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(myEditText.getWindowToken(), 0);
    }

    public static boolean isNetworkConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        return (cm != null ? cm.getActiveNetworkInfo() : null) != null;
    }

    /*
     * show keyborad
     *
     * @param myEditText
     */
    public static void showKeyBoard(Context ctx, EditText myEditText) {

        InputMethodManager inputMethodManager = (InputMethodManager) ctx.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.toggleSoftInputFromWindow(myEditText.getApplicationWindowToken(),
                InputMethodManager.SHOW_FORCED, 0);

    }


    /*
     * show TOAST
     *
     * @param msg
     */
    public static void showToast(Context ctx, String msg) {
        Toast.makeText(ctx, msg, Toast.LENGTH_LONG).show();
    }

    /**
     * Custom Toast for show msg's in entire application
     */
    public static void showAlert(Context ctx, String message) {
        // Create layout inflater object to inflate toast.xml file
        if (ctx != null) {

            try {
                final AlertDialog.Builder builder = AppUtils.getAlertDialog(ctx);
                builder.setMessage(message).setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(final DialogInterface dialog, final int id) {
                                dialog.cancel();
                            }
                        });
                final AlertDialog alert = builder.create();

                alert.setOnShowListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(DialogInterface dialog) {
                        alert.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.parseColor("#000000"));
                    }
                });
                alert.show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /*
     * alert dialog
     *
     * @param ctx
     * @return
     */
    public static AlertDialog.Builder getAlertDialog(Context ctx) {

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
            return new AlertDialog.Builder(ctx, R.style.AppCompatAlertDialogStyle);
        } else {
            return new AlertDialog.Builder(ctx);
        }

    }


    /*
     * show DAILOG
     *
     * @param ctx
     */
    public static void showDialog(Context ctx) {
        if(!((Activity) ctx).isFinishing())
        {
            //show dialog
            progressDialog = new ProgressDialog(ctx);
            progressDialog.setMessage("Loading....");
            progressDialog.setCancelable(true);
            if (!progressDialog.isShowing())
                progressDialog.show();
        }

    }

    /*
     * Dismiss DAILOG
     */
    public static void dismissDialog() {
        if (progressDialog != null)
            progressDialog.dismiss();
    }

    public static String getMotherOrPregnantType(Context ctx) {
        String type = SharedPrefsUtils.getMotherORPrgntType(ctx);

        if (type != null && type.equalsIgnoreCase("1")) {
            return AppConstants.STR_PREGNANT;
        } else {
            return AppConstants.STR_MOTHER;
        }
    }

    public static String getTodatDate() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd",Locale.getDefault());
        Date date = new Date();
        System.out.println(dateFormat.format(date));
        return "" + dateFormat.format(date);
    }

    public static String convertDateFormat(String inputText1) {
        String outputText = "";
        String inputText = inputText1;
        // "Z" appears not to be supported for some reason.
        DateFormat inputFormat = new SimpleDateFormat("dd MMM yyyy",Locale.getDefault());
        inputFormat.setTimeZone(TimeZone.getTimeZone("IST"));
        DateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd",Locale.getDefault());
        Date parsed = null;
        try {
            parsed = inputFormat.parse(inputText);

            outputText = outputFormat.format(parsed);

        } catch (android.net.ParseException e) {
            e.printStackTrace();
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {

            return outputText;
        }
    }

    public static String getPath(Activity context, Uri uri) {

        String[] projection = {MediaStore.Images.Media.DATA};

        Cursor cursor = context.managedQuery(uri, projection, null, null, null);

        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);

        cursor.moveToFirst();

        return cursor.getString(column_index);

    }

    /**
     * remove extension of file
     */
    public static String getFileNameWithExtn(String file) {

        String separator = System.getProperty("file.separator");
        String filename;

        // Remove the path upto the filename.
        int lastSeparatorIndex = file.lastIndexOf(separator);
        if (lastSeparatorIndex == -1) {
            filename = file;
        } else {
            filename = file.substring(lastSeparatorIndex + 1);
        }

        return filename;
    }

    public static long calculateDiffInDates(Date from, Date to){
        long fromTime = from.getTime();
        long toTime = to.getTime();
        long diffInTime = toTime-fromTime;
        return TimeUnit.DAYS.convert(diffInTime, TimeUnit.MILLISECONDS)+1;
    }

    /*
     * get Extn of file
     *
     * @param url
     * @return
     */
    public static String getMimeType(String url) {
        return url.substring((url.lastIndexOf(".") + 1), url.length());
    }


    public static void getDeviceWidthAndHeigth(Context context) {

        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);

        Display display = wm.getDefaultDisplay();

        //Display display = (Activity)activity.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = AppConstants.DEVICE_DISPLAY_WIDTH = size.x;
        int height = size.y;
    }

    public static String convertSimpleDayFormat(String val1) {

        if (val1 == null) {
            return "";
        }

        long val = Long.parseLong(val1);

        Calendar today = Calendar.getInstance();
        today = clearTimes(today);

        Calendar yesterday = Calendar.getInstance();
        yesterday.add(Calendar.DAY_OF_YEAR, -1);
        yesterday = clearTimes(yesterday);

        Calendar last7days = Calendar.getInstance();
        last7days.add(Calendar.DAY_OF_YEAR, -7);
        last7days = clearTimes(last7days);

        Calendar last30days = Calendar.getInstance();
        last30days.add(Calendar.DAY_OF_YEAR, -30);
        last30days = clearTimes(last30days);


        if (val > today.getTimeInMillis()) {
            return "Posted today at " + changeTimeFormate(val);
        } else if (val > yesterday.getTimeInMillis()) {
            return "Posted yesterday at " + changeTimeFormate(val);
        } else {
            DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy",Locale.getDefault());
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(val);

            //System.out.println(formatter.format(calendar.getTime()));

            String displayDay = "Posted " + formatter.format(calendar.getTime()) + " at " + changeTimeFormate(val);

            return displayDay;
        }
    }

    public static String convertSimpleDayFormat1(String val1) {

        if (val1 == null) {
            return "";
        }

        long val = Long.parseLong(val1);

        Calendar today = Calendar.getInstance();
        today = clearTimes(today);

        Calendar yesterday = Calendar.getInstance();
        yesterday.add(Calendar.DAY_OF_YEAR, -1);
        yesterday = clearTimes(yesterday);

        Calendar last7days = Calendar.getInstance();
        last7days.add(Calendar.DAY_OF_YEAR, -7);
        last7days = clearTimes(last7days);

        Calendar last30days = Calendar.getInstance();
        last30days.add(Calendar.DAY_OF_YEAR, -30);
        last30days = clearTimes(last30days);

        if (val > today.getTimeInMillis()) {
            return "Updated today at " + changeTimeFormate(val);
        } else if (val > yesterday.getTimeInMillis()) {
            return "Updated yesterday at " + changeTimeFormate(val);
        } else {
            DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy",Locale.getDefault());
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(val);

            //System.out.println(formatter.format(calendar.getTime()));

            String displayDay = "Updated " + formatter.format(calendar.getTime()) + " at " + changeTimeFormate(val);

            return displayDay;
        }

    }

    private static Calendar clearTimes(Calendar c) {
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        return c;
    }

    /*
     * Get CURRENT_TIME in AM and PM format
     *
     * @return currentTime
     */
    public static String changeTimeFormate(long milliseconds) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(milliseconds);

        int minute = cal.get(Calendar.MINUTE);

        int hourOfDay = cal.get(Calendar.HOUR_OF_DAY);

        String currentTime = "";

        String strMinute = String.valueOf(String.format(Locale.getDefault(),"%02d", minute));
        String strHour = String.valueOf(String.format(Locale.getDefault(),"%02d", hourOfDay - 12));

        if (hourOfDay > 12) {

            currentTime = strHour + ":" + strMinute + "pm";
        }
        if (hourOfDay == 12) {

            currentTime = "12" + ":" + strMinute + "pm";
        }
        if (hourOfDay < 12) {
            if (hourOfDay != 0) {
                currentTime = String.valueOf(String.format(Locale.getDefault(),"%02d", hourOfDay)) + ":" + strMinute + "am";
            } else {

                currentTime = "12" + ":" + strMinute + "am";
            }
        }
        return currentTime;
    }

    /*
     * @param uri
     * @return
     * @throws IOException
     */
    public static Bitmap getBitmapFromUri(Activity activity, Uri uri) throws IOException {
        ParcelFileDescriptor parcelFileDescriptor = activity.getContentResolver().openFileDescriptor(uri, "r");
        FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
        Bitmap image = BitmapFactory.decodeFileDescriptor(fileDescriptor);
        parcelFileDescriptor.close();
        return image;
    }

    /*
     * @param bm
     * @return
     */
    public static File createFileinSDcard(Bitmap bm) {
        File file;
        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "/Savika");
        myDir.mkdirs();
        Random generator = new Random();
        int n = 10000;
        n = generator.nextInt(n);
        String fname = "Image_1" /*+ n */ + ".jpg";
        file = new File(myDir, fname);
        Log.i("file", "" + file);
        if (file.exists())
            file.delete();
        try {
            FileOutputStream out = new FileOutputStream(file);
            bm.compress(Bitmap.CompressFormat.JPEG, 100, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return file;
    }

    public static String timeAgo(long time) {

        String result = null;
        long starttime = time;
        long presettime = System.currentTimeMillis();

        try {
            // in milliseconds
            long diff = presettime - starttime;
            long diffSeconds = diff / 1000 % 60;
            long diffMinutes = diff / (60 * 1000) % 60;
            long diffHours = diff / (60 * 60 * 1000) % 24;
            long diffDays = diff / (24 * 60 * 60 * 1000);
            long weeks = diffDays / 7;
            long diffyears = diffDays / 365;

            if (diffDays == 1) {

                result = diffDays + " day ago";

            } else if (diffDays > 1) {

                result = AppUtils.convertSimpleDayFormat(String.valueOf(time));

            } else if (diffHours == 1) {

                result = diffHours + " hr ago";

            } else if (diffHours > 1) {

                result = diffHours + " hrs ago";

            } else if (diffMinutes == 1) {

                result = diffMinutes + " min ago";
            } else if (diffMinutes > 1) {

                result = diffMinutes + " mins ago";
            } else if (diffSeconds >= 1) {
                result = "just now";
            }

        } catch (Exception e) {
            Logger.logStackTrace(e);
            result = "NA";
        }
        return result;
    }

    public static String get12HRSTime(String time){
        try {
            String _24HourTime = time;
            SimpleDateFormat _24HourSDF = new SimpleDateFormat("HH:mm", Locale.getDefault());
            SimpleDateFormat _12HourSDF = new SimpleDateFormat("hh:mm a", Locale.getDefault());
            Date _24HourDt = _24HourSDF.parse(_24HourTime);
            return _12HourSDF.format(_24HourDt);
        } catch (Exception e) {
            e.printStackTrace();
            return time;
        }
    }

    public static String get24HRSTime(String time){
        try {
            String _12HourTime = time;
            SimpleDateFormat _12HourSDF = new SimpleDateFormat("hh:mm a",Locale.getDefault());
            SimpleDateFormat _24HourSDF = new SimpleDateFormat("HH:mm",Locale.getDefault());
            Date _12HourDt = _12HourSDF.parse(_12HourTime);
            return _24HourSDF.format(_12HourDt);
        } catch (Exception e) {
            e.printStackTrace();
            return time;
        }
    }

    public static String getDisplayDate(String dateString){
        String outputText = "";
        String inputText = dateString;
        // "Z" appears not to be supported for some reason.
        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd",Locale.getDefault());
        inputFormat.setTimeZone(TimeZone.getTimeZone("IST"));
        DateFormat outputFormat = new SimpleDateFormat("dd MMM yyyy",Locale.getDefault());
        Date parsed = null;
        try {
            parsed = inputFormat.parse(inputText);

            outputText = outputFormat.format(parsed);

        } catch (android.net.ParseException e) {
            e.printStackTrace();
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {

            return outputText;
        }
    }

    public static void sessionExpired(final AppCompatActivity activity){
        activity.runOnUiThread(new AlertDialogUtility(activity, activity.getLayoutInflater(), activity.getResources().getString(R.string.app_name), "Session Expired. Please SignIN", "OK", new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                SharedPrefsUtils.setUserID(activity, null);

                Intent articals = new Intent(activity, Splash.class);
                articals.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                articals.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                articals.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                activity.startActivity(articals);
                activity.finishAffinity();

            }
        }, false));
    }

    public static void sessionExpired(final FragmentActivity activity) {
        activity.runOnUiThread(new AlertDialogUtility(activity, activity.getLayoutInflater(), activity.getResources().getString(R.string.app_name), "Session Expired. Please SignIN", "OK", new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                SharedPrefsUtils.setUserID(activity, null);

                Intent articals = new Intent(activity, Splash.class);
                articals.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                articals.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                articals.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                activity.startActivity(articals);
                activity.finishAffinity();

            }
        }, false));
    }
}
