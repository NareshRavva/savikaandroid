package com.savika.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.savika.GlobalConfigResObjModel;
import com.savika.authentication.OtpVerify;
import com.savika.constants.AppConstants;
import com.savika.homeScreen.HomeActivity;
import com.savika.profile.ProfileEditActivity;
import com.savika.recommendReadings.ArticalsModel;
import com.savika.recommendReadings.CatModel;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/*
 * Created by nisum on 17/02/17.
 */

public class SharedPrefsUtils {

    private static String TAG = "SharedPrefsUtils";

    /*
     * @param context
     * @param name
     */
    public static void setUsername(Context context, String name) {
        if (context != null) {
            SharedPreferences appPref = context.getSharedPreferences(AppConstants.SHARED_PREFERENCES, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = appPref.edit();

            if (editor != null) {
                editor.putString(AppConstants.PREF_USERNAME, name);
                editor.apply();
            }
        }
    }

    /*
     * @param context
     * @return
     */
    public static String getUsername(Context context) {
        SharedPreferences appPref = context.getSharedPreferences(AppConstants.SHARED_PREFERENCES, Context.MODE_PRIVATE);
        return appPref.getString(AppConstants.PREF_USERNAME, null);
    }


    /*
     * @param context
     * @return
     */
    public static String getOTP(Context context) {
        SharedPreferences appPref = context.getSharedPreferences(AppConstants.SHARED_PREFERENCES, Context.MODE_PRIVATE);
        return appPref.getString(AppConstants.PREF_OTP, null);
    }


    /*
     * @param context
     * @param name
     */
    public static void setOTP(Context context, String name) {
        if (context != null) {
            SharedPreferences appPref = context.getSharedPreferences(AppConstants.SHARED_PREFERENCES, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = appPref.edit();

            if (editor != null) {
                editor.putString(AppConstants.PREF_OTP, name);
                editor.apply();
            }
        }
    }

    /*
     * @param context
     * @return
     */
    public static String getMobileNum(Context context) {
        SharedPreferences appPref = context.getSharedPreferences(AppConstants.SHARED_PREFERENCES, Context.MODE_PRIVATE);
        return appPref.getString(AppConstants.PREF_MOBILE_NO, null);
    }


    /*
     * @param context
     * @param name
     */
    public static void setMobileNum(Context context, String name) {
        if (context != null) {
            SharedPreferences appPref = context.getSharedPreferences(AppConstants.SHARED_PREFERENCES, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = appPref.edit();

            if (editor != null) {
                editor.putString(AppConstants.PREF_MOBILE_NO, name);
                editor.apply();
            }
        }
    }

    /*
     * @param context
     * @return
     */
    public static String getEmail(Context context) {
        SharedPreferences appPref = context.getSharedPreferences(AppConstants.SHARED_PREFERENCES, Context.MODE_PRIVATE);
        return appPref.getString(AppConstants.PREF_EMAILID, null);
    }


    /*
     * @param context
     * @param name
     */
    public static void setEmail(Context context, String name) {
        if (context != null) {
            SharedPreferences appPref = context.getSharedPreferences(AppConstants.SHARED_PREFERENCES, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = appPref.edit();

            if (editor != null) {
                editor.putString(AppConstants.PREF_EMAILID, name);
                editor.apply();
            }
        }
    }

    /*
     * @param context
     * @return
     */
    public static String getUserID(Context context) {
        SharedPreferences appPref = context.getSharedPreferences(AppConstants.SHARED_PREFERENCES, Context.MODE_PRIVATE);
        return appPref.getString(AppConstants.PREF_USER_ID, null);
    }


    /*
     * @param context
     * @param name
     */
    public static void setUserID(Context context, String name) {
        if (context != null) {
            SharedPreferences appPref = context.getSharedPreferences(AppConstants.SHARED_PREFERENCES, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = appPref.edit();

            if (editor != null) {
                editor.putString(AppConstants.PREF_USER_ID, name);
                editor.apply();
            }
        }
    }

    /*
     * @param context
     * @return
     */
    public static String getProfilePic(Context context) {
        SharedPreferences appPref = context.getSharedPreferences(AppConstants.SHARED_PREFERENCES, Context.MODE_PRIVATE);
        return appPref.getString(AppConstants.PREF_PROFILEIMG, null);
    }


    /*
     * @param context
     * @param name
     */
    public static void setProfilePic(Context context, String name) {
        if (context != null) {
            SharedPreferences appPref = context.getSharedPreferences(AppConstants.SHARED_PREFERENCES, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = appPref.edit();

            if (editor != null) {
                editor.putString(AppConstants.PREF_PROFILEIMG, name);
                editor.apply();
            }
        }
    }


    /*
     * @param listCat
     */
    public static void setCatData(Context context, ArrayList<CatModel> listCat) {
        if (context != null) {
            SharedPreferences appPref = context.getSharedPreferences(AppConstants.SHARED_PREFERENCES, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = appPref.edit();

            if (editor != null) {

                Gson gson = new Gson();
                String json = gson.toJson(listCat);
                editor.putString(AppConstants.PREF_CAT_DATA, json);
                editor.apply();
            }
        }
    }

    /*
     * @param context
     * @return
     */
    public static String getCatData(Context context) {
        SharedPreferences appPref = context.getSharedPreferences(AppConstants.SHARED_PREFERENCES, Context.MODE_PRIVATE);
        return appPref.getString(AppConstants.PREF_CAT_DATA, null);
    }

    /*
     * @param listCat
     */
    public static void setPregntData(Context context, ArrayList<ArticalsModel> listCat) {
        if (context != null) {
            SharedPreferences appPref = context.getSharedPreferences(AppConstants.SHARED_PREFERENCES, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = appPref.edit();

            if (editor != null) {

                Gson gson = new Gson();
                String json = gson.toJson(listCat);
                editor.putString(AppConstants.PREF_PREGNT_DATA, json);
                editor.apply();
            }
        }
    }

    /*
     * @param context
     * @return
     */
    public static String getPregntData(Context context) {
        SharedPreferences appPref = context.getSharedPreferences(AppConstants.SHARED_PREFERENCES, Context.MODE_PRIVATE);
        return appPref.getString(AppConstants.PREF_PREGNT_DATA, null);
    }


    /*
     * @param listCat
     */
    public static void setMotherData(Context context, ArrayList<ArticalsModel> listCat) {
        if (context != null) {
            SharedPreferences appPref = context.getSharedPreferences(AppConstants.SHARED_PREFERENCES, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = appPref.edit();

            if (editor != null) {

                Gson gson = new Gson();
                String json = gson.toJson(listCat);
                editor.putString(AppConstants.PREF_MOTHER_DATA, json);
                editor.apply();
            }
        }
    }

    /*
     * @param context
     * @return
     */
    public static String getMotherData(Context context) {
        SharedPreferences appPref = context.getSharedPreferences(AppConstants.SHARED_PREFERENCES, Context.MODE_PRIVATE);
        return appPref.getString(AppConstants.PREF_MOTHER_DATA, null);
    }

    /*
     * @param context
     * @param name
     */
    public static void setMotherORPrgntType(Context context, String name) {
        if (context != null) {
            SharedPreferences appPref = context.getSharedPreferences(AppConstants.SHARED_PREFERENCES, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = appPref.edit();

            if (editor != null) {
                editor.putString(AppConstants.PREF_MOTHER_OR_PRGNT_TYPE, name);
                editor.apply();
            }
        }
    }

    /*
     * @param context
     * @return
     */
    public static String getMotherORPrgntType(Context context) {
        SharedPreferences appPref = context.getSharedPreferences(AppConstants.SHARED_PREFERENCES, Context.MODE_PRIVATE);
        return appPref.getString(AppConstants.PREF_MOTHER_OR_PRGNT_TYPE, null);
    }


    /*
     * @param context
     * @param count
     */
    public static void setChildCount(Context context, int count) {
        if (context != null) {
            SharedPreferences appPref = context.getSharedPreferences(AppConstants.SHARED_PREFERENCES, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = appPref.edit();

            if (editor != null) {
                editor.putInt(AppConstants.PREF_CHILD_COUNT, count);
                editor.apply();
            }
        }
    }

    /*
     * @param context
     * @return
     */
    public static int getChildCount(Context context) {
        SharedPreferences appPref = context.getSharedPreferences(AppConstants.SHARED_PREFERENCES, Context.MODE_PRIVATE);
        return appPref.getInt(AppConstants.PREF_CHILD_COUNT, 0);
    }

    public static void setChildIds(Context context, Set<String> childid) {
        if (context != null) {
            SharedPreferences appPref = context.getSharedPreferences(AppConstants.SHARED_PREFERENCES, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = appPref.edit();

            if (editor != null) {
                editor.putStringSet(AppConstants.PREF_CHILD_IDS, childid);
                editor.apply();
            }
        }
    }

    public static Set<String> getChildIds(Context context) {
        SharedPreferences appPref = context.getSharedPreferences(AppConstants.SHARED_PREFERENCES, Context.MODE_PRIVATE);
        return appPref.getStringSet(AppConstants.PREF_CHILD_IDS, new HashSet<String>());
    }

    public static boolean getNotificationStatus(Context context) {
        SharedPreferences appPref = context.getSharedPreferences(AppConstants.SHARED_PREFERENCES, Context.MODE_PRIVATE);
        return appPref.getBoolean(AppConstants.NOTIFICATION_SETTINGS, true);
    }

    public static void setNotificationStatus(Context context, boolean notif) {
        if (context != null) {
            SharedPreferences appPref = context.getSharedPreferences(AppConstants.SHARED_PREFERENCES, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = appPref.edit();

            if (editor != null) {
                editor.putBoolean(AppConstants.NOTIFICATION_SETTINGS, notif);
                editor.apply();
            }
        }
    }

    public static void setGlobalConfig(Context context, ArrayList<GlobalConfigResObjModel> globalConfig) {
        if (context != null) {
            SharedPreferences appPref = context.getSharedPreferences(AppConstants.SHARED_PREFERENCES, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = appPref.edit();

            if (editor != null) {

                Gson gson = new Gson();
                String json = gson.toJson(globalConfig);
                editor.putString(AppConstants.PREF_GLOBAL_CONFIG, json);
                editor.apply();
            }
        }
    }

    public static String getGlobalConfig(Context context) {
        SharedPreferences appPref = context.getSharedPreferences(AppConstants.SHARED_PREFERENCES, Context.MODE_PRIVATE);
        return appPref.getString(AppConstants.PREF_GLOBAL_CONFIG, null);
    }

    /*
     * @param context
     * @param count
     */
    public static void setUnReadNoticationsCount(Context context, int count) {
        if (context != null) {
            SharedPreferences appPref = context.getSharedPreferences(AppConstants.SHARED_PREFERENCES, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = appPref.edit();

            if (editor != null) {
                editor.putInt(AppConstants.PREF_UNREAD_COUNT, count);
                editor.apply();
            }
        }
    }

    /*
     * @param context
     * @return
     */
    public static int getUnReadNoticationsCount(Context context) {
        SharedPreferences appPref = context.getSharedPreferences(AppConstants.SHARED_PREFERENCES, Context.MODE_PRIVATE);
        return appPref.getInt(AppConstants.PREF_UNREAD_COUNT, 0);
    }

    public static void setCity(Context context, String city) {
        if (context != null) {
            SharedPreferences appPref = context.getSharedPreferences(AppConstants.SHARED_PREFERENCES, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = appPref.edit();

            if (editor != null) {
                editor.putString(AppConstants.PREF_CITY, city);
                editor.apply();
            }
        }
    }

    public static void setOccupation(Context context, String occupation) {
        if (context != null) {
            SharedPreferences appPref = context.getSharedPreferences(AppConstants.SHARED_PREFERENCES, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = appPref.edit();

            if (editor != null) {
                editor.putString(AppConstants.PREF_OCCUPATION, occupation);
                editor.apply();
            }
        }
    }

    public static String getCity(ProfileEditActivity context) {
        SharedPreferences appPref = context.getSharedPreferences(AppConstants.SHARED_PREFERENCES, Context.MODE_PRIVATE);
        return appPref.getString(AppConstants.PREF_CITY, "");
    }

    public static void setDob(Context context, String occupation) {
        if (context != null) {
            SharedPreferences appPref = context.getSharedPreferences(AppConstants.SHARED_PREFERENCES, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = appPref.edit();

            if (editor != null) {
                editor.putString(AppConstants.PREF_DOB, occupation);
                editor.apply();
            }
        }
    }

    public static String getDob(ProfileEditActivity context) {
        SharedPreferences appPref = context.getSharedPreferences(AppConstants.SHARED_PREFERENCES, Context.MODE_PRIVATE);
        return appPref.getString(AppConstants.PREF_DOB, "");
    }

    public static String getOccupation(ProfileEditActivity context) {
        SharedPreferences appPref = context.getSharedPreferences(AppConstants.SHARED_PREFERENCES, Context.MODE_PRIVATE);
        return appPref.getString(AppConstants.PREF_OCCUPATION, "");
    }

    public static String getFirebaseToken(HomeActivity context){
        SharedPreferences appPref = context.getSharedPreferences(AppConstants.SHARED_PREFERENCES, Context.MODE_PRIVATE);
        return appPref.getString(AppConstants.PREF_FIREBASE_TOKEN, "");
    }

    public static void setFirebaseToken(HomeActivity context, String firebase_token){
        if (context != null) {
            SharedPreferences appPref = context.getSharedPreferences(AppConstants.SHARED_PREFERENCES, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = appPref.edit();

            if (editor != null) {
                editor.putString(AppConstants.PREF_FIREBASE_TOKEN, firebase_token);
                editor.apply();
            }
        }
    }

    public static void setToken(Context context, String token) {
        if (context != null) {
            SharedPreferences appPref = context.getSharedPreferences(AppConstants.SHARED_PREFERENCES, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = appPref.edit();

            if (editor != null) {
                editor.putString(AppConstants.PREF_TOKEN, token);
                editor.apply();
            }
        }
    }

    public static String getToken(Context context){
        SharedPreferences appPref = context.getSharedPreferences(AppConstants.SHARED_PREFERENCES, Context.MODE_PRIVATE);
        return appPref.getString(AppConstants.PREF_TOKEN, "");
    }
}
