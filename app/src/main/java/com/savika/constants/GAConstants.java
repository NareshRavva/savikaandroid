package com.savika.constants;

/**
 * Created by Naresh Ravva on 04/10/17.
 */

public class GAConstants {


    //SIGNIN SCREEN
    public static String SIGN_IN_SCREEN = "SIGN_IN_SCREEN";
    public static String NORMAL_SIGNIN = "NORMAL_SIGNIN";
    public static String FACEBOOK_SIGNIN = "FACEBOOK_SIGNIN";


    //SIGNUP SCREEN
    public static String SIGN_UP_SCREEN = "SIGN_UP_SCREEN";
    public static String I_AM_PREGNANT = "I'M PREGNANT";
    public static String I_AM_A_MOTHER = "I'M_A_MOTHER";
    public static String ADD_CHILD = "ADD_CHILD";
    public static String NORMAL_SIGNUP = "NORMAL_SIGNUP";
    public static String FACEBOOK_SIGNUP = "FACEBOOK_SIGNUP";

    //OTP SCREEN
    public static String OTP_SCREEN = "OTP_SCREEN";
    public static String OTP_VERIFIED = "OTP_VERIFIED";

    //ARTICLE SCREEN
    public static String ARTICLE_SCREEN = "ARTICLE_SCREEN";
    public static String ARTICLE_READING = "ARTICLE_READING";
    public static String ARTICLE_SHARING = "ARTICLE_SHARING";
    public static String MOST_USED_CATEGORIES = "MOST_USED_CATEGORIES";

    //COMMUNITY SCREEN
    public static String COMMUNITY_SCREEN = "COMMUNITY_SCREEN";
    public static String ADD_COMMUNITY = "ADD_COMMUNITY";
    public static String ADD_LIKES = "ADD_LIKES";

    //HEALTH TRACK SCREEN
    public static String GRAPH_SCREEN = "GRAPH_SCREEN";
    public static String ADD_WEIGHT = "ADD_WEIGHT";
    public static String ADD_BP = "ADD_BP";
    public static String ADD_SUGAR= "ADD_SUGAR";
    public static String ADD_TEMP = "ADD_TEMP";
    public static String ADD_HEMOGLOBIN = "ADD_HEMOGLOBIN";
    public static String ADD_HEARTRATE = "ADD_HEARTRATE";
    public static String ADD_HEALTH_RECORD = "ADD_HEALTH_RECORD";

    //CALENDAR SCREEN
    public static String CALENDAR_SCREEN = "CALENDAR_SCREEN";
    public static String ADD_MILESTONE = "ADD_MILESTONE";
    public static String ADD_MEMORY = "ADD_MEMORY";
    public static String ADD_NOTE = "ADD_NOTE";
    public static String ADD_REMINDER = "ADD_REMINDER";


}
