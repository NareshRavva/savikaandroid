package com.savika.constants;

/**
 * Created by Naresh R on 30-01-2017.
 */

public class AppConstants {


    public static boolean PLAY_STORE_BUILD = true;
    public static String MOTHER = "2";
    public static String PREGNANT = "1";

    public static String STR_MOTHER = "mother";
    public static String STR_PREGNANT = "pregnant";
    public static String STR_CHILD = "child";

    /**
     * Shared Pref
     */

    public static String SHARED_PREFERENCES = "SHARED_PREFERENCES";
    public static String PREF_CAT_DATA = "PREF_CAT_DATA";
    public static String PREF_PREGNT_DATA = "PREF_PREGNT_DATA";
    public static String PREF_MOTHER_DATA = "PREF_MOTHER_DATA";
    public static String PREF_OTP = "PREF_USER_NAME";
    public static String PREF_MOBILE_NO = "PREF_MOBILE_NO";
    public static String PREF_USERNAME = "PREF_USERNAME";
    public static String PREF_EMAILID = "PREF_EMAILID";
    public static String PREF_USER_ID = "PREF_USER_ID";
    public static String PREF_PROFILEIMG = "PREF_PROFILEIMG";
    public static String PREF_MOTHER_OR_PRGNT_TYPE = "PREF_MOTHER_OR_PRGNT_TYPE";
    public static String PREF_CHILD_COUNT = "PREF_CHILD_COUNT";
    public static String PREF_CHILD_IDS = "PERF_CHILDIDS";
    public static String PREF_GLOBAL_CONFIG = "PREF_GLOBAL_CONFIG";
    public static String PREF_UNREAD_COUNT = "PREF_UNREAD_COUNT";
    public static String PREF_CITY = "PREF_CITY";
    public static String PREF_OCCUPATION = "PREF_OCCUPATION";
    public static String PREF_DOB = "PREF_DOB";
    public static String PREF_FIREBASE_TOKEN = "PREF_FIREBASE_TOKEN";
    public static String PREF_TOKEN = "PREF_TOKEN";

    public static String EXTRA_CHILD_DATA = "EXTRA_CHILD_DATA";
    public static String EXTRA_DUE_DATE = "EXTRA_DUE_DATE";
    public static String EXTRA_DATE_OF_LAST_PERIOD = "EXTRA_DATE_OF_LAST_PERIOD";
    public static String EXTRA_MOBILE_NUMBER = "EXTRA_MOBILE_NUMBER";
    public static String EXTRA_IS_FROM_SIGNIN = "EXTRA_IS_FROM_SIGNIN";

    public static String EXTRA_SIGNUP_REQUIRED = "EXTRA_SIGNUP_REQUIRED";
    public static String EXTRA_SIGNUP_DATA = "EXTRA_SIGNUP_DATA";

    public static String EXTRA_HEALTH_RECORD = "EXTRA_HEALTH_RECORD";
    public static String EXTRA_UPDATE_HEALTH_RECORD = "EXTRA_UPDATE_HEALTH_RECORD";
    public static String EXTRA_SELECTED_CHILD = "EXTRA_SELECTED_CHILD";
    public static String EXTRA_HEADER_VISIBLE = "EXTRA_HEADER_VISIBLE";
    public static String EXTRA_HEALTH_ID = "EXTRA_HEALTH_ID";
    public static String EXTRA_HEALTH_UNIT = "EXTRA_HEALTH_UNIT";
    public static String EXTRA_HEALTH_DESC = "EXTRA_HEALTH_DESC";
    public static String EXTRA_HEALTH_DATE = "EXTRA_HEALTH_DATE";


    // device height width
    public static int DEVICE_DISPLAY_WIDTH;
    public static int DEVICE_DISPLAY_HEIGHT;


    public static String WEB_URL = "WEB_URL";
    public static String WEB_VIEW_TITLE = "WEB_VIEW_TITLE";

    public static String BROAD_CAST_SHEDULE = "BROAD_CAST_SHEDULE";


    public static String HEALTH_HEIGHT = "1";
    public static String HEALTH_WEIGHT = "2";
    public static String HEALTH_BLOOD_PRESSURE = "3";
    public static String HEALTH_SUGAR = "4";
    public static String HEALTH_HEMOGLOBIN = "5";
    public static String HEALTH_TEMPERATURE = "6";
    public static String HEALTH_HEART_RATE = "7";

    public static String ARTICALS_REACH_END = "articals_reach_end";


    public static String COMMUNITY_LIKE = "community_like";
    public static String COMMUNITY_LIKE_COUNT = "community_like_count";
    public static String COMMUNITY_COMMMENT = "community_commment";
    public static String COMMUNITY_MORE = "community_more";

    /**
     * Notification Types
     */
    public static String NOTIFICATION_TYPE_LIKE = "community_like";
    public static String NOTIFICATION_TYPE_COMMENT = "community_commment";
    public static String NOTIFICATION_SETTINGS = "notif_settings";


    public static String CALNDAR_TYPE = "add_type";
    public static String CALNDAR_MODEL_1 = "calndar_model_1";
    public static String CALNDAR_MODEL_2 = "calndar_model_2";
    public static String MILESTONES = "milestones";
    public static String ALL = "all";
    public static String MEMORIES = "memories";
    public static String NOTES = "notes";
    public static String REMINDERS = "reminders";
    public static String ADD_MILESTONE = "Add Milestone";
    public static String ADD_MEMORIES = "Add Memory";
    public static String ADD_NOTES = "Add Note";
    public static String ADD_REMINDERS = "Add Reminder";
    public static String EDIT_MILESTONE = "Edit Milestone";
    public static String EDIT_MEMORIES = "Edit Memory";
    public static String EDIT_NOTES = "Edit Note";
    public static String EDIT_REMINDERS = "Edit Reminder";
    public static String DELETE_MILESTONE = "Delete Milestone";
    public static String DELETE_MEMORIES = "Delete Memory";
    public static String DELETE_NOTES = "Delete Note";
    public static String DELETE_REMINDERS = "Delete Reminder";

    /**
     * notifiactions
     */
    public static String BR_NOTIFICATION_COUNT = "br_notification_count";

}
