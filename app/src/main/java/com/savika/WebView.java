package com.savika;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.savika.logger.Logger;
import com.savika.constants.AppConstants;
import com.savika.utils.AppUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

/*
 * Created by Naresh Ravva on 18/07/17.
 */

public class WebView extends AppCompatActivity {

    @BindView(R.id.help_webview)
    android.webkit.WebView webView;

    @BindView(R.id.llBack)
    LinearLayout llBack;

    @BindView(R.id.txtTitle)
    TextView txtTitle;

    private String webUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview);

        ButterKnife.bind(this);

        llBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        if (getIntent().getStringExtra(AppConstants.WEB_VIEW_TITLE) != null) {
            txtTitle.setText(getIntent().getStringExtra(AppConstants.WEB_VIEW_TITLE));
        }

        if (getIntent().getStringExtra(AppConstants.WEB_URL) != null) {
            webUrl = getIntent().getStringExtra(AppConstants.WEB_URL);

            Logger.info("WEBVIEW", "URL_______>" + webUrl);

            AppUtils.showDialog(WebView.this);

            webView.getSettings().setJavaScriptEnabled(true);
            webView.setWebViewClient(new WebViewClient() {
                @Override
                public void onPageFinished(android.webkit.WebView view, String url) {

                    AppUtils.dismissDialog();
                }
            });
            if (webUrl.contains("pdf")){
                webView.loadUrl("https://docs.google.com/viewer?url="+webUrl);
            }
            else  {
                if (webUrl.contains("milestone"))
                    webView.loadDataWithBaseURL("file:///android_res/drawable/", "<body ><img src='"+  webUrl+"' style='background:red;text-align:center;display:block;margin:0 auto;' /></body>", "text/html", "utf-8", null);
               else webView.loadUrl(webUrl);
            }

            webView.getSettings().setLoadWithOverviewMode(true);
            webView.getSettings().setUseWideViewPort(true);
            webView.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
            webView.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);

            webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);

            webView.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        }
    }

    @Override
    public void onBackPressed() {
        if (webView != null && webView.canGoBack()) {
            webView.goBack();
        } else {
            finish();
        }
    }
}