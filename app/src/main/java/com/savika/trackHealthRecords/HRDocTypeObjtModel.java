package com.savika.trackHealthRecords;

/*
 * Created by Naresh Ravva on 16/10/17.
 */

public class HRDocTypeObjtModel {

    private String id, healthdocdesc;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getHealthdocdesc() {
        return healthdocdesc;
    }

    public void setHealthdocdesc(String healthdocdesc) {
        this.healthdocdesc = healthdocdesc;
    }
}
