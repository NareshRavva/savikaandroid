package com.savika.trackHealthRecords;

import android.Manifest;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.ParcelFileDescriptor;
import android.os.Parcelable;
import android.provider.OpenableColumns;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.savika.Api.ApiClient;
import com.savika.Api.ApiInterface;
import com.savika.Api.CommonResModel;
import com.savika.R;
import com.savika.amAMother.ChildModel;
import com.savika.amAMother.ProfileUploadResModel;
import com.savika.constants.AppConstants;
import com.savika.constants.GAConstants;
import com.savika.dialogUtils.AlertDialogUtility;
import com.savika.dialogUtils.DatePicketDialog;
import com.savika.googleAnalytics.GoogleAnalyticsHelper;
import com.savika.logger.Logger;
import com.savika.utils.AppUtils;
import com.savika.utils.ImageUtil;
import com.savika.utils.SharedPrefsUtils;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

/*
 * Created by Naresh Ravva on 16/10/17.
 */

public class AddHealthRecordActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.tvHeaderTitle)
    TextView tvHeaderTitle;

    @BindView(R.id.llBack)
    LinearLayout llBack;

    @BindView(R.id.spnrDocTypes)
    Spinner spnrDocTypes;

    @BindView(R.id.rlFilePickup)
    RelativeLayout rlFilePickup;

    @BindView(R.id.tvAddDoc)
    TextView tvAddDoc;

    @BindView(R.id.tvAddDate)
    TextView tvAddDate;

    @BindView(R.id.rlDate)
    RelativeLayout rlDate;

    @BindView(R.id.etvDocTitle)
    EditText etvDocTitle;

    @BindView(R.id.etvDocDesc)
    EditText etvDocDesc;

    @BindView(R.id.etvDoctorName)
    EditText etvDoctorName;

    @BindView(R.id.llProceed)
    LinearLayout llProceed;

    @BindView(R.id.tvMother)
    TextView tvMother;

    @BindView(R.id.tvChild1)
    TextView tvChild1;

    @BindView(R.id.tvChild2)
    TextView tvChild2;

    @BindView(R.id.cvLables)
    CardView cvMotherChildLables;

    @BindView(R.id.tvProceed)
    TextView tvProceed;

    @BindView(R.id.tvExtn)
    TextView tvExtn;

    @BindView(R.id.flRemoveExtn)
    FrameLayout flRemoveExtn;

    private String CHILD_ID = null;

    private String TYPE;

    String DATE, DOC_NAME, DOC_URL, HEALTH_DOC_ID, DATE_DISPLAY;

    ArrayList<ChildModel> ARR_CHILD_DATA = new ArrayList<>();

    private String TAG = this.getClass().getSimpleName();

    private ArrayList<HRDocTypeObjtModel> ARR_HR_DOC_TYPES = new ArrayList<>();

    private AddHRReqModel add_hr_req_model;
    private AddHRReqModel edit_req_model;

    GoogleAnalyticsHelper mGoogleHelper;
    File pdfFile;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_health_record);

        ButterKnife.bind(this);
        isStoragePermissionGranted();
        tvHeaderTitle.setText(R.string.add_health_records);

        TYPE = AppUtils.getMotherOrPregnantType(this);

        AppUtils.getDeviceWidthAndHeigth(this);

        getHealthDocTypes();

        rlFilePickup.setOnClickListener(this);
        llProceed.setOnClickListener(this);
        tvMother.setOnClickListener(this);
        llBack.setOnClickListener(this);
        flRemoveExtn.setOnClickListener(this);

        handleDatePicker();
        Date date =new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy", Locale.getDefault());
        tvAddDate.setText(sdf.format(date));
        DATE = AppUtils.convertDateFormat(sdf.format(date));

        if (SharedPrefsUtils.getMotherORPrgntType(this).equalsIgnoreCase("1")) {
            cvMotherChildLables.setVisibility(View.GONE);
        } else {
            if (getIntent().getSerializableExtra(AppConstants.EXTRA_CHILD_DATA) != null)
                ARR_CHILD_DATA = (ArrayList<ChildModel>) getIntent().getSerializableExtra(AppConstants.EXTRA_CHILD_DATA);
        }
        InitGoogleAnalytics();
        updateCardsActiveInActive();

    }

    // Google Analytics
    private void InitGoogleAnalytics() {
        mGoogleHelper = new GoogleAnalyticsHelper();
        mGoogleHelper.init(this);
    }

    public boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG, "Permission is granted");
                return true;
            } else {
                Log.v(TAG, "Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.v(TAG, "Permission is granted");
            return true;
        }
    }
    /**
     *
     */
    private void setExistingData() {
        if (getIntent().getSerializableExtra(AppConstants.EXTRA_HEALTH_RECORD) != null) {
            add_hr_req_model = (AddHRReqModel) getIntent().getSerializableExtra(AppConstants.EXTRA_HEALTH_RECORD);

            etvDocTitle.setText(add_hr_req_model.getDoc_title());
            etvDocDesc.setText(add_hr_req_model.getDoc_content());
            DateFormat df =  new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
            DateFormat sdf = new SimpleDateFormat("dd MMM yyyy", Locale.getDefault());
            try {
                Date selecteddate = df.parse(add_hr_req_model.getDate());
                tvAddDate.setText(sdf.format(selecteddate));
            }catch (Exception e){
                e.printStackTrace();
                tvAddDate.setText(add_hr_req_model.getDate());
            }
            etvDoctorName.setText(add_hr_req_model.getDoctorname());

            if (add_hr_req_model.getDocurl() != null && add_hr_req_model.getDocurl().length()>10) {
                String fileName = add_hr_req_model.getDocurl().substring(add_hr_req_model.getDocurl().lastIndexOf('/') + 1);
                if (fileName.split("-",2).length>1)
                    tvAddDoc.setText(fileName.split("-",2)[1]);
                else tvAddDoc.setText(fileName);
                tvExtn.setText(AppUtils.getMimeType(fileName).toUpperCase());
                flRemoveExtn.setVisibility(View.VISIBLE);
            }
            tvHeaderTitle.setText(R.string.edit_healthrecords);
            DATE = add_hr_req_model.getDate();
            DOC_URL = add_hr_req_model.getDocurl();
            HEALTH_DOC_ID = add_hr_req_model.getHealthdoctypeid();

            setExistingDataToSpinner(HEALTH_DOC_ID);

            tvProceed.setText(R.string.update);
        }
    }

    /*
     * @param health_doc_id
     */
    private void setExistingDataToSpinner(String health_doc_id) {

        for (int i = 0; i < ARR_HR_DOC_TYPES.size(); i++)
            if (ARR_HR_DOC_TYPES.get(i).getId().equalsIgnoreCase(health_doc_id)) {
                spnrDocTypes.setSelection(i);
            }

    }

    /*
     * update child record
     */
    private void updateHRServiceCall(AddHRReqModel model) {

        //format edit request
        edit_req_model = new AddHRReqModel();
        edit_req_model.setUserid(SharedPrefsUtils.getUserID(this));
        edit_req_model.setRecordid(add_hr_req_model.getId());
        edit_req_model.setHealthdoctypeid(HEALTH_DOC_ID);
        edit_req_model.setDoc_title(etvDocTitle.getText().toString());
        edit_req_model.setDoc_content(etvDocDesc.getText().toString());
        edit_req_model.setDate(DATE);
        edit_req_model.setDoctorname(etvDoctorName.getText().toString());
        edit_req_model.setDocurl(DOC_URL);

        //edit_req_model.setType(CALNDAR_TYPE);

        if (TYPE.equalsIgnoreCase(AppConstants.STR_CHILD)) {
            edit_req_model.setChildid(CHILD_ID);
        }

        if (AppUtils.isNetworkConnected(this)) {
            AppUtils.showDialog(this);

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            Call<CommonResModel> call = apiService.updateHealthRecord(SharedPrefsUtils.getToken(getApplicationContext()),model);

            call.enqueue(new retrofit2.Callback<CommonResModel>() {
                @Override
                public void onResponse(Call<CommonResModel> call, retrofit2.Response<CommonResModel> response) {

                    AppUtils.dismissDialog();

                    if (response.code() == 200 && response.body().isSuccess()) {
                        AppUtils.showToast(AddHealthRecordActivity.this, response.body().getMessage());
                        Intent intent = new Intent(AddHealthRecordActivity.this, DetailHealthRecordActivity.class);
                        setResult(2, intent);
                        finish();
                    } else {
                        if (response.body().getMessage()!=null) {
                            if (response.body().getMessage().equalsIgnoreCase("Unauthorized")) {
                                AppUtils.sessionExpired(AddHealthRecordActivity.this);
                            } else
                                AppUtils.showToast(getApplicationContext(), response.body().getMessage());
                        }
                    }
                }

                @Override
                public void onFailure(Call<CommonResModel> call, Throwable t) {
                    AppUtils.dismissDialog();
                }
            });
        }
        else {
            AppUtils.showToast(this,"It looks like your internet connectioin off. Please turn it on and try again.");
        }
    }

    /*
     *
     */
    private void updateCardsActiveInActive() {

        int childCount = SharedPrefsUtils.getChildCount(this);

        Logger.info(TAG, "childCount-->" + childCount);

        if (childCount == 1) {
            tvChild2.setAlpha(0.3f);
            tvChild1.setOnClickListener(this);
            tvChild2.setOnClickListener(null);

        } else if (childCount == 2) {
            tvChild1.setOnClickListener(this);
            tvChild2.setOnClickListener(this);
        }
    }

    private void handleDatePicker() {

        rlDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                runOnUiThread(new DatePicketDialog(AddHealthRecordActivity.this, getString(R.string.select_date), getString(R.string.please_pic_the_date), false, AddHealthRecordActivity.this.getLayoutInflater(), new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Logger.info("TAG", "Selected prblm Date-->" + view.getTag());

                        tvAddDate.setText(String.format("%s", view.getTag()));

                        DATE_DISPLAY = "" + view.getTag();

                        DATE = AppUtils.convertDateFormat("" + view.getTag());
                    }
                }));
            }
        });

    }

    /**
     * get health doc types
     */
    private void getHealthDocTypes() {

        if (AppUtils.isNetworkConnected(this)) {
            AppUtils.showDialog(this);

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            Call<HRDocTypeResModel> call = apiService.getHealthDocType(SharedPrefsUtils.getToken(getApplicationContext()),SharedPrefsUtils.getMotherORPrgntType(AddHealthRecordActivity.this));

            call.enqueue(new retrofit2.Callback<HRDocTypeResModel>() {
                @Override
                public void onResponse(Call<HRDocTypeResModel> call, retrofit2.Response<HRDocTypeResModel> response) {

                    AppUtils.dismissDialog();

                    if (response.code() == 200 && response.body().isSuccess()) {
                        ARR_HR_DOC_TYPES = response.body().arrHealthDocTypes;

                        Logger.info(TAG, "--ARR_HR_DOC_TYPES-->" + ARR_HR_DOC_TYPES.size());

                        setSpinnerData();

                        setExistingData();

                        if (getIntent().getIntExtra(AppConstants.EXTRA_SELECTED_CHILD, 0) == 1) {
                            selectChild1();
                        } else if (getIntent().getIntExtra(AppConstants.EXTRA_SELECTED_CHILD, 0) == 2) {
                            selectChild2();
                        }
                    }

                    else {
                        if (response.body().getMessage()!=null) {
                            if (response.body().getMessage().equalsIgnoreCase("Unauthorized")) {
                                AppUtils.sessionExpired(AddHealthRecordActivity.this);
                            } else
                                AppUtils.showToast(getApplicationContext(), response.body().getMessage());
                        }
                    }
                }

                @Override
                public void onFailure(Call<HRDocTypeResModel> call, Throwable t) {
                    AppUtils.dismissDialog();
                }
            });
        }
        else {
            AppUtils.showToast(this,"It looks like your internet connectioin off. Please turn it on and try again.");
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (pdfFile !=null && pdfFile.exists())
            pdfFile.delete();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.llBack:
                if (pdfFile !=null && pdfFile.exists())
                    pdfFile.delete();
                finish();
                break;

            case R.id.rlFilePickup:
                if (isStoragePermissionGranted())
                    showpopUp(view);
                else AppUtils.showToast(AddHealthRecordActivity.this,"Please grant the permission");
                break;

            case R.id.llProceed:

                if (etvDocTitle.getText().toString().length() < 1) {
                    AppUtils.showToast(this, "Enter Document Title");
                    return;
                }

                if (tvAddDate.getText().toString().length() < 1) {
                    AppUtils.showToast(this, "Add Date");
                    return;
                }

                if (getIntent().getBooleanExtra(AppConstants.EXTRA_UPDATE_HEALTH_RECORD, false)) {
                    updateHRServiceCall(edit_req_model);
                } else {
                    addNewHRServiceCall();
                }

                break;

            case R.id.tvMother:
                tvMother.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                tvChild1.setBackgroundColor(getResources().getColor(R.color.white));
                tvChild1.setBackgroundColor(getResources().getColor(R.color.white));

                tvMother.setTextColor(Color.WHITE);
                tvChild1.setTextColor(Color.BLACK);
                tvChild1.setTextColor(Color.BLACK);
                TYPE = AppConstants.STR_MOTHER;

                break;

            case R.id.tvChild1:

                selectChild1();

                break;

            case R.id.tvChild2:

                selectChild2();

                break;

            case R.id.flRemoveExtn:
                flRemoveExtn.setVisibility(View.GONE);
                if (pdfFile!=null){
                    if (pdfFile.exists()) {
                        pdfFile.delete();
                        pdfFile = null;
                    }
                }
                DOC_URL = null;
                tvAddDoc.setText("");
                break;
        }
    }

    private void showpopUp(View view) {
        final PopupWindow popup = new PopupWindow(this);
        View layout = getLayoutInflater().inflate(R.layout.popup_select_file_img, null);
        popup.setContentView(layout);
        // Set content width and height
        popup.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        popup.setWidth(WindowManager.LayoutParams.WRAP_CONTENT);
        // Closes the popup window when touch outside of it - when looses focus
        popup.setOutsideTouchable(true);
        popup.setFocusable(true);
        // Show anchored to button
        popup.setBackgroundDrawable(new BitmapDrawable());
        popup.showAsDropDown(view);

        View container ;
        WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        if (android.os.Build.VERSION.SDK_INT > 22) {
            container = (View) popup.getContentView().getParent().getParent();
        }else{
            container = (View) popup.getContentView().getParent();
        }
        WindowManager.LayoutParams p = (WindowManager.LayoutParams) container.getLayoutParams();
// add flag
        p.flags |= WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        p.dimAmount = 0.3f;
        wm.updateViewLayout(container, p);

        LinearLayout llShowImageOptions = layout.findViewById(R.id.llEditChild);
        LinearLayout llShowFileOptions = layout.findViewById(R.id.llDeleteChild);

        llShowImageOptions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                popup.dismiss();
                startCropImageActivity(null);


            }
        });

        llShowFileOptions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                popup.dismiss();
                Intent fileIntent = new Intent(Intent.ACTION_GET_CONTENT);
                Uri uri = Uri.parse(Environment.getExternalStorageDirectory()
                        .getPath());
                fileIntent.putExtra(Intent.EXTRA_LOCAL_ONLY,true);
                fileIntent.setDataAndType(uri, "application/pdf");
                List<Intent> intents = new ArrayList<>();
                List<ResolveInfo> listGallery = getPackageManager().queryIntentActivities(fileIntent, 0);
                for (ResolveInfo res : listGallery) {
                    Intent intent = new Intent(fileIntent);
                    intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
                    intent.setPackage(res.activityInfo.packageName);
                    intents.add(intent);

                }
//


                Intent target;
                if (intents.isEmpty()) {
                    target = new Intent();
                } else {
                    target = intents.get(intents.size() - 1);
                    intents.remove(intents.size() - 1);
                }

                // Create a chooser from the main  intent
                Intent chooserIntent = Intent.createChooser(target, "select PDF File");

                // Add all other intents
                chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, intents.toArray(new Parcelable[intents.size()]));

                startActivityForResult(chooserIntent, 123);

            }
        });
    }

    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri).setGuidelines(CropImageView.Guidelines.ON).setMultiTouchEnabled(true).setInitialCropWindowPaddingRatio(0).setAspectRatio(16, 10)
                .start(this);
    }

    /*
     *
     */
    private void selectChild2() {
        tvMother.setBackgroundColor(getResources().getColor(R.color.white));
        tvChild1.setBackgroundColor(getResources().getColor(R.color.white));
        tvChild2.setBackgroundColor(getResources().getColor(R.color.colorPrimary));

        tvMother.setTextColor(Color.BLACK);
        tvChild1.setTextColor(Color.BLACK);
        tvChild2.setTextColor(Color.WHITE);
        TYPE = AppConstants.STR_CHILD;

        if (ARR_CHILD_DATA.size() > 0 && ARR_CHILD_DATA.get(1) != null)
            CHILD_ID = ARR_CHILD_DATA.get(1).getChildid();
    }

    /*
     *
     */
    private void selectChild1() {
        tvMother.setBackgroundColor(getResources().getColor(R.color.white));
        tvChild1.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        tvChild2.setBackgroundColor(getResources().getColor(R.color.white));

        tvMother.setTextColor(Color.BLACK);
        tvChild1.setTextColor(Color.WHITE);
        tvChild2.setTextColor(Color.BLACK);
        TYPE = AppConstants.STR_CHILD;

        if (ARR_CHILD_DATA.size() > 0 && ARR_CHILD_DATA.get(0) != null)
            CHILD_ID = ARR_CHILD_DATA.get(0).getChildid();

        Logger.info(TAG, "---CHILD_ID-->" + CHILD_ID);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case 123:
                if (resultCode == RESULT_OK) {
                    String selectedImagePath = data.getData().getPath();

                    Logger.info(TAG, "--FilePath-->" + selectedImagePath);

                    if (selectedImagePath.contains("/external/")) {
                        selectedImagePath = AppUtils.getPath(this, data.getData());
                        Logger.info(TAG, "--FilePath-->" + selectedImagePath);
                    }

                    String fileName = AppUtils.getFileNameWithExtn(selectedImagePath);

                    String fileExt = AppUtils.getMimeType(selectedImagePath);
                    String fileext = getContentResolver().getType(data.getData());
                    if (fileext==null)
                        fileext = fileExt;
                    if (fileext.equalsIgnoreCase("application/pdf") || fileext.equalsIgnoreCase("pdf")){
                        String name = getFileName(data.getData());
                        System.out.print(name);
//                        String[] ext = name.split("\\.");
                        File file;
                        if (name.endsWith(".pdf"))
                           file  = createFileinSDcard(name,data);
                        else  file = createFileinSDcard(name+".pdf",data);
                        if (file!=null){
                            long len = file.length();
                            if ((len/1024)>2048){
                                if (file.exists())
                                    file.delete();
                                AppUtils.showToast(AddHealthRecordActivity.this,"Maximum file size should be 2mb");

                                return;
                            }
                            else if (len<1024){
                                if (file.exists())
                                    file.delete();
                                AppUtils.showToast(AddHealthRecordActivity.this,"Minimum file size is 1kb");

                                return;
                            }
                        }

                        Logger.info(TAG, "--fileName-->" + fileName + "--fileExt-->" + fileExt);

                        fileUploadToServer(pdfFile.getPath(), name, "pdf");
                    }
                    else {
                        AppUtils.showToast(AddHealthRecordActivity.this,"Only pdf files are accepted");
                        return;
                    }
                }
                break;

            case 203:
                CropImage.ActivityResult result = CropImage.getActivityResult(data);

                if (resultCode == RESULT_OK) {
                    Log.v("MainActivityPrblm", "Image Path-->" + result.getUri());

                    String selectedImagePath = result.getUri().getPath();

                    Logger.info(TAG, "--FilePath-->" + selectedImagePath);

                    if (selectedImagePath.contains("/external/")) {
                        selectedImagePath = AppUtils.getPath(AddHealthRecordActivity.this, data.getData());
                        Logger.info(TAG, "--FilePath-->" + selectedImagePath);
                    }

                    String fileName = AppUtils.getFileNameWithExtn(selectedImagePath);

                    String fileExt = AppUtils.getMimeType(selectedImagePath);

                    Logger.info(TAG, "--fileName-->" + fileName + "--fileExt-->" + fileExt);

                    imageUploadToServer(selectedImagePath, fileName, fileExt);
                }else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                    Toast.makeText(AddHealthRecordActivity.this, "Cropping failed: " + result.getError(), Toast.LENGTH_LONG).show();
                }

        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public String getFileName(Uri uri) {
        String result = null;
        if (uri.getScheme().equals("content")) {
            Cursor cursor = getContentResolver().query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                    System.out.println(getContentResolver().getType(uri));
//                    AppUtils.showToast(AddHealthRecordActivity.this,getContentResolver().getType(uri));
                }
            } finally {
                cursor.close();
            }
        }
        if (result == null) {
            result = uri.getPath();
            System.out.println(result);
            int cut = result.lastIndexOf('/');
            if (cut != -1) {
                result = result.substring(cut + 1);
            }
        }
        return result;
    }

    /*
     * @param selectedImagePath
     * @param fileName
     */
    private void fileUploadToServer(String selectedImagePath, final String fileName, final String fileExt) {


        if (AppUtils.isNetworkConnected(this)) {
            AppUtils.showDialog(this);

            try {

                File file = new File(selectedImagePath);

                Log.d(TAG, "Filename " + file.getName());

                //RequestBody mFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                RequestBody mFile = RequestBody.create(MediaType.parse("image/*"), file);
                MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("doc", file.getName(), mFile);

                RequestBody mobilenumber = RequestBody.create(okhttp3.MultipartBody.FORM, SharedPrefsUtils.getMobileNum(this));
                RequestBody userid = RequestBody.create(okhttp3.MultipartBody.FORM, SharedPrefsUtils.getUserID(this));
                RequestBody filename = RequestBody.create(okhttp3.MultipartBody.FORM, file.getName());

                ApiInterface uploadImage = ApiClient.getClient().create(ApiInterface.class);
                Call<ProfileUploadResModel> fileUpload = uploadImage.uploadDoc(SharedPrefsUtils.getToken(getApplicationContext()),fileToUpload, mobilenumber, userid, filename);

                fileUpload.enqueue(new Callback<ProfileUploadResModel>() {
                    @Override
                    public void onResponse(Call<ProfileUploadResModel> call, Response<ProfileUploadResModel> response) {
                        Logger.info(TAG, "Response " + response.code());

                        if (response.code() == 200 && response.body().isSuccess()) {
                            Logger.info(TAG, "profile URL--->" + response.body().getFile_url());

                            tvAddDoc.setText(fileName);
                            // tvAddDate.setText(DATE_DISPLAY);
                            AppUtils.dismissDialog();

                            DOC_NAME = fileName;
                            DOC_URL = response.body().getFile_url();
                            if (pdfFile != null && pdfFile.exists())
                                pdfFile.delete();
                            tvExtn.setText(fileExt.toUpperCase());
                            flRemoveExtn.setVisibility(View.VISIBLE);
                        }

                        else {
                            if (response.body().getMessage()!=null) {
                                if (response.body().getMessage().equalsIgnoreCase("Unauthorized")) {
                                    AppUtils.sessionExpired(AddHealthRecordActivity.this);
                                } else
                                    AppUtils.showToast(getApplicationContext(), response.body().getMessage());
                            }
                        }

                        AppUtils.dismissDialog();

                    }

                    @Override
                    public void onFailure(Call<ProfileUploadResModel> call, Throwable t) {
                        AppUtils.showToast(getApplicationContext(), getString(R.string.server_down));
                        Log.d(TAG, "Error " + t.getMessage());
                        AppUtils.dismissDialog();
                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else {
            AppUtils.showToast(this,"It looks like your internet connectioin off. Please turn it on and try again.");
        }

    }

    private File createFileinSDcard(String name, Intent data) {
        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "/Savika");
        myDir.mkdirs();
        Random generator = new Random();
        int n = 10000;
        n = generator.nextInt(n);
        pdfFile = new File(myDir, name);
        Log.i("file", "" + pdfFile);
        if (pdfFile.exists())
            pdfFile.delete();
        try {
            InputStream ii = getContentResolver().openInputStream(data.getData());
            FileOutputStream out = new FileOutputStream(pdfFile);
            byte[] buffer = new byte[8 * 1024];
            int bytesRead;
            while ((bytesRead = ii.read(buffer)) != -1) {
                out.write(buffer, 0, bytesRead);
            }
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }


        return pdfFile;
    }

    private void imageUploadToServer(String selectedImagePath, final String fileName, final String fileExt) {

        if (AppUtils.isNetworkConnected(this)) {
            AppUtils.showDialog(this);

            try {
                File file = new File(selectedImagePath);

                Log.d(TAG, "Filename " + file.getName());

                //RequestBody mFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                RequestBody mFile = RequestBody.create(MediaType.parse("image/*"), file);
                MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("image", file.getName(), mFile);

                RequestBody type = RequestBody.create(okhttp3.MultipartBody.FORM, "records");
                RequestBody user_id = RequestBody.create(okhttp3.MultipartBody.FORM, SharedPrefsUtils.getUserID(this));
                ApiInterface uploadImage = ApiClient.getClient().create(ApiInterface.class);
                Call<ProfileUploadResModel> fileUpload = uploadImage.uploadFile(SharedPrefsUtils.getToken(getApplicationContext()),fileToUpload, type, user_id, null);

                fileUpload.enqueue(new Callback<ProfileUploadResModel>() {
                    @Override
                    public void onResponse(Call<ProfileUploadResModel> call, Response<ProfileUploadResModel> response) {
                        Logger.info(TAG, "Response " + response.code());

                        if (response.code() == 200 && response.body().isSuccess()) {
                            Logger.info(TAG, "profile URL--->" + response.body().getFile_url());

                            tvAddDoc.setText(fileName);
                            // tvAddDate.setText(DATE_DISPLAY);
                            AppUtils.dismissDialog();

                            DOC_NAME = fileName;
                            DOC_URL = response.body().getProfileurl();

                            tvExtn.setText(fileExt.toUpperCase());
                            flRemoveExtn.setVisibility(View.VISIBLE);
                        }
                        else {
                            if (response.body().getMessage()!=null) {
                                if (response.body().getMessage().equalsIgnoreCase("Unauthorized")) {
                                    AppUtils.sessionExpired(AddHealthRecordActivity.this);
                                } else
                                    AppUtils.showToast(getApplication(), response.body().getMessage());
                            }
                        }
                        AppUtils.dismissDialog();


                    }

                    @Override
                    public void onFailure(Call<ProfileUploadResModel> call, Throwable t) {
                        AppUtils.showToast(getApplicationContext(), getString(R.string.server_down));
                        Log.d(TAG, "Error " + t.getMessage());
                        AppUtils.dismissDialog();
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else {
            AppUtils.showToast(this,"It looks like your internet connectioin off. Please turn it on and try again.");
        }

    }



    private class SpinnerAdapter extends BaseAdapter {

        private LayoutInflater mInflater;

        public SpinnerAdapter(AddHealthRecordActivity con) {
            mInflater = LayoutInflater.from(con);
        }

        @Override
        public int getCount() {
            return ARR_HR_DOC_TYPES.size();
        }

        @Override
        public HRDocTypeObjtModel getItem(int position) {
            return ARR_HR_DOC_TYPES.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            final ListContent holder;
            View v = convertView;
            if (v == null) {
                v = mInflater.inflate(R.layout.list_item_hr_doc_types, null);
                holder = new ListContent();

                holder.name = v.findViewById(R.id.tvSpinner);

                v.setTag(holder);
            } else {
                holder = (ListContent) v.getTag();
            }

            holder.name.setText(ARR_HR_DOC_TYPES.get(position).getHealthdocdesc());

            return v;
        }

    }

    static class ListContent {
        TextView name;
    }

    private void setSpinnerData() {
        spnrDocTypes.setAdapter(new SpinnerAdapter(AddHealthRecordActivity.this));
        spnrDocTypes.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                spnrDocTypes.setSelection(position);
                //String strDocType = (String) spnrDocTypes.getSelectedItem();

                HEALTH_DOC_ID = ARR_HR_DOC_TYPES.get(position).getId();

                Logger.info(TAG, "--strDocType-->" + ARR_HR_DOC_TYPES.get(position).getHealthdocdesc());

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


    /**
     * add new child record
     */
    private void addNewHRServiceCall() {

        AddHRReqModel model = new AddHRReqModel();
        model.setUserid(SharedPrefsUtils.getUserID(this));
        model.setHealthdoctypeid(HEALTH_DOC_ID);
        model.setDoc_title(etvDocTitle.getText().toString());
        model.setDoc_content(etvDocDesc.getText().toString());
        model.setDate(DATE);
        model.setDoctorname(etvDoctorName.getText().toString());
        model.setDocurl(DOC_URL);
        model.setUsername(SharedPrefsUtils.getUsername(this));

        model.setType(TYPE);

        if (TYPE.equalsIgnoreCase(AppConstants.STR_CHILD)) {
            model.setChildid(CHILD_ID);
        }

        if (AppUtils.isNetworkConnected(this)) {
            AppUtils.showDialog(this);

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            Call<CommonResModel> call = apiService.AddNewHealthRecord(SharedPrefsUtils.getToken(getApplicationContext()),model);

            call.enqueue(new retrofit2.Callback<CommonResModel>() {
                @Override
                public void onResponse(Call<CommonResModel> call, retrofit2.Response<CommonResModel> response) {

                    AppUtils.dismissDialog();

                    if (response.code() == 200 && response.body().isSuccess()) {
                        AppUtils.showToast(AddHealthRecordActivity.this, response.body().getMessage());
                        mGoogleHelper.SendEventGA(AddHealthRecordActivity.this, GAConstants.GRAPH_SCREEN, GAConstants.ADD_HEALTH_RECORD, "");
                        finish();
                    }
                    else {
                        if (response.body().getMessage()!=null) {
                            if (response.body().getMessage().equalsIgnoreCase("Unauthorized")) {
                                AppUtils.sessionExpired(AddHealthRecordActivity.this);
                            } else
                                AppUtils.showToast(getApplicationContext(), response.body().getMessage());
                        }
                    }
                }

                @Override
                public void onFailure(Call<CommonResModel> call, Throwable t) {
                    AppUtils.dismissDialog();
                }
            });
        }
        else {
            AppUtils.showToast(this,"It looks like your internet connectioin off. Please turn it on and try again.");
        }
    }


}
