package com.savika.trackHealthRecords;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.savika.Api.ApiClient;
import com.savika.Api.ApiInterface;
import com.savika.Api.CommonResModel;
import com.savika.R;
import com.savika.constants.AppConstants;
import com.savika.dialogUtils.AlertDialogUtility;
import com.savika.logger.Logger;
import com.savika.utils.AppUtils;
import com.savika.utils.ImageUtil;
import com.savika.utils.SharedPrefsUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;

/*
 * Created by Naresh Ravva on 21/10/17.
 */

public class DetailHealthRecordActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.tvTitle)
    TextView tvTitle;

    @BindView(R.id.tvDoctorName)
    TextView tvDoctorName;

    @BindView(R.id.tvReportName)
    TextView tvReportName;

    @BindView(R.id.tvDate)
    TextView tvDate;

    @BindView(R.id.tvDesc)
    TextView tvDesc;

    @BindView(R.id.tvHeaderTitle)
    TextView tvHeaderTitle;

    @BindView(R.id.tvExtn)
    TextView webView;

    @BindView(R.id.ivMore)
    ImageView ivMore;

    @BindView(R.id.fullscreen)
    ImageView fullscreen;

    @BindView(R.id.healt_record_image)
    ImageView healt_record_image;

    @BindView(R.id.llBack)
    LinearLayout llBack;

    @BindView(R.id.ivShare)
    ImageView ivShare;

    AddHRReqModel health_record_model;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_health_record);

        ButterKnife.bind(this);
        isStoragePermissionGranted();

        llBack.setOnClickListener(this);
        ivMore.setOnClickListener(this);
        ivShare.setOnClickListener(this);
        if (getIntent().getSerializableExtra(AppConstants.EXTRA_HEALTH_RECORD) != null) {
            health_record_model = (AddHRReqModel) getIntent().getSerializableExtra(AppConstants.EXTRA_HEALTH_RECORD);
        } else {
            AppUtils.showToast(this, "Unable to show detail health records");
            finish();
        }
        if (health_record_model.getDocurl()!=null && health_record_model.getDocurl().length()>10)
            fullscreen.setVisibility(View.VISIBLE);
        else fullscreen.setVisibility(View.INVISIBLE);
        fullscreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (health_record_model.getDocurl()!=null && health_record_model.getDocurl().length()>10)
                    openWebView(health_record_model);
            }
        });
        displayData(health_record_model);
    }

    public boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v("", "Permission is granted");
                return true;
            } else {
                Log.v("", "Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.v("", "Permission is granted");
            return true;
        }
    }

    /*
     * @param health_record_model
     */
    private void displayData(final AddHRReqModel health_record_model) {
        tvTitle.setText(health_record_model.getDoc_title());
        tvDoctorName.setText(health_record_model.getDoctorname());
        tvReportName.setText(health_record_model.getHealthdocdesc());
        tvDate.setText(AppUtils.getDisplayDate(health_record_model.getDate()));
        tvDesc.setText(health_record_model.getDoc_content());

        tvHeaderTitle.setText(health_record_model.getDoc_title());

        if (health_record_model.getDocurl() != null && health_record_model.getDocurl().length()>10) {
            healt_record_image.setVisibility(View.VISIBLE);
            webView.setVisibility(View.GONE);
            String fileName = health_record_model.getDocurl().substring(health_record_model.getDocurl().lastIndexOf('/') + 1);
            String ext = AppUtils.getMimeType(fileName).toUpperCase();
            //            webView.setText(fileName.split("\\.")[1].toUpperCase());
            if (ext.equalsIgnoreCase("pdf"))
                healt_record_image.setImageResource(R.drawable.pdf);
            else ImageUtil.LoadPicaso(this,healt_record_image,health_record_model.getDocurl());
        }
        else {
            healt_record_image.setVisibility(View.GONE);
            webView.setVisibility(View.VISIBLE);
        }

    }

    private void openWebView(AddHRReqModel model) {
        if (model.getDocurl() != null && model.getDocurl().length() > 1) {
            Intent webView = new Intent(DetailHealthRecordActivity.this, com.savika.WebView.class);
            webView.putExtra(AppConstants.WEB_URL, model.getDocurl());
            webView.putExtra(AppConstants.WEB_VIEW_TITLE, model.getDoc_title());
            startActivity(webView);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.llBack:
                finish();
                break;

            case R.id.ivShare:
                if (isStoragePermissionGranted())
                    shareContent(view);
                else AppUtils.showToast(DetailHealthRecordActivity.this,"Please grant the permission");
                break;

            case R.id.ivMore:
                final PopupWindow popup = new PopupWindow(this);
                View layout = getLayoutInflater().inflate(R.layout.popup_update_child, null);
                popup.setContentView(layout);
                // Set content width and height
                popup.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
                popup.setWidth(WindowManager.LayoutParams.WRAP_CONTENT);
                // Closes the popup window when touch outside of it - when looses focus
                popup.setOutsideTouchable(true);
                popup.setFocusable(true);
                // Show anchored to button
                popup.setBackgroundDrawable(new BitmapDrawable());
                popup.showAsDropDown(view);
                View container;
                if (android.os.Build.VERSION.SDK_INT > 22) {
                    container = (View) popup.getContentView().getParent().getParent();
                }else{
                    container = (View) popup.getContentView().getParent();
                }
                WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
                WindowManager.LayoutParams p = (WindowManager.LayoutParams) container.getLayoutParams();
// add flag
                p.flags |= WindowManager.LayoutParams.FLAG_DIM_BEHIND;
                p.dimAmount = 0.3f;
                wm.updateViewLayout(container, p);


                LinearLayout llEditChild = layout.findViewById(R.id.llEditChild);
                TextView llEditText = layout.findViewById(R.id.edit_record);
                llEditText.setText(R.string.edit_record);
                TextView llDeleteText = layout.findViewById(R.id.delete_record);
                llDeleteText.setText(R.string.delete_record);
                LinearLayout llDeleteChild = layout.findViewById(R.id.llDeleteChild);

                llEditChild.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        popup.dismiss();

                        moveToEditScreen(health_record_model.getRecordid());

                    }
                });

                llDeleteChild.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        popup.dismiss();

                        runOnUiThread(new AlertDialogUtility(DetailHealthRecordActivity.this, getLayoutInflater(), getResources().getString(R.string.app_name), getResources().getString(R.string.are_you_sure_you_want_to_delete_), "Yes", "No",
                                new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {

                                        Logger.info("hh", "--health record id-->" + health_record_model.getRecordid());

                                        deleteChild(health_record_model.getRecordid());

                                    }
                                }, new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                            }
                        }, true));
                    }
                });
                break;

        }
    }

    private void shareContent(View view) {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        View rootview = view.getRootView();
        rootview.setDrawingCacheEnabled(true);
        Bitmap bitmap = rootview.getDrawingCache();
        String bitmapPath = MediaStore.Images.Media.insertImage(getContentResolver(), bitmap,"title", null);
        Uri bitmapUri = Uri.parse(bitmapPath);
        StringBuilder sb = new StringBuilder();
        sb.append(health_record_model.getDoc_title());
        sb.append("\n");
        sb.append("\n");
        if (health_record_model.getDocurl()!=null && health_record_model.getDocurl().length()>10)
            sb.append("Doc_URl:"+health_record_model.getDocurl().replace(" ","%20"));
        sendIntent.setType("image/png");
        sendIntent.putExtra(Intent.EXTRA_STREAM,bitmapUri);
        sendIntent.putExtra(Intent.EXTRA_TEXT, sb.toString());
        startActivity(sendIntent);
    }

    private void moveToEditScreen(String health_record_id) {
                Intent addeditRecord = new Intent(this, AddHealthRecordActivity.class);
               // addeditRecord.putExtra(AppConstants.EXTRA_CHILD_DATA, ARR_CHILD_DATA);
                addeditRecord.putExtra(AppConstants.EXTRA_HEALTH_RECORD, health_record_model);
                addeditRecord.putExtra(AppConstants.EXTRA_UPDATE_HEALTH_RECORD, true);
         //       addeditRecord.putExtra(AppConstants.EXTRA_SELECTED_CHILD, SELECTED_CHILD);
                startActivityForResult(addeditRecord,12);

    }

    /*
     * @param recordID
     */
    private void deleteChild(String recordID) {

        if (AppUtils.isNetworkConnected(this)) {
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            //String child_id = health_record_model.getChildid();

//        if (health_record_model.getType().equalsIgnoreCase(AppConstants.STR_MOTHER)) {
//            child_id = null;
//        }

            Call<CommonResModel> call = apiService.deleteHealthRecord(SharedPrefsUtils.getToken(getApplicationContext()),SharedPrefsUtils.getUserID(DetailHealthRecordActivity.this), health_record_model.getId(), null);

            call.enqueue(new retrofit2.Callback<CommonResModel>() {
                @Override
                public void onResponse(Call<CommonResModel> call, retrofit2.Response<CommonResModel> response) {

                    AppUtils.dismissDialog();

                    if (response.code() == 200 && response.body().isSuccess()) {
                        AppUtils.showToast(DetailHealthRecordActivity.this, response.body().getMessage());
                        finish();
                    }
                    else{
                        if (response.body().getMessage()!=null) {
                            if (response.body().getMessage().equalsIgnoreCase("Unauthorized")) {
                                AppUtils.sessionExpired(DetailHealthRecordActivity.this);
                            } else
                                AppUtils.showToast(getApplicationContext(), response.body().getMessage());
                        }
                    }

                }

                @Override
                public void onFailure(Call<CommonResModel> call, Throwable t) {
                    AppUtils.dismissDialog();
                }
            });
        }
        else {
            AppUtils.showToast(this,"It looks like your internet connectioin off. Please turn it on and try again.");
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode){
            case 12:
                if (resultCode == 2){
                    finish();
                }
        }
    }
}
