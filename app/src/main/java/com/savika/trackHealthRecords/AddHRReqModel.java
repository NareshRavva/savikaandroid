package com.savika.trackHealthRecords;

import java.io.Serializable;

/*
 * Created by Naresh Ravva on 17/10/17.
 */

public class AddHRReqModel implements Serializable {

    private String userid, healthdoctypeid, doc_title, doc_content, date, doctorname, docurl, username, type,
            childid, healthdocdesc, recordid, id;

    public String getRecordid() {
        return recordid;
    }

    public void setRecordid(String recordid) {
        this.recordid = recordid;
    }

    public String getHealthdocdesc() {
        return healthdocdesc;
    }

    public void setHealthdocdesc(String healthdocdesc) {
        this.healthdocdesc = healthdocdesc;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getChildid() {
        return childid;
    }

    public void setChildid(String childid) {
        this.childid = childid;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getHealthdoctypeid() {
        return healthdoctypeid;
    }

    public void setHealthdoctypeid(String healthdoctypeid) {
        this.healthdoctypeid = healthdoctypeid;
    }

    public String getDoc_title() {
        return doc_title;
    }

    public void setDoc_title(String doc_title) {
        this.doc_title = doc_title;
    }

    public String getDoc_content() {
        return doc_content;
    }

    public void setDoc_content(String doc_content) {
        this.doc_content = doc_content;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDoctorname() {
        return doctorname;
    }

    public void setDoctorname(String doctorname) {
        this.doctorname = doctorname;
    }

    public String getDocurl() {
        return docurl;
    }

    public void setDocurl(String docurl) {
        this.docurl = docurl;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
