package com.savika.trackHealthRecords;

import com.google.gson.annotations.SerializedName;
import com.savika.tracking.tips.TipsCardModel;

import java.util.ArrayList;

/*
 * Created by Naresh Ravva on 16/10/17.
 */

public class HRDocTypeResModel {

    private boolean success;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    private String message;

    @SerializedName("healthdoctype")
    ArrayList<HRDocTypeObjtModel> arrHealthDocTypes = new ArrayList<>();


    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public ArrayList<HRDocTypeObjtModel> getArrHealthDocTypes() {
        return arrHealthDocTypes;
    }

    public void setArrHealthDocTypes(ArrayList<HRDocTypeObjtModel> arrHealthDocTypes) {
        this.arrHealthDocTypes = arrHealthDocTypes;
    }
}
