package com.savika.trackHealthRecords;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.savika.Api.ApiClient;
import com.savika.Api.ApiInterface;
import com.savika.Api.CommonResModel;
import com.savika.R;
import com.savika.amAMother.ChildModel;
import com.savika.constants.AppConstants;
import com.savika.dialogUtils.AlertDialogUtility;
import com.savika.logger.Logger;
import com.savika.utils.AppUtils;
import com.savika.utils.SharedPrefsUtils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;

/*
 * Created by Naresh Ravva on 15/10/17.
 */

public class HealthRecordsActivity extends AppCompatActivity implements HealthRecordsAdapter.ItemClickListener, View.OnClickListener, TextWatcher {

    @BindView(R.id.tvHeaderTitle)
    TextView tvHeaderTitle;

    @BindView(R.id.llBack)
    LinearLayout llBack;

    @BindView(R.id.rvHealthRecords)
    RecyclerView rvHealthRecords;

    @BindView(R.id.ivAddHealthRecrds)
    ImageView ivAddHealthRecrds;

    @BindView(R.id.tvMother)
    TextView tvMother;

    @BindView(R.id.tvChild1)
    TextView tvChild1;

    @BindView(R.id.tvChild2)
    TextView tvChild2;

    @BindView(R.id.cvLables)
    CardView cvMotherChildLables;

    @BindView(R.id.tvNoRecords)
    TextView tvNoRecords;

    @BindView(R.id.tvHealthSearch)
    EditText tvHealthSearch;

    @BindView(R.id.tvsearch)
    ImageView tvsearch;

    @BindView(R.id.sortby)
    TextView sortby;

    @BindView(R.id.llSort)
    LinearLayout llSort;

    private HealthRecordsAdapter adapter;

    private String TYPE;
    private String CHILD_ID = null;

    LayoutInflater inflater;

    private String TAG = this.getClass().getSimpleName();

    ArrayList<ChildModel> ARR_CHILD_DATA = new ArrayList<>();

    ArrayList<AddHRReqModel> ARR_HEALTH_RECORDS = new ArrayList<>();

    private int SELECTED_CHILD;

    public boolean isSort = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_track_health_records);

        ButterKnife.bind(this);

        inflater = this.getLayoutInflater();

        TYPE = AppUtils.getMotherOrPregnantType(this);

        tvHeaderTitle.setText(R.string.track_health_records);
        ivAddHealthRecrds.setOnClickListener(this);
        tvMother.setOnClickListener(this);
        llBack.setOnClickListener(this);
        tvsearch.setOnClickListener(this);
        tvHealthSearch.addTextChangedListener(this);
        llSort.setOnClickListener(this);

        if (SharedPrefsUtils.getMotherORPrgntType(this).equalsIgnoreCase("1")) {
            cvMotherChildLables.setVisibility(View.GONE);
        } else {
            if (getIntent().getSerializableExtra(AppConstants.EXTRA_CHILD_DATA) != null)
                ARR_CHILD_DATA = (ArrayList<ChildModel>) getIntent().getSerializableExtra(AppConstants.EXTRA_CHILD_DATA);
        }

        updateCardsActiveInActive();


    }

    @Override
    protected void onResume() {
        super.onResume();

        getHealthRecords();
    }

    /*
     *
     */
    private void updateCardsActiveInActive() {

        int childCount = SharedPrefsUtils.getChildCount(this);

        Logger.info(TAG, "childCount-->" + childCount);

        if (childCount == 0) {
            tvChild1.setAlpha(0.3f);
            tvChild2.setAlpha(0.3f);
            tvChild1.setOnClickListener(null);
            tvChild2.setOnClickListener(null);

        } else if (childCount == 1) {
            tvChild2.setAlpha(0.3f);
            tvChild1.setOnClickListener(this);
            tvChild2.setOnClickListener(null);

        } else if (childCount == 2) {
            tvChild1.setOnClickListener(this);
            tvChild2.setOnClickListener(this);
        }
    }


    /*
     * @param
     * @param arr_health_records
     */
    private void setDataToAdapter(ArrayList<AddHRReqModel> arr_health_records) {

        if (arr_health_records != null && arr_health_records.size() > 0) {
            rvHealthRecords.setLayoutManager(new LinearLayoutManager(this));

            adapter = new HealthRecordsAdapter(HealthRecordsActivity.this, arr_health_records);
            adapter.setClickListener(this);
            adapter.sortby("do");
            rvHealthRecords.setAdapter(adapter);

            tvNoRecords.setVisibility(View.GONE);
            rvHealthRecords.setVisibility(View.VISIBLE);
        } else {
            tvNoRecords.setVisibility(View.VISIBLE);
            rvHealthRecords.setVisibility(View.GONE);
        }
    }

    @Override
    public void onItemClick(View view, final AddHRReqModel model) {
        int id = view.getId();
        switch (id){
            case R.id.btnSendMail:
                sendMailRecord(model);
                break;
            case R.id.ivMore:
                showPopup(view,model.getId());
                break;
        }

    }

    private void sendMailRecord(AddHRReqModel model) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("message/rfc822");
        intent.putExtra(Intent.EXTRA_SUBJECT, "Details of Health Record ");
        intent.putExtra(Intent.EXTRA_TEXT, "\n Doctor Name :"+model.getDoctorname()+"\n Report Name:"+model.getHealthdocdesc()
        +"\n Description :"+model.getDoc_content()+"\n Date : "+model.getDate()+"\n Doc_Url : "+model.getDocurl().replace(" ","%20"));
        intent.setPackage("com.google.android.gm");
        if (intent.resolveActivity(getPackageManager()) != null)
            startActivity(intent);
    }

    private void showPopup(View view, final String health_record_id) {
        Logger.info(TAG, "Clicked Position--->" + health_record_id);


        final PopupWindow popup = new PopupWindow(this);
        View layout = getLayoutInflater().inflate(R.layout.popup_update_child, null);
        LinearLayout ll = layout.findViewById(R.id.card_view);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams((int) getResources().getDimension(R.dimen._135sdp), ViewGroup.LayoutParams.WRAP_CONTENT);
        params.setMargins(0, 0, (int) getResources().getDimension(R.dimen._11sdp),0 );
        ll.setLayoutParams(params);
        popup.setContentView(layout);

        // Set content width and height
        popup.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        popup.setWidth(WindowManager.LayoutParams.WRAP_CONTENT);
        // Closes the popup window when touch outside of it - when looses focus
        popup.setOutsideTouchable(true);
        popup.setFocusable(true);
        // Show anchored to button
        popup.setBackgroundDrawable(new BitmapDrawable());
        popup.showAsDropDown(view);
        View container ;
        if (android.os.Build.VERSION.SDK_INT > 22) {
            container = (View) popup.getContentView().getParent().getParent();
        }else{
            container = (View) popup.getContentView().getParent();
        }
        WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        WindowManager.LayoutParams p = (WindowManager.LayoutParams) container.getLayoutParams();
// add flag
        p.flags |= WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        p.dimAmount = 0.3f;
        wm.updateViewLayout(container, p);

        LinearLayout llEditChild = layout.findViewById(R.id.llEditChild);
        TextView llEditText = layout.findViewById(R.id.edit_record);
        llEditText.setText(R.string.edit_record);
        TextView llDeleteText = layout.findViewById(R.id.delete_record);
        llDeleteText.setText(R.string.delete_record);
        LinearLayout llDeleteChild = layout.findViewById(R.id.llDeleteChild);

        llEditChild.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                popup.dismiss();

                moveToEditScreen(health_record_id);

            }
        });

        llDeleteChild.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                popup.dismiss();

                runOnUiThread(new AlertDialogUtility(HealthRecordsActivity.this, inflater, getResources().getString(R.string.app_name), getResources().getString(R.string.are_you_sure_you_want_to_delete_), "Yes", "No",
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                Logger.info(TAG, "--health record id-->" + health_record_id);

                                deleteChild(health_record_id);

                            }
                        }, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                    }
                }, true));
            }
        });
    }

    private void moveToEditScreen(String health_record_id) {

        for (AddHRReqModel model : ARR_HEALTH_RECORDS) {

            if (model.getId().equalsIgnoreCase(health_record_id)) {
                Intent addeditRecord = new Intent(this, AddHealthRecordActivity.class);
                addeditRecord.putExtra(AppConstants.EXTRA_CHILD_DATA, ARR_CHILD_DATA);
                addeditRecord.putExtra(AppConstants.EXTRA_HEALTH_RECORD, model);
                addeditRecord.putExtra(AppConstants.EXTRA_UPDATE_HEALTH_RECORD, true);
                addeditRecord.putExtra(AppConstants.EXTRA_SELECTED_CHILD, SELECTED_CHILD);
                startActivity(addeditRecord);

            }
        }
    }

    /*
     * @param recordID
     */
    private void deleteChild(String recordID) {

        if (AppUtils.isNetworkConnected(this)) {
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            String child_id = CHILD_ID;

            if (TYPE.equalsIgnoreCase(AppConstants.STR_MOTHER)) {
                child_id = null;
            }

            Call<CommonResModel> call = apiService.deleteHealthRecord(SharedPrefsUtils.getToken(getApplicationContext()),SharedPrefsUtils.getUserID(HealthRecordsActivity.this), recordID, child_id);

            call.enqueue(new retrofit2.Callback<CommonResModel>() {
                @Override
                public void onResponse(Call<CommonResModel> call, retrofit2.Response<CommonResModel> response) {

                    AppUtils.dismissDialog();

                    if (response.code() == 200 && response.body().isSuccess()) {
                        AppUtils.showToast(HealthRecordsActivity.this, response.body().getMessage());

                        getHealthRecords();

                    }
                    else{
                        if (response.body().getMessage().equalsIgnoreCase("Unauthorized")){
                            AppUtils.sessionExpired(HealthRecordsActivity.this);
                        }
                        else AppUtils.showToast(getApplicationContext(),response.body().getMessage());
                    }

                }

                @Override
                public void onFailure(Call<CommonResModel> call, Throwable t) {
                    AppUtils.dismissDialog();
                }
            });
        }
        else {
            AppUtils.showToast(this,"It looks like your internet connectioin off. Please turn it on and try again.");
        }
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.llSort:
                showPopupDate(view);
                break;

            case R.id.tvsearch:
                if (adapter!=null)
                    adapter.getFilter().filter(tvHealthSearch.getText().toString());
                break;

            case R.id.llBack:
                finish();
                break;

            case R.id.ivAddHealthRecrds:
                Intent addHealthRecrds = new Intent(HealthRecordsActivity.this, AddHealthRecordActivity.class);
                addHealthRecrds.putExtra(AppConstants.EXTRA_CHILD_DATA, ARR_CHILD_DATA);
                startActivity(addHealthRecrds);
                break;

            case R.id.tvMother:
                updateMotherCards(tvMother);
                TYPE = AppConstants.STR_MOTHER;

                getHealthRecords();

                SELECTED_CHILD = 0;

                break;

            case R.id.tvChild1:
                if (ARR_CHILD_DATA.size() > 0 && ARR_CHILD_DATA.get(0) != null) {
                    CHILD_ID = ARR_CHILD_DATA.get(0).getChildid();

                    updateMotherCards(tvChild1);
                    TYPE = AppConstants.STR_CHILD;

                    SELECTED_CHILD = 1;

                    getHealthRecords();
                }

                break;

            case R.id.tvChild2:
                if (ARR_CHILD_DATA.size() > 0 && ARR_CHILD_DATA.get(1) != null) {
                    CHILD_ID = ARR_CHILD_DATA.get(1).getChildid();

                    updateMotherCards(tvChild2);
                    TYPE = AppConstants.STR_CHILD;

                    SELECTED_CHILD = 2;

                    getHealthRecords();
                }

                break;
        }
    }

    private void showPopupDate(View view) {

        final PopupWindow popup = new PopupWindow(this);
        View layout = getLayoutInflater().inflate(R.layout.popup_sortdate, null);
        LinearLayout ll = layout.findViewById(R.id.card_view);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams((int) getResources().getDimension(R.dimen._135sdp), ViewGroup.LayoutParams.WRAP_CONTENT);
        params.setMargins(0, 0, (int) getResources().getDimension(R.dimen._5sdp),0 );
        ll.setLayoutParams(params);
        popup.setContentView(layout);

        // Set content width and height
        popup.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        popup.setWidth(WindowManager.LayoutParams.WRAP_CONTENT);
        // Closes the popup window when touch outside of it - when looses focus
        popup.setOutsideTouchable(true);
        popup.setFocusable(true);
        // Show anchored to button
        popup.setBackgroundDrawable(new BitmapDrawable());
        popup.showAsDropDown(view);
        View container ;
        if (android.os.Build.VERSION.SDK_INT > 22) {
            container = (View) popup.getContentView().getParent().getParent();
        }else{
            container = (View) popup.getContentView().getParent();
        }
        WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        WindowManager.LayoutParams p = (WindowManager.LayoutParams) container.getLayoutParams();
// add flag
        p.flags |= WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        p.dimAmount = 0.3f;
        wm.updateViewLayout(container, p);
        LinearLayout llEditChild = layout.findViewById(R.id.llEditChild);
        LinearLayout llDeleteChild = layout.findViewById(R.id.llDeleteChild);

        llEditChild.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popup.dismiss();
                if (adapter!=null) {
                    isSort =true;
                    adapter.sortby("ao");
                    sortby.setText(R.string.ascen_order);
                }

            }
        });

        llDeleteChild.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popup.dismiss();
                if (adapter!=null) {
                    adapter.sortby("do");
                    sortby.setText(R.string.desend_order);
                    isSort = false;
                }
            }
        });
    }


    /*
     * @param tvView
     */
    private void updateMotherCards(TextView tvView) {
        tvMother.setBackgroundColor(getResources().getColor(R.color.white));
        tvChild1.setBackgroundColor(getResources().getColor(R.color.white));
        tvChild2.setBackgroundColor(getResources().getColor(R.color.white));

        tvMother.setTextColor(Color.BLACK);
        tvChild1.setTextColor(Color.BLACK);
        tvChild2.setTextColor(Color.BLACK);

        tvView.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        tvView.setTextColor(Color.WHITE);
    }

    /**
     * get total health records
     */
    private void getHealthRecords() {
        if (AppUtils.isNetworkConnected(this)) {
            AppUtils.showDialog(this);

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            String userID = SharedPrefsUtils.getUserID(this);

            String child_ID = null;
            if (TYPE.equalsIgnoreCase(AppConstants.STR_CHILD)) {
                child_ID = CHILD_ID;
            }

            Call<HRResponseModel> call = apiService.getHealthRecords(SharedPrefsUtils.getToken(getApplicationContext()),userID, child_ID);
            call.enqueue(new retrofit2.Callback<HRResponseModel>() {
                @Override
                public void onResponse(Call<HRResponseModel> call, retrofit2.Response<HRResponseModel> response) {

                    AppUtils.dismissDialog();

                    if (response.code() == 200 && response.body().isSuccess()) {
                        ARR_HEALTH_RECORDS = response.body().getArr_health_records();

                        setDataToAdapter(ARR_HEALTH_RECORDS);

                        //Logger.info(TAG, "--ARR_HEALTH_RECORDS-->" + ARR_HEALTH_RECORDS.size());
                    } else {
                        tvNoRecords.setVisibility(View.VISIBLE);
                        rvHealthRecords.setVisibility(View.GONE);

                        if (response.body().getMessage() != null) {
                            if (response.body().getMessage().equalsIgnoreCase("Unauthorized")){
                                AppUtils.sessionExpired(HealthRecordsActivity.this);
                            }
                            else if (!response.body().getMessage().contains("No records found")){
                                AppUtils.showToast(getApplicationContext(),response.body().getMessage());
                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call<HRResponseModel> call, Throwable t) {
                    AppUtils.dismissDialog();
                }
            });
        }
        else {
            AppUtils.showToast(this,"It looks like your internet connectioin off. Please turn it on and try again.");
        }
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {
        if (adapter!=null) {
            adapter.getFilter().filter(editable.toString());
            adapter.sortby("do");
            sortby.setText(R.string.desend_order);
            isSort =false;
        }

    }
}
