package com.savika.trackHealthRecords;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.savika.R;
import com.savika.constants.AppConstants;
import com.savika.utils.AppUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/*
 * Created by Naresh Ravva on 30/06/17.
 */

public class HealthRecordsAdapter extends RecyclerView.Adapter<HealthRecordsAdapter.ViewHolder> implements Filterable {

    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    private Context context;

    private ArrayList<AddHRReqModel> arrData = new ArrayList<>();
    private ArrayList<AddHRReqModel> arrData1 = new ArrayList<>();

    HealthRecordsAdapter(Context context, ArrayList<AddHRReqModel> arrData) {
        this.mInflater = LayoutInflater.from(context);
        this.arrData = arrData;
        this.context = context;
        this.arrData1 = arrData;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.list_item_health_record, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final AddHRReqModel model = arrData1.get(position);

        holder.tvDocTitle.setText(model.getDoc_title());
        holder.tvDate.setText(AppUtils.getDisplayDate(model.getDate()));
        holder.tvDoctorName.setText(model.getDoctorname());
        holder.tvReportName.setText(model.getHealthdocdesc());

        holder.tvDesc.setText(model.getDoc_content());

        holder.btnView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent detail_screen = new Intent(context, DetailHealthRecordActivity.class);
                detail_screen.putExtra(AppConstants.EXTRA_HEALTH_RECORD, model);
                context.startActivity(detail_screen);
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrData1.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    arrData1 = arrData;
                } else {
                    ArrayList<AddHRReqModel> filteredList = new ArrayList<>();
                    for (AddHRReqModel row : arrData) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getDoc_title() != null && row.getDoc_title().contains(charString)) {
                            filteredList.add(row);
                        }
                    }

                    arrData1 = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = arrData1;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                arrData1 = (ArrayList<AddHRReqModel>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    void sortby(String order) {
        Collections.sort(arrData1, new Comparator<AddHRReqModel>() {
            @Override
            public int compare(AddHRReqModel r1, AddHRReqModel r2) {
                return r1.getDate().compareTo(r2.getDate());
            }
        });
        if (order.equalsIgnoreCase("do")){
            Collections.reverse(arrData1);
        }
        notifyDataSetChanged();
    }




    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvDocTitle, tvDoctorName, tvDate, tvReportName, tvDesc;
        TextView btnView, btnSendMail;
        public ImageView ivMore;

        public ViewHolder(View itemView) {
            super(itemView);
            tvDocTitle = itemView.findViewById(R.id.tvDocTitle);
            tvDoctorName = itemView.findViewById(R.id.tvDoctorName);
            tvDate = itemView.findViewById(R.id.tvDate);
            tvReportName = itemView.findViewById(R.id.tvReportName);
            tvDesc = itemView.findViewById(R.id.tvDesc);
            btnView = itemView.findViewById(R.id.btnView);
            btnSendMail = itemView.findViewById(R.id.btnSendMail);
            btnSendMail.setOnClickListener(this);
            ivMore = itemView.findViewById(R.id.ivMore);
            ivMore.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) {
                mClickListener.onItemClick(view, arrData1.get(getAdapterPosition()));
                notifyDataSetChanged();
            }
        }
    }

    public AddHRReqModel getItem(int id) {
        return arrData.get(id);
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    public interface ItemClickListener {
        void onItemClick(View view, AddHRReqModel model);
    }
}