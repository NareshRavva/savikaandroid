package com.savika.trackHealthRecords;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/*
 * Created by Naresh Ravva on 17/10/17.
 */

public class HRResponseModel {

    private boolean success;

    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @SerializedName("health_records")
    private ArrayList<AddHRReqModel> arr_health_records = new ArrayList<>();

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public ArrayList<AddHRReqModel> getArr_health_records() {
        return arr_health_records;
    }

    public void setArr_health_records(ArrayList<AddHRReqModel> arr_health_records) {
        this.arr_health_records = arr_health_records;
    }
}
