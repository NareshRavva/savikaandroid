package com.savika.profile;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/*
 * Created by Naresh Ravva on 22/10/17.
 */

public class ProfileResModel {

    private boolean success;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    private String message;

    @SerializedName("child_details")
    ArrayList<ProfileUpdateReqModel> arr_profile_data = new ArrayList<>();


    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public ArrayList<ProfileUpdateReqModel> getArr_profile_data() {
        return arr_profile_data;
    }

    public void setArr_profile_data(ArrayList<ProfileUpdateReqModel> arr_profile_data) {
        this.arr_profile_data = arr_profile_data;
    }
}
