package com.savika.profile;

import java.io.Serializable;

@SuppressWarnings("serial")
public class LocationModel implements Serializable {

    private String fullName, cityName, state, country;


    public LocationModel() {

    }

    public LocationModel(String fullName, String cityName, String state, String country) {
        super();
        this.fullName = fullName;
        this.cityName = cityName;
        this.state = state;
        this.country = country;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
