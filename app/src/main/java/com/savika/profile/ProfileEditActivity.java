package com.savika.profile;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.ParcelFileDescriptor;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.gson.Gson;
import com.savika.Api.ApiClient;
import com.savika.Api.ApiInterface;
import com.savika.Api.CommonResModel;
import com.savika.R;
import com.savika.Splash;
import com.savika.amAMother.ProfileUploadResModel;
import com.savika.amAPregnant.AmAPregnant;
import com.savika.components.Button;
import com.savika.dialogUtils.AlertDialogUtility;
import com.savika.dialogUtils.DatePicketDialog;
import com.savika.firebase.FbConstants;
import com.savika.logger.Logger;
import com.savika.trackHealthRecords.AddHealthRecordActivity;
import com.savika.utils.AppUtils;
import com.savika.utils.ImageUtil;
import com.savika.utils.SharedPrefsUtils;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileDescriptor;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/*
 * Created by Naresh Ravva on 21/10/17.
 */

public class ProfileEditActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.tvHeaderTitle)
    TextView tvHeaderTitle;

    @BindView(R.id.llBack)
    LinearLayout llBack;

    @BindView(R.id.imgProfile)
    ImageView imgProfile;

    @BindView(R.id.llUpdate)
    LinearLayout llUpdate;

    @BindView(R.id.etvFirstName)
    EditText etvFirstName;

    @BindView(R.id.etvLastName)
    EditText etvLastName;

    @BindView(R.id.etvGender)
    EditText etvGender;

    @BindView(R.id.tvDate)
    TextView tvDate;

    @BindView(R.id.etvOccupation)
    EditText etvOccupation;

    @BindView(R.id.etvType)
    EditText etvType;

    @BindView(R.id.etvMobile)
    EditText etvMobile;

    @BindView(R.id.etvEmail)
    EditText etvEmail;

    @BindView(R.id.tvCity)
    TextView tvCity;

    String PROFILE_PIC_URL, DATE;
    String CITY = "", STATE = "", COUNTRY = "";

    ListView lvlist;

    private List<LocationModel> TempData = null;
    private List<LocationModel> arraData = new ArrayList<LocationModel>();

    private static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";
    private static final String TYPE_AUTOCOMPLETE = "/autocomplete";
    private static final String OUT_JSON = "/json";
    private static final String GOOGLE_API_KEY = "AIzaSyDEkVlxcMS3aQLXtpOoC0Nf-LeHUIhLzFM";

    private MyCurrenLocationAdapter adapter;

    private String TAG = this.getClass().getSimpleName();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        ButterKnife.bind(this);

        isStoragePermissionGranted();

        llBack.setOnClickListener(this);
        imgProfile.setOnClickListener(this);
        tvDate.setOnClickListener(this);
        tvCity.setOnClickListener(this);
        llUpdate.setOnClickListener(this);

        tvHeaderTitle.setText("Edit Profile");
        tvCity.setText(SharedPrefsUtils.getCity(this));
        etvOccupation.setText(SharedPrefsUtils.getOccupation(this));
        etvFirstName.setText(SharedPrefsUtils.getUsername(this));
        ImageUtil.LoadPicasoCicular(this, imgProfile, SharedPrefsUtils.getProfilePic(this));
        etvType.setText(AppUtils.getMotherOrPregnantType(this));
        etvMobile.setText(SharedPrefsUtils.getMobileNum(this));
        etvEmail.setText(SharedPrefsUtils.getEmail(this));
        tvDate.setText(SharedPrefsUtils.getDob(this));

        getProfileData();
    }

    /*
     *
     */
    private void getProfileData() {

        if (AppUtils.isNetworkConnected(this)) {
            AppUtils.showDialog(this);

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            Call<ProfileResModel> call = apiService.getProfileData(SharedPrefsUtils.getToken(getApplicationContext()),SharedPrefsUtils.getUserID(this));

            call.enqueue(new retrofit2.Callback<ProfileResModel>() {
                @Override
                public void onResponse(Call<ProfileResModel> call, retrofit2.Response<ProfileResModel> response) {

                    AppUtils.dismissDialog();

                    if (response.code() == 200 && response.body().isSuccess()) {
                        updateProfileData(response.body().getArr_profile_data());
                    }
                    else {
                        if (response.body().getMessage()!=null) {
                            if (response.body().getMessage().equalsIgnoreCase("Unauthorized")) {
                                AppUtils.sessionExpired(ProfileEditActivity.this);
                            } else
                                AppUtils.showToast(getApplicationContext(), response.body().getMessage());
                        }
                    }
                }

                @Override
                public void onFailure(Call<ProfileResModel> call, Throwable t) {
                    AppUtils.dismissDialog();
                }
            });
        }
        else {
            AppUtils.showToast(this,"It looks like your internet connectioin off. Please turn it on and try again.");
        }
    }

    /*
     * @param arr_profile_data
     */
    private void updateProfileData(ArrayList<ProfileUpdateReqModel> arr_profile_data) {

        ProfileUpdateReqModel profile_model = arr_profile_data.get(0);

        etvFirstName.setText(profile_model.getFirstname());
        etvLastName.setText(profile_model.getLastname());
        if (profile_model.getBirthdate()!=null && !profile_model.getBirthdate().equalsIgnoreCase("0000-00-00"))
            tvDate.setText(updateDateFormat(profile_model.getBirthdate()));
        else tvDate.setText("");
        etvOccupation.setText(profile_model.getOccupation());
        etvEmail.setText(profile_model.getEmail());

        Logger.info(TAG, "--profile URL-->" + profile_model.getProfileurl());

        DATE = profile_model.getBirthdate();

        if (profile_model.getCity() != null) {
            tvCity.setText(profile_model.getCity());

            CITY = profile_model.getCity();
            STATE = profile_model.getState();
            COUNTRY = profile_model.getCountry();
        }

        if (profile_model.getProfileurl() != null) {
            ImageUtil.LoadPicasoCicular(ProfileEditActivity.this, imgProfile, profile_model.getProfileurl());
            PROFILE_PIC_URL = profile_model.getProfileurl();
        }

    }

    private String updateDateFormat(String birthdate) {
        String outputText = "";
        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        inputFormat.setTimeZone(TimeZone.getTimeZone("IST"));
        DateFormat outputFormat = new SimpleDateFormat("dd MMM yyyy",Locale.getDefault());
        Date parsed = null;
        try {
            parsed = inputFormat.parse(birthdate);

            outputText = outputFormat.format(parsed);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return outputText;
    }


        /*
     * @return
     */
    public boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG, "Permission is granted");
                return true;
            } else {
                Log.v(TAG, "Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.v(TAG, "Permission is granted");
            return true;
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.llBack:
                finish();
                break;

            case R.id.imgProfile:
                if ( isStoragePermissionGranted())
                    startCropImageActivity(null);
                else AppUtils.showToast(ProfileEditActivity.this,"Please grant the permission");
                break;

            case R.id.tvDate:
                datedialog(tvDate);
                break;

            case R.id.llUpdate:
                String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
                String namePattern = "^[a-zA-Z\\s]*$";

                if (etvFirstName.getText().toString().length() < 1) {
                    AppUtils.showToast(ProfileEditActivity.this, "Enter FirstName");
                    return;
                }
                if (etvFirstName.getText().toString().length() < 3) {
                    AppUtils.showToast(ProfileEditActivity.this, "Minimun 3 Characters Required");
                    return;
                }
                if (!etvFirstName.getText().toString().matches(namePattern)){
                    AppUtils.showToast(ProfileEditActivity.this,"Enter Valid FirstName");
                    return;
                }
                if (!etvLastName.getText().toString().matches(namePattern)){
                    AppUtils.showToast(ProfileEditActivity.this,"Enter Valid LastName");
                    return;
                }
                if (!etvOccupation.getText().toString().matches(namePattern)){
                    AppUtils.showToast(ProfileEditActivity.this,"Enter Valid Occupation");
                    return;
                }
                if (etvEmail.getText().toString().length() < 1) {
                    AppUtils.showToast(ProfileEditActivity.this, "Enter Email");
                    return;
                }
                if (!etvEmail.getText().toString().matches(emailPattern)) {
                    AppUtils.showToast(ProfileEditActivity.this, "Enter Valid Email");
                    return;
                }

                updateProfileServiceCall(formateReqModel());
                break;

            case R.id.tvCity:
                selectLocationPopup();
                break;
        }
    }

    public void datedialog(final TextView etxtDob)
    {
        String defaultDate = etxtDob.getText().toString().trim();
        runOnUiThread(new DatePicketDialog(ProfileEditActivity.this, getString(R.string.select_date), getString(R.string.please_pic_the_date), false, 35, 0, defaultDate, ProfileEditActivity.this.getLayoutInflater(), new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Logger.info("TAG", "Selected prblm Date-->" + view.getTag());
                DateFormat df = new SimpleDateFormat("dd MMM yyyy");
                try {
                    Date selecteddate = df.parse((String) view.getTag());
                    Date date = new Date();
                    Date today = df.parse(df.format(date));
                    if (selecteddate.after(today) && !selecteddate.equals(date) ){
                        showAlert(ProfileEditActivity.this,"Please pick date before today",tvDate);
                        //tvDate.setText("");
                    }
                    else {
                        tvDate.setText("" + view.getTag());

                        DATE =  AppUtils.convertDateFormat((String) view.getTag());
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }

            }
        }));
    }
    private void showAlert(ProfileEditActivity ctx, String message,final TextView tvDate) {
        if (ctx != null) {

            try {
                final AlertDialog.Builder builder = AppUtils.getAlertDialog(ctx);
                View view  = getLayoutInflater().inflate(R.layout.pop_alert_date,null);
                builder.setView(view);
                final AlertDialog alert = builder.create();
                alert.show();
                TextView messagetxt = view.findViewById(R.id.tvdateerrorText);
                messagetxt.setText(message);
                Button ok = view.findViewById(R.id.datebtnPositive);
                ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alert.cancel();
                        datedialog(tvDate);
                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    /*
     * Start crop image activity for the given image.
     */
    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri).setGuidelines(CropImageView.Guidelines.ON).setMultiTouchEnabled(true).setInitialCropWindowPaddingRatio(0).setAspectRatio(1, 1)
                .start(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        // handle result of CropImageActivity
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {

            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Log.v("MainActivityPrblm", "Image Path-->" + result.getUri());

                uploadProfilePic(result.getUri());

                //ivProfile.setImageURI(result.getUri());

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Toast.makeText(this, "Cropping failed: " + result.getError(), Toast.LENGTH_LONG).show();
            }
        }
    }

    private void uploadProfilePic(Uri uri) {
        if (AppUtils.isNetworkConnected(this)) {

            AppUtils.showDialog(this);

            try {
                Bitmap imageBitMap = getBitmapFromUri(uri);
                File file = createFileinSDcard(imageBitMap);

                Log.d(TAG, "Filename " + file.getName());

                //RequestBody mFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                RequestBody mFile = RequestBody.create(MediaType.parse("image/*"), file);
                MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("image", file.getName(), mFile);

                RequestBody type = RequestBody.create(okhttp3.MultipartBody.FORM, AppUtils.getMotherOrPregnantType(this));
                RequestBody user_id = RequestBody.create(okhttp3.MultipartBody.FORM, SharedPrefsUtils.getUserID(this));
                ApiInterface uploadImage = ApiClient.getClient().create(ApiInterface.class);
                Call<ProfileUploadResModel> fileUpload = uploadImage.uploadFile(SharedPrefsUtils.getToken(getApplicationContext()),fileToUpload, type, user_id, null);

                fileUpload.enqueue(new Callback<ProfileUploadResModel>() {
                    @Override
                    public void onResponse(Call<ProfileUploadResModel> call, Response<ProfileUploadResModel> response) {
                        Logger.info(TAG, "Response " + response.code());

                        if (response.code() == 200 && response.body().getProfileurl() != null) {
                            Logger.info(TAG, "profile URL--->" + response.body().getProfileurl());
                            PROFILE_PIC_URL = response.body().getProfileurl();
                            ImageUtil.LoadPicasoCicular(ProfileEditActivity.this, imgProfile, response.body().getProfileurl());
                        }
                        else{
                            if (response.body().getMessage()!=null) {
                                if (response.body().getMessage().equalsIgnoreCase("Unauthorized")) {
                                    AppUtils.sessionExpired(ProfileEditActivity.this);
                                } else
                                    AppUtils.showToast(getApplicationContext(), response.body().getMessage());
                            }
                        }

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                AppUtils.dismissDialog();
                            }
                        }, 500);

                    }

                    @Override
                    public void onFailure(Call<ProfileUploadResModel> call, Throwable t) {
                        AppUtils.showToast(getApplicationContext(), getString(R.string.server_down));
                        Log.d(TAG, "Error " + t.getMessage());
                        AppUtils.dismissDialog();
                    }
                });

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else {
            AppUtils.showToast(this,"It looks like your internet connectioin off. Please turn it on and try again.");
        }

    }

    /*
     * @param uri
     * @return
     * @throws IOException
     */
    private Bitmap getBitmapFromUri(Uri uri) throws IOException {
        ParcelFileDescriptor parcelFileDescriptor = getContentResolver().openFileDescriptor(uri, "r");
        FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
        Bitmap image = BitmapFactory.decodeFileDescriptor(fileDescriptor);
        parcelFileDescriptor.close();
        return image;
    }

    /*
     * @param bm
     * @return
     */
    private File createFileinSDcard(Bitmap bm) {
        File file;
        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "/Savika");
        myDir.mkdirs();
        Random generator = new Random();
        int n = 10000;
        n = generator.nextInt(n);
        String fname = "Image_1" /*+ n */ + ".jpg";
        file = new File(myDir, fname);
        Log.i("file", "" + file);
        if (file.exists())
            file.delete();
        try {
            FileOutputStream out = new FileOutputStream(file);
            bm.compress(Bitmap.CompressFormat.JPEG, 100, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }


        return file;
    }

    /*
     *
     */
    private void selectLocationPopup() {
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.popup_select_city, null);
        alertDialogBuilder.setView(view);
        alertDialogBuilder.setCancelable(true);
        final AlertDialog dialog = alertDialogBuilder.create();
        dialog.show();

        EditText etxtSearch = view.findViewById(R.id.etxtSearch);
        lvlist = view.findViewById(R.id.lvlist);

        etxtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().length() > 0) {
                    new SearchCityAsync().execute(s.toString());
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        lvlist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {

                // Close dialog
                dialog.dismiss();

                if (TempData != null && TempData.size() > 0) {
                    Logger.info(TAG, "Selected City-->" + TempData.get(pos).getCityName());

                    CITY = TempData.get(pos).getCityName();
                    STATE = TempData.get(pos).getState();
                    COUNTRY = TempData.get(pos).getCountry();

                    tvCity.setText(CITY);
                }
            }
        });

    }


    /*
     * taking area name and city name or only city name from address
     */
    public class SearchCityAsync extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... params) {
            if (params != null) {
                TempData = new ArrayList<LocationModel>();
                TempData.addAll(arraData);
                if (params[0] != null && params[0].trim().length() > 0) {
                    List<LocationModel> ddd = autocomplete(params[0], ProfileEditActivity.this);
                    if (ddd != null && ddd.size() > 0)
                        TempData.addAll(ddd);

                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            if (TempData != null) {
                Logger.info(TAG, "TempData-->" + TempData.size());
            }

            setDataToAdapter(TempData);
        }
    }

    /*
     * @param arraData
     */
    private void setDataToAdapter(List<LocationModel> arraData) {
        adapter = new MyCurrenLocationAdapter(this, arraData);
        lvlist.setAdapter(adapter);
    }

    /*
     * search results for user enter location
     *
     * @param input
     * @return
     */
    public ArrayList<LocationModel> autocomplete(String input, Context ctx) {
        ArrayList<LocationModel> resultList = null;

        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();
        try {
            StringBuilder sb = new StringBuilder(PLACES_API_BASE + TYPE_AUTOCOMPLETE + OUT_JSON);
            sb.append("?key=" + GOOGLE_API_KEY);
            sb.append("&input=" + URLEncoder.encode(input, "utf8"));

            URL url = new URL(sb.toString());

            System.out.println("URL: " + url);
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());

            // Load the results into a StringBuilder
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            Log.e(TAG, "Error processing Places API URL", e);
            return resultList;
        } catch (IOException e) {
            Log.e(TAG, "Error connecting to Places API", e);
            return resultList;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        try {
            // Create a JSON object hierarchy from the results
            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");

            // Extract the Place descriptions from the results
            resultList = new ArrayList<LocationModel>(predsJsonArray.length());
            for (int i = 0; i < predsJsonArray.length(); i++) {
                System.out.println(predsJsonArray.getJSONObject(i).getString("description"));
                System.out.println("============================================================");
                String name = predsJsonArray.getJSONObject(i).getString("description");
                String placeID = predsJsonArray.getJSONObject(i).getString("place_id");

                JSONArray terms = predsJsonArray.getJSONObject(i).getJSONArray("terms");

                String country = "";
                String city = terms.getJSONObject(0).getString("value");
                String state = terms.getJSONObject(1).getString("value");

                try {
                    country = terms.getJSONObject(2).getString("value");
                } catch (JSONException e) {
                    country = "";
                    e.printStackTrace();
                }

                Logger.info(TAG, "--city-->" + city + "--state-->" + state + "--country-->" + country);

                resultList.add(new LocationModel(name, city, state, country));
            }
        } catch (JSONException e) {
            Log.e(TAG, "Cannot process JSON results", e);
        }

        return resultList;
    }

    /*
     * update child record
     */
    private void updateProfileServiceCall(ProfileUpdateReqModel model) {

        if (AppUtils.isNetworkConnected(this)) {
            AppUtils.showDialog(this);

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            Call<CommonResModel> call = apiService.updateProfile(SharedPrefsUtils.getToken(getApplicationContext()),model);

            call.enqueue(new retrofit2.Callback<CommonResModel>() {
                @Override
                public void onResponse(Call<CommonResModel> call, retrofit2.Response<CommonResModel> response) {

                    AppUtils.dismissDialog();

                    if (response.code() == 200 && response.body().isSuccess()) {
                        AppUtils.showToast(ProfileEditActivity.this, response.body().getMessage());

                        String userName = etvFirstName.getText().toString() + " " + etvLastName.getText().toString();
                        updateUserData(userName, PROFILE_PIC_URL);

                    } else {
                        if (response.body().getMessage()!=null) {
                            if (response.body().getMessage().equalsIgnoreCase("Unauthorized")) {
                                AppUtils.sessionExpired(ProfileEditActivity.this);
                            } else
                                AppUtils.showToast(getApplicationContext(), response.body().getMessage());
                        }
                    }

                    finish();
                }

                @Override
                public void onFailure(Call<CommonResModel> call, Throwable t) {
                    AppUtils.dismissDialog();
                }
            });
        }
        else {
            AppUtils.showToast(this,"It looks like your internet connectioin off. Please turn it on and try again.");
        }
    }

    /*
     * update user presence Ex: Online,Offline..
     */
    private void updateUserData(String name, String profile_pic) {
        //Update token to firebase db
        DatabaseReference dbPath = FirebaseDatabase.getInstance().getReference().
                child(FbConstants.TABLE_USERS).child(SharedPrefsUtils.getMobileNum(this));

        dbPath.child(FbConstants.USER_NAME).setValue(name);

        dbPath.child(FbConstants.USER_PROFILE_PIC).setValue(profile_pic);
    }

    private ProfileUpdateReqModel formateReqModel() {
        ProfileUpdateReqModel model = new ProfileUpdateReqModel();
        model.setFirstname(etvFirstName.getText().toString());
        model.setLastname(etvLastName.getText().toString());
        model.setBirthdate(DATE);
        model.setOccupation(etvOccupation.getText().toString());
        model.setTypeid(SharedPrefsUtils.getMotherORPrgntType(this));
        model.setMobilenumber(SharedPrefsUtils.getMobileNum(this));
        model.setEmail(etvEmail.getText().toString());
        model.setCity(CITY);
        model.setState(STATE);
        model.setCountry(COUNTRY);
        model.setProfileurl(PROFILE_PIC_URL);
        model.setUserid(SharedPrefsUtils.getUserID(this));

        SharedPrefsUtils.setProfilePic(this, PROFILE_PIC_URL);
        //SharedPrefsUtils.setUsername(this, model.getFirstname() + " " + model.getLastname());
        SharedPrefsUtils.setUsername(this, model.getFirstname());
        SharedPrefsUtils.setCity(this,CITY);
        SharedPrefsUtils.setOccupation(this,etvOccupation.getText().toString());
        SharedPrefsUtils.setDob(this,tvDate.getText().toString());

        Gson gson = new Gson();
        String json = gson.toJson(model);

        Logger.info(TAG, "--json-->" + json);

        return model;
    }
}
