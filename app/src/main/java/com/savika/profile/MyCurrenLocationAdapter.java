package com.savika.profile;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.savika.R;

import java.util.List;

/*
 * Notifications Adapter
 *
 * @author Naresh
 */
public class MyCurrenLocationAdapter extends BaseAdapter {
    private Context context;
    private List<LocationModel> data;
    private String TAG = "MyCurrenLocationAdapter";

    public MyCurrenLocationAdapter(Context context, List<LocationModel> arraData) {
        this.context = context;
        this.data = arraData;
    }

    public void refreshListView(List<LocationModel> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ViewHolder holder = null;

        if (row == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(R.layout.select_location_item, parent, false);

            holder = new ViewHolder();
            holder.txtTitle = (TextView) row.findViewById(R.id.tv_title);
            holder.tv_desc = (TextView) row.findViewById(R.id.tv_desc);

            row.setTag(holder);
        } else {
            holder = (ViewHolder) row.getTag();

        }

        final LocationModel item = data.get(position);

        String location = item.getFullName();

        if (location != null) {
            holder.tv_desc.setText(location);

//            if (location.split(",")[0] != null && location.split(",")[0].length() > 0)
//                holder.txtTitle.setText(location.split(",")[0]);
        }

        return row;

    }

    static class ViewHolder {
        private TextView txtTitle, tv_desc;
    }

    @Override
    public int getCount() {
        if (data != null) {
            return data.size();
        }

        return 0;
    }

    @Override
    public LocationModel getItem(int position) {
        if (data != null) {
            return data.get(position);
        } else {
            return null;
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

}