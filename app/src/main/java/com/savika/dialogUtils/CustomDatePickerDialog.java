package com.savika.dialogUtils;

import android.app.AlertDialog;
import android.content.Context;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.savika.R;
import com.savika.constants.AppConstants;
import com.savika.controls.CustomDialog;
import com.savika.logger.Logger;
import com.savika.utils.AppUtils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import static com.google.android.gms.internal.zzahn.runOnUiThread;


//For showing Dialog message.
public class CustomDatePickerDialog implements Runnable {

    private LayoutInflater inflater;
    private Context context; // Context for showing dialog
    private OnClickListener positiveBtnClickListener;

    private String[] months = new String[]{"Jan", "Feb", "Mar", "Apr",
            "May", "Jun", "Jul", "Aug", "Sep",
            "Oct", "Nov", "Dec"};

    int YEAR = 0;
    int MONTH = 0;

    private TextView tvFromDate, tvToDate;
    private String FROM_DATE, TO_DATE;

    private String TAG = "CustomDatePickerDialog";

    public CustomDatePickerDialog(Context context, String fromdate, String todate, LayoutInflater inflater, OnClickListener positiveBtnClickListener) {
        this.context = context;
        this.inflater = inflater;
        FROM_DATE = getDisplayDate(fromdate);
        TO_DATE = getDisplayDate(todate);
        this.positiveBtnClickListener = positiveBtnClickListener;
    }

    @Override
    public void run() {
        try {
            View view = inflater.inflate(R.layout.popup_custom_date_picker1, null);
            final CustomDialog dialog = new CustomDialog(context, view, AppConstants.DEVICE_DISPLAY_WIDTH - 60, LayoutParams.WRAP_CONTENT, true);
            LinearLayout fromll = (LinearLayout) view.findViewById(R.id.fromll1);
            LinearLayout toll = (LinearLayout) view.findViewById(R.id.toll1);
            ImageView ivClose = (ImageView) view.findViewById(R.id.ivClose);
            Button btnValue = (Button) view.findViewById(R.id.btnValue);
            tvFromDate = (TextView) view.findViewById(R.id.tvFromDate);
            tvToDate = (TextView) view.findViewById(R.id.tvToDate);
            tvFromDate.setText(FROM_DATE);
            tvToDate.setText(TO_DATE);
            fromll.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    tvFromDate.setTag("from");
                    datedialog(tvFromDate);
                }
            });
            toll.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    tvToDate.setTag("to");
                    datedialog(tvToDate);
                }
            });
            ivClose.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            btnValue.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    if (positiveBtnClickListener != null) {
                        StringBuilder builder = new StringBuilder();
                        if (FROM_DATE.isEmpty()){
                            builder.append("null");
                        }
                        else builder.append(convertDateFormat(FROM_DATE));
                        builder.append("###");
                        builder.append(convertDateFormat(TO_DATE));

                        v.setOnClickListener(positiveBtnClickListener);
                        v.setTag(builder.toString());
                        v.performClick();
                    }
                }
            });


            if (!dialog.isShowing())
                dialog.show();// this is to show the dialog.
        } catch (Exception e) {
            e.printStackTrace();

        }
    }

    public void datedialog(final TextView tvadddate) {
        String defaultdate = "";
        if (tvadddate.getTag().equals("from")){
            defaultdate = FROM_DATE;
        }
        else defaultdate = TO_DATE;
        runOnUiThread(new DatePicketDialog(context, context.getString(R.string.select_date), "Choose "+tvadddate.getTag()+" date", false, 5, 0,defaultdate, inflater, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Logger.info("TAG", "Selected prblm Date-->" + view.getTag());
                DateFormat df = new SimpleDateFormat("dd MMM yyyy");
                try {
                    Date selecteddate = df.parse( (String) view.getTag());
                    Date date = new Date();
                    Date today = df.parse(df.format(date));
                    if (selecteddate.after(today) && !selecteddate.equals(date))
                        showAlert(context, "Please pick before date", tvadddate);
                    else {
                        tvadddate.setText(""+view.getTag());
                        if (tvadddate.getTag().equals("from")){
                            FROM_DATE = (String) view.getTag();
                        }
                        else {
                            TO_DATE = (String) view.getTag();
                        }
                    }


                } catch (ParseException e) {
                    e.printStackTrace();
                }

            }
        }));
    }

    private void showAlert(Context ctx, String message, final TextView tvadddate) {
        if (ctx != null) {

            try {
                final AlertDialog.Builder builder = AppUtils.getAlertDialog(ctx);
                View view = inflater.inflate(R.layout.pop_alert_date, null);
                builder.setView(view);
                final AlertDialog alert = builder.create();
                alert.show();
                TextView heading = (TextView) view.findViewById(R.id.tvdateerrorTitle);
                if (message.equalsIgnoreCase("Please pick before date"))
                    heading.setText("Check Date");
                TextView messagetxt = (TextView) view.findViewById(R.id.tvdateerrorText);
                messagetxt.setText(message);
                com.savika.components.Button ok = (com.savika.components.Button) view.findViewById(R.id.datebtnPositive);
                ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alert.cancel();
                        datedialog(tvadddate);
                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public String convertDateFormat(String inputText1) {
        String outputText = "";
        // "Z" appears not to be supported for some reason.
        DateFormat inputFormat = new SimpleDateFormat("dd MMM yyyy",Locale.getDefault());
        inputFormat.setTimeZone(TimeZone.getTimeZone("IST"));
        DateFormat outputFormat = new SimpleDateFormat("dd-MM-yyyy",Locale.getDefault());
        Date parsed;
        try {
            parsed = inputFormat.parse(inputText1);
            outputText = outputFormat.format(parsed);
        } catch (android.net.ParseException e) {
            e.printStackTrace();
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return outputText;
    }

    private String getDisplayDate(String dateString){
        String outputText = "";
        // "Z" appears not to be supported for some reason.
        DateFormat inputFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
        inputFormat.setTimeZone(TimeZone.getTimeZone("IST"));
        DateFormat outputFormat = new SimpleDateFormat("dd MMM yyyy", Locale.getDefault());
        Date parsed;
        try {
            parsed = inputFormat.parse(dateString);

            outputText = outputFormat.format(parsed);

        } catch (android.net.ParseException e) {
            e.printStackTrace();
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return outputText;
    }
}