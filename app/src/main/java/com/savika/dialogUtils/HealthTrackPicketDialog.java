package com.savika.dialogUtils;

import android.app.AlertDialog;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.savika.GlobalConfigResObjModel;
import com.savika.R;
import com.savika.constants.AppConstants;
import com.savika.controls.CustomDialog;
import com.savika.logger.Logger;
import com.savika.tracking.TrackGraphActivity;
import com.savika.utils.AppUtils;
import com.shawnlin.numberpicker.NumberPicker;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;


//For showing Dialog message.
public class HealthTrackPicketDialog implements Runnable {

    private LayoutInflater inflater;
    private TrackGraphActivity context; // Context for showing dialog
    private OnClickListener positiveBtnClickListener;
    boolean isCancellable = true;
    private String health_title, health_id, health_unit;
    GlobalConfigResObjModel healthrecord;
    GlobalConfigResObjModel healthrecord2;
    String defValue;

    private String TAG = "DatePicketDialog";

    public HealthTrackPicketDialog(TrackGraphActivity context, String title, String unit, String value, GlobalConfigResObjModel healthrecord, LayoutInflater inflater, OnClickListener positiveBtnClickListener) {
        this.context = context;
        this.inflater = inflater;
        this.health_title = title;
        this.health_unit = unit;
        this.healthrecord = healthrecord;
        this.defValue = value;
        this.positiveBtnClickListener = positiveBtnClickListener;
    }

    public HealthTrackPicketDialog(TrackGraphActivity context, String title, String unit, String value, GlobalConfigResObjModel healthrecord, GlobalConfigResObjModel healthrecord2, LayoutInflater inflater, OnClickListener positiveBtnClickListener) {
        this.context = context;
        this.inflater = inflater;
        this.health_title = title;
        this.health_unit = unit;
        this.healthrecord = healthrecord;
        this.healthrecord2 = healthrecord2;
        this.defValue = value;
        this.positiveBtnClickListener = positiveBtnClickListener;
    }

    @Override
    public void run() {
        try {
            View view = inflater.inflate(R.layout.popup_health_record_picker, null);
            final CustomDialog dialog = new CustomDialog(context, view, AppConstants.DEVICE_DISPLAY_WIDTH, LayoutParams.WRAP_CONTENT, isCancellable);

            final NumberPicker numberPicker1, numberPicker2;
            LinearLayout bp_sugar = view.findViewById(R.id.bp_sugarlayout);
            TextView systolic = view.findViewById(R.id.systolic);
            TextView diastolic = view.findViewById(R.id.diastolic);
            numberPicker1 = view.findViewById(R.id.number_picker1);
            numberPicker2 = view.findViewById(R.id.number_picker2);
            TextView tvTitle = view.findViewById(R.id.tvTitle);
            TextView tvUnit = view.findViewById(R.id.tvUnit);
            TextView tvseperator = view.findViewById(R.id.tvSeperator);
            ImageView ivClose = view.findViewById(R.id.ivClose);
            Button btnValue = view.findViewById(R.id.btnValue);
            final TextView tvadddate  = view.findViewById(R.id.tvAddDate);
            int year = Calendar.getInstance().get(Calendar.YEAR);
            int month = Calendar.getInstance().get(Calendar.MONTH)+1;
            final int date = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
            DateFormat df = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
            DateFormat sdf = new SimpleDateFormat("dd MMM yyyy", Locale.getDefault());
            Date dt = df.parse(date+"-"+month+"-"+year);
            tvadddate.setText(sdf.format(dt));

            tvTitle.setText("Add " + health_title);
            tvUnit.setText(health_unit);

            setNumberPickrStyles(numberPicker1);
            setNumberPickrStyles(numberPicker2);

            if (health_title.equalsIgnoreCase("blood pressure") || health_title.equalsIgnoreCase("sugar")){
                if (health_title.equalsIgnoreCase("sugar")){
                    systolic.setText("Fasting");
                    diastolic.setText("Post Prandial");
                    bp_sugar.getLayoutParams().width = (int) context.getResources().getDimension(R.dimen._150sdp);
                    bp_sugar.setPadding((int) context.getResources().getDimension(R.dimen._20sdp),0,0,0);
                    systolic.setPadding(0,0, (int) context.getResources().getDimension(R.dimen._8sdp),0);
                }else {
                    bp_sugar.getLayoutParams().width = (int) context.getResources().getDimension(R.dimen._135sdp);
                    diastolic.setPadding((int) context.getResources().getDimension(R.dimen._10sdp),0,0,0);
                }
                bp_sugar.setVisibility(View.VISIBLE);
                numberPicker1.setMaxValue(Integer.parseInt(healthrecord.getMax()));
                numberPicker1.setMinValue(Integer.parseInt(healthrecord.getMin()));
                tvseperator.setText("/");
                numberPicker2.setMinValue(Integer.parseInt(healthrecord2.getMin()));
                numberPicker2.setMaxValue(Integer.parseInt(healthrecord2.getMax()));
                if (defValue.equalsIgnoreCase("")){
                    numberPicker1.setValue(Integer.parseInt(healthrecord.getMin()));
                    numberPicker2.setValue(Integer.parseInt(healthrecord2.getMin()));
                }
                else {
                    String[] ss = defValue.split("/");
                    numberPicker1.setValue(Integer.parseInt(ss[0]));
                    numberPicker2.setValue(Integer.parseInt(ss[1]));
                }
            }
            else if (health_title.equalsIgnoreCase("heart rate")){
                numberPicker1.setMaxValue(Integer.parseInt(healthrecord.getMax()));
                numberPicker1.setMinValue(Integer.parseInt(healthrecord.getMin()));
                tvseperator.setVisibility(View.GONE);
                numberPicker2.setVisibility(View.GONE);
                if (defValue.equalsIgnoreCase("")){
                    numberPicker1.setValue(Integer.parseInt(healthrecord.getMin()));
                }
                else {
                    numberPicker1.setValue(Integer.parseInt(defValue));
                }
            }
            else {
                numberPicker1.setMaxValue(Integer.parseInt(healthrecord.getMax()));
                numberPicker1.setMinValue(Integer.parseInt(healthrecord.getMin()));
                numberPicker2.setMinValue(0);
                numberPicker2.setMaxValue(99);
                if (defValue.equalsIgnoreCase("")){
                    numberPicker1.setValue(Integer.parseInt(healthrecord.getMin()));
                    numberPicker2.setValue(0);
                }
                else {
                    String[] ss = defValue.split("\\.");
                    numberPicker1.setValue(Integer.parseInt(ss[0]));
                    numberPicker2.setValue(Integer.parseInt(ss[1]));
                }
            }


            ivClose.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });

            btnValue.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    if (positiveBtnClickListener != null) {

                        StringBuilder builder = new StringBuilder();
                        builder.append(new DecimalFormat("00").format(numberPicker1.getValue()));
                        if (health_title.equalsIgnoreCase("blood pressure") || health_title.equalsIgnoreCase("sugar")){
                            builder.append("/");
                        }
                        else if (health_title.equalsIgnoreCase("weight") || health_title.equalsIgnoreCase("temperature") || health_title.equalsIgnoreCase("hemoglobin"))
                            builder.append(".");
                        if (!(health_title.equalsIgnoreCase("heart rate")))
                            builder.append(new DecimalFormat("00").format(numberPicker2.getValue()));
                        builder.append("/n");
                        builder.append(tvadddate.getText().toString().trim());

                        v.setOnClickListener(positiveBtnClickListener);
                        v.setTag(builder.toString());
                        v.performClick();
                    }
                }
            });

            tvadddate.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    datedialog(tvadddate);
                }
            });


            if (dialog != null && !dialog.isShowing())
                dialog.show();// this is to show the dialog.
        } catch (Exception e) {
            e.printStackTrace();

        }
    }

    public void datedialog(final TextView tvadddate) {
        String defaultdate = "";
        context.runOnUiThread(new DatePicketDialog(context, context.getString(R.string.select_date), "Choose a date", false, 5, 0,defaultdate, inflater, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Logger.info("TAG", "Selected prblm Date-->" + view.getTag());
                DateFormat df = new SimpleDateFormat("dd MMM yyyy", Locale.getDefault());
                try {
                    Date selecteddate = df.parse( (String) view.getTag());
                    Date date = new Date();
                    Date today = df.parse(df.format(date));
                    if (selecteddate.after(today) && !selecteddate.equals(date))
                        showAlert(context, "Please pick before date", tvadddate);
                    else {
                        tvadddate.setText(""+view.getTag());
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }

            }
        }));
    }

    private void showAlert(TrackGraphActivity ctx, String message, final TextView tvadddate) {
        if (ctx != null) {

            try {
                final AlertDialog.Builder builder = AppUtils.getAlertDialog(ctx);
                View view = inflater.inflate(R.layout.pop_alert_date, null);
                builder.setView(view);
                final AlertDialog alert = builder.create();
                alert.show();
                TextView heading = view.findViewById(R.id.tvdateerrorTitle);
                if (message.equalsIgnoreCase("Please pick before date"))
                    heading.setText("Check Date");
                TextView messagetxt = view.findViewById(R.id.tvdateerrorText);
                messagetxt.setText(message);
                com.savika.components.Button ok = view.findViewById(R.id.datebtnPositive);
                ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alert.cancel();
                        datedialog(tvadddate);
                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    private void setNumberPickrStyles(NumberPicker numberPicker) {

        // set divider color
        numberPicker.setDividerColor(ContextCompat.getColor(context, R.color.colorPrimary));

        numberPicker.setDividerThickness(1);


        // set formatter
        numberPicker.setFormatter(R.string.number_picker_formatter);

        // set selected text color
        numberPicker.setSelectedTextColor(ContextCompat.getColor(context, R.color.black));

        // set selected text size
        numberPicker.setSelectedTextSize(R.dimen.selected_text_size);

        // set text color
        numberPicker.setTextColor(ContextCompat.getColor(context, R.color.dark_grey));

        // set text size
        numberPicker.setTextSize(R.dimen.text_size);

        // set typeface
        Typeface face = Typeface.createFromAsset(context.getAssets(), "fonts/Quicksand-Medium.ttf");
        numberPicker.setTypeface(face);
    }

}