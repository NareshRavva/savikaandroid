package com.savika.dialogUtils;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.savika.R;
import com.savika.constants.AppConstants;
import com.savika.controls.CustomDialog;
import com.savika.logger.Logger;
import com.savika.utils.AppUtils;
import com.shawnlin.numberpicker.NumberPicker;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;


//For showing Dialog message.
public class CustomDatePicketDialog implements Runnable, OnClickListener {

    private LayoutInflater inflater;
    private Context context; // Context for showing dialog
    private OnClickListener positiveBtnClickListener;

    private String title, msg;
    boolean isCancellable = true;

    String[] months = new String[]{"Jan", "Feb", "Mar", "Apr",
            "May", "Jun", "Jul", "Aug", "Sep",
            "Oct", "Nov", "Dec"};

    int YEAR = 0;
    int MONTH = 0;

    private NumberPicker numberPicker1, numberPicker2, numberPicker3;
    private TextView tvTitle, tvMsg;
    private ImageView ivClose;
    private EditText etvFromDate, etvToDate;
    private Button btnValue;
    LinearLayout fromll,toll;
    private boolean isFromCursorON = true;

    String FROM_DATE, TO_DATE;

    private String TAG = "CustomDatePicketDialog";

    public CustomDatePicketDialog(Context context, String title, String msg, String fromdate, String todate, LayoutInflater inflater, OnClickListener positiveBtnClickListener) {
        this.context = context;
        this.inflater = inflater;
        this.msg = msg;
        FROM_DATE = fromdate;
        TO_DATE = todate;
        this.title = title;

        this.positiveBtnClickListener = positiveBtnClickListener;
    }

    @Override
    public void run() {
        try {
            View view = inflater.inflate(R.layout.popup_custom_date_picker, null);
            final CustomDialog dialog = new CustomDialog(context, view, AppConstants.DEVICE_DISPLAY_WIDTH - 60, LayoutParams.WRAP_CONTENT, isCancellable);
            int year = Calendar.getInstance().get(Calendar.YEAR);
            int month = Calendar.getInstance().get(Calendar.MONTH);
            int date = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
            fromll = view.findViewById(R.id.fromll);
            toll = view.findViewById(R.id.toll);
            numberPicker1 = view.findViewById(R.id.number_picker1);
            numberPicker2 = view.findViewById(R.id.number_picker2);
            numberPicker3 = view.findViewById(R.id.number_picker3);
            tvTitle = view.findViewById(R.id.tvTitle);
            tvMsg = view.findViewById(R.id.tvText);
            ivClose = view.findViewById(R.id.ivClose);
            btnValue = view.findViewById(R.id.btnValue);
            etvFromDate = view.findViewById(R.id.etvFromDate);

            etvToDate = view.findViewById(R.id.etvToDate);
            etvFromDate.setText(getDisplayDate(FROM_DATE));
            etvToDate.setText(getDisplayDate(TO_DATE));
            etvFromDate.setRawInputType(InputType.TYPE_CLASS_TEXT);
            etvFromDate.setTextIsSelectable(true);

//            etvFromDate.setOnClickListener(this);
//            etvToDate.setOnClickListener(this);

            etvFromDate.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent event) {
                    isFromCursorON = true;
                    etvFromDate.setRawInputType(InputType.TYPE_CLASS_TEXT);
                    etvFromDate.setTextIsSelectable(true);
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            Logger.info(TAG, "ACTION_DOWN etvFromDate");
                            break;
                        case MotionEvent.ACTION_UP:
                            Logger.info(TAG, "ACTION_UP etvFromDate");
                            break;
                        default:
                            break;
                    }
                    return false;
                }
            });

            etvToDate.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent event) {
                    isFromCursorON = false;
                    etvToDate.setRawInputType(InputType.TYPE_CLASS_TEXT);
                    etvToDate.setTextIsSelectable(true);
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            Logger.info(TAG, "ACTION_DOWN etvToDate");
                            break;
                        case MotionEvent.ACTION_UP:
                            Logger.info(TAG, "ACTION_UP etvToDate");
                            break;
                        default:
                            break;
                    }
                    return false;
                }
            });

            //tvTitle.setText(title);
            //tvMsg.setText(msg);

            ivClose.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            setNumberPickrStyles(numberPicker1);
            setNumberPickrStyles(numberPicker2);
            setNumberPickrStyles(numberPicker3);

            numberPicker1.setMaxValue(31);
            numberPicker1.setMinValue(1);
            numberPicker1.setValue(date);

            numberPicker3.setMaxValue(2025);
            numberPicker3.setMinValue(2016);
            numberPicker3.setValue(year);

            numberPicker2.setMinValue(0);
            numberPicker2.setMaxValue(11);
            numberPicker2.setValue(month);
            numberPicker2.setDisplayedValues(months);

            YEAR = numberPicker3.getValue();
            MONTH = numberPicker2.getValue();


            numberPicker1.setOnScrollListener(new NumberPicker.OnScrollListener() {
                @Override
                public void onScrollStateChange(NumberPicker view, int scrollState) {
                    setDateToEditext();
                }
            });

            numberPicker2.setOnScrollListener(new NumberPicker.OnScrollListener() {
                @Override
                public void onScrollStateChange(NumberPicker view, int scrollState) {
                    MONTH = numberPicker2.getValue();
                    updateDaysPicker(numberPicker1);

                    setDateToEditext();
                }
            });

            numberPicker3.setOnScrollListener(new NumberPicker.OnScrollListener() {
                @Override
                public void onScrollStateChange(NumberPicker view, int scrollState) {
                    YEAR = numberPicker3.getValue();
                    updateDaysPicker(numberPicker1);

                    setDateToEditext();
                }
            });


            btnValue.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    if (positiveBtnClickListener != null) {

                        StringBuilder builder = new StringBuilder();
                        //builder.append(new DecimalFormat("00").format(numberPicker1.getValue()));
                        //builder.append(" ");
                        if (FROM_DATE.isEmpty()){
                            builder.append("null");
                        }
                        else builder.append(FROM_DATE);
                        builder.append("###");
                        builder.append(TO_DATE);

                        v.setOnClickListener(positiveBtnClickListener);
                        v.setTag(builder.toString());
                        v.performClick();
                    }
                }
            });


            if (dialog != null && !dialog.isShowing())
                dialog.show();// this is to show the dialog.
        } catch (Exception e) {
            e.printStackTrace();

        }
    }

    /**
     *
     */
    private void setDateToEditext() {
        StringBuilder builder = new StringBuilder();
        builder.append(new DecimalFormat("00").format(numberPicker1.getValue()));
        builder.append(" ");

        builder.append(months[numberPicker2.getValue()]);
        builder.append(" ");
        builder.append(numberPicker3.getValue());

        if (isFromCursorON) {
            etvFromDate.setText(builder.toString());
            FROM_DATE = convertDateFormat(builder.toString());
        } else {
            etvToDate.setText(builder.toString());
            TO_DATE = convertDateFormat(builder.toString());
        }
    }

    private void updateDaysPicker(NumberPicker numberPicker1) {
        numberPicker1.setMaxValue(numberOfDaysInMonth(MONTH, YEAR));

        Log.d("TAG", "numberOfDaysInMonth--->" + numberOfDaysInMonth(MONTH, YEAR));
    }

    /*
     * @param month
     * @param year
     * @return
     */
    public static int numberOfDaysInMonth(int month, int year) {
        Calendar mycal = new GregorianCalendar(year, month, 1);
        int daysInMonth = mycal.getActualMaximum(Calendar.DAY_OF_MONTH); // 28

        return daysInMonth;
    }

    private void setNumberPickrStyles(NumberPicker numberPicker) {

        // set divider color
        numberPicker.setDividerColor(ContextCompat.getColor(context, R.color.colorPrimary));

        numberPicker.setDividerThickness(1);

        // set formatter
        numberPicker.setFormatter(R.string.number_picker_formatter);

        // set selected text color
        numberPicker.setSelectedTextColor(ContextCompat.getColor(context, R.color.black));

        // set selected text size
        numberPicker.setSelectedTextSize(R.dimen.selected_text_size);

        // set text color
        numberPicker.setTextColor(ContextCompat.getColor(context, R.color.dark_grey));

        // set text size
        numberPicker.setTextSize(R.dimen.text_size);

        // set typeface
        Typeface face = Typeface.createFromAsset(context.getAssets(), "fonts/Quicksand-Medium.ttf");
        numberPicker.setTypeface(face);
    }

    public String convertDateFormat(String inputText1) {
        String outputText = "";
        String inputText = inputText1;
        // "Z" appears not to be supported for some reason.
        DateFormat inputFormat = new SimpleDateFormat("dd MMM yyyy", Locale.getDefault());
        inputFormat.setTimeZone(TimeZone.getTimeZone("IST"));
        DateFormat outputFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
        Date parsed = null;
        try {
            parsed = inputFormat.parse(inputText);
            outputText = outputFormat.format(parsed);
        } catch (android.net.ParseException e) {
            e.printStackTrace();
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return outputText;
        }
    }

    public String getDisplayDate(String dateString){
        String outputText = "";
        String inputText = dateString;
        // "Z" appears not to be supported for some reason.
        DateFormat inputFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
        inputFormat.setTimeZone(TimeZone.getTimeZone("IST"));
        DateFormat outputFormat = new SimpleDateFormat("dd MMM yyyy", Locale.getDefault());
        Date parsed = null;
        try {
            parsed = inputFormat.parse(inputText);

            outputText = outputFormat.format(parsed);

        } catch (android.net.ParseException e) {
            e.printStackTrace();
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {

            return outputText;
        }
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id){
            case R.id.etvFromDate:
                isFromCursorON = true;
                etvFromDate.setRawInputType(InputType.TYPE_CLASS_TEXT);
                etvFromDate.setTextIsSelectable(true);
//                etvFromDate.setCursorVisible(true);
//                etvToDate.setCursorVisible(false);
                break;
            case R.id.etvToDate:
                isFromCursorON = false;
                etvToDate.setRawInputType(InputType.TYPE_CLASS_TEXT);
                etvToDate.setTextIsSelectable(true);
//                etvFromDate.setCursorVisible(false);
//                etvToDate.setCursorVisible(true);
                break;
        }
    }
}