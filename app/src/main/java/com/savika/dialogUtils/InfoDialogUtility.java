package com.savika.dialogUtils;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.savika.R;
import com.savika.constants.AppConstants;
import com.savika.controls.CustomDialog;
import com.savika.logger.Logger;


//For showing Dialog message.
public class InfoDialogUtility implements Runnable {

    private String strTitle;//Title of the dialog
    private LayoutInflater inflater;
    private String strMessage, strPositiveBtnText, strNegativeBtnText;// Message to be shown in dialog
    private Context context; // Context for showing dialog
    private OnClickListener positiveBtnClickListener, negativeBtnClickListener;
    private boolean isCancellable;

    private String TAG = "AlertDialogUtility";

    public InfoDialogUtility(Context context, LayoutInflater inflater, String strTitle, String strMessage, String postiveBtnTest, String negativeBtnText, OnClickListener positiveBtnClickListener, OnClickListener negativeBtnClickListener, boolean isCancellable) {
        this.context = context;
        this.inflater = inflater;

        this.strTitle = strTitle;
        this.strMessage = strMessage;

        this.strPositiveBtnText = postiveBtnTest;
        this.strNegativeBtnText = negativeBtnText;

        this.positiveBtnClickListener = positiveBtnClickListener;
        this.negativeBtnClickListener = negativeBtnClickListener;

        this.isCancellable = isCancellable;
    }

    @Override
    public void run() {

        try {
            View view = inflater.inflate(R.layout.popup_info, null);
            final CustomDialog dialog = new CustomDialog(context, view, AppConstants.DEVICE_DISPLAY_WIDTH - 60, LayoutParams.WRAP_CONTENT, isCancellable);
            TextView tvTitle = view.findViewById(R.id.tvTitle);
            TextView tvMsg = view.findViewById(R.id.tvText);
            LinearLayout llButtons = view.findViewById(R.id.llButtons);
            Button btnPositive = view.findViewById(R.id.btnPositive);
            Button btnNegative = view.findViewById(R.id.btnNegative);

            tvTitle.setText(strTitle);
            //tvMsg.setText(strMessage);

            if (strPositiveBtnText != null && !strPositiveBtnText.equalsIgnoreCase("")) {
                llButtons.setVisibility(View.VISIBLE);
                //btnPositive.setText("");
                btnPositive.setVisibility(View.VISIBLE);

                btnPositive.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        if (positiveBtnClickListener != null) {
                            v.setOnClickListener(positiveBtnClickListener);
                            v.performClick();
                        }
                    }
                });
            }

            if (strNegativeBtnText != null && !strNegativeBtnText.equalsIgnoreCase("")) {
                llButtons.setVisibility(View.VISIBLE);
                //btnNegative.setText("");
                btnNegative.setVisibility(View.VISIBLE);

                btnNegative.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        if (negativeBtnClickListener != null) {
                            v.setOnClickListener(negativeBtnClickListener);
                            v.performClick();
                        }
                    }
                });
            }

            if (dialog != null && !dialog.isShowing())
                dialog.show();// this is to show the dialog.
        } catch (Exception e) {
            e.printStackTrace();
            Logger.info(TAG, "Exception-->>");
        }
    }
}