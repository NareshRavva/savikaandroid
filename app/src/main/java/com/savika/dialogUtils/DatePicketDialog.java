package com.savika.dialogUtils;

import android.content.Context;
import android.graphics.Typeface;
import android.media.Image;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.savika.R;
import com.savika.constants.AppConstants;
import com.savika.controls.CustomDialog;
import com.savika.logger.Logger;
import com.savika.utils.AppUtils;
import com.shawnlin.numberpicker.NumberPicker;

import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;


//For showing Dialog message.
public class DatePicketDialog implements Runnable {

    private LayoutInflater inflater;
    private Context context; // Context for showing dialog
    private OnClickListener positiveBtnClickListener;

    private String title, msg;

    boolean isCancellable = true;
    boolean hideDatePicker = false;
    int min=0,max=0;
    boolean cons = false;

    String[] months = new String[]{"Jan", "Feb", "Mar", "Apr",
            "May", "Jun", "Jul", "Aug", "Sep",
            "Oct", "Nov", "Dec"};

    int YEAR = 0;
    int MONTH = 0;
    String defaultDate;

    private String TAG = "DatePicketDialog";

    public DatePicketDialog(Context context, String title, String msg, boolean hideDateSpinner, LayoutInflater inflater, OnClickListener positiveBtnClickListener) {
        this.context = context;
        this.inflater = inflater;
        this.hideDatePicker = hideDateSpinner;
        this.msg = msg;
        this.title = title;

        this.positiveBtnClickListener = positiveBtnClickListener;
    }

    public DatePicketDialog(Context context, String title, String msg, boolean hideDateSpinner, int min, int max, String defaultDate, LayoutInflater inflater, OnClickListener positiveBtnClickListener) {
        this.context = context;
        this.inflater = inflater;
        this.hideDatePicker = hideDateSpinner;
        this.msg = msg;
        this.title = title;
        this.min = min;
        this.max =max;
        cons = true;
        this.defaultDate = defaultDate;
        this.positiveBtnClickListener = positiveBtnClickListener;
    }

    @Override
    public void run() {


        try {

            int year = Calendar.getInstance().get(Calendar.YEAR);
            int month = Calendar.getInstance().get(Calendar.MONTH);
            int date = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);

            View view = inflater.inflate(R.layout.popup_date_picker, null);
            final CustomDialog dialog = new CustomDialog(context, view, AppConstants.DEVICE_DISPLAY_WIDTH - 60, LayoutParams.WRAP_CONTENT, isCancellable);

            final NumberPicker numberPicker1, numberPicker2, numberPicker3;
            final TextView tvTitle, tvMsg;
            final ImageView ivClose;

            numberPicker1 = view.findViewById(R.id.number_picker1);
            numberPicker2 = view.findViewById(R.id.number_picker2);
            numberPicker3 = view.findViewById(R.id.number_picker3);
            tvTitle = view.findViewById(R.id.tvTitle);
            tvMsg = view.findViewById(R.id.tvText);
            ivClose = view.findViewById(R.id.ivClose);
            Button btnValue = view.findViewById(R.id.btnValue);

            tvTitle.setText(title);
            tvMsg.setText(msg);

            ivClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            setNumberPickrStyles(numberPicker1);
            setNumberPickrStyles(numberPicker2);
            setNumberPickrStyles(numberPicker3);

            Logger.info("Splash", "year-->" + year + "--date-->" + date + "--month-->" + month);
            if (defaultDate!=null && !defaultDate.equalsIgnoreCase("")){
                String[] def = AppUtils.convertDateFormat(defaultDate).split("-");
                date = Integer.parseInt(def[2]);
                month = Integer.parseInt(def[1])-1;
                year = Integer.parseInt(def[0]);
            }

            numberPicker1.setMaxValue(31);
            numberPicker1.setMinValue(1);
            numberPicker1.setValue(date);

            if (cons){
                numberPicker3.setMaxValue(year+max);
                numberPicker3.setMinValue(year-min);
            }
            else {
                numberPicker3.setMaxValue(year+40);
                numberPicker3.setMinValue(year-30);
            }

            numberPicker3.setValue(year);

            numberPicker2.setMinValue(0);
            numberPicker2.setMaxValue(11);
            numberPicker2.setDisplayedValues(months);
            numberPicker2.setValue(month);

            YEAR = numberPicker3.getValue();
            MONTH = numberPicker2.getValue();

            numberPicker3.setOnScrollListener(new NumberPicker.OnScrollListener() {
                @Override
                public void onScrollStateChange(NumberPicker view, int scrollState) {
                    YEAR = numberPicker3.getValue();

                    updateDaysPicker(numberPicker1);
                }
            });

            numberPicker2.setOnScrollListener(new NumberPicker.OnScrollListener() {
                @Override
                public void onScrollStateChange(NumberPicker view, int scrollState) {
                    MONTH = numberPicker2.getValue();
                    updateDaysPicker(numberPicker1);
                }
            });

            if (hideDatePicker) {
                numberPicker1.setVisibility(View.GONE);
            }


            btnValue.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    if (positiveBtnClickListener != null) {

                        StringBuilder builder = new StringBuilder();
                        if (!hideDatePicker) {
                            builder.append(new DecimalFormat("00").format(numberPicker1.getValue()));
                            builder.append(" ");
                        }
                        builder.append(months[numberPicker2.getValue()]);
                        builder.append(" ");
                        builder.append(numberPicker3.getValue());

                        v.setOnClickListener(positiveBtnClickListener);
                        v.setTag(builder.toString());
                        v.performClick();
                    }
                }
            });


            if (dialog != null && !dialog.isShowing())
                dialog.show();// this is to show the dialog.
        } catch (Exception e) {
            e.printStackTrace();

        }
    }

    private void updateDaysPicker(NumberPicker numberPicker1) {
        numberPicker1.setMaxValue(numberOfDaysInMonth(MONTH, YEAR));

        // Log.d("TAG", "numberOfDaysInMonth--->" + numberOfDaysInMonth(MONTH, YEAR));
    }

    /**
     * @param month
     * @param year
     * @return
     */
    public static int numberOfDaysInMonth(int month, int year) {
        Calendar mycal = new GregorianCalendar(year, month, 1);
        int daysInMonth = mycal.getActualMaximum(Calendar.DAY_OF_MONTH); // 28

        return daysInMonth;
    }

    private void setNumberPickrStyles(com.shawnlin.numberpicker.NumberPicker numberPicker) {

        // set divider color
        numberPicker.setDividerColor(ContextCompat.getColor(context, R.color.colorPrimary));

        numberPicker.setDividerThickness(1);

        // set formatter
        numberPicker.setFormatter(R.string.number_picker_formatter);

        // set selected text color
        numberPicker.setSelectedTextColor(ContextCompat.getColor(context, R.color.black));

        // set selected text size
        numberPicker.setSelectedTextSize(R.dimen.selected_text_size);

        // set text color
        numberPicker.setTextColor(ContextCompat.getColor(context, R.color.dark_grey));

        // set text size
        numberPicker.setTextSize(R.dimen.text_size);

        // set typeface
        Typeface face = Typeface.createFromAsset(context.getAssets(), "fonts/Quicksand-Medium.ttf");
        numberPicker.setTypeface(face);
    }

}