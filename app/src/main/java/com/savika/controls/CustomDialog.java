package com.savika.controls;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;

/**
 * class to create the Custom Dialog
 **/
public class CustomDialog extends Dialog {
    /*
     * Constructor
     *
     * @param context
     * @param view
     */
    public CustomDialog(Context context, View view) {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(new ColorDrawable(0));
        setContentView(view);
    }

    /*
     * Constructor
     *
     * @param context
     * @param view
     * @param lpW
     * @param lpH
     */
    public CustomDialog(Context context, View view, int lpW, int lpH) {
        this(context, view, lpW, lpH, true);
    }

    /*
     * Constructor
     *
     * @param context
     * @param view
     * @param lpW
     * @param lpH
     * @param isCancellable
     */
    public CustomDialog(Context context, View view, int lpW, int lpH, boolean isCancellable) {
        super(context);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(new ColorDrawable(0));
        setContentView(view, new LayoutParams(lpW, lpH));

        this.setCancelable(isCancellable);
    }
}
