package com.savika;

/*
 * Created by nexivo on 22/11/17.
 */

public class GlobalConfigResObjModel {
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMin() {
        return min;
    }

    public void setMin(String min) {
        this.min = min;
    }

    public String getMax() {
        return max;
    }

    public void setMax(String max) {
        this.max = max;
    }

    private String id, type, version, min, max;;

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    String url;

    public String getAbnormal_min() {
        return abnormal_min;
    }

    public void setAbnormal_min(String abnormal_min) {
        this.abnormal_min = abnormal_min;
    }

    public String getAbnormal_max() {
        return abnormal_max;
    }

    public void setAbnormal_max(String abnormal_max) {
        this.abnormal_max = abnormal_max;
    }

    private String abnormal_min, abnormal_max;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    private String status;
    String createdat;

    public String getMandatory() {
        return mandatory;
    }

    public void setMandatory(String mandatory) {
        this.mandatory = mandatory;
    }

    String mandatory;

}
