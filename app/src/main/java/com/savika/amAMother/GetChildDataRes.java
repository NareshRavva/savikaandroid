package com.savika.amAMother;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/*
 * Created by Naresh Ravva on 12/10/17.
 */

public class GetChildDataRes {

    private boolean success;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    private String message;

    @SerializedName("child_details")
    ArrayList<ChildModel> arrChildData = new ArrayList<>();

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public ArrayList<ChildModel> getArrChildData() {
        return arrChildData;
    }

    public void setArrChildData(ArrayList<ChildModel> arrChildData) {
        this.arrChildData = arrChildData;
    }
}
