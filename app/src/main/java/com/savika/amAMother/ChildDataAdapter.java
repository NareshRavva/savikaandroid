package com.savika.amAMother;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.savika.R;
import com.savika.utils.ImageUtil;


import java.util.ArrayList;

/*
 * Created by Naresh Ravva on 30/06/17.
 */

public class ChildDataAdapter extends RecyclerView.Adapter<ChildDataAdapter.ViewHolder> {

    ArrayList<ChildModel> arrList = new ArrayList<>();
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    private Context context;

    public ChildDataAdapter(Context context, ArrayList<ChildModel> arrList) {
        this.mInflater = LayoutInflater.from(context);
        this.arrList = arrList;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.list_item_child, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        ChildModel model = arrList.get(position);

        holder.tvName.setText(model.getName());
        holder.tvTitle1.setText(model.getDob());
        holder.tvTitle2.setText(model.getGender());

        if (model.getProfileurl() != null && model.getProfileurl().length() > 5) {
            ImageUtil.LoadPicasoRoundCorners(context, holder.ivProfile, model.getProfileurl());
        } else {
            holder.ivProfile.setImageResource(R.drawable.add_child_default);
        }

    }

    @Override
    public int getItemCount() {
        return arrList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView tvName, tvTitle1, tvTitle2, tvDelete, tvEdit;
        public ImageView ivProfile, imgMore;
        public RelativeLayout rlPopup;

        public ViewHolder(View itemView) {
            super(itemView);
            ivProfile = itemView.findViewById(R.id.ivProfile);
            tvName = itemView.findViewById(R.id.tvName);
            tvDelete = itemView.findViewById(R.id.tvDelete);
            tvEdit = itemView.findViewById(R.id.tvEdit);
            tvTitle1 = itemView.findViewById(R.id.tvDob);
            tvTitle2 = itemView.findViewById(R.id.tvGender);
            imgMore = itemView.findViewById(R.id.imgMore);
            rlPopup = itemView.findViewById(R.id.rlPopup);
            imgMore.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }

    public ChildModel getItem(int id) {
        return arrList.get(id);
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}