package com.savika.amAMother;

import android.Manifest;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.ParcelFileDescriptor;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.savika.Api.ApiClient;
import com.savika.Api.ApiInterface;
import com.savika.Api.CommonResModel;
import com.savika.R;
import com.savika.constants.GAConstants;
import com.savika.dialogUtils.AlertDialogUtility;
import com.savika.dialogUtils.DatePicketDialog;
import com.savika.googleAnalytics.GoogleAnalyticsHelper;
import com.savika.logger.Logger;
import com.savika.authentication.SignUpActivity;
import com.savika.constants.AppConstants;
import com.savika.utils.AppUtils;
import com.savika.utils.ImageUtil;
import com.savika.utils.SharedPrefsUtils;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.io.FileDescriptor;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Random;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/*
 * Created by Naresh Ravva on 31/08/17.
 */

public class AddChildDataActivity extends AppCompatActivity implements View.OnClickListener, ChildDataAdapter.ItemClickListener {

    @BindView(R.id.llMainBack)
    LinearLayout llMainBack;

    @BindView(R.id.tvHeaderTitle)
    TextView tvHeaderTitle;

    @BindView(R.id.llBack)
    LinearLayout llBack;

    @BindView(R.id.llAddChild)
    LinearLayout llAddChild;

    @BindView(R.id.rvChildData)
    RecyclerView rvChildData;

    @BindView(R.id.tvNoData)
    TextView tvNoData;

    @BindView(R.id.llProceed)
    LinearLayout llProceed;

    ImageView imgProfile, ivCalndr;

    String PROFILE_PIC_URL = "";

    String gender;

    ChildDataAdapter adapter;

    private Calendar myCalendar;
    private DatePickerDialog.OnDateSetListener date1;

    int month = 0;
    int year1 = 0;
    int date = 0;

    private String[] spnr_data_gender = {"Boy", "Girl"};

    LayoutInflater inflater;

    private String TAG = this.getClass().getSimpleName();

    String CHILD_DATE = "";
    String CHILD_ID;

    GoogleAnalyticsHelper mGoogleHelper;

    private ArrayList<ChildModel> ARR_CHILD_DATA = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_child_list);

        inflater = this.getLayoutInflater();

        ButterKnife.bind(this);

        tvHeaderTitle.setText("Add Child");

        llAddChild.setOnClickListener(this);
        llProceed.setOnClickListener(this);
        llBack.setOnClickListener(this);

        setDataToAdapter(ARR_CHILD_DATA);

        InitGoogleAnalytics();

        mGoogleHelper.SendScreenNameGA(this, GAConstants.SIGN_UP_SCREEN);

        if (SharedPrefsUtils.getUserID(AddChildDataActivity.this) != null) {

            getChildData();

            llProceed.setVisibility(View.INVISIBLE);
            llMainBack.setVisibility(View.VISIBLE);
        } else {
            llMainBack.setVisibility(View.GONE);
        }

    }

    /**
     * get child info
     */
    private void getChildData() {

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<GetChildDataRes> call = apiService.getChildData(SharedPrefsUtils.getToken(getApplicationContext()),SharedPrefsUtils.getUserID(AddChildDataActivity.this));

        call.enqueue(new retrofit2.Callback<GetChildDataRes>() {
            @Override
            public void onResponse(Call<GetChildDataRes> call, retrofit2.Response<GetChildDataRes> response) {

                AppUtils.dismissDialog();

                if (response.code() == 200 && response.body().isSuccess() == true) {
                    ARR_CHILD_DATA = response.body().arrChildData;

                    setDataToAdapter(ARR_CHILD_DATA);

                    handleAddChildBtn(ARR_CHILD_DATA.size());
                }
                else{
                    if (response.body().getMessage()!=null) {
                        if (response.body().getMessage().equalsIgnoreCase("Unauthorized")) {
                            AppUtils.sessionExpired(AddChildDataActivity.this);
                        } else
                            AppUtils.showToast(getApplicationContext(), response.body().getMessage());
                    }
                }

            }

            @Override
            public void onFailure(Call<GetChildDataRes> call, Throwable t) {
                AppUtils.dismissDialog();
            }
        });
    }

    /*
     * stop adding child if he reach count as 2
     *
     * @param size
     */
    private void handleAddChildBtn(int size) {
        if (size >= 2) {
            llAddChild.setAlpha(0.4f);
            llAddChild.setOnClickListener(null);
        }
    }

    // Google Analytics
    private void InitGoogleAnalytics() {
        mGoogleHelper = new GoogleAnalyticsHelper();
        mGoogleHelper.init(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.llAddChild:
                addNewChildPopUp();
                break;
            case R.id.llProceed:
                moveToNxtScreen();
                break;
            case R.id.llBack:
                finish();
                break;
        }
    }

    private void moveToNxtScreen() {
        if (ARR_CHILD_DATA.size() > 0) {
            Intent intent = new Intent(getApplicationContext(), SignUpActivity.class);
            intent.putExtra(AppConstants.EXTRA_CHILD_DATA, ARR_CHILD_DATA);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        } else {
            AppUtils.showToast(this, getString(R.string.add_atleast_one_child));
        }
    }

    /*
     *
     */
    private void addNewChildPopUp() {
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.popup_add_child, null);
        alertDialogBuilder.setView(view);
        alertDialogBuilder.setCancelable(true);
        final AlertDialog dialog = alertDialogBuilder.create();
        dialog.show();

        Spinner spnrGender = view.findViewById(R.id.spnr);
        setSpinnerData(spnrGender);

        final EditText etvChildName = view.findViewById(R.id.etvChildName);
        final TextView etvDob = view.findViewById(R.id.etvDob);
        LinearLayout llDate = view.findViewById(R.id.llDate);
        imgProfile = view.findViewById(R.id.imgProfile);
        ivCalndr = view.findViewById(R.id.ivCalndr);

        handleDatePicker(etvDob, llDate);

        handleProfilePicUpload(imgProfile);

        LinearLayout llSaveChild = view.findViewById(R.id.llSaveChild);
        // if decline button is clicked, close the custom dialog
        llSaveChild.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (etvChildName.getText().toString().length() < 1) {
                    AppUtils.showToast(AddChildDataActivity.this, "Enter Child Name");
                    return;
                }
                if (etvDob.getText().toString().length() < 1) {
                    AppUtils.showToast(AddChildDataActivity.this, "Enter DOB");
                    return;
                }

                // Close dialog
                dialog.dismiss();

                ChildModel model = new ChildModel();
                model.setName(etvChildName.getText().toString());
                model.setDob(CHILD_DATE);
                model.setGender(gender);
                model.setProfileurl(PROFILE_PIC_URL);

                if (SharedPrefsUtils.getUserID(AddChildDataActivity.this) != null)
                    model.setUserid(SharedPrefsUtils.getUserID(AddChildDataActivity.this));

                if (SharedPrefsUtils.getUsername(AddChildDataActivity.this) != null)
                    model.setUsername(SharedPrefsUtils.getUsername(AddChildDataActivity.this));

                if (SharedPrefsUtils.getUserID(AddChildDataActivity.this) != null) {
                    addNewChildServiceCall(model); //Send data to server
                } else {
                    ARR_CHILD_DATA.add(model);//store local for SIGNUP Usage

                    if (ARR_CHILD_DATA.size() <= 2)
                        setDataToAdapter(ARR_CHILD_DATA);

                    handleAddChildBtn(ARR_CHILD_DATA.size());
                }

                mGoogleHelper.SendEventGA(AddChildDataActivity.this, GAConstants.SIGN_UP_SCREEN, GAConstants.ADD_CHILD, "");


                PROFILE_PIC_URL = "";

            }
        });
    }


    /*
     *
     */
    private void editChildDataPopUp(ChildModel model, final int position) {
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.popup_add_child, null);
        alertDialogBuilder.setView(view);
        alertDialogBuilder.setCancelable(true);
        final AlertDialog dialog = alertDialogBuilder.create();
        dialog.show();

        Spinner spnrGender = view.findViewById(R.id.spnr);
        setSpinnerData(spnrGender);

        final EditText etvChildName = view.findViewById(R.id.etvChildName);
        final TextView etvDob = view.findViewById(R.id.etvDob);
        LinearLayout llDate = view.findViewById(R.id.llDate);
        imgProfile = view.findViewById(R.id.imgProfile);
        ivCalndr = view.findViewById(R.id.ivCalndr);

        /*
         * SET EXISTING DATA
         */
        etvChildName.setText(model.getName());

        if (model.getGender().equalsIgnoreCase(spnr_data_gender[0])) {
            spnrGender.setSelection(0);
        } else {
            spnrGender.setSelection(1);
        }
        etvDob.setText(model.getDob());

        gender = model.getGender();
        CHILD_DATE = model.getDob();
        PROFILE_PIC_URL = model.getProfileurl();
        CHILD_ID = model.getChildid();

        final String child_ID = model.getChildid();

        handleDatePicker(etvDob, llDate);
        handleProfilePicUpload(imgProfile);

        LinearLayout llSaveChild = view.findViewById(R.id.llSaveChild);
        // if decline button is clicked, close the custom dialog
        llSaveChild.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (etvChildName.getText().toString().length() < 1) {
                    AppUtils.showToast(AddChildDataActivity.this, "Enter Child Name");
                    return;
                }
                if (etvDob.getText().toString().length() < 1) {
                    AppUtils.showToast(AddChildDataActivity.this, "Enter DOB");
                    return;
                }

                // Close dialog
                dialog.dismiss();

                ChildModel model = new ChildModel();
                model.setName(etvChildName.getText().toString());
                model.setDob(CHILD_DATE);
                model.setGender(gender);
                model.setProfileurl(PROFILE_PIC_URL);
                model.setUserid(SharedPrefsUtils.getUserID(AddChildDataActivity.this));
                model.setChildid(child_ID);


                if (SharedPrefsUtils.getUserID(AddChildDataActivity.this) != null) {
                    updateChildServiceCall(model);
                } else {
                    ARR_CHILD_DATA.remove(position);
                    ARR_CHILD_DATA.add(model);
                    setDataToAdapter(ARR_CHILD_DATA);
                }

                mGoogleHelper.SendEventGA(AddChildDataActivity.this, GAConstants.SIGN_UP_SCREEN, GAConstants.ADD_CHILD, "");


                PROFILE_PIC_URL = "";

            }
        });
    }


    /*
     * update child record
     */
    private void updateChildServiceCall(ChildModel model) {

        AppUtils.showDialog(this);

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<CommonResModel> call = apiService.updateChild(SharedPrefsUtils.getToken(getApplicationContext()),model);

        call.enqueue(new retrofit2.Callback<CommonResModel>() {
            @Override
            public void onResponse(Call<CommonResModel> call, retrofit2.Response<CommonResModel> response) {

                AppUtils.dismissDialog();

                if (response.code() == 200 && response.body().isSuccess() == true) {
                    getChildData();
                } else {
                    if (response.body().getMessage()!=null) {
                        if (response.body().getMessage().equalsIgnoreCase("Unauthorized")) {
                            AppUtils.sessionExpired(AddChildDataActivity.this);
                        } else
                            AppUtils.showToast(getApplicationContext(), response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<CommonResModel> call, Throwable t) {
                AppUtils.dismissDialog();
            }
        });
    }


    /*
     * add new child record
     */
    private void addNewChildServiceCall(ChildModel model) {

        AppUtils.showDialog(this);

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<CommonResModel> call = apiService.addChild(SharedPrefsUtils.getToken(getApplicationContext()),model);

        call.enqueue(new retrofit2.Callback<CommonResModel>() {
            @Override
            public void onResponse(Call<CommonResModel> call, retrofit2.Response<CommonResModel> response) {

                AppUtils.dismissDialog();

                if (response.code() == 200 && response.body().isSuccess() == true) {
                    getChildData();
                }
                else{
                    if (response.body().getMessage()!=null) {
                        if (response.body().getMessage().equalsIgnoreCase("Unauthorized")) {
                            AppUtils.sessionExpired(AddChildDataActivity.this);
                        } else
                            AppUtils.showToast(getApplicationContext(), response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<CommonResModel> call, Throwable t) {
                AppUtils.dismissDialog();
                AppUtils.showToast(AddChildDataActivity.this, getString(R.string.server_down));
            }
        });
    }

    /*
     * @param imgProfile
     */
    private void handleProfilePicUpload(ImageView imgProfile) {

        imgProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isStoragePermissionGranted();
                startCropImageActivity(null);
            }
        });
    }

    /*
     * Start crop image activity for the given image.
     */
    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri).setGuidelines(CropImageView.Guidelines.ON).setMultiTouchEnabled(true).setInitialCropWindowPaddingRatio(0).setAspectRatio(1, 1)
                .start(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        // handle result of CropImageActivity
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {

            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Log.v("MainActivityPrblm", "Image Path-->" + result.getUri());

                uploadProfilePic(result.getUri());

                //ivProfile.setImageURI(result.getUri());

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Toast.makeText(this, "Cropping failed: " + result.getError(), Toast.LENGTH_LONG).show();
            }
        }
    }

    private void uploadProfilePic(Uri uri) {

        AppUtils.showDialog(this);

        try {
            Bitmap imageBitMap = getBitmapFromUri(uri);
            File file = createFileinSDcard(imageBitMap);

            Log.d(TAG, "Filename " + file.getName());

            //RequestBody mFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
            RequestBody mFile = RequestBody.create(MediaType.parse("image/*"), file);
            MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("image", file.getName(), mFile);

            RequestBody type = RequestBody.create(okhttp3.MultipartBody.FORM, "child");
            RequestBody userid = RequestBody.create(okhttp3.MultipartBody.FORM, SharedPrefsUtils.getUserID(this));
            RequestBody childid = RequestBody.create(okhttp3.MultipartBody.FORM, CHILD_ID);

            ApiInterface uploadImage = ApiClient.getClient().create(ApiInterface.class);
            Call<ProfileUploadResModel> fileUpload = uploadImage.uploadFile(SharedPrefsUtils.getToken(getApplicationContext()),fileToUpload, type, userid, childid);

            fileUpload.enqueue(new Callback<ProfileUploadResModel>() {
                @Override
                public void onResponse(Call<ProfileUploadResModel> call, Response<ProfileUploadResModel> response) {
                    Logger.info(TAG, "Response " + response.code());

                    if (response.code() == 200 && response.body().getProfileurl() != null) {
                        Logger.info(TAG, "profile URL--->" + response.body().getProfileurl());
                        PROFILE_PIC_URL = response.body().getProfileurl();
                        ImageUtil.LoadPicaso(AddChildDataActivity.this, imgProfile, response.body().getProfileurl());
                    }

                    else {
                        if (response.body().getMessage()!=null) {
                            if (response.body().getMessage().equalsIgnoreCase("Unauthorized")) {
                                AppUtils.sessionExpired(AddChildDataActivity.this);
                            } else
                                AppUtils.showToast(getApplication(), response.body().getMessage());
                        }
                    }

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            AppUtils.dismissDialog();
                        }
                    }, 500);


                }

                @Override
                public void onFailure(Call<ProfileUploadResModel> call, Throwable t) {
                    AppUtils.showToast(getApplicationContext(), getString(R.string.server_down));
                    Log.d(TAG, "Error " + t.getMessage());
                    AppUtils.dismissDialog();
                }
            });

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /*
     * @param uri
     * @return
     * @throws IOException
     */
    private Bitmap getBitmapFromUri(Uri uri) throws IOException {
        ParcelFileDescriptor parcelFileDescriptor = getContentResolver().openFileDescriptor(uri, "r");
        FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
        Bitmap image = BitmapFactory.decodeFileDescriptor(fileDescriptor);
        parcelFileDescriptor.close();
        return image;
    }

    /*
     * @param bm
     * @return
     */
    private File createFileinSDcard(Bitmap bm) {
        File file;
        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "/Savika");
        myDir.mkdirs();
        Random generator = new Random();
        int n = 10000;
        n = generator.nextInt(n);
        String fname = "Image_1" /*+ n */ + ".jpg";
        file = new File(myDir, fname);
        Log.i("file", "" + file);
        if (file.exists())
            file.delete();
        try {
            FileOutputStream out = new FileOutputStream(file);
            bm.compress(Bitmap.CompressFormat.JPEG, 100, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }


        return file;
    }

    public boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG, "Permission is granted");
                return true;
            } else {
                Log.v(TAG, "Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.v(TAG, "Permission is granted");
            return true;
        }
    }

    private void handleDatePicker(final TextView etxtDob, LinearLayout llDate) {

        llDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                runOnUiThread(new DatePicketDialog(AddChildDataActivity.this, getString(R.string.select_date), getString(R.string.please_pic_the_date), false, AddChildDataActivity.this.getLayoutInflater(), new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Logger.info("TAG", "Selected prblm Date-->" + view.getTag());

                        etxtDob.setText("" + view.getTag());
                        etxtDob.setTextColor(Color.BLACK);

                        CHILD_DATE = convertDateFormat("" + view.getTag());
                    }
                }));
            }
        });
    }

    public static String convertDateFormat(String inputText1) {
        String outputText = "";
        String inputText = inputText1;
        // "Z" appears not to be supported for some reason.
        DateFormat inputFormat = new SimpleDateFormat("dd MMM yyyy", Locale.getDefault());
        inputFormat.setTimeZone(TimeZone.getTimeZone("IST"));
        DateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        Date parsed = null;
        try {
            parsed = inputFormat.parse(inputText);

            outputText = outputFormat.format(parsed);

        } catch (android.net.ParseException e) {
            e.printStackTrace();
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {

            return outputText;
        }
    }

    /*
     *
     */
    private void setSpinnerData(final Spinner spnrGender) {
        spnrGender.setAdapter(new SpinnerAdapter(this));

        spnrGender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                spnrGender.setSelection(position);
                gender = spnr_data_gender[position];
                Logger.info(TAG, "spinner selection--->" + gender);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void setDataToAdapter(ArrayList<ChildModel> data) {
        rvChildData.setLayoutManager(new LinearLayoutManager(this));
        adapter = new ChildDataAdapter(this, data);
        adapter.setClickListener(this);
        rvChildData.setAdapter(adapter);

        if (data.size() > 0) {
            tvNoData.setVisibility(View.GONE);
            rvChildData.setVisibility(View.VISIBLE);
        } else {
            tvNoData.setVisibility(View.VISIBLE);
            rvChildData.setVisibility(View.GONE);
        }
    }

    @Override
    public void onItemClick(View view1, final int position) {
        Logger.info(TAG, "Clicked Position--->" + position);


        final PopupWindow popup = new PopupWindow(this);
        View layout = getLayoutInflater().inflate(R.layout.popup_update_child, null);
        popup.setContentView(layout);
        // Set content width and height
        popup.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        popup.setWidth(WindowManager.LayoutParams.WRAP_CONTENT);
        // Closes the popup window when touch outside of it - when looses focus
        popup.setOutsideTouchable(true);
        popup.setFocusable(true);
        // Show anchored to button
        popup.setBackgroundDrawable(new BitmapDrawable());
        popup.showAsDropDown(view1);


        LinearLayout llEditChild = layout.findViewById(R.id.llEditChild);
        LinearLayout llDeleteChild = layout.findViewById(R.id.llDeleteChild);

        llEditChild.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                popup.dismiss();

                editChildDataPopUp(ARR_CHILD_DATA.get(position), position);

            }
        });

        llDeleteChild.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                popup.dismiss();

                runOnUiThread(new AlertDialogUtility(AddChildDataActivity.this, inflater, getResources().getString(R.string.app_name), getResources().getString(R.string.are_you_sure_you_want_to_delete_), "Yes", "No", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        if (SharedPrefsUtils.getUserID(AddChildDataActivity.this) != null) {
                            deleteChild(ARR_CHILD_DATA.get(position).getChildid());
                        } else {
                            ARR_CHILD_DATA.remove(position);
                            setDataToAdapter(ARR_CHILD_DATA);
                        }

                    }
                }, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                    }
                }, true));
            }
        });


    }

    /*
     * @param childID
     */
    private void deleteChild(String childID) {

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<CommonResModel> call = apiService.deleteChild(SharedPrefsUtils.getToken(getApplicationContext()),SharedPrefsUtils.getUserID(AddChildDataActivity.this), childID);

        call.enqueue(new retrofit2.Callback<CommonResModel>() {
            @Override
            public void onResponse(Call<CommonResModel> call, retrofit2.Response<CommonResModel> response) {

                AppUtils.dismissDialog();

                if (response.code() == 200 && response.body().isSuccess() == true) {
                    getChildData();
                } else {
                    if (response.body().getMessage()!=null) {
                        if (response.body().getMessage().equalsIgnoreCase("Unauthorized")) {
                            AppUtils.sessionExpired(AddChildDataActivity.this);
                        } else
                            AppUtils.showToast(getApplicationContext(), response.body().getMessage());
                    }
                }

            }

            @Override
            public void onFailure(Call<CommonResModel> call, Throwable t) {
                AppUtils.dismissDialog();
            }
        });
    }

    /**
     *
     */
    private class SpinnerAdapter extends BaseAdapter {

        private LayoutInflater mInflater;

        public SpinnerAdapter(AddChildDataActivity con) {
            mInflater = LayoutInflater.from(con);
        }

        @Override
        public int getCount() {
            return spnr_data_gender.length;
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            final ListContent holder;
            View v = convertView;
            if (v == null) {
                v = mInflater.inflate(R.layout.list_item_spinner, null);
                holder = new ListContent();

                holder.name = v.findViewById(R.id.tvSpinner);
                holder.imgGenderIcon = v.findViewById(R.id.imgGenderIcon);
                v.setTag(holder);

            } else {
                holder = (ListContent) v.getTag();
            }

            holder.name.setText("" + spnr_data_gender[position]);

            if (holder.name.getText().toString().equalsIgnoreCase("Boy")) {
                holder.imgGenderIcon.setImageResource(R.drawable.addchild_boy_icon);
            } else {
                holder.imgGenderIcon.setImageResource(R.drawable.addchild_girl_icon);
            }

            return v;
        }

    }

    static class ListContent {
        TextView name;
        ImageView imgGenderIcon;
    }
}
