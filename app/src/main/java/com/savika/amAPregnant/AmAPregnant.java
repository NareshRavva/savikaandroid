package com.savika.amAPregnant;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.savika.R;
import com.savika.components.Button;
import com.savika.components.edittext.EditText;
import com.savika.authentication.SignUpActivity;
import com.savika.dialogUtils.DatePicketDialog;
import com.savika.logger.Logger;
import com.savika.constants.AppConstants;
import com.savika.utils.AppUtils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;

/*
 * Created by Naresh Ravva on 20/09/17.
 */

public class AmAPregnant extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.tvDueDate)
    TextView tvDueDate;

    @BindView(R.id.tvDateLastPeriod)
    TextView tvDateLastPeriod;

    @BindView(R.id.etvDate)
    EditText etvDate;

    @BindView(R.id.llContinue)
    LinearLayout llContinue;

    private String TAG = this.getClass().getSimpleName();

    int min = 1;
    int max = 1;

    boolean isUserSelectedDueDate = true;

    String DUE_DATE, DATE_OF_LAST_PERIOD;
    String DISPLAY_DUE_DATE = "", DISPLAY_DATE_OF_LAST_PERIOD = "";


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_am_pregenant);

        ButterKnife.bind(this);

        tvDueDate.setOnClickListener(this);
        tvDateLastPeriod.setOnClickListener(this);
        llContinue.setOnClickListener(this);
        handleDatePicker(etvDate);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tvDueDate:
                isUserSelectedDueDate = true;
                tvDueDate.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                tvDateLastPeriod.setBackgroundColor(Color.WHITE);
                min = 1;
                max = 1;
                tvDueDate.setTextColor(Color.WHITE);
                tvDateLastPeriod.setTextColor(Color.BLACK);
                etvDate.setText(DISPLAY_DUE_DATE);
                break;

            case R.id.tvDateLastPeriod:
                isUserSelectedDueDate = false;
                min = 1;
                max = 1;
                tvDateLastPeriod.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                tvDueDate.setBackgroundColor(Color.WHITE);

                tvDueDate.setTextColor(Color.BLACK);
                tvDateLastPeriod.setTextColor(Color.WHITE);
                etvDate.setText(DISPLAY_DATE_OF_LAST_PERIOD);
                break;

            case R.id.llContinue:

                Logger.info(TAG, "DUE_DATE-->" + DUE_DATE);
                Logger.info(TAG, "DATE_OF_LAST_PERIOD-->" + DATE_OF_LAST_PERIOD);

                if (DUE_DATE != null) {
                    Intent intent = new Intent(getApplicationContext(), SignUpActivity.class);
                    intent.putExtra(AppConstants.EXTRA_DUE_DATE, DUE_DATE);
                    intent.putExtra(AppConstants.EXTRA_DATE_OF_LAST_PERIOD, DATE_OF_LAST_PERIOD);
                    startActivity(intent);
                } else {
                    AppUtils.showToast(AmAPregnant.this, "Please Enter Date.");
                }
                break;
        }

    }

    private void handleDatePicker(final EditText etxtDob) {


        etxtDob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                datedialog(etxtDob);
            }
        });
    }

    public void datedialog(final EditText etxtDob)
    {
        String defaultdate = etvDate.getText().toString().trim();
        runOnUiThread(new DatePicketDialog(AmAPregnant.this, getString(R.string.select_date), getString(R.string.please_pic_the_date), false, min, max,defaultdate,  AmAPregnant.this.getLayoutInflater(), new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Logger.info("TAG", "Selected prblm Date-->" + view.getTag());

                if (isUserSelectedDueDate){
                    DateFormat df = new SimpleDateFormat("dd MMM yyyy", Locale.getDefault());
                    try {
                        Date selecteddate = df.parse((String) view.getTag());
                        Date date = new Date();
                        Date today = df.parse(df.format(date));
                        Calendar cal = Calendar.getInstance();
                        cal.add(Calendar.MONTH, +9);
                        Date m9 = cal.getTime();
                        if (selecteddate.before(today) && !selecteddate.equals(date) ){
                            showAlert(AmAPregnant.this,"Choose date after today",etxtDob);
                            etxtDob.setText("");
                        }
                        else if (selecteddate.after(m9)){
                            showAlert(AmAPregnant.this,"Invalid date",etxtDob);
                        }
                        else {
                            DISPLAY_DUE_DATE = "" + view.getTag();
                            DUE_DATE = convertDateFormat("" + view.getTag());

                            try {
                                removeMonths("" + view.getTag());
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            etxtDob.setText(DISPLAY_DUE_DATE);
                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }

                else {
                    DateFormat df = new SimpleDateFormat("dd MMM yyyy", Locale.getDefault());
                    try {
                        Date selecteddate = df.parse((String) view.getTag());
                        Date date = new Date();
                        Date today = df.parse(df.format(date));
                        Calendar cal = Calendar.getInstance();
                        cal.add(Calendar.MONTH, -9);
                        Date m9 = cal.getTime();
                        if (selecteddate.after(today) && !selecteddate.equals(date)){
                            showAlert(AmAPregnant.this,"Choose date before today",etxtDob);
                            etxtDob.setText("");
                        }
                        else if (selecteddate.before(m9)){
                            showAlert(AmAPregnant.this,"Invalid date",etxtDob);
                        }
                        else {
                            DISPLAY_DATE_OF_LAST_PERIOD = "" + view.getTag();
                            DATE_OF_LAST_PERIOD = convertDateFormat("" + view.getTag());

                            try {
                                addMonths("" + view.getTag());
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            etxtDob.setText(DISPLAY_DATE_OF_LAST_PERIOD);
                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
            }
        }));
    }

    public void showAlert(Context ctx, String message, final EditText etxtDob) {
        // Create layout inflater object to inflate toast.xml file
        if (ctx != null) {

            try {
                final AlertDialog.Builder builder = AppUtils.getAlertDialog(ctx);
                View view  = getLayoutInflater().inflate(R.layout.pop_alert_date,null);
                builder.setView(view);
                final AlertDialog alert = builder.create();
                alert.show();
                TextView messagetxt = view.findViewById(R.id.tvdateerrorText);
                messagetxt.setText(message);
                Button ok = view.findViewById(R.id.datebtnPositive);
                ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alert.cancel();
                        datedialog(etxtDob);
                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static String convertDateFormat(String inputText1) {
        String outputText = "";
        // "Z" appears not to be supported for some reason.
        DateFormat inputFormat = new SimpleDateFormat("dd MMM yyyy", Locale.getDefault());
        inputFormat.setTimeZone(TimeZone.getTimeZone("IST"));
        DateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        Date parsed;
        try {
            parsed = inputFormat.parse(inputText1);

            outputText = outputFormat.format(parsed);

        } catch (android.net.ParseException e) {
            e.printStackTrace();
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {

            return outputText;
        }
    }

    /*
     * @param dateAsString
     * @param nbMonths
     * @return
     * @throws ParseException
     */
    public void addMonths(String dateAsString) throws ParseException {
        String format = "dd MMM yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.getDefault());
        SimpleDateFormat display_format = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        Date dateAsObj = sdf.parse(dateAsString);
        Calendar cal = Calendar.getInstance();
        cal.setTime(dateAsObj);
        cal.add(Calendar.MONTH, 9);
        Date dateAsObjAfterAMonth = cal.getTime();
        System.out.println(sdf.format(dateAsObjAfterAMonth));

        DISPLAY_DUE_DATE = sdf.format(dateAsObjAfterAMonth);
        DUE_DATE = display_format.format(dateAsObjAfterAMonth);

        Logger.info(TAG,"DISPLAY_DUE_DATE-->"+DISPLAY_DUE_DATE+"--DUE_DATE-->"+DUE_DATE);


        sdf.format(dateAsObjAfterAMonth);
    }

    /*
     * @param dateAsString
     * @return
     * @throws ParseException
     */
    public void removeMonths(String dateAsString) throws ParseException {
        String format = "dd MMM yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.getDefault());
        SimpleDateFormat display_format = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        Date dateAsObj = sdf.parse(dateAsString);
        Calendar cal = Calendar.getInstance();
        cal.setTime(dateAsObj);
        cal.add(Calendar.MONTH, -9);
        Date dateAsObjAfterAMonth = cal.getTime();
        System.out.println(sdf.format(dateAsObjAfterAMonth));


        DISPLAY_DATE_OF_LAST_PERIOD = sdf.format(dateAsObjAfterAMonth);
        DATE_OF_LAST_PERIOD = display_format.format(dateAsObjAfterAMonth);

        Logger.info(TAG,"DISPLAY_DATE_OF_LAST_PERIOD-->"+DISPLAY_DATE_OF_LAST_PERIOD+"--DATE_OF_LAST_PERIOD-->"+DATE_OF_LAST_PERIOD);

        sdf.format(dateAsObjAfterAMonth);
    }


}
