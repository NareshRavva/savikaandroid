package com.savika;

import android.app.Application;
import android.support.multidex.MultiDexApplication;
import android.view.Display;

import com.crashlytics.android.Crashlytics;
import com.savika.recommendReadings.ArticalsModel;

import java.util.ArrayList;

import io.fabric.sdk.android.Fabric;

import static com.crashlytics.android.core.CrashlyticsCore.getInstance;

/*
 * Created by Naresh Ravva
 */

public class SavikaApplication extends MultiDexApplication {

    public final static String APIKEY = "4b96b3e1";

    public static ArrayList<ArticalsModel> data = new ArrayList<>();

    public static void setData(ArrayList<ArticalsModel> obj) {
        data = obj;
    }

    public static ArrayList<ArticalsModel> getData() {
        return data;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this,new Crashlytics());



//        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder(this)
//                .name(Realm.DEFAULT_REALM_NAME)
//                .schemaVersion(0)
//                .deleteRealmIfMigrationNeeded()
//                .build();
//
//        Realm.setDefaultConfiguration(realmConfiguration);

        //splunkCrassLogTrack();
    }
//
//    private void splunkCrassLogTrack() {
//        BugSenseHandler.initAndStartSession(this, APIKEY);
//        BugSenseHandler.startSession(this);
//        BugSenseHandler.closeSession(this);
//        BugSenseHandler.flush(this);
//
//        String versionName;
//        try {
//            versionName = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
//            if (versionName != null) {
//                BugSenseHandler.setUserIdentifier("Version " + versionName);
//            }
//        } catch (PackageManager.NameNotFoundException e) {
//            e.printStackTrace();
//        }
//    }
}
